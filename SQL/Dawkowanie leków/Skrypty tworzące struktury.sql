ALTER TABLE [dbo].[UACTac8f3072d9ae4d13834c7bdae21b099a]
ADD [CzestotliwoscId] [int] NULL,
	[Interwal] [int] NULL,
	[Archiwum] [bit] NULL,
	[Uwagi] [nvarchar](255) NULL,
	[CzestotliwoscCzestotliwosc] [nvarchar](255) NULL,
	[CzestotliwoscDni] [nvarchar](255) NULL,
	[CzestotliwoscDniMonday] [bit] NULL,
	[CzestotliwoscDniTuesday] [bit] NULL,
	[CzestotliwoscDniWednesday] [bit] NULL,
	[CzestotliwoscDniThursday] [bit] NULL,
	[CzestotliwoscDniFriday] [bit] NULL,
	[CzestotliwoscDniSaturday] [bit] NULL,
	[CzestotliwoscDniSunday] [bit] NULL;

ALTER TABLE [dbo].[UACTff06667115f142dda4e10a6ae0b0e8f4]
ADD [PodaniaLekow] [xml] NULL;

Create TABLE [dbo].[ZaleceniaLekarskieLekiDni](
	[fldId] [int] IDENTITY(1,1) NOT NULL,
	[IdZaleceniaStacjonarne] [int] NULL, -- UACTac8f3072d9ae4d13834c7bdae21b099a, WIP: UACTa0a5b8148c70418c86af68eefa910af3
	[IdZaleceniaDomowe] [int] NULL, -- UACT27a2a9fbe4f44556bb09f8ba6d9f4a1c
	[NumerDnia] [smallint] NOT NULL,
 CONSTRAINT [PK_ZaleceniaLekarskieLekiDni] PRIMARY KEY CLUSTERED 
(
	[fldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[ZaleceniaLekarskieLekiGodziny](
	[fldId] [int] IDENTITY(1,1) NOT NULL,
	[IdZaleceniaStacjonarne] [int] NULL, -- UACTac8f3072d9ae4d13834c7bdae21b099a, WIP: UACTa0a5b8148c70418c86af68eefa910af3
	[IdZaleceniaDomowe] [int] NULL, -- UACT27a2a9fbe4f44556bb09f8ba6d9f4a1c
	[Godzina] [time](7) NOT NULL,
 CONSTRAINT [PK_ZaleceniaLekarskieLekiGodziny] PRIMARY KEY CLUSTERED 
(
	[fldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[ZaleceniaLekarskiePodaneLeki](
	[fldId] [int] IDENTITY(1,1) NOT NULL,
	[IdGodzinyZalecenia] [int] NOT NULL,
	[IdPielegniarki] [int] NOT NULL,
	[DataWykonania] [datetime] NOT NULL,
	[Podano] [bit] NOT NULL,
	[Uwagi] [ntext] NULL,
	[DataPlanowanegoPodania] [datetime] NULL,
	[DataDodaniaDoBazy] [datetime] NULL,
 CONSTRAINT [PK_ZaleceniaLekarskiePodaneLeki] PRIMARY KEY CLUSTERED 
(
	[fldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ZaleceniaLekarskiePodaneLeki]  WITH CHECK ADD  CONSTRAINT [FK_ZaleceniaLekarskiePodaneLeki_ZaleceniaLekarskieLekiGodziny] FOREIGN KEY([IdGodzinyZalecenia])
REFERENCES [dbo].[ZaleceniaLekarskieLekiGodziny] ([fldId])
GO

ALTER TABLE [dbo].[ZaleceniaLekarskiePodaneLeki] CHECK CONSTRAINT [FK_ZaleceniaLekarskiePodaneLeki_ZaleceniaLekarskieLekiGodziny]
GO

CREATE TABLE [dbo].[GodzinyZmian](
	[fldId] [int] IDENTITY(1,1) NOT NULL,
	[GodzinaOd] [time](7) NOT NULL,
	[GodzinaDo] [time](7) NOT NULL,
 CONSTRAINT [PK_GodzinyZmian] PRIMARY KEY CLUSTERED 
(
	[fldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO [dbo].[GodzinyZmian] ([GodzinaOd],[GodzinaDo])
     VALUES ('07:00', '19:00'), ('19:00', '07:00')
GO


CREATE TABLE [dbo].[ZleceniaLekarskieLekiCzestotliwosc](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Czestotliwosc] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ZleceniaLekarskieLekiCzestotliwosc] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO [dbo].[ZleceniaLekarskieLekiCzestotliwosc] ([Czestotliwosc])
     VALUES ('Dzienna'),('Tygodniowa')
GO


USE [Sequence]
GO
/****** Object:  Trigger [dbo].[ZleceniaLekarskieWeeklyDays_Insert]    Script Date: 2015-12-09 17:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	  
	  
CREATE TRIGGER [dbo].[ZleceniaLekarskieWeeklyDays_InsertUpdate] ON [dbo].[UACTac8f3072d9ae4d13834c7bdae21b099a]  -- UACTac8f3072d9ae4d13834c7bdae21b099a, WIP: UACTa0a5b8148c70418c86af68eefa910af3
FOR INSERT, UPDATE
AS
BEGIN
	declare @IdZlecenia int;
	declare @monday bit;
	declare @tuesday bit;
	declare @wednesday bit;
	declare @thursday bit;
	declare @friday bit;
	declare @saturday bit;
	declare @sunday bit;
	declare @dzienna int;

	select @IdZlecenia=i.fldId from inserted i;	
	select @monday=i.CzestotliwoscDniMonday from inserted i;
	select @tuesday=i.CzestotliwoscDniTuesday from inserted i;
	select @wednesday=i.CzestotliwoscDniWednesday from inserted i;
	select @thursday=i.CzestotliwoscDniThursday from inserted i;
	select @friday=i.CzestotliwoscDniFriday from inserted i;
	select @saturday=i.CzestotliwoscDniSaturday from inserted i;
	select @sunday=i.CzestotliwoscDniSunday from inserted i;
	select @dzienna=i.CzestotliwoscId from inserted i;
	
	if @dzienna = 1
	BEGIN
		delete from [dbo].[ZaleceniaLekarskieLekiDni]
		where IdZaleceniaStacjonarne = @IdZlecenia
	END
	ELSE
	BEGIN
		if (@monday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 2)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
				  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia) 
				values(@IdZlecenia,null,2);
			END
		END
		if (@tuesday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 3)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia) 
			values(@IdZlecenia,null,3);
			END
		END

		if (@wednesday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 4)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia) 
			values(@IdZlecenia,null,4);
			END
		END

		if (@thursday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 5)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia) 
			values(@IdZlecenia,null,5);
			END
		END

		if (@friday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 6)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia) 
			values(@IdZlecenia,null,6);
			END
		END

		if (@saturday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 7)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia) 
			values(@IdZlecenia,null,7);
			END
		END

		if (@sunday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 1)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia) 
			values(@IdZlecenia,null,1);
			END
		END

		if (@monday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 2
		END
		if (@tuesday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 3
		END

		if (@wednesday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 4
		END

		if (@thursday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 5
		END

		if (@friday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 6
		END

		if (@saturday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 7
		END

		if (@sunday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 1
		END
	END
END
GO

CREATE TRIGGER [dbo].[ZleceniaLekarskiePodanoLek_InsertUpdate] ON [dbo].[UACTff06667115f142dda4e10a6ae0b0e8f4] -- UACTff06667115f142dda4e10a6ae0b0e8f4, WIP: UACTfe7034d9cd7546f6a6835a08d5a9d3f5
FOR INSERT,UPDATE
AS
BEGIN
DECLARE @fldIWfId int
DECLARE @IdGodzinyZaleceniaORG int
DECLARE @IdGodzinyZalecenia int
DECLARE @IdPielegniarki int
DECLARE @DataWykonania nvarchar(50)
DECLARE @Podano bit
DECLARE @DataPlanowanegoPodania nvarchar(50)
DECLARE @Uwagi nvarchar(max)
DECLARE @PodaniaLekow XML


SELECT @fldIWfId=i.fldIWfId from inserted i;

SELECT @PodaniaLekow = PodaniaLekow FROM UACTff06667115f142dda4e10a6ae0b0e8f4 WHERE fldIWfId = @fldIWfId
DECLARE MY_CURSOR CURSOR FOR SELECT 
	   PL.NODE.value('@IdGodzinyZaleceniaORG', 'int') as IdGodzinyZaleceniaORG,
	   PL.NODE.value('@IdGodzinyZalecenia', 'int') as IdGodzinyZalecenia,
	   PL.NODE.value('@IdPielegniarki', 'int') as IdPielegniarki,
	   PL.NODE.value('@DataWykonania', 'nvarchar(50)') as DataWykonania,
	   PL.NODE.value('@Podano', 'bit') as Podano,
	   PL.NODE.value('node()[1]', 'nvarchar(max)') as Uwagi,
	   PL.NODE.value('@DataPlanowanegoPodania', 'nvarchar(50)') as DataPlanowanegoPodania
FROM @PodaniaLekow.nodes('/root/item') as PL(NODE)

OPEN MY_CURSOR
FETCH NEXT FROM MY_CURSOR INTO @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania
WHILE @@FETCH_STATUS = 0
	BEGIN 
    
		--SELECT @IdGodzinyZaleceniaORG
		IF @IdGodzinyZalecenia = @IdGodzinyZaleceniaORG
		BEGIN
			UPDATE [dbo].[ZaleceniaLekarskiePodaneLeki]
			SET IdPielegniarki=@IdPielegniarki, DataWykonania=@DataWykonania, Podano=@Podano, Uwagi=@Uwagi, DataPlanowanegoPodania = @DataPlanowanegoPodania
			WHERE IdGodzinyZalecenia=@IdGodzinyZalecenia AND DataPlanowanegoPodania = @DataPlanowanegoPodania;
		END
		IF @IdGodzinyZalecenia = 0
		BEGIN
			--Select @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania 
			--select @IdGodzinyZaleceniaORG,@IdPielegniarki,cast(@DataWykonania as datetime),@Podano,@Uwagi,cast(@DataPlanowanegoPodania as datetime),GETDATE()
			INSERT INTO [dbo].[ZaleceniaLekarskiePodaneLeki]
				  (IdGodzinyZalecenia,IdPielegniarki,DataWykonania,Podano,Uwagi,DataPlanowanegoPodania,DataDodaniaDoBazy) 
				VALUES(@IdGodzinyZaleceniaORG,@IdPielegniarki,cast(@DataWykonania as datetime),@Podano,@Uwagi,cast(@DataPlanowanegoPodania as datetime),GETDATE());
		END
		FETCH NEXT FROM MY_CURSOR INTO @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania	
	END
	CLOSE MY_CURSOR
	DEALLOCATE MY_CURSOR
END


ALTER TRIGGER [dbo].[ZbiorczeLekiPodanie_InsertUpdate] ON [dbo].[UACT01c44720535f4c3f9c17e401098b73f3]
FOR INSERT,UPDATE
AS
BEGIN
DECLARE @fldIWfId int
DECLARE @IdGodzinyZaleceniaORG int
DECLARE @IdGodzinyZalecenia int
DECLARE @IdPielegniarki int
DECLARE @DataWykonania nvarchar(50)
DECLARE @Podano bit
DECLARE @DataPlanowanegoPodania nvarchar(50)
DECLARE @Uwagi nvarchar(max)
DECLARE @PodaniaLekow XML


SELECT @fldIWfId=i.fldIWfId from inserted i;

SELECT @PodaniaLekow = PodaniaLekow FROM UACT01c44720535f4c3f9c17e401098b73f3 WHERE fldIWfId = @fldIWfId 
DECLARE MY_CURSOR CURSOR FOR SELECT 
	   PL.NODE.value('@IdGodzinyZaleceniaORG', 'int') as IdGodzinyZaleceniaORG,
	   PL.NODE.value('@IdGodzinyZalecenia', 'int') as IdGodzinyZalecenia,
	   PL.NODE.value('@IdPielegniarki', 'int') as IdPielegniarki,
	   PL.NODE.value('@DataWykonania', 'nvarchar(50)') as DataWykonania,
	   PL.NODE.value('@Podano', 'bit') as Podano,
	   PL.NODE.value('node()[1]', 'nvarchar(max)') as Uwagi,
	   PL.NODE.value('@DataPlanowanegoPodania', 'nvarchar(50)') as DataPlanowanegoPodania
FROM @PodaniaLekow.nodes('/root/item') as PL(NODE)

OPEN MY_CURSOR
FETCH NEXT FROM MY_CURSOR INTO @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania
WHILE @@FETCH_STATUS = 0
	BEGIN 
    
		--SELECT @IdGodzinyZaleceniaORG
		IF @IdGodzinyZalecenia = @IdGodzinyZaleceniaORG
		BEGIN
			UPDATE [dbo].[ZaleceniaLekarskiePodaneLeki]
			SET IdPielegniarki=@IdPielegniarki, DataWykonania=@DataWykonania, Podano=@Podano, Uwagi=@Uwagi, DataPlanowanegoPodania = @DataPlanowanegoPodania
			WHERE IdGodzinyZalecenia=@IdGodzinyZalecenia AND DataPlanowanegoPodania = @DataPlanowanegoPodania;
		END
		IF @IdGodzinyZalecenia = 0
		BEGIN
			--Select @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania 
			--select @IdGodzinyZaleceniaORG,@IdPielegniarki,cast(@DataWykonania as datetime),@Podano,@Uwagi,cast(@DataPlanowanegoPodania as datetime),GETDATE()
			INSERT INTO [dbo].[ZaleceniaLekarskiePodaneLeki]
				  (IdGodzinyZalecenia,IdPielegniarki,DataWykonania,Podano,Uwagi,DataPlanowanegoPodania,DataDodaniaDoBazy) 
				VALUES(@IdGodzinyZaleceniaORG,@IdPielegniarki,cast(@DataWykonania as datetime),@Podano,@Uwagi,cast(@DataPlanowanegoPodania as datetime),GETDATE());
		END
		FETCH NEXT FROM MY_CURSOR INTO @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania	
	END
	CLOSE MY_CURSOR
	DEALLOCATE MY_CURSOR
END


--CREATE FUNCTION ZaleceniaLekarskieLekiCzyPodacWdniuHS (@idZalecenia int, @day datetime)
CREATE FUNCTION ZaleceniaLekarskieLekiCzyPodacWdniuHS (@idZalecenia int, @day datetime)
RETURNS bit
AS
BEGIN
	RETURN CASE 
		WHEN EXISTS (
			SELECT 1 FROM UACTac8f3072d9ae4d13834c7bdae21b099a l --ORG: UACTac8f3072d9ae4d13834c7bdae21b099a WIP: UACTa0a5b8148c70418c86af68eefa910af3
			WHERE l.fldId = @idZalecenia
				AND @day BETWEEN l.DataRozpoczecia AND ISNULL(l.DataZakonczenia, @day) -- during period (or lasting forever)
				AND (
					-- daily
					l.CzestotliwoscId = 1 AND DATEDIFF(DAY, l.DataRozpoczecia, @day) % l.Interwal = 0
					OR -- weekly
					l.CzestotliwoscId = 2 --AND DATEDIFF(WEEK, l.DataRozpoczecia, @day) % l.Interwal = 0 AND ( --bug! datediff always treats sunday as start of the week
						AND DATEDIFF(WEEK, 
								DATEADD(DAY, 1 - DATEPART(WEEKDAY, l.DataRozpoczecia), l.DataRozpoczecia), -- start of the week
								DATEADD(DAY, 1 - DATEPART(WEEKDAY, @day), @day) -- start of the week
						) % l.Interwal = 0 AND (
						(
							NOT EXISTS ( -- days of week not specified
								SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
								WHERE d.IdZaleceniaStacjonarne = l.fldId 
							)
							AND DATEPART(WEEKDAY, l.DataRozpoczecia) = DATEPART(WEEKDAY, @day) -- same day of week
						)
						OR EXISTS ( -- specific days of the week
							SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
							WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.NumerDnia = DATEPART(WEEKDAY, @day)
						)
					)
					OR -- monthly
					l.CzestotliwoscId = 3 AND DATEDIFF(MONTH, l.DataRozpoczecia, @day) % l.Interwal = 0 AND (
						(
							NOT EXISTS ( -- days of month not specified
								SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
								WHERE d.IdZaleceniaStacjonarne = l.fldId 
							)
							AND DATEPART(DAY, l.DataRozpoczecia) = DATEPART(DAY, @day) -- same day of month
						)
						OR EXISTS ( -- specific days of the month
							SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
							WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.NumerDnia = DATEPART(DAY, @day)
						)
					)
				)
		) THEN 1
		ELSE 0 
	END
END
GO

CREATE FUNCTION ZaleceniaLekarskieLekiCzyPodacNaZmianieHS (@idGodzinyZalecenia int, @time time)
--ALTER FUNCTION ZaleceniaLekarskieLekiCzyPodacNaZmianieHS (@idGodzinyZalecenia int, @time time)
RETURNS bit
AS
BEGIN
	RETURN CASE 
		WHEN EXISTS (
			SELECT 1 FROM ZaleceniaLekarskieLekiGodziny lg
			INNER JOIN GodzinyZmian gz ON 
				(gz.GodzinaOd < gz.GodzinaDo AND @time >= gz.GodzinaOd AND @time < gz.GodzinaDo)
				OR (gz.GodzinaOd > gz.GodzinaDo AND (@time >= gz.GodzinaOd OR @time < gz.GodzinaDo))
			WHERE --lg.IdZalecenia = @idZalecenia
				lg.fldId = @idGodzinyZalecenia
				AND (
					gz.GodzinaOd < gz.GodzinaDo AND lg.Godzina >= gz.GodzinaOd AND lg.Godzina < gz.GodzinaDo
					OR gz.GodzinaOd > gz.GodzinaDo AND (lg.Godzina >= gz.GodzinaOd OR lg.Godzina < gz.GodzinaDo)
				)
		) THEN 1
		ELSE 0
	END
END 
GO




CREATE  PROCEDURE [dbo].[USP_ZaleceniaLekarskieLekiDoPodania_InsertUpdate] -- UACTac8f3072d9ae4d13834c7bdae21b099a, WIP: UACTa0a5b8148c70418c86af68eefa910af3
	@day datetime,
	@fldIWfId int
AS
BEGIN
	SELECT lg.fldId, convert(nvarchar(50), cast(@day as date) + cast(lg.Godzina as datetime), 120) as Godzina, l.Nazwaleku, l.Dawka, l.DrogaPodaniafldId, l.DrogaPodaniadrogapodania, pl.DataWykonania, pl.Podano as 'Wykonano', pl.Uwagi, pl.IdGodzinyZalecenia
	FROM UACTac8f3072d9ae4d13834c7bdae21b099a l
	INNER JOIN ZaleceniaLekarskieLekiGodziny lg ON lg.IdZaleceniaStacjonarne = l.fldId
	LEFT JOIN ZaleceniaLekarskiePodaneLeki pl ON pl.IdGodzinyZalecenia = lg.fldId AND pl.DataPlanowanegoPodania = cast(@day as date) + cast(lg.Godzina as datetime)
	WHERE l.Archiwum = 0 OR l.Archiwum IS NULL
		AND dbo.ZaleceniaLekarskieLekiCzyPodacWdniuHS(l.fldId, @day) = 1
		AND dbo.ZaleceniaLekarskieLekiCzyPodacNaZmianieHS(lg.fldId, @day) = 1
		AND l.fldIWfId = @fldIWfId
	
		
END
GO

ALTER  PROCEDURE [dbo].[USP_ZbiorczeLekiDoPodania] 
	@day datetime
AS
BEGIN
	SELECT lg.fldId, convert(nvarchar(50), cast(@day as date) + cast(lg.Godzina as datetime), 120) as Godzina, l.Nazwaleku, l.Dawka, l.DrogaPodaniafldId, l.DrogaPodaniadrogapodania, pl.DataWykonania, pl.Podano as 'Wykonano', pl.Uwagi, pl.IdGodzinyZalecenia, p.txtImiepacjenta as 'Imie', p.txtNazwiskopacjenta as 'Nazwisko', p.PESEL as 'PESEL', s.ntxtnumersali as 'Sala'
	FROM UACTac8f3072d9ae4d13834c7bdae21b099a l
	INNER JOIN ZaleceniaLekarskieLekiGodziny lg ON lg.IdZaleceniaStacjonarne = l.fldId
	LEFT JOIN ZaleceniaLekarskiePodaneLeki pl ON pl.IdGodzinyZalecenia = lg.fldId AND pl.DataPlanowanegoPodania = cast(@day as date) + cast(lg.Godzina as datetime)
	LEFT JOIN UACT2c02d32cb7a04ce3a9a07923a9cf8d14 p ON l.fldIWfId = p.fldIWfId		
	LEFT JOIN UACT65a5c2ec1bba4d6f843944b8e404e300 s ON l.fldIWfId = s.fldIWfId
	WHERE l.Archiwum = 0 OR l.Archiwum IS NULL
		AND dbo.ZaleceniaLekarskieLekiCzyPodacWdniuHS(l.fldId, @day) = 1
		AND dbo.ZaleceniaLekarskieLekiCzyPodacNaZmianieHS(lg.fldId, @day) = 1
	ORDER BY p.txtNazwiskopacjenta, lg.Godzina
		
END
GO