CREATE FUNCTION ZaleceniaLekarskieLekiCzyPodacWdniuHD (@idZalecenia int, @day datetime)
--ALTER FUNCTION ZaleceniaLekarskieLekiCzyPodacWdniuHD (@idZalecenia int, @day datetime)
RETURNS bit
AS
BEGIN
	RETURN CASE 
		WHEN EXISTS (
			SELECT 1 FROM UACT27a2a9fbe4f44556bb09f8ba6d9f4a1c l
			WHERE l.fldId = @idZalecenia
				AND @day BETWEEN l.DataRozpoczecia AND ISNULL(l.DataZakonczenia, @day) -- during period (or lasting forever)
				AND (
					-- daily
					l.CzestotliwoscId = 1 AND DATEDIFF(DAY, l.DataRozpoczecia, @day) % l.Interwal = 0
					OR -- weekly
					l.CzestotliwoscId = 2 --AND DATEDIFF(WEEK, l.DataRozpoczecia, @day) % l.Interwal = 0 AND ( --bug! datediff always treats sunday as start of the week
						AND DATEDIFF(WEEK, 
								DATEADD(DAY, 1 - DATEPART(WEEKDAY, l.DataRozpoczecia), l.DataRozpoczecia), -- start of the week
								DATEADD(DAY, 1 - DATEPART(WEEKDAY, @day), @day) -- start of the week
						) % l.Interwal = 0 AND (
						(
							NOT EXISTS ( -- days of week not specified
								SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
								WHERE d.IdZaleceniaDomowe = l.fldId 
							)
							AND DATEPART(WEEKDAY, l.DataRozpoczecia) = DATEPART(WEEKDAY, @day) -- same day of week
						)
						OR EXISTS ( -- specific days of the week
							SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
							WHERE d.IdZaleceniaDomowe = l.fldId AND d.NumerDnia = DATEPART(WEEKDAY, @day)
						)
					)
					OR -- monthly
					l.CzestotliwoscId = 3 AND DATEDIFF(MONTH, l.DataRozpoczecia, @day) % l.Interwal = 0 AND (
						(
							NOT EXISTS ( -- days of month not specified
								SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
								WHERE d.IdZaleceniaDomowe = l.fldId 
							)
							AND DATEPART(DAY, l.DataRozpoczecia) = DATEPART(DAY, @day) -- same day of month
						)
						OR EXISTS ( -- specific days of the month
							SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
							WHERE d.IdZaleceniaDomowe = l.fldId AND d.NumerDnia = DATEPART(DAY, @day)
						)
					)
				)
		) THEN 1
		ELSE 0 
	END
END
GO

CREATE FUNCTION ZaleceniaLekarskieLekiCzyPodacNaZmianieHD (@idGodzinyZalecenia int, @time time)
--ALTER FUNCTION ZaleceniaLekarskieLekiCzyPodacNaZmianieHD (@idGodzinyZalecenia int, @time time)
RETURNS bit
AS
BEGIN
	RETURN CASE 
		WHEN EXISTS (
			SELECT 1 FROM ZaleceniaLekarskieLekiGodziny lg
			INNER JOIN GodzinyZmian gz ON 
				(gz.GodzinaOd < gz.GodzinaDo AND @time >= gz.GodzinaOd AND @time < gz.GodzinaDo)
				OR (gz.GodzinaOd > gz.GodzinaDo AND (@time >= gz.GodzinaOd OR @time < gz.GodzinaDo))
			WHERE --lg.IdZalecenia = @idZalecenia
				lg.fldId = @idGodzinyZalecenia
				AND (
					gz.GodzinaOd < gz.GodzinaDo AND lg.Godzina >= gz.GodzinaOd AND lg.Godzina < gz.GodzinaDo
					OR gz.GodzinaOd > gz.GodzinaDo AND (lg.Godzina >= gz.GodzinaOd OR lg.Godzina < gz.GodzinaDo)
				)
		) THEN 1
		ELSE 0
	END
END 
GO
