SET DATEFIRST 1 -- monday is first
--SET DATEFIRST 7 -- sunday is first
GO

--select datepart(w, getdate())
DECLARE @day datetime = '2015-12-16 12:00'
DECLARE @fldIWfId int = '6585'

select *, @day, CAST(@day AS time), 
	DATEPART(WEEKDAY, l.DataRozpoczecia) dayofweek, DATEPART(WEEKDAY, @day) dayofweek,
	DATEPART(DAY, l.DataRozpoczecia) day, DATEPART(DAY, @day) day,
	DATEDIFF(DAY, l.DataRozpoczecia, @day) days, DATEDIFF(dd, l.DataRozpoczecia, @day) % l.Interwal days,
	DATEDIFF(WEEK, l.DataRozpoczecia, @day) weeks, DATEDIFF(WEEK, l.DataRozpoczecia, @day) % l.Interwal weeks
from UACTa0a5b8148c70418c86af68eefa910af3 l
where l.fldIWfId = @fldIWfId



select * from ZaleceniaLekarskieLekiDni

--select * from ZaleceniaLekarskieLekiGodziny
--select * from ZaleceniaLekarskiePodaneLeki

--SELECT * --, dbo.ZaleceniaLekarskieLekiCzyPodacWdniu(l.fldId, @day) AS CzyPodacDzisiaj
--	--dbo.ZaleceniaLekarskieLekiCzyPodacNaZmianie (l.fldId, @day) AS CzyPodacNaZmianie
--	,dbo.ZaleceniaLekarskieLekiCzyPodacNaZmianie(lg.fldId, @day) AS CzyPodacNaZmianie
--FROM ZaleceniaLekarskieLeki l
----INNER JOIN GodzinyZmian gz ON
----	(gz.GodzinaOd < gz.GodzinaDo AND CAST(@day AS time) >= gz.GodzinaOd AND CAST(@day AS time) < gz.GodzinaDo)
----	OR (gz.GodzinaOd > gz.GodzinaDo AND (CAST(@day AS time) >= gz.GodzinaOd OR CAST(@day AS time) < gz.GodzinaDo))
--INNER JOIN ZaleceniaLekarskieLekiGodziny lg ON lg.IdZalecenia = l.fldId
----	AND (
----		gz.GodzinaOd < gz.GodzinaDo AND lg.Godzina >= gz.GodzinaOd AND lg.Godzina < gz.GodzinaDo
----		OR gz.GodzinaOd > gz.GodzinaDo AND (lg.Godzina >= gz.GodzinaOd OR lg.Godzina < gz.GodzinaDo)
----	)
--WHERE l.Archiwum = 0 
--	AND dbo.ZaleceniaLekarskieLekiCzyPodacWdniu(l.fldId, @day) = 1
--	--AND dbo.ZaleceniaLekarskieLekiCzyPodacNaZmianie(l.fldId, @day) = 1
--	--AND dbo.ZaleceniaLekarskieLekiCzyPodacNaZmianie(lg.fldId, @day) = 1

DECLARE @day datetime = '2015-12-11 12:00'
DECLARE @fldIWfId int = '6587'

SELECT *
FROM UACTa0a5b8148c70418c86af68eefa910af3 l
INNER JOIN ZaleceniaLekarskieLekiGodziny lg ON lg.IdZaleceniaStacjonarne = l.fldId
WHERE l.Archiwum = 0 OR l.Archiwum IS NULL
	AND dbo.ZaleceniaLekarskieLekiCzyPodacWdniuHS(l.fldId, @day) = 1
	AND dbo.ZaleceniaLekarskieLekiCzyPodacNaZmianieHS(lg.fldId, @day) = 1
