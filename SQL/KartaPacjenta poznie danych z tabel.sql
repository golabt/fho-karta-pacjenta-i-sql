--STACJONARNE

select * from UACT2c02d32cb7a04ce3a9a07923a9cf8d14 --stacjonarni pacjenci

--LEKARZ
select * from UACT9a0885e607604a2992f170e2c415df67 -- historia choroby i dyskryminator subwf
select * from UACT310fab7627d4493fba7fc808ab864c3e -- Karta badania przedmiotowego
	select * from UACT29408faab9e34d21aa23a615a5dbe06b -- leki z karty
select * from UACT66ec3f2fb5ca4519afd801631b68b583 -- wizyty
select * from UACT447f42706de54e6e8e7db1b9a4f2c372 -- zlecenia dla pielęgniarki
	select * from UACTac8f3072d9ae4d13834c7bdae21b099a --leki
	select * from UACTdbfc872e7ef64d3b9ef58417cd289591 --zabiegi
	select * from UACT78f89301f60c46da8485465610a5ef9b --opatrunki
select * from UACT375729a022a249a09a5da1d62cd79ef8 -- skierowanie do psychologa
select * from UACT54ee5d6ac1524bc684589d3d61b260bf -- skierowania do psychoterapeuty
select * from UACT39bb7eed6da043be94587b2bf86c5028 -- skierowania na badania
select * from UACT97577b8be1d14aa18e6a71007927ca1c -- opis lekarski

--PIELEGNIARKA
select * from UACTb22fe135275c4253b3b5f544bb37347a -- rejestracja pacjenta dyskryminator w połączeniu z wizytą dla subwf
select * from UACT65a5c2ec1bba4d6f843944b8e404e300 -- wizyta pielęgniarki 
select * from UACTb6fa3fa99e234d148795e1c36873c194 -- bilans płynów
	select * from UACTfb6f531f7170470f91a25de70945ac67 -- bilans
select * from UACT829419f8d5bb4562a2abe2b92b9744dc -- profil glikemii
	select * from UACT8288eb4ff1a24b0f9b8ddb85148dfdc2 -- profile
select * from UACT5bf9030f4d5a4a55ba5f24f9160f091e -- Zbiorcza uproszczona ocena stanu chorego
select * from UACTff06667115f142dda4e10a6ae0b0e8f4 -- zlecenia lekarskie
	select * from UACTac8f3072d9ae4d13834c7bdae21b099a --leki
	select * from UACTdbfc872e7ef64d3b9ef58417cd289591 --zabiegi
	select * from UACT78f89301f60c46da8485465610a5ef9b --opatrunki
select * from UACT3ee279dab0e8423cb5ec1413b431c0d6 -- Indywidualna karta pielęgniarska
select * from UACT8cd2e113d2e9403aa0dd562fed57a20e -- Karta gorączkowa
	select * from UACT48f391f865e9438686e65188272943be -- karta
select * from UACT6d0c0f13fb3c4fcaac44c1367caebe9c -- Protokół leczenia zmian nowotworowych
	select * from UACT9f827511c07b47c9876b293656f9dc01 -- protokoły
select * from UACT9a9001e417b54beca5a519f4ef2eb08c -- protokół odleżyn
	select * from UACT757848ab39c140719f0da29f393ae644 -- protokoły

--PSYCHOLOG
select * from UACT391dfe53eef14e91a16d6e1f9103bd45 -- dyskryminator psychologaHS
select * from UACTf172990cc31547789aa858de04693a38 -- wizyta psychologa
select * from UACTe78866b3d79d4ac0a0f81090b7d7dd17 -- Skala akceptacji chroby
select * from UACT71c3591aa2aa44a3980b1f735171c5c8 -- Kwestionariusz KADS-M 
select * from UACTdb5ef3e94f684d62a3ab4eba43d643c1 -- wynik kwestionariusza KADS-M
select * from UACTe31388ca5c7f4510bf69e43af3cedc5d -- Geriatryczna skala oceny depresji wraz z wynikiem
select * from UACT75594210bc164f279cdc247fccd2d9f8 -- wynik Geriatryczna skala oceny depresji wraz z wynikiem
select * from UACTeb005a929f244fd5bb7a9c40788cc1e6 -- Test MMSE wraz z wynikiem
select * from UACTaf56823c8df14590a806674bd7f36116 -- wynik Test MMSE wraz z wynikiem


--FIZJOTERAPEUTA
select * from UACT90fb171b663d460e80858bd4477d19f3 -- dyskryminator fizjoterapeuty
select * from UACT351b31f2da54445a9c7705fa00633b63 -- pierwsza wizyta
select * from UACTf901a57807974712ae4fde85233c8a19 -- karta pacjenta
	select * from UACT0998ae0bcbca4dc2b8b2040d0841ba71 -- karta
select * from UACT566c61bda9b74e8291451fc5c70fea42 -- wizyta
SELECT * from UACTc2956493e9bc496b9cdc6bc2dd54c07f -- skala borga
select * from UACT78d5ab4cc7a246fa8742d49791866fcc -- stopień sprawności wg. karnofskiego
select * from UACT991b8df4773c4ff19af697f6b595f9aa -- Indeks czynności jelit BFI
select * from UACT51b87692e2db49889591fdea6ba92868 -- RSCL Rotterdamska lista objawów
select * from UACTc4f3c045469a4f56b9c3294e8b1e17af -- skala bólu vas
select * from UACTd8de471604944469a115cfd4d5cf6532 -- Skala Barthel wg pacjenta
select * from UACTabb0749e67ad44f0988687f143c1d0b3 -- wynik Skala Barthel wg pacjenta
select * from UACTfc2395aaa8bf4fd4b9e492093df0b684 -- Skala Barthel wg FIZJOTERAPEUTA
select * from UACT54adf1686f554d538a55d8b34ab3a798 -- wynik Skala Barthel wg FIZJOTERAPEUTA


--DANE PACJENTA:
select * from UACT2c02d32cb7a04ce3a9a07923a9cf8d14 -- dyskryminator
select * from UACTb22fe135275c4253b3b5f544bb37347a -- rejestracja pacjenta
select * from UACTd44c30b0c7034b3fa0bb5cc21cae9caa -- ubezpieczenie
select * from UACTe7134ccb2540437bb8f1756483ec0336 -- założenie historii choroby
select * from UACT9b34929a538c4e96a60935eded611f07 -- dane o pacjencie i o rodzinie
select * from UACTb9985cfac1d54d90b8a894c212037f38 -- Finalizowanie przyjęcia pacjenta
	select * from UACT53cccc387e18462e9bc9094551eb1af2 -- dane osób do kontaktu
select * from UACT92495a4484f44e91b2292d77d21e7de5 -- Formularz oceny ryzyka zakażenia
select * from UACT78297bb21589423994d10490e7ec5f5f -- wynik Formularz oceny ryzyka zakażenia
select * from UACTd715eaf74aec461489263677755158d7 -- Epikryza wypisowa
select * from UACTc151b469aa8d488e8d689133cf4441f6 -- Epikryza
select * from UACTdfa3c31754134cea9009a9798f3cb5b7 -- pobyt w szpitalu





--DOMOWE

select * from UACTf0f18fd8365e480cbbac90d0526c2f6b -- domowi pacjenci
select * from UACTb0754dc342914290a483a53eddf8c8b6 -- rejestracja i dyskryminator dla mainwf

select * from UACTf3923fe7dd314226871e59eaa8c40af2
--LEKARZ
select * from UACTc1ee29a5ebde42d78c0528c7c4a460eb -- widok pacjentow dla workflow wizyta lekarza h domowe i dyskryminator subwf
select * from UACT2c9cc6b4b5a64cfaa38c363fee0d5fad -- pierwsza wizyta
select * from UACT9ea300b75bbc4f7691459d546a02215d -- wizyta
select * from UACTe5e915af4f0f4b428562701c2c51ecba -- interwencja
select * from UACT4c0de6fa33fd49319a98c95544674719 -- Karta badania przedmiotowego
	select * from UACT07e02e094f66435a898fe07a02b57ca5 -- leki
	/* OLD badanie przedmiotowe i podmiotowe
select * from UACTb0938139ff624fdea442d3923c47b38e -- badanie przedmiotowe i podmiotowe
	select * from UACT27a2a9fbe4f44556bb09f8ba6d9f4a1c --leki
	select * from UACT6772b3c90a0a4aea80ef844072d6b2b0 --zabiegi
	select * from UACT5728fb01fe9344e89b276195fafec3c2 --opatrunki
	select * from UACT0f9e4fe520f447958ba9a7db1da6cfe1 --dieta
	select * from UACTb50c57f266464faa886064d27454dded --parametry
	*/
select * from UACTe92b8f0a598245929aff74666be0acbf -- zlecenia dla pielęgniarki
select * from UACTc5cf62dfe55143fa87e057bb23e856f7 -- skierowania do psychologa
select * from UACTf2e8c2282acd41be959fd58a4a4b7e73 -- skierowanie do fizjoterapeuty
select * from UACT51e5a5839ef4422995a33e7d91642d87 -- zlecenia inne
select * from UACT5f1a16db687f4105a47306243fb81312 -- skierowanie na badania
select * from UACTb415f0b0fe074b019bc14a132346c28e -- rejestracja leków
	select * from UACTf1bfbe56dcb8437dad21397bc8505335 -- leki
select * from UACT277f8de77e0746588b30e796b0d155ee -- karta wizyt lekarskich
select * from UACTcd00270d03ff4ecc92cfccd36e7f1b25 -- wizyta kwalifikacyjn
select * from UACT733ac251dd5f41fc80e7ef869cc676c3 -- założenie historii choroby

--PIELĘGNIARKA
select * from UACT0d4c209a23ab40788016b7ace5b5b7f5 -- zlecenia lekarskie
	select * from UACT27a2a9fbe4f44556bb09f8ba6d9f4a1c --leki
	select * from UACT6772b3c90a0a4aea80ef844072d6b2b0 --zabiegi
	select * from UACT5728fb01fe9344e89b276195fafec3c2 --opatrunki
select * from UACT2259bc2125f74b7aae99fad72a7914f7 -- Założenie historii choroby
	select * from UACTff3f780f7b3945449811c5d7aa0332a0 --Toaleta chorego
	select * from UACTf0960d5f4acd4141912d8ce189cb6989 --Zabiegi
	select * from UACT74c9feaf0f0a444e95dba3109a5d8029 --Świadczenia edukacyjne
	select * from UACT2451aefd8f2f40e4b6e25e6d5b65dccc --Świadczenia medyczne
	select * from UACTa0f10413dcdc4644b3667f298d3e07ef --Świadczenia rehabilitacyjne
	select * from UACTc7086661af79433b8baa8b0ce48e2c3a --Other

--PSYCHOLOG
select * from UACT0b2212bedfc94a049c8781a6a3f9975b -- dyskryminator psychologaHS
select * from UACTf2d9417c5d8b4a41a6a5b29764927501 -- wizyta psychologa
select * from UACTc62d268b6aef4d819fb614abcfef2c07 -- Skala akceptacji chroby
select * from UACT2a7c3e7c56b943be86d9dff74e159433 -- Kwestionariusz KADS-M 
select * from UACT3cb41e9013d1426f9edc496dfb9fd43b -- wynik kwestionariusza KADS-M
select * from UACTf28343c0cd9e4e74a4fca212755b28cc -- Geriatryczna skala oceny depresji wraz z wynikiem
select * from UACTd42461f80aed4557b007b3a3881ef91e -- wynik Geriatryczna skala oceny depresji wraz z wynikiem
select * from UACT6ef6aed69eb445579a44392764b493f5 -- Test MMSE wraz z wynikiem
select * from UACT3068922b849c4422b5db36644b04386d -- wynik Test MMSE wraz z wynikiem

--FIZJOTERAPEUTA
select * from UACT51b9bf0dc09e4810abb8d9aae3692809 -- dyskryminator fizjoterapeuty
select * from UACT7a8413a760154c6a899ddb68fb9c57f0 -- pierwsza wizyta
select * from UACT776d3eae9b8348f5886c655fe699c19c -- karta pacjenta
	select * from UACT60b96b6395444b8890fbd2e1ccac74db -- karta
select * from UACTc0d948ccb10549bc9563d87f49139e0f -- wizyta
SELECT * from UACTbe03dd3ad74d415a8cdb313f76e5ba9a -- skala borga
select * from UACT13635254c31e4f1daaeacc66706f24ca -- stopień sprawności wg. karnofskiego
select * from UACTe58116fa66734723855b2f7a68db2d24 -- Indeks czynności jelit BFI
select * from UACT83bf3cdc7b2d45918b0efb708c8577f3 -- RSCL Rotterdamska lista objawów
select * from UACT0c94724cc3e44370872fe7cd4eb64991 -- skala bólu vas
select * from UACTa558bd769f244046a5753a6871c555ef -- Skala Barthel wg pacjenta
select * from UACT2be5e5394d2d450f82f663e1f2ad0acd -- wynik Skala Barthel wg pacjenta
select * from UACTb822bfb0345f4820aa8e93a870604401 -- Skala Barthel wg FIZJOTERAPEUTA
select * from UACTeb6f603d20e8432f90df5e0a4ff142a2 -- wynik Skala Barthel wg FIZJOTERAPEUTA

--dane pacjenta

select * from UACTba571a6ad1304ff9b84c6ea83cc4af2b -- ponowna rejestracja
select * from UACTc5b9f23845d34c439a8fc1572f1714d2 -- ubezpieczenie
select * from UACT8f45f0ee923c40b1a03b7d4d2f1bf7bd -- Podpisanie zgody na leczenie hospicyjne
select * from UACT6b9280a6ace0410e833f431177fc8210 -- epikryza
select * from UACT761851d997e145b9bef3c36a7a50a23a -- Epikryza wypisowa
select * from UACT19b7f45e38b5479f9ed95287351ba992 -- Wypis na stałe do szpitala
select * from UACTa88e52f48006475fbb5d936be3313948 -- Pobyt w szpitalu


