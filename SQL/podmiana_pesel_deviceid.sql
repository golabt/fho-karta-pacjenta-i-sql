

declare @nowyPESEL nvarchar(20)
declare @dataOd datetime
declare @dataDo	datetime
declare @centralkaORG nvarchar(20)
declare @centralkaNEW nvarchar(20)
declare @pompaORG nvarchar(20)
declare @pompaNEW nvarchar(20)
declare @ecgORG nvarchar(20)
declare @ecgNEW nvarchar(20)
set @centralkaORG = 'HX1107E00093F'; --nr z jakiego przychodzi wynik
set @centralkaNEW = 'HX1107E000393'; --nr na jaki chcemy przypi�� wynik
set @pompaORG = '863071017716967'; --nr z jakiego przychodzi wynik
set @pompaNEW = '22564799'; --nr na jaki chcemy przypi�� wynik
set @ecgORG = '865733022215917'; --nr z jakiego przychodzi wynik
set @ecgNEW = '865733022215255'; --nr na jaki chcemy przypi�� wynik

set @nowyPESEL = '63080714313';
set @dataOd = '2016-01-21 00:00:00.000';

UPDATE [tblMedicalDevicesLog]
SET patientID=@nowyPESEL
WHERE dateTime > @dataOd

UPDATE [tblMedicalDevicesLog]
SET mesureDeviceSN=@centralkaNEW, deviceID = @centralkaNEW
WHERE dateTime > @dataOd AND mesureDeviceSN = @centralkaORG

UPDATE [tblMedicalDevicesLog]
SET mesureDeviceSN=@ecgNEW, deviceID = @ecgNEW
WHERE dateTime > @dataOd AND mesureDeviceSN = @ecgORG

UPDATE [tblInfusionInfo]
SET patientID=@nowyPESEL, DeviceId = @pompaNEW
WHERE StatusDate > @dataOd AND DeviceId = @pompaORG

UPDATE [tblPumpInfo]
SET DeviceId = @pompaNEW
WHERE StatusDate > @dataOd AND DeviceId = @pompaORG
