USE [Sequence]
GO

/****** Object:  Table [dbo].[DeviceRental]    Script Date: 2015-12-08 10:02:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DeviceRental](
	[DeviceRentalId] [int] IDENTITY(1,1) NOT NULL,
	[DeviceId] [nvarchar](255) NULL,
	[PatientPESEL] [nvarchar](255) NULL,
	[RentedSince] [datetime] NULL,
	[RentedUntil] [datetime] NULL,
	[fldIWfId] [int] NULL,
 CONSTRAINT [PK_DeviceRental] PRIMARY KEY CLUSTERED 
(
	[DeviceRentalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO




USE [Sequence]
GO

/****** Object:  StoredProcedure [dbo].[spWFADummy]    Script Date: 2015-12-08 13:31:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER  PROCEDURE [dbo].[spWFADeviceReturned]
	@workflowInstanceId int,
	@returnDate datetime
AS
BEGIN
	UPDATE
		[dbo].[DeviceRental]
	SET
		[RentedUntil] = @returnDate		
	WHERE
		[fldIWfId] = @workflowInstanceId
	AND
		[DeviceId] IN 
		(SELECT DeviceId from [dbo].[DeviceRental]
			WHERE EXISTS
			(SELECT [Numer Seryjny] FROM [dbo].[Centralka] WHERE [Numer seryjny] = [dbo].[DeviceRental].DeviceId AND (StanSprzetu = 'Zwr�cono sprz�t sprawny, kompletny' OR StanSprzetu = 'Zwr�cono sprz�t sprawny, niekompletny' OR StanSprzetu = 'Zwr�cono sprz�t uszkodzony, do naprawy'))
			OR EXISTS
			(SELECT [Numer Seryjny] FROM [dbo].[Pompy] WHERE [Numer seryjny] = [dbo].[DeviceRental].DeviceId AND (StanSprzetu = 'Zwr�cono sprz�t sprawny, kompletny' OR StanSprzetu = 'Zwr�cono sprz�t sprawny, niekompletny' OR StanSprzetu = 'Zwr�cono sprz�t uszkodzony, do naprawy'))
		)

		
END
GO






