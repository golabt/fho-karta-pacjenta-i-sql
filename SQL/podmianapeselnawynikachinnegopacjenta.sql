

declare @nowyPESEL nvarchar(10)
declare @dataOd datetime
set @nowyPESEL = '63080714313';
set @dataOd = '2016-01-21 00:00:00.000';

UPDATE [tblMedicalDevicesLog]
SET patientID=@nowyPESEL
WHERE dateTime > @dataOd

UPDATE [tblInfusionInfo]
SET patientID=@nowyPESEL
WHERE StatusDate > @dataOd