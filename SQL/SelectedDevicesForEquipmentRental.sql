USE [Sequence]
GO

/****** Object:  View [dbo].[View_SelectedDevicesForEquipentRental]    Script Date: 2015-10-17 11:09:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[View_SelectedDevicesForEquipentRental]
AS
SELECT dbo.Centralka.fldId, dbo.Centralka.fldIWfId, dbo.Centralka.Nazwa, dbo.Centralka.[Numer seryjny], dbo.Centralka.[Numer karty SIM], dbo.Centralka.Status, dbo.Centralka.Selected
FROM dbo.Centralka 
WHERE dbo.Centralka.Selected = 1 AND dbo.Centralka.fldIWfId IS NOT NULL

UNION
SELECT dbo.Cisnieniomierz.fldId, dbo.Cisnieniomierz.fldIWfId, dbo.Cisnieniomierz.Nazwa, dbo.Cisnieniomierz.[Numer seryjny], NULL as [Numer karty SIM], dbo.Cisnieniomierz.Status, dbo.Cisnieniomierz.Selected
FROM dbo.Cisnieniomierz 
WHERE dbo.Cisnieniomierz.Selected = 1 AND dbo.Cisnieniomierz.fldIWfId IS NOT NULL

UNION
SELECT dbo.EKG.fldId, dbo.EKG.fldIWfId, dbo.EKG.Nazwa, dbo.EKG.[Numer seryjny], dbo.EKG.[Numer karty SIM], dbo.EKG.Status, dbo.EKG.Selected
FROM dbo.EKG  
WHERE dbo.EKG.Selected = 1 AND dbo.EKG.fldIWfId IS NOT NULL

UNION
SELECT dbo.Pompy.fldId, dbo.Pompy.fldIWfId, dbo.Pompy.Nazwa, dbo.Pompy.[Numer seryjny], dbo.Pompy.[Numer karty SIM], dbo.Pompy.Status, dbo.Pompy.Selected
FROM dbo.Pompy  
WHERE dbo.Pompy.Selected = 1 AND dbo.Pompy.fldIWfId IS NOT NULL

UNION
SELECT dbo.Pulsoksymetr.fldId, dbo.Pulsoksymetr.fldIWfId, dbo.Pulsoksymetr.Nazwa, dbo.Pulsoksymetr.[Numer seryjny], dbo.Pulsoksymetr.[Numer karty SIM], dbo.Pulsoksymetr.Status, dbo.Pulsoksymetr.Selected
FROM dbo.Pulsoksymetr  
WHERE dbo.Pulsoksymetr.Selected = 1 AND dbo.Pulsoksymetr.fldIWfId IS NOT NULL

UNION
SELECT dbo.Respirator.fldId, dbo.Respirator.fldIWfId, dbo.Respirator.Nazwa, dbo.Respirator.[Numer seryjny], NULL as [Numer karty SIM], dbo.Respirator.Status, dbo.Respirator.Selected
FROM dbo.Respirator  
WHERE dbo.Respirator.Selected = 1 AND dbo.Respirator.fldIWfId IS NOT NULL

UNION
SELECT dbo.Waga.fldId, dbo.Waga.fldIWfId, dbo.Waga.Nazwa, dbo.Waga.[Numer seryjny], NULL as [Numer karty SIM], dbo.Waga.Status, dbo.Waga.Selected
FROM dbo.Waga  
WHERE dbo.Waga.Selected = 1 AND dbo.Waga.fldIWfId IS NOT NULL

UNION
SELECT dbo.SprzetSzpitalny.fldId, dbo.SprzetSzpitalny.fldIWfId, dbo.SprzetSzpitalny.Nazwa, dbo.SprzetSzpitalny.[Numer seryjny], NULL as [Numer karty SIM], dbo.SprzetSzpitalny.Status, dbo.SprzetSzpitalny.Selected
FROM dbo.SprzetSzpitalny  
WHERE dbo.SprzetSzpitalny.Selected = 1 AND dbo.SprzetSzpitalny.fldIWfId IS NOT NULL





GO


