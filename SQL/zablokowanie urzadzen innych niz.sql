

declare @dostepneCentralka nvarchar(25)
declare @dostepnePompa nvarchar(25)
declare @dostepnePompaModul nvarchar(25)
declare @dostepneEKG	nvarchar(25)
declare @dostepnaPulsoksymetr nvarchar(25)
set @dostepneCentralka = 'HX1107E00093F'; --nr urzadzenia jakie ma byc dostepne w tabeli Centralka
set @dostepnePompa = '22564800'; --nr urzadzenia jakie ma byc dostepne w tabeli Pompa	
set @dostepnePompaModul = '863071017716967'; --nr urzadzenia jakie ma byc dostepne w tabeli Pompa	
set @dostepneEKG = '865733022215917'; --nr urzadzenia jakie ma byc dostepne w tabeli EKG
set @dostepnaPulsoksymetr = '865733022216535 '; --nr urzadzenia jakie ma byc dostepne w tabeli Pulsoksymetr

UPDATE [Pompy]
SET Status = 'Dostępny', Selected = NULL, fldIWfId = NULL
WHERE [Numer seryjny] = @dostepnePompa OR [Numer seryjny] = @dostepnePompaModul

UPDATE [Pompy]
SET Status = 'Wypożyczony', Selected = '1', fldIWfId = '100'
WHERE [Numer seryjny] != @dostepnePompaModul AND [Numer IMEI] IS NOT NULL AND [Numer karty SIM] IS NOT NULL AND [Numer seryjny] IS NOT NULL

UPDATE [Pompy]
SET Status = 'Wypożyczony', Selected = '1', fldIWfId = '100'
WHERE [Numer seryjny] != @dostepnePompa AND [Numer IMEI] IS NULL AND [Numer karty SIM] IS NULL AND [Numer seryjny] IS NOT NULL

UPDATE [Pulsoksymetr]
SET Status = 'Dostępny', Selected = NULL, fldIWfId = NULL
WHERE [Numer seryjny] = @dostepnaPulsoksymetr

UPDATE [Pulsoksymetr]
SET Status = 'Wypożyczony', Selected = '1', fldIWfId = '100'
WHERE [Numer seryjny] != @dostepnaPulsoksymetr AND [Numer IMEI] IS NOT NULL AND [Numer karty SIM] IS NOT NULL AND [Numer seryjny] IS NOT NULL

UPDATE [EKG]
SET Status = 'Dostępny', Selected = NULL, fldIWfId = NULL
WHERE [Numer seryjny] = @dostepneEKG

UPDATE [EKG]
SET Status = 'Wypożyczony', Selected = '1', fldIWfId = '100'
WHERE [Numer seryjny] != @dostepneEKG AND [Numer IMEI] IS NOT NULL AND [Numer karty SIM] IS NOT NULL AND [Numer seryjny] IS NOT NULL

UPDATE [Pompy]
SET Status = 'Dostępny', Selected = NULL, fldIWfId = NULL
WHERE [Numer seryjny] = @dostepnePompa OR [Numer seryjny] = @dostepnePompaModul

UPDATE [Pompy]
SET Status = 'Wypożyczony', Selected = '1', fldIWfId = '100'
WHERE [Numer seryjny] != @dostepnePompaModul

UPDATE [Pompy]
SET Status = 'Wypożyczony', Selected = '1', fldIWfId = '100'
WHERE [Numer seryjny] != @dostepnePompa AND [Numer IMEI] IS NULL AND [Numer karty SIM] IS NULL AND [Numer seryjny] IS NOT NULL

UPDATE [Centralka]
SET Status = 'Dostępny', Selected = NULL, fldIWfId = NULL
WHERE [Numer seryjny] = @dostepneCentralka

UPDATE [Centralka]
SET Status = 'Wypożyczony', Selected = '1', fldIWfId = '100'
WHERE [Numer seryjny] != @dostepneCentralka AND [Numer IMEI] IS NULL AND [Numer karty SIM] IS NULL