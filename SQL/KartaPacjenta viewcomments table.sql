USE [Sequence]
GO

/****** Object:  Table [dbo].[ViewComments]    Script Date: 2015-11-18 20:13:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ViewComments](
	[CommentId] [int] IDENTITY(1,1) NOT NULL,
	[Author] [nvarchar](255) NULL,
	[Date] [datetime] NULL,
	[Content] [ntext] NULL,
	[ParentViewName] [nvarchar](255) NULL,
	[fldId] [int] NULL,
 CONSTRAINT [PK_ViewComments] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


