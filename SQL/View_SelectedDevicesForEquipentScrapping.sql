USE [Sequence]
GO

/****** Object:  View [dbo].[View_SelectedDevicesForEquipmentScrappingOrRepair]    Script Date: 2015-10-23 13:15:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




Create VIEW [dbo].[View_SelectedDevicesForEquipmentScrapping]
AS
SELECT dbo.Centralka.fldId, dbo.Centralka.fldIWfId, dbo.Centralka.Nazwa, NULL as [Model], dbo.Centralka.[Numer seryjny], dbo.Centralka.[Numer karty SIM], dbo.Centralka.Status, dbo.Centralka.Selected, dbo.Centralka.StanSprzetu
FROM dbo.Centralka 
WHERE dbo.Centralka.Selected = 1 AND dbo.Centralka.fldIWfId IS NOT NULL AND dbo.Centralka.StanSprzetu = 'Zwr�cono sprz�t uszkodzony, do z�omowania'

UNION
SELECT dbo.Cisnieniomierz.fldId, dbo.Cisnieniomierz.fldIWfId, dbo.Cisnieniomierz.Nazwa, NULL as [Model], dbo.Cisnieniomierz.[Numer seryjny], NULL as [Numer karty SIM], dbo.Cisnieniomierz.Status, dbo.Cisnieniomierz.Selected, dbo.Cisnieniomierz.StanSprzetu
FROM dbo.Cisnieniomierz 
WHERE dbo.Cisnieniomierz.Selected = 1 AND dbo.Cisnieniomierz.fldIWfId IS NOT NULL AND dbo.Cisnieniomierz.StanSprzetu = 'Zwr�cono sprz�t uszkodzony, do z�omowania'

UNION
SELECT dbo.EKG.fldId, dbo.EKG.fldIWfId, dbo.EKG.Nazwa, NULL as [Model], dbo.EKG.[Numer seryjny], dbo.EKG.[Numer karty SIM], dbo.EKG.Status, dbo.EKG.Selected, dbo.EKG.StanSprzetu
FROM dbo.EKG  
WHERE dbo.EKG.Selected = 1 AND dbo.EKG.fldIWfId IS NOT NULL AND dbo.EKG.StanSprzetu = 'Zwr�cono sprz�t uszkodzony, do z�omowania'

UNION
SELECT dbo.Pompy.fldId, dbo.Pompy.fldIWfId, dbo.Pompy.Nazwa, NULL as [Model], dbo.Pompy.[Numer seryjny], dbo.Pompy.[Numer karty SIM], dbo.Pompy.Status, dbo.Pompy.Selected, dbo.Pompy.StanSprzetu
FROM dbo.Pompy  
WHERE dbo.Pompy.Selected = 1 AND dbo.Pompy.fldIWfId IS NOT NULL AND dbo.Pompy.StanSprzetu = 'Zwr�cono sprz�t uszkodzony, do z�omowania'

UNION
SELECT dbo.Pulsoksymetr.fldId, dbo.Pulsoksymetr.fldIWfId, dbo.Pulsoksymetr.Nazwa, NULL as [Model], dbo.Pulsoksymetr.[Numer seryjny], dbo.Pulsoksymetr.[Numer karty SIM], dbo.Pulsoksymetr.Status, dbo.Pulsoksymetr.Selected, dbo.Pulsoksymetr.StanSprzetu
FROM dbo.Pulsoksymetr  
WHERE dbo.Pulsoksymetr.Selected = 1 AND dbo.Pulsoksymetr.fldIWfId IS NOT NULL AND dbo.Pulsoksymetr.StanSprzetu = 'Zwr�cono sprz�t uszkodzony, do z�omowania'

UNION
SELECT dbo.Respirator.fldId, dbo.Respirator.fldIWfId, dbo.Respirator.Nazwa, NULL as [Model], dbo.Respirator.[Numer seryjny], NULL as [Numer karty SIM], dbo.Respirator.Status, dbo.Respirator.Selected, dbo.Respirator.StanSprzetu
FROM dbo.Respirator  
WHERE dbo.Respirator.Selected = 1 AND dbo.Respirator.fldIWfId IS NOT NULL AND dbo.Respirator.StanSprzetu = 'Zwr�cono sprz�t uszkodzony, do z�omowania'

UNION
SELECT dbo.Waga.fldId, dbo.Waga.fldIWfId, dbo.Waga.Nazwa, NULL as [Model], dbo.Waga.[Numer seryjny], NULL as [Numer karty SIM], dbo.Waga.Status, dbo.Waga.Selected, dbo.Waga.StanSprzetu
FROM dbo.Waga  
WHERE dbo.Waga.Selected = 1 AND dbo.Waga.fldIWfId IS NOT NULL AND dbo.Waga.StanSprzetu = 'Zwr�cono sprz�t uszkodzony, do z�omowania'

UNION
SELECT dbo.SprzetSzpitalny.fldId, dbo.SprzetSzpitalny.fldIWfId, dbo.SprzetSzpitalny.Nazwa, dbo.SprzetSzpitalny.model as [Model], dbo.SprzetSzpitalny.[Numer seryjny], NULL as [Numer karty SIM], dbo.SprzetSzpitalny.Status, dbo.SprzetSzpitalny.Selected, dbo.SprzetSzpitalny.StanSprzetu
FROM dbo.SprzetSzpitalny  
WHERE dbo.SprzetSzpitalny.Selected = 1 AND dbo.SprzetSzpitalny.fldIWfId IS NOT NULL AND dbo.SprzetSzpitalny.StanSprzetu = 'Zwr�cono sprz�t uszkodzony, do z�omowania'






GO


