USE [Sequence]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
ALTER TABLE [Centralka]
ADD [StanSprzetu] [nvarchar](50) NULL

ALTER TABLE [Cisnieniomierz]
ADD [StanSprzetu] [nvarchar](50) NULL

ALTER TABLE [EKG]
ADD [StanSprzetu] [nvarchar](50) NULL

ALTER TABLE [Pompy]
ADD [StanSprzetu] [nvarchar](50) NULL

ALTER TABLE [Pulsoksymetr]
ADD [StanSprzetu] [nvarchar](50) NULL

ALTER TABLE [Respirator]
ADD [StanSprzetu] [nvarchar](50) NULL

ALTER TABLE [Waga]
ADD [StanSprzetu] [nvarchar](50) NULL

ALTER TABLE [SprzetSzpitalny]
ADD [StanSprzetu] [nvarchar](50) NULL

