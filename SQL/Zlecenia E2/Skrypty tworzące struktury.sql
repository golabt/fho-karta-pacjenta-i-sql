ALTER TABLE dbo.UACTac8f3072d9ae4d13834c7bdae21b099a ADD Rodzaj smallint NULL
ALTER TABLE dbo.[ZaleceniaLekarskieLekiDni] ADD Rodzaj smallint NULL
ALTER TABLE dbo.[ZaleceniaLekarskieLekiGodziny] ADD Rodzaj smallint NULL

ALTER TABLE [dbo].[UACT78f89301f60c46da8485465610a5ef9b]
ADD [CzestotliwoscId] [int] NULL,
	[Interwal] [int] NULL,
	[Archiwum] [bit] NULL,
	[CzestotliwoscCzestotliwosc] [nvarchar](255) NULL,
	[CzestotliwoscDni] [nvarchar](255) NULL,
	[CzestotliwoscDniMonday] [bit] NULL,
	[CzestotliwoscDniTuesday] [bit] NULL,
	[CzestotliwoscDniWednesday] [bit] NULL,
	[CzestotliwoscDniThursday] [bit] NULL,
	[CzestotliwoscDniFriday] [bit] NULL,
	[CzestotliwoscDniSaturday] [bit] NULL,
	[CzestotliwoscDniSunday] [bit] NULL,
	[Datarozpoczecia] [datetime] NULL,
	[Datazakonczenia] [datetime] NULL;



ALTER TABLE [dbo].[UACTdbfc872e7ef64d3b9ef58417cd289591]
ADD [CzestotliwoscId] [int] NULL,
	[Interwal] [int] NULL,
	[Archiwum] [bit] NULL,
	[CzestotliwoscCzestotliwosc] [nvarchar](255) NULL,
	[CzestotliwoscDni] [nvarchar](255) NULL,
	[CzestotliwoscDniMonday] [bit] NULL,
	[CzestotliwoscDniTuesday] [bit] NULL,
	[CzestotliwoscDniWednesday] [bit] NULL,
	[CzestotliwoscDniThursday] [bit] NULL,
	[CzestotliwoscDniFriday] [bit] NULL,
	[CzestotliwoscDniSaturday] [bit] NULL,
	[CzestotliwoscDniSunday] [bit] NULL,
	[Datarozpoczecia] [datetime] NULL,
	[Datazakonczenia] [datetime] NULL;

ALTER TRIGGER [dbo].[ZleceniaLekarskieDefaultHourLeki_Insert] ON [dbo].[UACTac8f3072d9ae4d13834c7bdae21b099a]  
FOR INSERT
AS
BEGIN
	declare @IdZlecenia int;
	declare @DataRozpoczecia datetime;
	declare @GodzinaDefault time;

	select @IdZlecenia=i.fldId from inserted i;
	select @DataRozpoczecia=i.Datarozpoczecia from inserted i;
	set @GodzinaDefault = '00:00:00';
	set @GodzinaDefault = DATEADD(HOUR, DATEPART(HOUR, @DataRozpoczecia)+1, @GodzinaDefault);
	
	IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiGodziny] zg WHERE zg.IdZaleceniaStacjonarne = @IdZlecenia AND zg.Rodzaj = 0)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiGodziny]
				  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,Godzina,Rodzaj) 
				values(@IdZlecenia,null,@GodzinaDefault,0);
			END
END

ALTER TRIGGER [dbo].[ZleceniaLekarskieDefaultHourOpatrunki_Insert] ON [dbo].[UACT78f89301f60c46da8485465610a5ef9b]  
FOR INSERT
AS
BEGIN
	declare @IdZlecenia int;
	declare @DataRozpoczecia datetime;
	declare @GodzinaDefault time;

	select @IdZlecenia=i.fldId from inserted i;
	select @DataRozpoczecia=i.Datarozpoczecia from inserted i;
	set @GodzinaDefault = '00:00:00';
	set @GodzinaDefault = DATEADD(HOUR, DATEPART(HOUR, @DataRozpoczecia)+1, @GodzinaDefault);
	
	IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiGodziny] zg WHERE zg.IdZaleceniaStacjonarne = @IdZlecenia AND zg.Rodzaj = 1)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiGodziny]
				  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,Godzina,Rodzaj) 
				values(@IdZlecenia,null,@GodzinaDefault,1);
			END
END

ALTER TRIGGER [dbo].[ZleceniaLekarskieDefaultHourZabiegi_Insert] ON [dbo].[UACTdbfc872e7ef64d3b9ef58417cd289591]  
FOR INSERT
AS
BEGIN
	declare @IdZlecenia int;
	declare @DataRozpoczecia datetime;
	declare @GodzinaDefault time;

	select @IdZlecenia=i.fldId from inserted i;
	select @DataRozpoczecia=i.Datarozpoczecia from inserted i;
	set @GodzinaDefault = '00:00:00';
	set @GodzinaDefault = DATEADD(HOUR, DATEPART(HOUR, @DataRozpoczecia)+1, @GodzinaDefault);
	
	IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiGodziny] zg WHERE zg.IdZaleceniaStacjonarne = @IdZlecenia AND zg.Rodzaj = 2)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiGodziny]
				  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,Godzina,Rodzaj) 
				values(@IdZlecenia,null,@GodzinaDefault,2);
			END
END


--leki 
ALTER TRIGGER [dbo].[ZleceniaLekarskieWeeklyDays_InsertUpdate] ON [dbo].[UACTac8f3072d9ae4d13834c7bdae21b099a]  -- UACTac8f3072d9ae4d13834c7bdae21b099a, WIP: UACTa0a5b8148c70418c86af68eefa910af3
FOR INSERT, UPDATE
AS
BEGIN
	declare @IdZlecenia int;
	declare @monday bit;
	declare @tuesday bit;
	declare @wednesday bit;
	declare @thursday bit;
	declare @friday bit;
	declare @saturday bit;
	declare @sunday bit;
	declare @dzienna int;

	select @IdZlecenia=i.fldId from inserted i;	
	select @monday=i.CzestotliwoscDniMonday from inserted i;
	select @tuesday=i.CzestotliwoscDniTuesday from inserted i;
	select @wednesday=i.CzestotliwoscDniWednesday from inserted i;
	select @thursday=i.CzestotliwoscDniThursday from inserted i;
	select @friday=i.CzestotliwoscDniFriday from inserted i;
	select @saturday=i.CzestotliwoscDniSaturday from inserted i;
	select @sunday=i.CzestotliwoscDniSunday from inserted i;
	select @dzienna=i.CzestotliwoscId from inserted i;
	
	if @dzienna = 1
	BEGIN
		delete from [dbo].[ZaleceniaLekarskieLekiDni]
		where IdZaleceniaStacjonarne = @IdZlecenia
	END
	ELSE
	BEGIN
		if (@monday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 2)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
				  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
				values(@IdZlecenia,null,2,0);
			END
		END
		if (@tuesday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 3)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,3,0);
			END
		END

		if (@wednesday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 4)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,4,0);
			END
		END

		if (@thursday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 5)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,5,0);
			END
		END

		if (@friday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 6)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,6,0);
			END
		END

		if (@saturday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 7)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,7,0);
			END
		END

		if (@sunday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 1)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,1,0);
			END
		END

		if (@monday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 2 AND Rodzaj = 0
		END
		if (@tuesday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 3 AND Rodzaj = 0
		END

		if (@wednesday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 4 AND Rodzaj = 0
		END

		if (@thursday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 5 AND Rodzaj = 0
		END

		if (@friday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 6 AND Rodzaj = 0
		END

		if (@saturday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 7 AND Rodzaj = 0
		END

		if (@sunday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 1 AND Rodzaj = 0
		END
	END
END
GO

--opatrunki	  
ALTER TRIGGER [dbo].[ZleceniaLekarskieWeeklyDaysOp_InsertUpdate] ON [dbo].[UACT78f89301f60c46da8485465610a5ef9b]  
FOR INSERT, UPDATE
AS
BEGIN
	declare @IdZlecenia int;
	declare @monday bit;
	declare @tuesday bit;
	declare @wednesday bit;
	declare @thursday bit;
	declare @friday bit;
	declare @saturday bit;
	declare @sunday bit;
	declare @dzienna int;

	select @IdZlecenia=i.fldId from inserted i;	
	select @monday=i.CzestotliwoscDniMonday from inserted i;
	select @tuesday=i.CzestotliwoscDniTuesday from inserted i;
	select @wednesday=i.CzestotliwoscDniWednesday from inserted i;
	select @thursday=i.CzestotliwoscDniThursday from inserted i;
	select @friday=i.CzestotliwoscDniFriday from inserted i;
	select @saturday=i.CzestotliwoscDniSaturday from inserted i;
	select @sunday=i.CzestotliwoscDniSunday from inserted i;
	select @dzienna=i.CzestotliwoscId from inserted i;
	
	if @dzienna = 1
	BEGIN
		delete from [dbo].[ZaleceniaLekarskieLekiDni]
		where IdZaleceniaStacjonarne = @IdZlecenia
	END
	ELSE
	BEGIN
		if (@monday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 2)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
				  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
				values(@IdZlecenia,null,2,1);
			END
		END
		if (@tuesday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 3)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,3,1);
			END
		END

		if (@wednesday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 4)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,4,1);
			END
		END

		if (@thursday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 5)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,5,1);
			END
		END

		if (@friday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 6)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,6,1);
			END
		END

		if (@saturday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 7)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,7,1);
			END
		END

		if (@sunday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 1)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,1,1);
			END
		END

		if (@monday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 2 AND Rodzaj = 1
		END
		if (@tuesday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 3 AND Rodzaj = 1
		END

		if (@wednesday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 4 AND Rodzaj = 1
		END

		if (@thursday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 5 AND Rodzaj = 1
		END

		if (@friday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 6 AND Rodzaj = 1
		END

		if (@saturday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 7 AND Rodzaj = 1
		END

		if (@sunday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 1 AND Rodzaj = 1
		END
	END
END
GO

--zabiegi	  
ALTER TRIGGER [dbo].[ZleceniaLekarskieWeeklyDaysZab_InsertUpdate] ON [dbo].[UACTdbfc872e7ef64d3b9ef58417cd289591]  
FOR INSERT, UPDATE
AS
BEGIN
	declare @IdZlecenia int;
	declare @monday bit;
	declare @tuesday bit;
	declare @wednesday bit;
	declare @thursday bit;
	declare @friday bit;
	declare @saturday bit;
	declare @sunday bit;
	declare @dzienna int;

	select @IdZlecenia=i.fldId from inserted i;	
	select @monday=i.CzestotliwoscDniMonday from inserted i;
	select @tuesday=i.CzestotliwoscDniTuesday from inserted i;
	select @wednesday=i.CzestotliwoscDniWednesday from inserted i;
	select @thursday=i.CzestotliwoscDniThursday from inserted i;
	select @friday=i.CzestotliwoscDniFriday from inserted i;
	select @saturday=i.CzestotliwoscDniSaturday from inserted i;
	select @sunday=i.CzestotliwoscDniSunday from inserted i;
	select @dzienna=i.CzestotliwoscId from inserted i;
	
	if @dzienna = 1
	BEGIN
		delete from [dbo].[ZaleceniaLekarskieLekiDni]
		where IdZaleceniaStacjonarne = @IdZlecenia
	END
	ELSE
	BEGIN
		if (@monday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 2)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
				  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
				values(@IdZlecenia,null,2,2);
			END
		END
		if (@tuesday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 3)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,3,2);
			END
		END

		if (@wednesday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 4)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,4,2);
			END
		END

		if (@thursday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 5)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,5,2);
			END
		END

		if (@friday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 6)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,6,2);
			END
		END

		if (@saturday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 7)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,7,2);
			END
		END

		if (@sunday = 1)
		BEGIN
			IF NOT EXISTS(SELECT TOP 1 * FROM [ZaleceniaLekarskieLekiDni] WHERE IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 1)
			BEGIN
				insert into [dbo].[ZaleceniaLekarskieLekiDni]
			  (IdZaleceniaStacjonarne,IdZaleceniaDomowe,NumerDnia,Rodzaj) 
			values(@IdZlecenia,null,1,2);
			END
		END

		if (@monday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 2 AND Rodzaj = 2
		END
		if (@tuesday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 3 AND Rodzaj = 2
		END

		if (@wednesday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 4 AND Rodzaj = 2
		END

		if (@thursday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 5 AND Rodzaj = 2
		END

		if (@friday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 6 AND Rodzaj = 2
		END

		if (@saturday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 7 AND Rodzaj = 2
		END

		if (@sunday = 0)
		BEGIN
			delete from [dbo].[ZaleceniaLekarskieLekiDni]
			where IdZaleceniaStacjonarne = @IdZlecenia AND NumerDnia = 1 AND Rodzaj = 2
		END
	END
END
GO

--opatrunki
CREATE TRIGGER [dbo].[ZbiorczeOpatrunkiPodanie_InsertUpdate] ON [dbo].[UACT844f6c6f6cda4cbca0af8064b4c9eaca]
FOR INSERT,UPDATE
AS
BEGIN
DECLARE @fldIWfId int
DECLARE @IdGodzinyZaleceniaORG int
DECLARE @IdGodzinyZalecenia int
DECLARE @IdPielegniarki int
DECLARE @DataWykonania nvarchar(50)
DECLARE @Podano bit
DECLARE @DataPlanowanegoPodania nvarchar(50)
DECLARE @Uwagi nvarchar(max)
DECLARE @PodaniaLekow XML


SELECT @fldIWfId=i.fldIWfId from inserted i;

SELECT @PodaniaLekow = PodaniaLekow FROM [UACT844f6c6f6cda4cbca0af8064b4c9eaca] WHERE fldIWfId = @fldIWfId 
DECLARE MY_CURSOR CURSOR FOR SELECT 
	   PL.NODE.value('@IdGodzinyZaleceniaORG', 'int') as IdGodzinyZaleceniaORG,
	   PL.NODE.value('@IdGodzinyZalecenia', 'int') as IdGodzinyZalecenia,
	   PL.NODE.value('@IdPielegniarki', 'int') as IdPielegniarki,
	   PL.NODE.value('@DataWykonania', 'nvarchar(50)') as DataWykonania,
	   PL.NODE.value('@Podano', 'bit') as Podano,
	   PL.NODE.value('node()[1]', 'nvarchar(max)') as Uwagi,
	   PL.NODE.value('@DataPlanowanegoPodania', 'nvarchar(50)') as DataPlanowanegoPodania
FROM @PodaniaLekow.nodes('/root/item') as PL(NODE)

OPEN MY_CURSOR
FETCH NEXT FROM MY_CURSOR INTO @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania
WHILE @@FETCH_STATUS = 0
	BEGIN 
    
		--SELECT @IdGodzinyZaleceniaORG
		IF @IdGodzinyZalecenia = @IdGodzinyZaleceniaORG
		BEGIN
			UPDATE [dbo].[ZaleceniaLekarskiePodaneLeki]
			SET IdPielegniarki=@IdPielegniarki, DataWykonania=@DataWykonania, Podano=@Podano, Uwagi=@Uwagi, DataPlanowanegoPodania = @DataPlanowanegoPodania
			WHERE IdGodzinyZalecenia=@IdGodzinyZalecenia AND DataPlanowanegoPodania = @DataPlanowanegoPodania;
		END
		IF @IdGodzinyZalecenia = 0
		BEGIN
			--Select @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania 
			--select @IdGodzinyZaleceniaORG,@IdPielegniarki,cast(@DataWykonania as datetime),@Podano,@Uwagi,cast(@DataPlanowanegoPodania as datetime),GETDATE()
			INSERT INTO [dbo].[ZaleceniaLekarskiePodaneLeki]
				  (IdGodzinyZalecenia,IdPielegniarki,DataWykonania,Podano,Uwagi,DataPlanowanegoPodania,DataDodaniaDoBazy) 
				VALUES(@IdGodzinyZaleceniaORG,@IdPielegniarki,cast(@DataWykonania as datetime),@Podano,@Uwagi,cast(@DataPlanowanegoPodania as datetime),GETDATE());
		END
		FETCH NEXT FROM MY_CURSOR INTO @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania	
	END
	CLOSE MY_CURSOR
	DEALLOCATE MY_CURSOR
END

--zabiegi
CREATE TRIGGER [dbo].[ZbiorczeZabiegiPodanie_InsertUpdate] ON [dbo].[UACTd69cac25f4d94d379d6ca2b0905c64cc]
FOR INSERT,UPDATE
AS
BEGIN
DECLARE @fldIWfId int
DECLARE @IdGodzinyZaleceniaORG int
DECLARE @IdGodzinyZalecenia int
DECLARE @IdPielegniarki int
DECLARE @DataWykonania nvarchar(50)
DECLARE @Podano bit
DECLARE @DataPlanowanegoPodania nvarchar(50)
DECLARE @Uwagi nvarchar(max)
DECLARE @PodaniaLekow XML


SELECT @fldIWfId=i.fldIWfId from inserted i;

SELECT @PodaniaLekow = PodaniaLekow FROM [UACTd69cac25f4d94d379d6ca2b0905c64cc] WHERE fldIWfId = @fldIWfId 
DECLARE MY_CURSOR CURSOR FOR SELECT 
	   PL.NODE.value('@IdGodzinyZaleceniaORG', 'int') as IdGodzinyZaleceniaORG,
	   PL.NODE.value('@IdGodzinyZalecenia', 'int') as IdGodzinyZalecenia,
	   PL.NODE.value('@IdPielegniarki', 'int') as IdPielegniarki,
	   PL.NODE.value('@DataWykonania', 'nvarchar(50)') as DataWykonania,
	   PL.NODE.value('@Podano', 'bit') as Podano,
	   PL.NODE.value('node()[1]', 'nvarchar(max)') as Uwagi,
	   PL.NODE.value('@DataPlanowanegoPodania', 'nvarchar(50)') as DataPlanowanegoPodania
FROM @PodaniaLekow.nodes('/root/item') as PL(NODE)

OPEN MY_CURSOR
FETCH NEXT FROM MY_CURSOR INTO @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania
WHILE @@FETCH_STATUS = 0
	BEGIN 
    
		--SELECT @IdGodzinyZaleceniaORG
		IF @IdGodzinyZalecenia = @IdGodzinyZaleceniaORG
		BEGIN
			UPDATE [dbo].[ZaleceniaLekarskiePodaneLeki]
			SET IdPielegniarki=@IdPielegniarki, DataWykonania=@DataWykonania, Podano=@Podano, Uwagi=@Uwagi, DataPlanowanegoPodania = @DataPlanowanegoPodania
			WHERE IdGodzinyZalecenia=@IdGodzinyZalecenia AND DataPlanowanegoPodania = @DataPlanowanegoPodania;
		END
		IF @IdGodzinyZalecenia = 0
		BEGIN
			--Select @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania 
			--select @IdGodzinyZaleceniaORG,@IdPielegniarki,cast(@DataWykonania as datetime),@Podano,@Uwagi,cast(@DataPlanowanegoPodania as datetime),GETDATE()
			INSERT INTO [dbo].[ZaleceniaLekarskiePodaneLeki]
				  (IdGodzinyZalecenia,IdPielegniarki,DataWykonania,Podano,Uwagi,DataPlanowanegoPodania,DataDodaniaDoBazy) 
				VALUES(@IdGodzinyZaleceniaORG,@IdPielegniarki,cast(@DataWykonania as datetime),@Podano,@Uwagi,cast(@DataPlanowanegoPodania as datetime),GETDATE());
		END
		FETCH NEXT FROM MY_CURSOR INTO @IdGodzinyZaleceniaORG, @IdGodzinyZalecenia, @IdPielegniarki, @DataWykonania, @Podano, @Uwagi, @DataPlanowanegoPodania	
	END
	CLOSE MY_CURSOR
	DEALLOCATE MY_CURSOR
END


ALTER FUNCTION [dbo].[ZaleceniaLekarskieZabiegiCzyPodacWdniuHS] (@idZalecenia int, @day datetime)
RETURNS bit
AS
BEGIN
	RETURN CASE 
		WHEN EXISTS (
			SELECT 1 FROM UACTdbfc872e7ef64d3b9ef58417cd289591 l --ORG: UACTac8f3072d9ae4d13834c7bdae21b099a WIP: UACTa0a5b8148c70418c86af68eefa910af3
			WHERE l.fldId = @idZalecenia 
				AND @day BETWEEN l.DataRozpoczecia AND ISNULL(l.DataZakonczenia, @day) -- during period (or lasting forever)
				AND (
					-- daily
					l.CzestotliwoscId = 1 AND DATEDIFF(DAY, l.DataRozpoczecia, @day) % l.Interwal = 0
					OR -- weekly
					l.CzestotliwoscId = 2 --AND DATEDIFF(WEEK, l.DataRozpoczecia, @day) % l.Interwal = 0 AND ( --bug! datediff always treats sunday as start of the week
						AND DATEDIFF(WEEK, 
								DATEADD(DAY, 1 - DATEPART(WEEKDAY, l.DataRozpoczecia), l.DataRozpoczecia), -- start of the week
								DATEADD(DAY, 1 - DATEPART(WEEKDAY, @day), @day) -- start of the week
						) % l.Interwal = 0 AND (
						(
							NOT EXISTS ( -- days of week not specified
								SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
								WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.Rodzaj = 2
							)
							AND DATEPART(WEEKDAY, l.DataRozpoczecia) = DATEPART(WEEKDAY, @day) -- same day of week
						)
						OR EXISTS ( -- specific days of the week
							SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
							WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.NumerDnia = DATEPART(WEEKDAY, @day) AND d.Rodzaj = 2
						)
					)
					OR -- monthly
					l.CzestotliwoscId = 3 AND DATEDIFF(MONTH, l.DataRozpoczecia, @day) % l.Interwal = 0 AND (
						(
							NOT EXISTS ( -- days of month not specified
								SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
								WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.Rodzaj = 2
							)
							AND DATEPART(DAY, l.DataRozpoczecia) = DATEPART(DAY, @day) -- same day of month
						)
						OR EXISTS ( -- specific days of the month
							SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
							WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.NumerDnia = DATEPART(DAY, @day) AND d.Rodzaj = 2
						)
					)
				)
		) THEN 1
		ELSE 0 
	END
END


ALTER FUNCTION [dbo].[ZaleceniaLekarskieOpatrunkiCzyPodacWdniuHS] (@idZalecenia int, @day datetime)
RETURNS bit
AS
BEGIN
	RETURN CASE 
		WHEN EXISTS (
			SELECT 1 FROM UACT78f89301f60c46da8485465610a5ef9b l --ORG: UACTac8f3072d9ae4d13834c7bdae21b099a WIP: UACTa0a5b8148c70418c86af68eefa910af3
			WHERE l.fldId = @idZalecenia
				AND @day BETWEEN l.DataRozpoczecia AND ISNULL(l.DataZakonczenia, @day) -- during period (or lasting forever)
				AND (
					-- daily
					l.CzestotliwoscId = 1 AND DATEDIFF(DAY, l.DataRozpoczecia, @day) % l.Interwal = 0
					OR -- weekly
					l.CzestotliwoscId = 2 --AND DATEDIFF(WEEK, l.DataRozpoczecia, @day) % l.Interwal = 0 AND ( --bug! datediff always treats sunday as start of the week
						AND DATEDIFF(WEEK, 
								DATEADD(DAY, 1 - DATEPART(WEEKDAY, l.DataRozpoczecia), l.DataRozpoczecia), -- start of the week
								DATEADD(DAY, 1 - DATEPART(WEEKDAY, @day), @day) -- start of the week
						) % l.Interwal = 0 AND (
						(
							NOT EXISTS ( -- days of week not specified
								SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
								WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.Rodzaj = 1
							)
							AND DATEPART(WEEKDAY, l.DataRozpoczecia) = DATEPART(WEEKDAY, @day) -- same day of week
						)
						OR EXISTS ( -- specific days of the week
							SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
							WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.NumerDnia = DATEPART(WEEKDAY, @day) AND d.Rodzaj = 1
						)
					)
					OR -- monthly
					l.CzestotliwoscId = 3 AND DATEDIFF(MONTH, l.DataRozpoczecia, @day) % l.Interwal = 0 AND (
						(
							NOT EXISTS ( -- days of month not specified
								SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
								WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.Rodzaj = 1 
							)
							AND DATEPART(DAY, l.DataRozpoczecia) = DATEPART(DAY, @day) -- same day of month
						)
						OR EXISTS ( -- specific days of the month
							SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
							WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.NumerDnia = DATEPART(DAY, @day) AND d.Rodzaj = 1
						)
					)
				)
		) THEN 1
		ELSE 0 
	END
END

ALTER FUNCTION [dbo].[ZaleceniaLekarskieLekiCzyPodacWdniuHS] (@idZalecenia int, @day datetime)
RETURNS bit
AS
BEGIN
	RETURN CASE 
		WHEN EXISTS (
			SELECT 1 FROM UACTac8f3072d9ae4d13834c7bdae21b099a l --ORG: UACTac8f3072d9ae4d13834c7bdae21b099a WIP: UACTa0a5b8148c70418c86af68eefa910af3
			WHERE l.fldId = @idZalecenia
				AND @day BETWEEN l.DataRozpoczecia AND ISNULL(l.DataZakonczenia, @day) -- during period (or lasting forever)
				AND (
					-- daily
					l.CzestotliwoscId = 1 AND DATEDIFF(DAY, l.DataRozpoczecia, @day) % l.Interwal = 0
					OR -- weekly
					l.CzestotliwoscId = 2 --AND DATEDIFF(WEEK, l.DataRozpoczecia, @day) % l.Interwal = 0 AND ( --bug! datediff always treats sunday as start of the week
						AND DATEDIFF(WEEK, 
								DATEADD(DAY, 1 - DATEPART(WEEKDAY, l.DataRozpoczecia), l.DataRozpoczecia), -- start of the week
								DATEADD(DAY, 1 - DATEPART(WEEKDAY, @day), @day) -- start of the week
						) % l.Interwal = 0 AND (
						(
							NOT EXISTS ( -- days of week not specified
								SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
								WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.Rodzaj = 0 
							)
							AND DATEPART(WEEKDAY, l.DataRozpoczecia) = DATEPART(WEEKDAY, @day) -- same day of week
						)
						OR EXISTS ( -- specific days of the week
							SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
							WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.NumerDnia = DATEPART(WEEKDAY, @day) AND d.Rodzaj = 0 
						)
					)
					OR -- monthly
					l.CzestotliwoscId = 3 AND DATEDIFF(MONTH, l.DataRozpoczecia, @day) % l.Interwal = 0 AND (
						(
							NOT EXISTS ( -- days of month not specified
								SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
								WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.Rodzaj = 0  
							)
							AND DATEPART(DAY, l.DataRozpoczecia) = DATEPART(DAY, @day) -- same day of month
						)
						OR EXISTS ( -- specific days of the month
							SELECT 1 FROM ZaleceniaLekarskieLekiDni d 
							WHERE d.IdZaleceniaStacjonarne = l.fldId AND d.NumerDnia = DATEPART(DAY, @day) AND d.Rodzaj = 0 
						)
					)
				)
		) THEN 1
		ELSE 0 
	END
END


ALTER FUNCTION [dbo].[ZaleceniaLekarskieLekiCzyPodacNaZmianieHS] (@idGodzinyZalecenia int, @time time)
RETURNS bit
AS
BEGIN
	RETURN CASE 
		WHEN EXISTS (
			SELECT 1 FROM ZaleceniaLekarskieLekiGodziny lg
			INNER JOIN GodzinyZmian gz ON 
				(gz.GodzinaOd < gz.GodzinaDo AND @time >= gz.GodzinaOd AND @time < gz.GodzinaDo)
				OR (gz.GodzinaOd > gz.GodzinaDo AND (@time >= gz.GodzinaOd OR @time < gz.GodzinaDo))
			WHERE --lg.IdZalecenia = @idZalecenia
				lg.fldId = @idGodzinyZalecenia
				AND (
					gz.GodzinaOd < gz.GodzinaDo AND lg.Godzina >= gz.GodzinaOd AND lg.Godzina < gz.GodzinaDo
					OR gz.GodzinaOd > gz.GodzinaDo AND (lg.Godzina >= gz.GodzinaOd OR lg.Godzina < gz.GodzinaDo)
				)
				AND lg.Rodzaj = 0
		) THEN 1
		ELSE 0
	END
END 

ALTER FUNCTION [dbo].[ZaleceniaLekarskieOpatrunkiCzyPodacNaZmianieHS] (@idGodzinyZalecenia int, @time time)
RETURNS bit
AS
BEGIN
	RETURN CASE 
		WHEN EXISTS (
			SELECT 1 FROM ZaleceniaLekarskieLekiGodziny lg
			INNER JOIN GodzinyZmian gz ON 
				(gz.GodzinaOd < gz.GodzinaDo AND @time >= gz.GodzinaOd AND @time < gz.GodzinaDo)
				OR (gz.GodzinaOd > gz.GodzinaDo AND (@time >= gz.GodzinaOd OR @time < gz.GodzinaDo))
			WHERE --lg.IdZalecenia = @idZalecenia
				lg.fldId = @idGodzinyZalecenia
				AND (
					gz.GodzinaOd < gz.GodzinaDo AND lg.Godzina >= gz.GodzinaOd AND lg.Godzina < gz.GodzinaDo
					OR gz.GodzinaOd > gz.GodzinaDo AND (lg.Godzina >= gz.GodzinaOd OR lg.Godzina < gz.GodzinaDo)
				)
				AND lg.Rodzaj = 1
		) THEN 1
		ELSE 0
	END
END 

ALTER FUNCTION [dbo].[ZaleceniaLekarskieZabiegiCzyPodacNaZmianieHS] (@idGodzinyZalecenia int, @time time)
RETURNS bit
AS
BEGIN
	RETURN CASE 
		WHEN EXISTS (
			SELECT 1 FROM ZaleceniaLekarskieLekiGodziny lg
			INNER JOIN GodzinyZmian gz ON 
				(gz.GodzinaOd < gz.GodzinaDo AND @time >= gz.GodzinaOd AND @time < gz.GodzinaDo)
				OR (gz.GodzinaOd > gz.GodzinaDo AND (@time >= gz.GodzinaOd OR @time < gz.GodzinaDo))
			WHERE --lg.IdZalecenia = @idZalecenia
				lg.fldId = @idGodzinyZalecenia
				AND (
					gz.GodzinaOd < gz.GodzinaDo AND lg.Godzina >= gz.GodzinaOd AND lg.Godzina < gz.GodzinaDo
					OR gz.GodzinaOd > gz.GodzinaDo AND (lg.Godzina >= gz.GodzinaOd OR lg.Godzina < gz.GodzinaDo)
				)
				AND lg.Rodzaj = 2
		) THEN 1
		ELSE 0
	END
END 


ALTER  PROCEDURE [dbo].[USP_ZaleceniaLekarskieLekiDoPodania] -- UACTac8f3072d9ae4d13834c7bdae21b099a, WIP: UACTa0a5b8148c70418c86af68eefa910af3
	@day datetime,
	@fldIWfId int
AS
BEGIN
	
	SELECT lg.fldId, convert(nvarchar(50), cast(@day as date) + cast(lg.Godzina as datetime), 120) as Godzina, l.Nazwaleku, l.Dawka, l.DrogaPodaniafldId, l.DrogaPodaniadrogapodania, pl.DataWykonania, pl.Podano as 'Wykonano', pl.Uwagi, pl.IdGodzinyZalecenia
	FROM UACTac8f3072d9ae4d13834c7bdae21b099a l
	INNER JOIN ZaleceniaLekarskieLekiGodziny lg ON lg.IdZaleceniaStacjonarne = l.fldId
	LEFT JOIN ZaleceniaLekarskiePodaneLeki pl ON pl.IdGodzinyZalecenia = lg.fldId AND pl.DataPlanowanegoPodania = cast(@day as date) + cast(lg.Godzina as datetime)
	WHERE l.Archiwum = 0 OR l.Archiwum IS NULL
		AND dbo.ZaleceniaLekarskieLekiCzyPodacWdniuHS(l.fldId, @day) = 1
		AND dbo.ZaleceniaLekarskieLekiCzyPodacNaZmianieHS(lg.fldId, @day) = 1
		AND l.fldIWfId = @fldIWfId
END
GO

ALTER  PROCEDURE [dbo].[USP_ZaleceniaLekarskieOpatrunkiDoWykonania] -- UACTac8f3072d9ae4d13834c7bdae21b099a, WIP: UACTa0a5b8148c70418c86af68eefa910af3
	@day datetime,
	@fldIWfId int
AS
BEGIN
	
	SELECT lg.fldId, convert(nvarchar(50), cast(@day as date) + cast(lg.Godzina as datetime), 120) as Godzina, l.Rodzajopatrunku, l.Miejsce, l.Czynno��czynnosc as 'Czynnosc', pl.DataWykonania, pl.Podano as 'Wykonano', pl.Uwagi, pl.IdGodzinyZalecenia
	FROM UACT78f89301f60c46da8485465610a5ef9b l
	INNER JOIN ZaleceniaLekarskieLekiGodziny lg ON lg.IdZaleceniaStacjonarne = l.fldId
	LEFT JOIN ZaleceniaLekarskiePodaneLeki pl ON pl.IdGodzinyZalecenia = lg.fldId AND pl.DataPlanowanegoPodania = cast(@day as date) + cast(lg.Godzina as datetime)
	WHERE l.Archiwum = 0 OR l.Archiwum IS NULL
		AND dbo.ZaleceniaLekarskieOpatrunkiCzyPodacWdniuHS(l.fldId, @day) = 1
		AND dbo.ZaleceniaLekarskieOpatrunkiCzyPodacNaZmianieHS(lg.fldId, @day) = 1
		AND l.fldIWfId = @fldIWfId	
END
GO

ALTER  PROCEDURE [dbo].[USP_ZaleceniaLekarskieZabiegiDoWykonania] -- UACTac8f3072d9ae4d13834c7bdae21b099a, WIP: UACTa0a5b8148c70418c86af68eefa910af3
	@day datetime,
	@fldIWfId int
AS
BEGIN
	
	SELECT lg.fldId, convert(nvarchar(50), cast(@day as date) + cast(lg.Godzina as datetime), 120) as Godzina, l.Zabiegrodzajzabiegu as 'Zabieg', pl.DataWykonania, pl.Podano as 'Wykonano', pl.Uwagi, pl.IdGodzinyZalecenia
	FROM UACTdbfc872e7ef64d3b9ef58417cd289591 l
	INNER JOIN ZaleceniaLekarskieLekiGodziny lg ON lg.IdZaleceniaStacjonarne = l.fldId
	LEFT JOIN ZaleceniaLekarskiePodaneLeki pl ON pl.IdGodzinyZalecenia = lg.fldId AND pl.DataPlanowanegoPodania = cast(@day as date) + cast(lg.Godzina as datetime)
	WHERE l.Archiwum = 0 OR l.Archiwum IS NULL
		AND dbo.ZaleceniaLekarskieZabiegiCzyPodacWdniuHS(l.fldId, @day) = 1
		AND dbo.ZaleceniaLekarskieZabiegiCzyPodacNaZmianieHS(lg.fldId, @day) = 1
		AND l.fldIWfId = @fldIWfId
END
GO

ALTER  PROCEDURE [dbo].[USP_ZbiorczeLekiDoPodania] 
	@day datetime
AS
BEGIN
	
		SELECT lg.fldId, convert(nvarchar(50), cast(@day as date) + cast(lg.Godzina as datetime), 120) as Godzina, l.Nazwaleku, l.Dawka, l.DrogaPodaniafldId, l.DrogaPodaniadrogapodania, pl.DataWykonania, pl.Podano as 'Wykonano', pl.Uwagi, pl.IdGodzinyZalecenia, p.txtimi� as 'Imie', p.txtnazwisko as 'Nazwisko', p.PESEL as 'PESEL', s.ntxtnumersali as 'Sala'
		FROM UACTac8f3072d9ae4d13834c7bdae21b099a l
		INNER JOIN ZaleceniaLekarskieLekiGodziny lg ON lg.IdZaleceniaStacjonarne = l.fldId
		LEFT JOIN ZaleceniaLekarskiePodaneLeki pl ON pl.IdGodzinyZalecenia = lg.fldId AND pl.DataPlanowanegoPodania = cast(@day as date) + cast(lg.Godzina as datetime)
		LEFT JOIN UACT9a0885e607604a2992f170e2c415df67 p ON l.fldIWfId = p.fldIWfId		
		LEFT JOIN UACT65a5c2ec1bba4d6f843944b8e404e300 s ON l.fldIWfId = s.fldIWfId
		WHERE l.Archiwum = 0 OR l.Archiwum IS NULL
			AND dbo.ZaleceniaLekarskieLekiCzyPodacWdniuHS(l.fldId, @day) = 1
			AND dbo.ZaleceniaLekarskieLekiCzyPodacNaZmianieHS(lg.fldId, @day) = 1
		ORDER BY p.txtnazwisko, lg.Godzina
	
END
GO

ALTER  PROCEDURE [dbo].[USP_ZbiorczeOpatrunkiDoWykonania] 
	@day datetime
AS
BEGIN
	
		SELECT lg.fldId, convert(nvarchar(50), cast(@day as date) + cast(lg.Godzina as datetime), 120) as Godzina, l.Rodzajopatrunku, l.Miejsce, l.Czynno��czynnosc as 'Czynnosc', pl.DataWykonania, pl.Podano as 'Wykonano', pl.Uwagi, pl.IdGodzinyZalecenia, p.txtimi� as 'Imie', p.txtnazwisko as 'Nazwisko', p.PESEL as 'PESEL', s.ntxtnumersali as 'Sala'
		FROM UACT78f89301f60c46da8485465610a5ef9b l
		INNER JOIN ZaleceniaLekarskieLekiGodziny lg ON lg.IdZaleceniaStacjonarne = l.fldId
		LEFT JOIN ZaleceniaLekarskiePodaneLeki pl ON pl.IdGodzinyZalecenia = lg.fldId AND pl.DataPlanowanegoPodania = cast(@day as date) + cast(lg.Godzina as datetime)
		LEFT JOIN UACT9a0885e607604a2992f170e2c415df67 p ON l.fldIWfId = p.fldIWfId		
		LEFT JOIN UACT65a5c2ec1bba4d6f843944b8e404e300 s ON l.fldIWfId = s.fldIWfId
		WHERE l.Archiwum = 0 OR l.Archiwum IS NULL
			AND dbo.ZaleceniaLekarskieOpatrunkiCzyPodacWdniuHS(l.fldId, @day) = 1
			AND dbo.ZaleceniaLekarskieOpatrunkiCzyPodacNaZmianieHS(lg.fldId, @day) = 1
		ORDER BY p.txtnazwisko, lg.Godzina
	
END
GO


ALTER  PROCEDURE [dbo].[USP_ZbiorczeZabiegiDoWykonania] 
	@day datetime
AS
BEGIN
	
		SELECT lg.fldId, convert(nvarchar(50), cast(@day as date) + cast(lg.Godzina as datetime), 120) as Godzina, l.Zabiegrodzajzabiegu as 'Zabieg', pl.DataWykonania, pl.Podano as 'Wykonano', pl.Uwagi, pl.IdGodzinyZalecenia, p.txtimi� as 'Imie', p.txtnazwisko as 'Nazwisko', p.PESEL as 'PESEL', s.ntxtnumersali as 'Sala'
		FROM UACTdbfc872e7ef64d3b9ef58417cd289591 l
		INNER JOIN ZaleceniaLekarskieLekiGodziny lg ON lg.IdZaleceniaStacjonarne = l.fldId
		LEFT JOIN ZaleceniaLekarskiePodaneLeki pl ON pl.IdGodzinyZalecenia = lg.fldId AND pl.DataPlanowanegoPodania = cast(@day as date) + cast(lg.Godzina as datetime)
		LEFT JOIN UACT9a0885e607604a2992f170e2c415df67 p ON l.fldIWfId = p.fldIWfId		
		LEFT JOIN UACT65a5c2ec1bba4d6f843944b8e404e300 s ON l.fldIWfId = s.fldIWfId
		WHERE l.Archiwum = 0 OR l.Archiwum IS NULL
			AND dbo.ZaleceniaLekarskieZabiegiCzyPodacWdniuHS(l.fldId, @day) = 1
			AND dbo.ZaleceniaLekarskieZabiegiCzyPodacNaZmianieHS(lg.fldId, @day) = 1
		ORDER BY p.txtnazwisko, lg.Godzina
END
GO

