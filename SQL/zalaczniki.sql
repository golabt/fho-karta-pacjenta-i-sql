select * from [View_PatientAttachmentsHS]

ALTER VIEW [dbo].[View_PatientAttachmentsHS]
AS
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACT93ea22f865a1420293b00891f8782c68 x -- 1. Oswiadczenie o posiadanym ubezpieczeniu
join tblAttachments a on a.fldId = x.osiwadczenieoposiadanymubezpieczeniuValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTd44c30b0c7034b3fa0bb5cc21cae9caa x  -- 2. Ubezpieczenie
join tblAttachments a on a.fldId = x.SkanDecyzjiValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTb22fe135275c4253b3b5f544bb37347a x  -- 3. Rejestracja pacjenta
join tblAttachments a 
on a.fldId = x.SkandowoduValue 
or a.fldId = x.skandowodu2Value
or a.fldId = x.skanpotwierdzeniachorobynowotworowejValue 
or a.fldId = x.skandowoduubezpieczeniaValue
or a.fldId = x.skierowanieValue
or a.fldId = x.orzeczenieValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTc1f1498ef17045ea99f5337627efc27d x  -- 4. Ponowna rejestracja pacjenta
join tblAttachments a 
on a.fldId = x.SkandowoduValue 
or a.fldId = x.skandowodu2Value
or a.fldId = x.skanpotwierdzeniachorobynowotworowejValue 
or a.fldId = x.skandowoduubezpieczeniaValue
or a.fldId = x.skierowanieValue
or a.fldId = x.orzeczenieValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTb1688d7f6c59480dac95ace459b1b3fd x  -- 5. Ponowna rejestracja
join tblAttachments a 
on a.fldId = x.skanskierowaniaValue 
or a.fldId = x.skanwypisuzeszpitalaValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTb9985cfac1d54d90b8a894c212037f38 x  -- 6. Finalizowanie przyjecia pacjenta
join tblAttachments a 
on a.fldId = x.skanupowaznieniadoinformacjimedycznychValue 
OR a.fldId = x.zgodaleczeniehospicyjneValue 
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTd715eaf74aec461489263677755158d7 x  -- 7. Epikryza wypisowa
join tblAttachments a 
on a.fldId = x.ZalacznikValue 
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTc151b469aa8d488e8d689133cf4441f6 x  -- 8. Epikryza
join tblAttachments a 
on a.fldId = x.ZalacznikValue 
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTdfa3c31754134cea9009a9798f3cb5b7 x  -- 9. Pobyt w szpitalu
join tblAttachments a 
on a.fldId = x.ZalacznikValue 
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACT9bb33ffd85cd4dd4b097f9fe56787c63 x  -- 10. Monitorowanie pobytu w szpitalu
join tblAttachments a 
on a.fldId = x.kartaszpitalValue 

ALTER VIEW [dbo].[View_PatientAttachmentsHD]
AS
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTb0754dc342914290a483a53eddf8c8b6 x -- 1. Rejestracja pacjenta
join tblAttachments a 
on a.fldId = x.SkandowoduValue 
or a.fldId = x.skandowodu2Value
or a.fldId = x.skanpotwierdzeniachorobynowotworowejValue 
or a.fldId = x.skandowoduubezpieczeniaValue
or a.fldId = x.skierowanieValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTba571a6ad1304ff9b84c6ea83cc4af2b x  -- 2. Ponowna rejestracja pacjenta
join tblAttachments a 
on a.fldId = x.skanskierowaniaValue 
or a.fldId = x.skanwypisuzeszpitalaValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTc5b9f23845d34c439a8fc1572f1714d2 x  -- 3. Ubezpieczenie
join tblAttachments a 
on a.fldId = x.SkanDecyzjiValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACT9114b8bfd8e5498789f41e3919e570d3 x  -- 4. Powr�t pacjenta
join tblAttachments a 
on a.fldId = x.skanwypsiuzeszpitalaValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACT691f84e1028a4143b491cb30897a904b x  -- 5. O�wiadczenie o posiadanym ubezpieczeniu
join tblAttachments a 
on a.fldId = x.osiwadczenieoposiadanymubezpieczeniuValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACT9aa0fbd5499646328df8b7223a0a9ae6 x  -- 6. Podpisanie dokument�w z pacjentem
join tblAttachments a 
on a.fldId = x.skanupowaznieniadoinformacjimedycznychValue
or a.fldId = x.skanzgodadanemedyczneValue
or a.fldId = x.skanzgodadaneosoboweValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACT8f45f0ee923c40b1a03b7d4d2f1bf7bd x  -- 7. Podpisanie zgody na leczenie hospicyjne z pacjentem
join tblAttachments a 
on a.fldId = x.zgodaleczeniehospcyjneValue
or a.fldId = x.zgodanaleczenieValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACT6b9280a6ace0410e833f431177fc8210 x  -- 8. Epikryza
join tblAttachments a 
on a.fldId = x.FileUpload1Value
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACT761851d997e145b9bef3c36a7a50a23a x  -- 9. Epikryza wypisowa
join tblAttachments a 
on a.fldId = x.FileUpload1Value
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACT19b7f45e38b5479f9ed95287351ba992 x  -- 10. Wypis na sta�e do szpitala
join tblAttachments a
on a.fldId = x.FileUpload1Value
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTa88e52f48006475fbb5d936be3313948 x  -- 11. Pobyt w szpitalu
join tblAttachments a 
on a.fldId = x.FileUpload1Value
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTb4ed9a056fa640fcae740a425ce528bd x  -- 12. Monitorowanie pobytu w szpitalu
join tblAttachments a 
on a.fldId = x.kartaszpitalValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACT64115b52e8dc4d2a86ee8ea9678d76ac x  -- 13. Karta informacyjna wypisowa
join tblAttachments a 
on a.fldId = x.WynikibadanValue
union
select a.fldId, a.fldFileName, a.fldContentType, a.fldContentLength, a.fldContent, x.fldIWfId from UACTdc2be552a6774c8a9c83e9b9a2fd5fa6 x  -- 14. Wypozyczenie sprzetu medycznego
join tblAttachments a 
on a.fldId = x.SkanKaucjaValue
or a.fldId = x.UmowaValue