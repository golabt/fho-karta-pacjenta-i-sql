﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospice.Domain.Concrete
{
    public static class DbConnectionSettings
    {
        private static string defaultConnectionStringName = "DefaultConnection";

        public static string GetDefaultConnectionStringName()
        {
            return defaultConnectionStringName;
        }
    }
}
