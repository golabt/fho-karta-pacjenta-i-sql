﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientWfPhysiotherapistDiscriminatorHDRepository : IPatientWfPhysiotherapistDiscriminatorHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientWfPhysiotherapistDiscriminatorHD> PatientWfPhysiotherapistDiscriminatorHD
        {
            get { return context.UACT51b9bf0dc09e4810abb8d9aae3692809; }
        }

    }
}
