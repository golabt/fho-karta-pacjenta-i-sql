﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientWfPsychologistDiscriminatorHSRepository : IPatientWfPsychologistDiscriminatorHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientWfPsychologistDiscriminatorHS> PatientWfPsychologistDiscriminatorHS
        {
            get { return context.UACT391dfe53eef14e91a16d6e1f9103bd45; }
        }

    }
}
