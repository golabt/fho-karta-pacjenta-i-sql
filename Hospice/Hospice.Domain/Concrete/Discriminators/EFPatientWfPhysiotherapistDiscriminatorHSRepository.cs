﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientWfPhysiotherapistDiscriminatorHSRepository : IPatientWfPhysiotherapistDiscriminatorHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientWfPhysiotherapistDiscriminatorHS> PatientWfPhysiotherapistDiscriminatorHS
        {
            get { return context.UACT90fb171b663d460e80858bd4477d19f3; }
        }

    }
}
