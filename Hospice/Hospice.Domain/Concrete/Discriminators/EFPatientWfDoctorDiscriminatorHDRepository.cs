﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientWfDoctorDiscriminatorHDRepository : IPatientWfDoctorDiscriminatorHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientWfDoctorDiscriminatorHD> PatientWfDoctorDiscriminatorHD
        {
            get { return context.UACTc1ee29a5ebde42d78c0528c7c4a460eb; }
        }

    }
}
