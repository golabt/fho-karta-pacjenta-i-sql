﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientWfPsychologistDiscriminatorHDRepository : IPatientWfPsychologistDiscriminatorHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientWfPsychologistDiscriminatorHD> PatientWfPsychologistDiscriminatorHD
        {
            get { return context.UACT0b2212bedfc94a049c8781a6a3f9975b; }
        }

    }
}
