﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientHDRepository: IPatientHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientHD> PatientsHD
        {
            get { return context.UACTf0f18fd8365e480cbbac90d0526c2f6b; }
        }

    }
}
