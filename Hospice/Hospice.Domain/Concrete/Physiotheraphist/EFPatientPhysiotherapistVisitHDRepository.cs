﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistVisitHDRepository : IPatientPhysiotherapistVisitHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistVisitHD> PatientPhysiotherapistVisitHD
        {
            get { return context.UACTc0d948ccb10549bc9563d87f49139e0f; }
        }

    }
}
