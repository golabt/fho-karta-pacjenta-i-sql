﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBFIIndexHDRepository : IPatientPhysiotherapistBFIIndexHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBFIIndexHD> PatientPhysiotherapistBFIIndexHD
        {
            get { return context.UACTe58116fa66734723855b2f7a68db2d24; }
        }

    }
}
