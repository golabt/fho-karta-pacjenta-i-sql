﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBarthelPatientResultHSRepository : IPatientPhysiotherapistBarthelPatientResultHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBarthelPatientResultHS> PatientPhysiotherapistBarthelPatientResultHS
        {
            get { return context.UACTabb0749e67ad44f0988687f143c1d0b3; }
        }

    }
}
