﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBorgScaleHDRepository : IPatientPhysiotherapistBorgScaleHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBorgScaleHD> PatientPhysiotherapistBorgScaleHD
        {
            get { return context.UACTbe03dd3ad74d415a8cdb313f76e5ba9a; }
        }

    }
}
