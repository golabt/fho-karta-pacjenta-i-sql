﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistRSCLHSRepository : IPatientPhysiotherapistRSCLHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistRSCLHS> PatientPhysiotherapistRSCLHS
        {
            get { return context.UACT51b87692e2db49889591fdea6ba92868; }
        }

    }
}
