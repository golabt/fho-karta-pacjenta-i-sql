﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistFirstVisitHSRepository : IPatientPhysiotherapistFirstVisitHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistFirstVisitHS> PatientPhysiotherapistFirstVisitHS
        {
            get { return context.UACT351b31f2da54445a9c7705fa00633b63; }
        }

    }
}
