﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistFirstVisitHDRepository : IPatientPhysiotherapistFirstVisitHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistFirstVisitHD> PatientPhysiotherapistFirstVisitHD
        {
            get { return context.UACT7a8413a760154c6a899ddb68fb9c57f0; }
        }

    }
}
