﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBarthelPhysiotherapistHDRepository : IPatientPhysiotherapistBarthelPhysiotherapistHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBarthelPhysiotherapistHD> PatientPhysiotherapistBarthelPhysiotherapistHD
        {
            get { return context.UACTb822bfb0345f4820aa8e93a870604401; }
        }

    }
}
