﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBarthelPhysiotherapistHSRepository : IPatientPhysiotherapistBarthelPhysiotherapistHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBarthelPhysiotherapistHS> PatientPhysiotherapistBarthelPhysiotherapistHS
        {
            get { return context.UACTfc2395aaa8bf4fd4b9e492093df0b684; }
        }

    }
}
