﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistKarnofskyPerformanceStatusHSRepository : IPatientPhysiotherapistKarnofskyPerformanceStatusHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistKarnofskyPerformanceStatusHS> PatientPhysiotherapistKarnofskyPerformanceStatusHS
        {
            get { return context.UACT78d5ab4cc7a246fa8742d49791866fcc; }
        }

    }
}
