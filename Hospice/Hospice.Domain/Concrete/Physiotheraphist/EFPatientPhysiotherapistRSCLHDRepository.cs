﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistRSCLHDRepository : IPatientPhysiotherapistRSCLHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistRSCLHD> PatientPhysiotherapistRSCLHD
        {
            get { return context.UACT83bf3cdc7b2d45918b0efb708c8577f3; }
        }

    }
}
