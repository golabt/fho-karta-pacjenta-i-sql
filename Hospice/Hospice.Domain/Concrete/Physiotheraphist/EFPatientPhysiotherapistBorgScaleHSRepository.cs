﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBorgScaleHSRepository : IPatientPhysiotherapistBorgScaleHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBorgScaleHS> PatientPhysiotherapistBorgScaleHS
        {
            get { return context.UACTc2956493e9bc496b9cdc6bc2dd54c07f; }
        }

    }
}
