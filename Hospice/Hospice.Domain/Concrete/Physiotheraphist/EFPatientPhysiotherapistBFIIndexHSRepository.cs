﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBFIIndexHSRepository : IPatientPhysiotherapistBFIIndexHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBFIIndexHS> PatientPhysiotherapistBFIIndexHS
        {
            get { return context.UACT991b8df4773c4ff19af697f6b595f9aa; }
        }

    }
}
