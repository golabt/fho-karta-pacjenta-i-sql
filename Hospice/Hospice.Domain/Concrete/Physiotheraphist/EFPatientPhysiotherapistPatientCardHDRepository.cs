﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistPatientCardHDRepository : IPatientPhysiotherapistPatientCardHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistPatientCardHD> PatientPhysiotherapistPatientCardHD
        {
            get { return context.UACT776d3eae9b8348f5886c655fe699c19c; }
        }

    }
}
