﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBarthelPatientResultHDRepository : IPatientPhysiotherapistBarthelPatientResultHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBarthelPatientResultHD> PatientPhysiotherapistBarthelPatientResultHD
        {
            get { return context.UACT2be5e5394d2d450f82f663e1f2ad0acd; }
        }

    }
}
