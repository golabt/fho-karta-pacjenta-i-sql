﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistKarnofskyPerformanceStatusHDRepository : IPatientPhysiotherapistKarnofskyPerformanceStatusHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistKarnofskyPerformanceStatusHD> PatientPhysiotherapistKarnofskyPerformanceStatusHD
        {
            get { return context.UACT13635254c31e4f1daaeacc66706f24ca; }
        }

    }
}
