﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistVisitHSRepository : IPatientPhysiotherapistVisitHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistVisitHS> PatientPhysiotherapistVisitHS
        {
            get { return context.UACT566c61bda9b74e8291451fc5c70fea42; }
        }

    }
}
