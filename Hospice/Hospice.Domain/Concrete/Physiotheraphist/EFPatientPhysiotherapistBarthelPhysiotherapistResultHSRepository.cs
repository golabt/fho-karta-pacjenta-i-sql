﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBarthelPhysiotherapistResultHSRepository : IPatientPhysiotherapistBarthelPhysiotherapistResultHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBarthelPhysiotherapistResultHS> PatientPhysiotherapistBarthelPhysiotherapistResultHS
        {
            get { return context.UACT54adf1686f554d538a55d8b34ab3a798; }
        }

    }
}
