﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBarthelPatientHDRepository : IPatientPhysiotherapistBarthelPatientHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBarthelPatientHD> PatientPhysiotherapistBarthelPatientHD
        {
            get { return context.UACTa558bd769f244046a5753a6871c555ef; }
        }

    }
}
