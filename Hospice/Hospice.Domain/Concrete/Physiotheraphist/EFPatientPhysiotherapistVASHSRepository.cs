﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistVASHSRepository : IPatientPhysiotherapistVASHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistVASHS> PatientPhysiotherapistVASHS
        {
            get { return context.UACTc4f3c045469a4f56b9c3294e8b1e17af; }
        }

    }
}
