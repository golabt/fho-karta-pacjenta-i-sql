﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBarthelPatientHSRepository : IPatientPhysiotherapistBarthelPatientHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBarthelPatientHS> PatientPhysiotherapistBarthelPatientHS
        {
            get { return context.UACTd8de471604944469a115cfd4d5cf6532; }
        }

    }
}
