﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistBarthelPhysiotherapistResultHDRepository : IPatientPhysiotherapistBarthelPhysiotherapistResultHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistBarthelPhysiotherapistResultHD> PatientPhysiotherapistBarthelPhysiotherapistResultHD
        {
            get { return context.UACTeb6f603d20e8432f90df5e0a4ff142a2; }
        }

    }
}
