﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPhysiotherapistVASHDRepository : IPatientPhysiotherapistVASHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPhysiotherapistVASHD> PatientPhysiotherapistVASHD
        {
            get { return context.UACT0c94724cc3e44370872fe7cd4eb64991; }
        }

    }
}
