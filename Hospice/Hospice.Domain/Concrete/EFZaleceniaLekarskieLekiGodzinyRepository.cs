﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFZaleceniaLekarskieLekiGodzinyRepository : IZaleceniaLekarskieLekiGodzinyRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<ZaleceniaLekarskieLekiGodziny> ZaleceniaLekarskieLekiGodziny
        {
            get { return context.ZaleceniaLekarskieLekiGodziny; }
        }

    }
}
