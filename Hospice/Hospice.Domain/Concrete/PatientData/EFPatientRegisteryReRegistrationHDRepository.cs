﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryReRegistrationHDRepository : IPatientRegisteryReRegistrationHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryReRegistrationHD> PatientRegisteryReRegistrationHD
        {
            get { return context.UACTba571a6ad1304ff9b84c6ea83cc4af2b; }
        }

    }
}
