﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryFinalizingAdmissionHSRepository : IPatientRegisteryFinalizingAdmissionHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryFinalizingAdmissionHS> PatientRegisteryFinalizingAdmissionHS
        {
            get { return context.UACTb9985cfac1d54d90b8a894c212037f38; }
        }

    }
}
