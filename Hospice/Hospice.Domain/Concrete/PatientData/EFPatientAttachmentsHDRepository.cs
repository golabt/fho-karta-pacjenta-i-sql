﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientAttachmentsHDRepository : IPatientAttachmentsHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientAttachmentsHD> PatientAttachmentsHD
        {
            get { return context.View_PatientAttachmentsHD; }
        }

    }
}
