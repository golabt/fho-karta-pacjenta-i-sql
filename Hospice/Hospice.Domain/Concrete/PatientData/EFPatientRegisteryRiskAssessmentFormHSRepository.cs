﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryRiskAssessmentFormHSRepository : IPatientRegisteryRiskAssessmentFormHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryRiskAssessmentFormHS> PatientRegisteryRiskAssessmentFormHS
        {
            get { return context.UACT92495a4484f44e91b2292d77d21e7de5; }
        }

    }
}
