﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryRiskAssessmentFormResultHSRepository : IPatientRegisteryRiskAssessmentFormResultHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryRiskAssessmentFormResultHS> PatientRegisteryRiskAssessmentFormResultHS
        {
            get { return context.UACT78297bb21589423994d10490e7ec5f5f; }
        }

    }
}
