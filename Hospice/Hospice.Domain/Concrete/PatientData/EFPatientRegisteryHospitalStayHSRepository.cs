﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryHospitalStayHSRepository : IPatientRegisteryHospitalStayHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryHospitalStayHS> PatientRegisteryHospitalStayHS
        {
            get { return context.UACTdfa3c31754134cea9009a9798f3cb5b7; }
        }

    }
}
