﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryEpikryzaHSRepository : IPatientRegisteryEpikryzaHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryEpikryzaHS> PatientRegisteryEpikryzaHS
        {
            get { return context.UACTc151b469aa8d488e8d689133cf4441f6; }
        }

    }
}
