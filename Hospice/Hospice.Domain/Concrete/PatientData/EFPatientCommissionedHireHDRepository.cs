﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientCommissionedHireHDRepository : IPatientCommissionedHireHDRepository
    {
        private EFDbContext context = new EFDbContext(); 

        public IQueryable<PatientCommissionedHireHD> PatientCommissionedHireHD
        {
            get { return context.UACT8a08cede945440499d6f6165aa4ad17a; }
        }

    }
}
