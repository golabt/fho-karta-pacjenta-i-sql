﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryHospitalStayHDRepository : IPatientRegisteryHospitalStayHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryHospitalStayHD> PatientRegisteryHospitalStayHD
        {
            get { return context.UACTa88e52f48006475fbb5d936be3313948; }
        }

    }
}
