﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryFamilyDataHSRepository : IPatientRegisteryFamilyDataHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryFamilyDataHS> PatientRegisteryFamilyDataHS
        {
            get { return context.UACT9b34929a538c4e96a60935eded611f07; }
        }

    }
}
