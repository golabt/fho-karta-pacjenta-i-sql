﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryInsuranceHSRepository : IPatientRegisteryInsuranceHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryInsuranceHS> PatientRegisteryInsuranceHS
        {
            get { return context.UACTd44c30b0c7034b3fa0bb5cc21cae9caa; }
        }

    }
}
