﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryEpikryzaExtractsHSRepository : IPatientRegisteryEpikryzaExtractsHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryEpikryzaExtractsHS> PatientRegisteryEpikryzaExtractsHS
        {
            get { return context.UACTd715eaf74aec461489263677755158d7; }
        }

    }
}
