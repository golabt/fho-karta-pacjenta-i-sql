﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryEstablishmentOfMedicalHistoryHSRepository : IPatientRegisteryEstablishmentOfMedicalHistoryHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryEstablishmentOfMedicalHistoryHS> PatientRegisteryEstablishmentOfMedicalHistoryHS
        {
            get { return context.UACTe7134ccb2540437bb8f1756483ec0336; }
        }

    }
}
