﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryPermanentlyToHospitalHDRepository : IPatientRegisteryPermanentlyToHospitalHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryPermanentlyToHospitalHD> PatientRegisteryPermanentlyToHospitalHD
        {
            get { return context.UACT19b7f45e38b5479f9ed95287351ba992; }
        }

    }
}
