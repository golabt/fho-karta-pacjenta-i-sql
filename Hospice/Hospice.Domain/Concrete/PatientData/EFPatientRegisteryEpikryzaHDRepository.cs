﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryEpikryzaHDRepository : IPatientRegisteryEpikryzaHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryEpikryzaHD> PatientRegisteryEpikryzaHD
        {
            get { return context.UACT6b9280a6ace0410e833f431177fc8210; }
        }

    }
}
