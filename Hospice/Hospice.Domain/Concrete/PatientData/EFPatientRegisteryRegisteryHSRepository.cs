﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryRegisteryHSRepository : IPatientRegisteryRegisteryHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryRegisteryHS> PatientRegisteryRegisteryHS
        {
            get { return context.UACTb22fe135275c4253b3b5f544bb37347a; }
        }

    }
}
