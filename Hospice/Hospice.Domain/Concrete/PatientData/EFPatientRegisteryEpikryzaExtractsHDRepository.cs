﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryEpikryzaExtractsHDRepository : IPatientRegisteryEpikryzaExtractsHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryEpikryzaExtractsHD> PatientRegisteryEpikryzaExtractsHD
        {
            get { return context.UACT761851d997e145b9bef3c36a7a50a23a; }
        }

    }
}
