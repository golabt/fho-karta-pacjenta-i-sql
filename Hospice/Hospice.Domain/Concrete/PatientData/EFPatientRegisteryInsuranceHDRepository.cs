﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryInsuranceHDRepository : IPatientRegisteryInsuranceHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryInsuranceHD> PatientRegisteryInsuranceHD
        {
            get { return context.UACTc5b9f23845d34c439a8fc1572f1714d2; }
        }

    }
}
