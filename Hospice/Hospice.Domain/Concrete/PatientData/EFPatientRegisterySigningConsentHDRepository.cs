﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisterySigningConsentHDRepository : IPatientRegisterySigningConsentHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisterySigningConsentHD> PatientRegisterySigningConsentHD
        {
            get { return context.UACT8f45f0ee923c40b1a03b7d4d2f1bf7bd; }
        }

    }
}
