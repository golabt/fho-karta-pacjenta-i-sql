﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFViewCommentRepository : IViewCommentRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<ViewComment> ViewComment
        {
            get { return context.ViewComments; }
        }


        public ViewComment SaveViewComment(ViewComment viewComment)
        {


            if (viewComment.CommentId == 0)
            {
                context.ViewComments.Add(viewComment);
            }


            context.SaveChanges();

            return viewComment;
        }   

    }
}
