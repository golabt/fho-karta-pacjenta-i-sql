﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientReferralToPsychologistHSRepository : IPatientReferralToPsychologistHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientReferralToPsychologistHS> PatientReferralsToPsychologistHS
        {
            get { return context.UACT375729a022a249a09a5da1d62cd79ef8; }
        }

    }
}
