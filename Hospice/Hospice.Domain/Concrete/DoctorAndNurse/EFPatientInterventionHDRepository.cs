﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientInterventionHDRepository : IPatientInterventionHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientInterventionHD> PatientInterventionHD
        {
            get { return context.UACTe5e915af4f0f4b428562701c2c51ecba; }
        }

    }
}
