﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientFluidBalanceFluidHSRepository : IPatientFluidBalanceFluidHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientFluidBalanceFluidHS> PatientFluidBalanceFluidHS
        {
            get { return context.UACTfb6f531f7170470f91a25de70945ac67; }
        }

    }
}
