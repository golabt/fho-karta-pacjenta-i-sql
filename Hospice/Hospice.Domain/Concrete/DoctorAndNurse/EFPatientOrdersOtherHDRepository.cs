﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientOrdersOtherHDRepository : IPatientOrdersOtherHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientOrdersOtherHD> PatientOrdersOtherHD
        {
            get { return context.UACT51e5a5839ef4422995a33e7d91642d87; }
        }

    }
}
