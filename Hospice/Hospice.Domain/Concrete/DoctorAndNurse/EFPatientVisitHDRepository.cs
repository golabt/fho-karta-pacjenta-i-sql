﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientVisitHDRepository : IPatientVisitHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientVisitHD> PatientVisitHD
        {
            get { return context.UACT9ea300b75bbc4f7691459d546a02215d; }
        }

    }
}
