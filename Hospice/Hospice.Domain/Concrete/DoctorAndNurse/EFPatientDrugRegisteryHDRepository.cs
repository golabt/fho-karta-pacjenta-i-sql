﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientDrugRegisteryHDRepository : IPatientDrugRegisteryHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientDrugRegisteryHD> PatientDrugRegisteryHD
        {
            get { return context.UACTb415f0b0fe074b019bc14a132346c28e; }
        }

    }
}
