﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientOrdersMedicalHSRepository : IPatientOrdersMedicalHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientOrdersMedicalHS> PatientOrdersMedicalHS
        {
            get { return context.UACTff06667115f142dda4e10a6ae0b0e8f4; }
        }

    }
}
