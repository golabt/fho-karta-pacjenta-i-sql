﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientMedicalHistoryHSRepository: IPatientMedicalHistoryHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientMedicalHistoryHS> PatientMedicalHistoriesHS
        {
            get { return context.UACT9a0885e607604a2992f170e2c415df67; }
        }

    }
}
