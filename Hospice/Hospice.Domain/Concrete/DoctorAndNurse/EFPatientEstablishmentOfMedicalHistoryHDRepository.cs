﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientEstablishmentOfMedicalHistoryHDRepository : IPatientEstablishmentOfMedicalHistoryHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientEstablishmentOfMedicalHistoryHD> PatientEstablishmentOfMedicalHistoryHD
        {
            get { return context.UACT733ac251dd5f41fc80e7ef869cc676c3; }
        }

    }
}
