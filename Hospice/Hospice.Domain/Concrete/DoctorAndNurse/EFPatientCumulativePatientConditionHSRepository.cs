﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientCumulativePatientConditionHSRepository : IPatientCumulativePatientConditionHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientCumulativePatientConditionHS> PatientCumulativePatientConditionHS
        {
            get { return context.UACT5bf9030f4d5a4a55ba5f24f9160f091e; }
        }

    }
}
