﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientFluidBalanceHSRepository : IPatientFluidBalanceHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientFluidBalanceHS> PatientFluidBalanceHS
        {
            get { return context.UACTb6fa3fa99e234d148795e1c36873c194; }
        }

    }
}
