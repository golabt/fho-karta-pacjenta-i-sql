﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientBedsoresProtocolHSRepository : IPatientBedsoresProtocolHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientBedsoresProtocolHS> PatientBedsoresProtocolHS
        {
            get { return context.UACT9a9001e417b54beca5a519f4ef2eb08c; }
        }

    }
}
