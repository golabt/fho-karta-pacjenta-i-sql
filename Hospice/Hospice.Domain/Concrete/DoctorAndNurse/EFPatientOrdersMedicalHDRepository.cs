﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientOrdersMedicalHDRepository : IPatientOrdersMedicalHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientOrdersMedicalHD> PatientsOrdersMedicalHD
        {
            get { return context.UACT0d4c209a23ab40788016b7ace5b5b7f5; }
        }

    }
}
