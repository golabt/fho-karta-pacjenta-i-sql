﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientOrdersForNursesDressingHSRepository : IPatientOrdersForNursesDressingHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientOrdersForNursesDressingHS> PatientOrdersForNursesDressingHS
        {
            get { return context.UACT78f89301f60c46da8485465610a5ef9b; }
        }

    }
}
