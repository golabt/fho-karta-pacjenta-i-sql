﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientOrdersForNursesDrugHSRepository : IPatientOrdersForNursesDrugHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientOrdersForNursesDrugHS> PatientOrdersForNursesDrugHS
        {
            get { return context.UACTac8f3072d9ae4d13834c7bdae21b099a; }
        }

    }
}
