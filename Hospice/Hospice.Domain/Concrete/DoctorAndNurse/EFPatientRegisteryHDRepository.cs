﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientRegisteryHDRepository : IPatientRegisteryHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientRegisteryHD> PatientRegisteryHD
        {
            get { return context.UACTb0754dc342914290a483a53eddf8c8b6; }
        }

    }
}
