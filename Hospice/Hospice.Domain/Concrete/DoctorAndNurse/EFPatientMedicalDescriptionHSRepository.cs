﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientMedicalDescriptionHSRepository : IPatientMedicalDescriptionHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientMedicalDescriptionHS> PatientMedicalDescriptionHS
        {
            get { return context.UACT97577b8be1d14aa18e6a71007927ca1c; }
        }

    }
}
