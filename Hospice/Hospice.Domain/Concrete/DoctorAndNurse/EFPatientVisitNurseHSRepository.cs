﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientVisitNurseHSRepository : IPatientVisitNurseHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientVisitNurseHS> PatientVisitNurseHS
        {
            get { return context.UACT65a5c2ec1bba4d6f843944b8e404e300; }
        }

    }
}
