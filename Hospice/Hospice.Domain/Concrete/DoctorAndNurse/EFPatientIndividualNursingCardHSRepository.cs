﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientIndividualNursingCardHSRepository : IPatientIndividualNursingCardHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientIndividualNursingCardHS> PatientIndividualNursingCardHS
        {
            get { return context.UACT3ee279dab0e8423cb5ec1413b431c0d6; }
        }

    }
}
