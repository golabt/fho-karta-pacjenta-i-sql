﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientFullExaminationHDRepository : IPatientFullExaminationHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientFullExaminationHD> PatientFullExaminationHD
        {
            get { return context.UACT4c0de6fa33fd49319a98c95544674719; }
        }

    }
}
