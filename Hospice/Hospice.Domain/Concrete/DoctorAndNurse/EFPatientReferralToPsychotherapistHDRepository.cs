﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientReferralToPsychotherapistHDRepository : IPatientReferralToPsychotherapistHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientReferralToPsychotherapistHD> PatientReferralToPsychotherapistHD
        {
            get { return context.UACTf2e8c2282acd41be959fd58a4a4b7e73; }
        }

    }
}
