﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientOrdersForNursesTreatmentHSRepository : IPatientOrdersForNursesTreatmentHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientOrdersForNursesTreatmentHS> PatientOrdersForNursesTreatmentHS
        {
            get { return context.UACTdbfc872e7ef64d3b9ef58417cd289591; }
        }

    }
}
