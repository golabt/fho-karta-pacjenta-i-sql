﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientOrdersForNursesHDRepository : IPatientOrdersForNursesHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientOrdersForNursesHD> PatientOrdersForNursesHD
        {
            get { return context.UACTe92b8f0a598245929aff74666be0acbf; }
        }

    }
}
