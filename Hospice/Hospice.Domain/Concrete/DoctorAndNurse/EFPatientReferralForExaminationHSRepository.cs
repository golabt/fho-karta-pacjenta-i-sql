﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientReferralForExaminationHSRepository : IPatientReferralForExaminationHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientReferralForExaminationHS> PatientReferralForExaminationHS
        {
            get { return context.UACT39bb7eed6da043be94587b2bf86c5028; }
        }

    }
}
