﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientReferralToPsychotherapistHSRepository : IPatientReferralToPsychotherapistHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientReferralToPsychotherapistHS> PatientReferralToPsychotherapistHS
        {
            get { return context.UACT54ee5d6ac1524bc684589d3d61b260bf; }
        }

    }
}
