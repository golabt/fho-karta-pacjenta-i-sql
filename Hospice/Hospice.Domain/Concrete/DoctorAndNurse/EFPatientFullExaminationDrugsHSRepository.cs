﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientFullExaminationDrugsHSRepository : IPatientFullExaminationDrugsHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientFullExaminationDrugsHS> PatientFullExaminationDrugsHS
        {
            get { return context.UACT29408faab9e34d21aa23a615a5dbe06b; }
        }

    }
}
