﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientFirstVisitHDRepository : IPatientFirstVisitHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientFirstVisitHD> PatientFirstVisitHD
        {
            get { return context.UACT2c9cc6b4b5a64cfaa38c363fee0d5fad; }
        }

    }
}
