﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientReferralForExaminationHDRepository : IPatientReferralForExaminationHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientReferralForExaminationHD> PatientReferralForExaminationHD
        {
            get { return context.UACT5f1a16db687f4105a47306243fb81312; }
        }

    }
}
