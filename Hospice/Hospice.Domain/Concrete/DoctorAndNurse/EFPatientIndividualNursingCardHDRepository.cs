﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientIndividualNursingCardHDRepository : IPatientIndividualNursingCardHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientIndividualNursingCardHD> PatientIndividualNursingCardHD
        {
            get { return context.UACT2259bc2125f74b7aae99fad72a7914f7; }
        }

    }
}
