﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientOrdersForNursesHSRepository : IPatientOrdersForNursesHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientOrdersForNursesHS> PatientOrdersForNursesHS
        {
            get { return context.UACT447f42706de54e6e8e7db1b9a4f2c372; }
        }

    }
}
