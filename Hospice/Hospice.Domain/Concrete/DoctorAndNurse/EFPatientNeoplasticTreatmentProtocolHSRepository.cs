﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientNeoplasticTreatmentProtocolHSRepository : IPatientNeoplasticTreatmentProtocolHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientNeoplasticTreatmentProtocolHS> PatientNeoplasticTreatmentProtocolHS
        {
            get { return context.UACT6d0c0f13fb3c4fcaac44c1367caebe9c; }
        }

    }
}
