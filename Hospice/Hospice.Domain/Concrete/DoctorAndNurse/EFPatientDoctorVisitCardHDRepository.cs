﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientDoctorVisitCardHDRepository : IPatientDoctorVisitCardHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientDoctorVisitCardHD> PatientDoctorVisitCardHD
        {
            get { return context.UACT277f8de77e0746588b30e796b0d155ee; }
        }

    }
}
