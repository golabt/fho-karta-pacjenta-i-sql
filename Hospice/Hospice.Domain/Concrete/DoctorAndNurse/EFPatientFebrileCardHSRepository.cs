﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientFebrileCardHSRepository : IPatientFebrileCardHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientFebrileCardHS> PatientFebrileCardHS
        {
            get { return context.UACT8cd2e113d2e9403aa0dd562fed57a20e; }
        }

    }
}
