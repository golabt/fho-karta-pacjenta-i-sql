﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientReferralToPsychologistHDRepository : IPatientReferralToPsychologistHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientReferralToPsychologistHD> PatientReferralsToPsychologistHD
        {
            get { return context.UACTc5cf62dfe55143fa87e057bb23e856f7; }
        }

    }
}
