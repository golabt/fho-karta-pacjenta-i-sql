﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientFullExaminationHSRepository: IPatientFullExaminationHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientFullExaminationHS> PatientFullExaminationsHS
        {
            get { return context.UACT310fab7627d4493fba7fc808ab864c3e; }
        }

    }
}
