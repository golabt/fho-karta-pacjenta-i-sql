﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientMedicalVisitHSRepository: IPatientMedicalVisitHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientMedicalVisitHS> PatientMedicalVisitsHS
        {
            get { return context.UACT66ec3f2fb5ca4519afd801631b68b583; }
        }

    }
}
