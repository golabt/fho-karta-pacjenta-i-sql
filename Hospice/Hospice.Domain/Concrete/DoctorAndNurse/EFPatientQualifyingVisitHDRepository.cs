﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientQualifyingVisitHDRepository : IPatientQualifyingVisitHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientQualifyingVisitHD> PatientQualifyingVisitHD
        {
            get { return context.UACTcd00270d03ff4ecc92cfccd36e7f1b25; }
        }

    }
}
