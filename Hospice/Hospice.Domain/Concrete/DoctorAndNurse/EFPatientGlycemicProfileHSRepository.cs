﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientGlycemicProfileHSRepository : IPatientGlycemicProfileHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientGlycemicProfileHS> PatientGlycemicProfileHS
        {
            get { return context.UACT829419f8d5bb4562a2abe2b92b9744dc; }
        }

    }
}
