﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientHSRepository: IPatientHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientHS> PatientsHS
        {
            get { return context.UACT2c02d32cb7a04ce3a9a07923a9cf8d14; }
        }

    }
}
