﻿using Hospice.Domain.Entities;
using System.Data.Entity;


namespace Hospice.Domain.Concrete
{

    public class EFDbContext : DbContext
    {

        public EFDbContext()
            : base(DbConnectionSettings.GetDefaultConnectionStringName())
        {
            Database.SetInitializer<DbContext>(null);
        }
        #region all
        public DbSet<TblInstanceWorkflows> TblInstanceWorkflows { get; set; }
        public DbSet<PatientHS> UACT2c02d32cb7a04ce3a9a07923a9cf8d14 { get; set; }
        public DbSet<PatientHD> UACTf0f18fd8365e480cbbac90d0526c2f6b { get; set; }
        public DbSet<PatientWfDoctorDiscriminatorHD> UACTc1ee29a5ebde42d78c0528c7c4a460eb { get; set; }
        public DbSet<PatientWfPsychologistDiscriminatorHS> UACT391dfe53eef14e91a16d6e1f9103bd45 { get; set; }
        public DbSet<PatientWfPsychologistDiscriminatorHD> UACT0b2212bedfc94a049c8781a6a3f9975b { get; set; }
        public DbSet<PatientMedicalHistoryHS> UACT9a0885e607604a2992f170e2c415df67 { get; set; }
        public DbSet<PatientFullExaminationHS> UACT310fab7627d4493fba7fc808ab864c3e { get; set; }
        public DbSet<PatientFullExaminationDrugsHS> UACT29408faab9e34d21aa23a615a5dbe06b { get; set; }
        public DbSet<PatientMedicalVisitHS> UACT66ec3f2fb5ca4519afd801631b68b583 { get; set; }
        public DbSet<PatientOrdersForNursesHS> UACT447f42706de54e6e8e7db1b9a4f2c372 { get; set; }
        public DbSet<PatientOrdersForNursesDrugHS> UACTac8f3072d9ae4d13834c7bdae21b099a { get; set; }
        public DbSet<PatientOrdersForNursesTreatmentHS> UACTdbfc872e7ef64d3b9ef58417cd289591 { get; set; }
        public DbSet<PatientOrdersForNursesDressingHS> UACT78f89301f60c46da8485465610a5ef9b { get; set; }
        public DbSet<ZaleceniaLekarskieLekiGodziny> ZaleceniaLekarskieLekiGodziny { get; set; }
        public DbSet<ZaleceniaLekarskieLekiDoPodaniaUSP> ZaleceniaLekarskieLekiDoPodaniaUSP { get; set; }
        public DbSet<ZaleceniaLekarskieOpatrunkiDoWykonaniaUSP> ZaleceniaLekarskieOpatrunkiDoWykonaniaUSP { get; set; }
        public DbSet<ZaleceniaLekarskieZabiegiDoWykonaniaUSP> ZaleceniaLekarskieZabiegiDoWykonaniaUSP { get; set; }
        public DbSet<PatientReferralToPsychologistHS> UACT375729a022a249a09a5da1d62cd79ef8 { get; set; }
        public DbSet<PatientReferralToPsychotherapistHS> UACT54ee5d6ac1524bc684589d3d61b260bf { get; set; }
        public DbSet<PatientReferralForExaminationHS> UACT39bb7eed6da043be94587b2bf86c5028 { get; set; }
        public DbSet<PatientMedicalDescriptionHS> UACT97577b8be1d14aa18e6a71007927ca1c { get; set; }
        public DbSet<PatientFirstVisitHD> UACT2c9cc6b4b5a64cfaa38c363fee0d5fad { get; set; }
        public DbSet<PatientVisitHD> UACT9ea300b75bbc4f7691459d546a02215d { get; set; }
        public DbSet<PatientInterventionHD> UACTe5e915af4f0f4b428562701c2c51ecba { get; set; }
        public DbSet<PatientFullExaminationHD> UACT4c0de6fa33fd49319a98c95544674719 { get; set; }
        public DbSet<PatientOrdersForNursesHD> UACTe92b8f0a598245929aff74666be0acbf { get; set; }
        public DbSet<PatientReferralToPsychologistHD> UACTc5cf62dfe55143fa87e057bb23e856f7 { get; set; }
        public DbSet<PatientReferralToPsychotherapistHD> UACTf2e8c2282acd41be959fd58a4a4b7e73 { get; set; }
        public DbSet<PatientOrdersOtherHD> UACT51e5a5839ef4422995a33e7d91642d87 { get; set; }
        public DbSet<PatientReferralForExaminationHD> UACT5f1a16db687f4105a47306243fb81312 { get; set; }
        public DbSet<PatientDrugRegisteryHD> UACTb415f0b0fe074b019bc14a132346c28e { get; set; }
        public DbSet<PatientDoctorVisitCardHD> UACT277f8de77e0746588b30e796b0d155ee { get; set; }
        public DbSet<PatientRegisteryHD> UACTb0754dc342914290a483a53eddf8c8b6 { get; set; }
        public DbSet<PatientQualifyingVisitHD> UACTcd00270d03ff4ecc92cfccd36e7f1b25 { get; set; }
        public DbSet<PatientEstablishmentOfMedicalHistoryHD> UACT733ac251dd5f41fc80e7ef869cc676c3 { get; set; }
        public DbSet<PatientVisitNurseHS> UACT65a5c2ec1bba4d6f843944b8e404e300 { get; set; }
        //public DbSet<PatientRegisteryHS> UACTb22fe135275c4253b3b5f544bb37347a { get; set; }
        public DbSet<PatientFluidBalanceHS> UACTb6fa3fa99e234d148795e1c36873c194 { get; set; }
        public DbSet<PatientFluidBalanceFluidHS> UACTfb6f531f7170470f91a25de70945ac67 { get; set; }
        public DbSet<PatientGlycemicProfileHS> UACT829419f8d5bb4562a2abe2b92b9744dc { get; set; }
        public DbSet<PatientCumulativePatientConditionHS> UACT5bf9030f4d5a4a55ba5f24f9160f091e { get; set; }
        public DbSet<PatientOrdersMedicalHS> UACTff06667115f142dda4e10a6ae0b0e8f4 { get; set; }
        public DbSet<PatientIndividualNursingCardHS> UACT3ee279dab0e8423cb5ec1413b431c0d6 { get; set; }
        public DbSet<PatientFebrileCardHS> UACT8cd2e113d2e9403aa0dd562fed57a20e { get; set; }
        public DbSet<PatientNeoplasticTreatmentProtocolHS> UACT6d0c0f13fb3c4fcaac44c1367caebe9c { get; set; }
        public DbSet<PatientBedsoresProtocolHS> UACT9a9001e417b54beca5a519f4ef2eb08c { get; set; }
        public DbSet<PatientOrdersMedicalHD> UACT0d4c209a23ab40788016b7ace5b5b7f5 { get; set; }
        public DbSet<PatientIndividualNursingCardHD> UACT2259bc2125f74b7aae99fad72a7914f7 { get; set; }
        public DbSet<PatientPsychologistVisitHS> UACTf172990cc31547789aa858de04693a38 { get; set; }
        public DbSet<PatientPsychologistDiseaseAcceptanceScaleHS> UACTe78866b3d79d4ac0a0f81090b7d7dd17 { get; set; }
        public DbSet<PatientPsychologistQuestionnaireHADSMHS> UACT71c3591aa2aa44a3980b1f735171c5c8 { get; set; }
        public DbSet<PatientPsychologistQuestionnaireHADSMResultHS> UACTdb5ef3e94f684d62a3ab4eba43d643c1 { get; set; }
        public DbSet<PatientPsychologistGeriatricEvaluationScaleHS> UACTe31388ca5c7f4510bf69e43af3cedc5d { get; set; }
        public DbSet<PatientPsychologistGeriatricEvaluationScaleResultHS> UACT75594210bc164f279cdc247fccd2d9f8 { get; set; }
        public DbSet<PatientPsychologistMMSETestHS> UACTeb005a929f244fd5bb7a9c40788cc1e6 { get; set; }
        public DbSet<PatientPsychologistMMSETestResultHS> UACTaf56823c8df14590a806674bd7f36116 { get; set; }
        public DbSet<PatientPsychologistVisitHD> UACTf2d9417c5d8b4a41a6a5b29764927501 { get; set; }
        public DbSet<PatientPsychologistDiseaseAcceptanceScaleHD> UACTc62d268b6aef4d819fb614abcfef2c07 { get; set; }
        public DbSet<PatientPsychologistQuestionnaireHADSMHD> UACT2a7c3e7c56b943be86d9dff74e159433 { get; set; }
        public DbSet<PatientPsychologistQuestionnaireHADSMResultHD> UACT3cb41e9013d1426f9edc496dfb9fd43b { get; set; }
        public DbSet<PatientPsychologistGeriatricEvaluationScaleHD> UACTf28343c0cd9e4e74a4fca212755b28cc { get; set; }
        public DbSet<PatientPsychologistGeriatricEvaluationScaleResultHD> UACTd42461f80aed4557b007b3a3881ef91e { get; set; }
        public DbSet<PatientPsychologistMMSETestHD> UACT6ef6aed69eb445579a44392764b493f5 { get; set; }
        public DbSet<PatientPsychologistMMSETestResultHD> UACT3068922b849c4422b5db36644b04386d { get; set; }
        public DbSet<ViewComment> ViewComments { get; set; }
        public DbSet<InfusionInfo> tblInfusionInfo { get; set; }
        public DbSet<MedicalDevicesLog> tblMedicalDevicesLog { get; set; }
        public DbSet<Centralka> Centralka { get; set; }
        public DbSet<Pompy> Pompy { get; set; }
        public DbSet<HireMedicalDevice> UACTdc2be552a6774c8a9c83e9b9a2fd5fa6 { get; set; }
        public DbSet<ReturnMedicalDevice> UACTc523fab4575246c2b8453b90fe2b3ed9 { get; set; }
        public DbSet<DeviceRental> DeviceRental { get; set; }
        public DbSet<PatientAttachmentsHS> View_PatientAttachmentsHS { get; set; }
        public DbSet<PatientAttachmentsHD> View_PatientAttachmentsHD { get; set; }
        public DbSet<PatientCommissionedHireHD> UACT8a08cede945440499d6f6165aa4ad17a { get; set; }

#endregion 

        #region physiotherapist
        public DbSet<PatientWfPhysiotherapistDiscriminatorHS> UACT90fb171b663d460e80858bd4477d19f3 { get; set; }
        public DbSet<PatientPhysiotherapistFirstVisitHS> UACT351b31f2da54445a9c7705fa00633b63 { get; set; }
        public DbSet<PatientPhysiotherapistPatientCardHS> UACTf901a57807974712ae4fde85233c8a19 { get; set; }
        public DbSet<PatientPhysiotherapistVisitHS> UACT566c61bda9b74e8291451fc5c70fea42 { get; set; }
        public DbSet<PatientPhysiotherapistBorgScaleHS> UACTc2956493e9bc496b9cdc6bc2dd54c07f { get; set; }
        public DbSet<PatientPhysiotherapistKarnofskyPerformanceStatusHS> UACT78d5ab4cc7a246fa8742d49791866fcc { get; set; }
        public DbSet<PatientPhysiotherapistBFIIndexHS> UACT991b8df4773c4ff19af697f6b595f9aa { get; set; }
        public DbSet<PatientPhysiotherapistRSCLHS> UACT51b87692e2db49889591fdea6ba92868 { get; set; }
        public DbSet<PatientPhysiotherapistVASHS> UACTc4f3c045469a4f56b9c3294e8b1e17af { get; set; }
        public DbSet<PatientPhysiotherapistBarthelPatientHS> UACTd8de471604944469a115cfd4d5cf6532 { get; set; }
        public DbSet<PatientPhysiotherapistBarthelPatientResultHS> UACTabb0749e67ad44f0988687f143c1d0b3 { get; set; }
        public DbSet<PatientPhysiotherapistBarthelPhysiotherapistHS> UACTfc2395aaa8bf4fd4b9e492093df0b684 { get; set; }
        public DbSet<PatientPhysiotherapistBarthelPhysiotherapistResultHS> UACT54adf1686f554d538a55d8b34ab3a798 { get; set; }

        public DbSet<PatientWfPhysiotherapistDiscriminatorHD> UACT51b9bf0dc09e4810abb8d9aae3692809 { get; set; }
        public DbSet<PatientPhysiotherapistFirstVisitHD> UACT7a8413a760154c6a899ddb68fb9c57f0 { get; set; }
        public DbSet<PatientPhysiotherapistPatientCardHD> UACT776d3eae9b8348f5886c655fe699c19c { get; set; }
        public DbSet<PatientPhysiotherapistVisitHD> UACTc0d948ccb10549bc9563d87f49139e0f { get; set; }
        public DbSet<PatientPhysiotherapistBorgScaleHD> UACTbe03dd3ad74d415a8cdb313f76e5ba9a { get; set; }
        public DbSet<PatientPhysiotherapistKarnofskyPerformanceStatusHD> UACT13635254c31e4f1daaeacc66706f24ca { get; set; }
        public DbSet<PatientPhysiotherapistBFIIndexHD> UACTe58116fa66734723855b2f7a68db2d24 { get; set; }
        public DbSet<PatientPhysiotherapistRSCLHD> UACT83bf3cdc7b2d45918b0efb708c8577f3 { get; set; }
        public DbSet<PatientPhysiotherapistVASHD> UACT0c94724cc3e44370872fe7cd4eb64991 { get; set; }
        public DbSet<PatientPhysiotherapistBarthelPatientHD> UACTa558bd769f244046a5753a6871c555ef { get; set; }
        public DbSet<PatientPhysiotherapistBarthelPatientResultHD> UACT2be5e5394d2d450f82f663e1f2ad0acd { get; set; }
        public DbSet<PatientPhysiotherapistBarthelPhysiotherapistHD> UACTb822bfb0345f4820aa8e93a870604401 { get; set; }
        public DbSet<PatientPhysiotherapistBarthelPhysiotherapistResultHD> UACTeb6f603d20e8432f90df5e0a4ff142a2 { get; set; }

        #endregion

        public DbSet<PatientRegisteryRegisteryHS> UACTb22fe135275c4253b3b5f544bb37347a { get; set; }
        public DbSet<PatientRegisteryInsuranceHS> UACTd44c30b0c7034b3fa0bb5cc21cae9caa { get; set; }
        public DbSet<PatientRegisteryEstablishmentOfMedicalHistoryHS> UACTe7134ccb2540437bb8f1756483ec0336 { get; set; }
        public DbSet<PatientRegisteryFamilyDataHS> UACT9b34929a538c4e96a60935eded611f07 { get; set; }
        public DbSet<PatientRegisteryFinalizingAdmissionHS> UACTb9985cfac1d54d90b8a894c212037f38 { get; set; }
        public DbSet<PatientRegisteryRiskAssessmentFormHS> UACT92495a4484f44e91b2292d77d21e7de5 { get; set; }
        public DbSet<PatientRegisteryRiskAssessmentFormResultHS> UACT78297bb21589423994d10490e7ec5f5f { get; set; }
        public DbSet<PatientRegisteryEpikryzaExtractsHS> UACTd715eaf74aec461489263677755158d7 { get; set; }
        public DbSet<PatientRegisteryEpikryzaHS> UACTc151b469aa8d488e8d689133cf4441f6 { get; set; }
        public DbSet<PatientRegisteryHospitalStayHS> UACTdfa3c31754134cea9009a9798f3cb5b7 { get; set; }
        public DbSet<PatientRegisteryReRegistrationHD> UACTba571a6ad1304ff9b84c6ea83cc4af2b { get; set; }
        public DbSet<PatientRegisteryInsuranceHD> UACTc5b9f23845d34c439a8fc1572f1714d2 { get; set; }
        public DbSet<PatientRegisterySigningConsentHD> UACT8f45f0ee923c40b1a03b7d4d2f1bf7bd { get; set; }
        public DbSet<PatientRegisteryEpikryzaHD> UACT6b9280a6ace0410e833f431177fc8210 { get; set; }
        public DbSet<PatientRegisteryEpikryzaExtractsHD> UACT761851d997e145b9bef3c36a7a50a23a { get; set; }
        public DbSet<PatientRegisteryPermanentlyToHospitalHD> UACT19b7f45e38b5479f9ed95287351ba992 { get; set; }
        public DbSet<PatientRegisteryHospitalStayHD> UACTa88e52f48006475fbb5d936be3313948 { get; set; }
    }

   
}
