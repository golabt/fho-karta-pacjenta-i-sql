﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFReturnMedicalDeviceRepository : IReturnMedicalDeviceRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<ReturnMedicalDevice> ReturnMedicalDevice
        {
            get { return context.UACTc523fab4575246c2b8453b90fe2b3ed9; }
        }

    }
}
