﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFHireMedicalDeviceRepository : IHireMedicalDeviceRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<HireMedicalDevice> HireMedicalDevice
        {
            get { return context.UACTdc2be552a6774c8a9c83e9b9a2fd5fa6; }
        }

    }
}
