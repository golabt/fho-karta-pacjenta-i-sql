﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFMedicalDevicesLogRepository : IMedicalDevicesLogRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<MedicalDevicesLog> MedicalDevicesLog
        {
            get { return context.tblMedicalDevicesLog; }
        }

        public void UpdateMedicalDevicesLog(int? Id)
        {
            MedicalDevicesLog MedicalDevicesLog = context.tblMedicalDevicesLog.FirstOrDefault(p => p.MedicalDevicesLogId == Id);

            MedicalDevicesLog.Visited = true;

            context.SaveChanges();
        }   

    }
}
