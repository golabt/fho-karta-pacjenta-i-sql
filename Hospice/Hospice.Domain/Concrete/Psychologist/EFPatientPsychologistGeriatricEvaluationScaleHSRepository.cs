﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistGeriatricEvaluationScaleHSRepository : IPatientPsychologistGeriatricEvaluationScaleHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistGeriatricEvaluationScaleHS> PatientPsychologistGeriatricEvaluationScaleHS
        {
            get { return context.UACTe31388ca5c7f4510bf69e43af3cedc5d; }
        }

    }
}
