﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistMMSETestResultHSRepository : IPatientPsychologistMMSETestResultHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistMMSETestResultHS> PatientPsychologistMMSETestResultHS
        {
            get { return context.UACTaf56823c8df14590a806674bd7f36116; }
        }

    }
}
