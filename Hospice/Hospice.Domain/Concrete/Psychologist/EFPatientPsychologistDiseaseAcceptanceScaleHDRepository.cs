﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistDiseaseAcceptanceScaleHDRepository : IPatientPsychologistDiseaseAcceptanceScaleHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistDiseaseAcceptanceScaleHD> PatientPsychologistDiseaseAcceptanceScaleHD
        {
            get { return context.UACTc62d268b6aef4d819fb614abcfef2c07; }
        }

    }
}
