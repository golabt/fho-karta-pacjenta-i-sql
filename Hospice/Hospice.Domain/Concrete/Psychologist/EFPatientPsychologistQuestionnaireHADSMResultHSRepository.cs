﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistQuestionnaireHADSMResultHSRepository : IPatientPsychologistQuestionnaireHADSMResultHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistQuestionnaireHADSMResultHS> PatientPsychologistQuestionnaireHADSMResultHS
        {
            get { return context.UACTdb5ef3e94f684d62a3ab4eba43d643c1; }
        }

    }
}
