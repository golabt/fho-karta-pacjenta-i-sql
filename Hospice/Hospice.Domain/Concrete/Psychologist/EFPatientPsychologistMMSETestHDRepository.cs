﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistMMSETestHDRepository : IPatientPsychologistMMSETestHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistMMSETestHD> PatientPsychologistMMSETestHD
        {
            get { return context.UACT6ef6aed69eb445579a44392764b493f5; }
        }

    }
}
