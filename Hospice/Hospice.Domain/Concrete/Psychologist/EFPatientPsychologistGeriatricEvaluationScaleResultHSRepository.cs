﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistGeriatricEvaluationScaleResultHSRepository : IPatientPsychologistGeriatricEvaluationScaleResultHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistGeriatricEvaluationScaleResultHS> PatientPsychologistGeriatricEvaluationScaleResultHS
        {
            get { return context.UACT75594210bc164f279cdc247fccd2d9f8; }
        }

    }
}
