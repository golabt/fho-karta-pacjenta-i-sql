﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistGeriatricEvaluationScaleResultHDRepository : IPatientPsychologistGeriatricEvaluationScaleResultHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistGeriatricEvaluationScaleResultHD> PatientPsychologistGeriatricEvaluationScaleResultHD
        {
            get { return context.UACTd42461f80aed4557b007b3a3881ef91e; }
        }

    }
}
