﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistDiseaseAcceptanceScaleHSRepository : IPatientPsychologistDiseaseAcceptanceScaleHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistDiseaseAcceptanceScaleHS> PatientPsychologistDiseaseAcceptanceScaleHS
        {
            get { return context.UACTe78866b3d79d4ac0a0f81090b7d7dd17; }
        }

    }
}
