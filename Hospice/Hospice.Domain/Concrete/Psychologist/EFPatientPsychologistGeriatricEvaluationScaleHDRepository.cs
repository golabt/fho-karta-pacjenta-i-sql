﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistGeriatricEvaluationScaleHDRepository : IPatientPsychologistGeriatricEvaluationScaleHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistGeriatricEvaluationScaleHD> PatientPsychologistGeriatricEvaluationScaleHD
        {
            get { return context.UACTf28343c0cd9e4e74a4fca212755b28cc; }
        }

    }
}
