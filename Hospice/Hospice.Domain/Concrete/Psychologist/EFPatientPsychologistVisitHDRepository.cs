﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistVisitHDRepository : IPatientPsychologistVisitHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistVisitHD> PatientPsychologistVisitHD
        {
            get { return context.UACTf2d9417c5d8b4a41a6a5b29764927501; }
        }

    }
}
