﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistQuestionnaireHADSMHDRepository : IPatientPsychologistQuestionnaireHADSMHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistQuestionnaireHADSMHD> PatientPsychologistQuestionnaireHADSMHD
        {
            get { return context.UACT2a7c3e7c56b943be86d9dff74e159433; }
        }

    }
}
