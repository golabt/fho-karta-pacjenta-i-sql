﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistQuestionnaireHADSMResultHDRepository : IPatientPsychologistQuestionnaireHADSMResultHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistQuestionnaireHADSMResultHD> PatientPsychologistQuestionnaireHADSMResultHD
        {
            get { return context.UACT3cb41e9013d1426f9edc496dfb9fd43b; }
        }

    }
}
