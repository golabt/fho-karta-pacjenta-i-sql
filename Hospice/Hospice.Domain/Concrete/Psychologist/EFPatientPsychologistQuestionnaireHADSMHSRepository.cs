﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistQuestionnaireHADSMHSRepository : IPatientPsychologistQuestionnaireHADSMHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistQuestionnaireHADSMHS> PatientPsychologistQuestionnaireHADSMHS
        {
            get { return context.UACT71c3591aa2aa44a3980b1f735171c5c8; }
        }

    }
}
