﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistMMSETestResultHDRepository : IPatientPsychologistMMSETestResultHDRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistMMSETestResultHD> PatientPsychologistMMSETestResultHD
        {
            get { return context.UACT3068922b849c4422b5db36644b04386d; }
        }

    }
}
