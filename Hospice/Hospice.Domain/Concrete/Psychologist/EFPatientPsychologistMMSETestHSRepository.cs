﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistMMSETestHSRepository : IPatientPsychologistMMSETestHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistMMSETestHS> PatientPsychologistMMSETestHS
        {
            get { return context.UACTeb005a929f244fd5bb7a9c40788cc1e6; }
        }

    }
}
