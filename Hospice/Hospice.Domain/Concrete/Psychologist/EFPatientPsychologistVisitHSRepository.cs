﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Concrete
{
    public class EFPatientPsychologistVisitHSRepository : IPatientPsychologistVisitHSRepository
    {
        private EFDbContext context = new EFDbContext();

        public IQueryable<PatientPsychologistVisitHS> PatientPsychologistVisitHS
        {
            get { return context.UACTf172990cc31547789aa858de04693a38; }
        }

    }
}
