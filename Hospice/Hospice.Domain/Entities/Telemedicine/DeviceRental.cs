﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("DeviceRental")]
    public class DeviceRental
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int DeviceRentalId { get; set; }        
        public string DeviceId { get; set; }                
        public string PatientPESEL { get; set; }              
        public DateTime? RentedSince { get; set; }              
        public DateTime? RentedUntil { get; set; }              
        public int? fldIWfId { get; set; }

    }
}


