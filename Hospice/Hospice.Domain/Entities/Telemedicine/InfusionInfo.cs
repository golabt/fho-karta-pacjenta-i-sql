﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("tblInfusionInfo")]
    public class InfusionInfo
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int InfusionInfoId { get; set; }
        public string DeviceId { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? Dilution { get; set; }
        public string DilutionUnit { get; set; }
        public string DrugName { get; set; }
        public int? FlowRate { get; set; }
        public string FlowRateUnit { get; set; }
        public int? LeftVolume { get; set; }
        public Int16? Pressure { get; set; }
        public string SyringName { get; set; }
        public byte? SyringSize { get; set; }
        public int? UserMass { get; set; }
        public string UserMassUnit { get; set; }
        public int? UserVolume { get; set; }
        public int? UserVolumePri { get; set; }
        public int? UserVolumeSec { get; set; }
        public Int16? VolDilution { get; set; }
        public int? Volume { get; set; }
        public string PatientId { get; set; }
        public bool? Visited { get; set; }
    }
}


