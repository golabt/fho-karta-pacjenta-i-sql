﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("tblMedicalDevicesLog")]
    public class MedicalDevicesLog
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int MedicalDevicesLogId { get; set; }
        public DateTime? dateTime { get; set; }
        public string mesureDeviceModel { get; set; }
        public string mesureDeviceSN { get; set; }
        public string deviceID { get; set; }
        public string patientID { get; set; }
        public int? spo2Val { get; set; }
        public int? heartRateVal { get; set; }
        public int? weighVal { get; set; }
        public int? systolicVal { get; set; }
        public int? diastolicVal { get; set; }
        public int? sugarValue { get; set; }
        public string ecgJson { get; set; }
        public int? measureType { get; set; }
        public bool? Visited { get; set; }


    }
}


