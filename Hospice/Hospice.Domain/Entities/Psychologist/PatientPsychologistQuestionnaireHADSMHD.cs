﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT2a7c3e7c56b943be86d9dff74e159433")]
    public class PatientPsychologistQuestionnaireHADSMHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "<strong>Kwestionariusz HADS-M wraz z wynikiem</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "Czułem(-am) się napięty(-a) lub podenerwowany(-a)")]
        public int? RadioButtonList1 { get; set; }

        [Display(Name = "Wciąż cieszą mnie rzeczy, które zwykle sprawiały mi radość")]
        public int? RadioButtonList2 { get; set; }

        [Display(Name = "Odczuwałem(-am) przerażające uczucie, jakby miało zdarzyć się coś okropnego")]
        public int? RadioButtonList3 { get; set; }

        [Display(Name = "Potrafię się śmiać i dostrzegać zabawną stronę zdarzeń")]
        public int? RadioButtonList4 { get; set; }

        [Display(Name = "Nachodzą mnie smutne myśli")]
        public int? RadioButtonList5 { get; set; }

        [Display(Name = "Czuję się wesoły(-a) i pogodny(-a)")]
        public int? RadioButtonList6 { get; set; }

        [Display(Name = "Mogę siedzieć spokojnie i czuć się zrelaksowany(-a)")]
        public int? RadioButtonList7 { get; set; }

        [Display(Name = "Czuję się jakbym był(-a) w „psychicznym dołku”")]
        public int? RadioButtonList8 { get; set; }

        [Display(Name = "Mam zatrważające uczucie, jakby mi się coś trzęsło w środku")]
        public int? RadioButtonList9 { get; set; }

        [Display(Name = "Przestałem(-am) interesować się swoim wyglądem zewnętrznym")]
        public int? RadioButtonList10 { get; set; }

        [Display(Name = "Nie mogę spokojne usiedzieć na miejscu")]
        public int? RadioButtonList11 { get; set; }

        [Display(Name = "Oczekuję z radością na różne sprawy")]
        public int? RadioButtonList12 { get; set; }

        [Display(Name = "Miewam nagłe uczucie panicznego lęku")]
        public int? RadioButtonList13 { get; set; }

        [Display(Name = "Mogę cieszyć się dobrą książką, programem TV")]
        public int? RadioButtonList14 { get; set; }

        [Display(Name = "Zdarzyło się, że w ciągu ostatniego tygodnia wybuchłem(-am) gniewem")]
        public int? RadioButtonList15 { get; set; }

        [Display(Name = "Zdarzało się, że denerwowałem(-am) się i złościłem")]
        public int? RadioButtonList16 { get; set; }
    }
}


