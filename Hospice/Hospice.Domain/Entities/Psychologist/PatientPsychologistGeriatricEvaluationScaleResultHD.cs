﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTd42461f80aed4557b007b3a3881ef91e")]
    public class PatientPsychologistGeriatricEvaluationScaleResultHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }



        [Display(Name = "<br/><strong>Suma zdobytych punktów</strong>")]
        public int? ntxtsuma { get; set; }

        [Display(Name = "Interpretacja wyników")]
        public string txtainterpretacja { get; set; }
        
    }
}


