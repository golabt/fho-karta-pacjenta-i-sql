﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTf172990cc31547789aa858de04693a38")]
    public class PatientPsychologistVisitHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "Data wizyty")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawizyty { get; set; }

        [Display(Name = "Lekarz przeprowadzający wizytę")]
        public string txtpsychologwizyta { get; set; }

        [Display(Name = "Forma kontaktu")]
        public string formakontaktu { get; set; }

        [Display(Name = "W jakiej sprawie")]
        public string konsultacjapsychologiczna { get; set; }

        [Display(Name = "")]
        public string zasieganieinformacji { get; set; }

        [Display(Name = "")]
        public string interwencjakryzysowa { get; set; }

        [Display(Name = "")]
        public string psychoedukacja { get; set; }

        [Display(Name = "")]
        public string wsparciepsygicznecholo { get; set; }

        [Display(Name = "")]
        public string pracawzalobie { get; set; }

        [Display(Name = "")]
        public string innasprawa { get; set; }

        [Display(Name = "Opis wizyty")]
        public string txtaopis { get; set; }

        [Display(Name = "Test MMSE")]
        public string testMMSE { get; set; }

        [Display(Name = "Kwestionariusz HADS-M")]
        public string HADSm { get; set; }

        [Display(Name = "Geriatryczna Skala Oceny Depresji")]
        public string skaladepresji { get; set; }

        [Display(Name = "Skala Akceptacji Choroby")]
        public string skalaakceptacjichoroby { get; set; }



        [HiddenInput(DisplayValue = false)]
        public string RadioButtonList1 { get; set; }
        [HiddenInput(DisplayValue = false)]
        public string RadioButtonList2 { get; set; }
    }
}


