﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTe31388ca5c7f4510bf69e43af3cedc5d")]
    public class PatientPsychologistGeriatricEvaluationScaleHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "<strong>Geriatryczna Skala Oceny Depresji wraz z wynikiem</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "Myśląc o całym swoim życiu, czy jest Pan(i) z niego zadowolony(a) ?")]
        public int? RadioButtonList1 { get; set; }

        [Display(Name = "Czy zmniejszyła się liczba Pana(i) aktywności i zainteresowań ?")]
        public int? RadioButtonList2 { get; set; }

        [Display(Name = "Czy ma Pan(i) uczucie, że życie jest puste ?")]
        public int? RadioButtonList3 { get; set; }

        [Display(Name = "Czy często czuje się Pan(i) znudzony(a) ?")]
        public int? RadioButtonList4 { get; set; }

        [Display(Name = "Czy jest Pan(i) w dobrym nastroju przez większość czasu ?")]
        public int? RadioButtonList5 { get; set; }

        [Display(Name = "Czy obawia się Pan(i), że może zdarzyć się Panu(i) coś złego ?")]
        public int? RadioButtonList6 { get; set; }

        [Display(Name = "Czy przez większość czasu czuje się Pan(i) szczęśliwy(a) ?")]
        public int? RadioButtonList7 { get; set; }

        [Display(Name = "Czy często czuje się Pan(i) bezradny(a) ?")]
        public int? RadioButtonList8 { get; set; }

        [Display(Name = "Czy zamiast wyjść wieczorem z domu, woli Pan(i) w nim pozostać ?")]
        public int? RadioButtonList9 { get; set; }

        [Display(Name = "Czy czuje Pan(i), że ma więcej kłopotów z pamięcią niż inni ludzie ?")]
        public int? RadioButtonList10 { get; set; }

        [Display(Name = "Czy myśli Pan(i), że wspaniale jest żyć ?")]
        public int? RadioButtonList11 { get; set; }

        [Display(Name = "Czy obecnie czuje się Pan(i) gorszy(a) od innych ludzi ?")]
        public int? RadioButtonList12 { get; set; }

        [Display(Name = "Czy czuje się Pan(i) pełny(a) energii ?")]
        public int? RadioButtonList13 { get; set; }

        [Display(Name = "Czy uważa Pan(i), że sytuacja jest beznadziejna ?")]
        public int? RadioButtonList14 { get; set; }

        [Display(Name = "Czy myśli Pan(i), że ludzie są lepsi niż Pan(i) ?")]
        public int? RadioButtonList15 { get; set; }
        
    }
}


