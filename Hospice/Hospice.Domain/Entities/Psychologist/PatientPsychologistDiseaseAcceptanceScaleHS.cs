﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTe78866b3d79d4ac0a0f81090b7d7dd17")]
    public class PatientPsychologistDiseaseAcceptanceScaleHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "<strong>Skala akceptacji choroby</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "Mam kłopoty z przystosowaniem się do ograniczeń narzuconych przez chorobę.")]
        public int? RadioButtonList1 { get; set; }

        [Display(Name = "Z powodu swojego stanu zdrowie nie jestem w stanie robić tego, co najbardziej lubię.")]
        public int? RadioButtonList2 { get; set; }

        [Display(Name = "Choroba sprawia, że czasem czuję się niepotrzebny.")]
        public int? RadioButtonList3 { get; set; }

        [Display(Name = "Problemy ze zdrowiem sprawiają, że jestem bardziej zależny od innych niż tego chcę.")]
        public int? RadioButtonList4 { get; set; }

        [Display(Name = "Choroba sprawia, że jestem ciężarem dla swojej rodziny i przyjaciół.")]
        public int? RadioButtonList5 { get; set; }

        [Display(Name = "Mój stan zdrowia sprawia, że nie czuję się pełnowartościowym człowiekiem.")]
        public int? RadioButtonList6 { get; set; }

        [Display(Name = "Nigdy nie będę samowystarczalnym w takim stopniu, w jakim chciałbym być.")]
        public int? RadioButtonList7 { get; set; }

        [Display(Name = "Myślę, że ludzie przebywający ze mną są często zakłopotani z powodu mojej choroby.")]
        public int? RadioButtonList8 { get; set; }


    }
}


