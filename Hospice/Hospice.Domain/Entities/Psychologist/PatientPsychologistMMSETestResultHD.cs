﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT3068922b849c4422b5db36644b04386d")]
    public class PatientPsychologistMMSETestResultHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "<br/><strong>Suma punktów uzyskana w teście</strong>")]
        public int? ntxtwynik { get; set; }

        [Display(Name = "Interpretacja wyników")]
        public string txtainterpretacjawynikow { get; set; }
        
    }
}


