﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTdb5ef3e94f684d62a3ab4eba43d643c1")]
    public class PatientPsychologistQuestionnaireHADSMResultHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "<strong>Suma punktów w poszczególnych kategoriach</strong>")]
        [NotMapped]
        public string HolderSuma { get; set; }

        [Display(Name = "Lęk")]
        public int? ntxtLek { get; set; }

        [Display(Name = "Depresja")]
        public int? ntxtdepresja { get; set; }

        [Display(Name = "Rozdrażnienie (agresja)")]
        public int? ntxtagresja { get; set; }

    }
}


