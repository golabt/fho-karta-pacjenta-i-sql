﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTeb005a929f244fd5bb7a9c40788cc1e6")]
    public class PatientPsychologistMMSETestHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "<strong>Kwestionariusz MMSE wraz z wynikiem</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "<strong>Orientacja w czasie</strong>")]
        [NotMapped]
        public string Holder1 { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList1value { get; set; }

        [Display(Name = "Jaki jest teraz rok ?")]
        public string RadioButtonList1odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList2value { get; set; }

        [Display(Name = "Jaka jest teraz pora roku ?")]
        public string RadioButtonList2odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList3value { get; set; }

        [Display(Name = "Jaki jest teraz miesiąc ?")]
        public string RadioButtonList3odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList4value { get; set; }

        [Display(Name = "Jaka jest dzisiejsza data ?")]
        public string RadioButtonList4odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList5value { get; set; }

        [Display(Name = "Jaki jest dzisiaj dzień tygodnia ?")]
        public string RadioButtonList5odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList6value { get; set; }



        [Display(Name = "<strong>Orientacja w miejscu</strong>")]
        [NotMapped]
        public string Holder2{ get; set; }

        [Display(Name = "W jakim kraju się znajdujemy ? ")]
        public string RadioButtonList6odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList7value { get; set; }

        [Display(Name = "W jakim województwie się znajdujemy ?")]
        public string RadioButtonList7odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList8value { get; set; }

        [Display(Name = "W jakim mieście się teraz znajdujemy ?")]
        public string RadioButtonList8odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList9value { get; set; }

        [Display(Name = "Jak nazywa się miejsce, w którym się teraz znajdujemy ?")]
        public string RadioButtonList9odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList10value { get; set; }

        [Display(Name = "Na którym piętrze się obecnie znajdujemy ?")]
        public string RadioButtonList10odpowiedz { get; set; }



        [Display(Name = "<strong>Zapamiętywanie</strong>")]
        [NotMapped]
        public string Holder3 { get; set; }

        [Display(Name = "Na wstępie prosimy badanego o wyrażenie zgody na badanie pamięci. Następnie, w tempie 1 słowo na 1 sekundę, podajemy wolno i wyraźnie słowa: \"JABŁKO, TELEFON, RZEKA\". Po czym prosimy badanego o powtórzenie ich i zapamiętanie. Można próbować nauczyć pacjenta, aby powtórzył więcej słów.")]
        public int? RadioButtonList11 { get; set; }

        [Display(Name = "<strong>Uwaga</strong>")]
        [NotMapped]
        public string Holder4 { get; set; }

        [Display(Name = "Należy poprosić pacjenta o przeliterowanie wstecz słowa \"KWIAT\".")]
        public int? RadioButtonList12 { get; set; }

        [Display(Name = "<strong>Przypomnienie</strong>")]
        [NotMapped]
        public string Holder5 { get; set; }

        [Display(Name = "Prosimy spytać pacjenta, czy przypomina sobie trzy słowa, które powtarzał uprzednio (jabłko, telefon, rzeka).")]
        public int? RadioButtonList13 { get; set; }

        [Display(Name = "<strong>Język</strong>")]
        [NotMapped]
        public string Holder6 { get; set; }

        [Display(Name = "Pokazujemy pacjentowi zegarek na rękę i pytamy co to jest. Następnie pytamy o ołówek.")]
        public int? RadioButtonList14 { get; set; }

        [Display(Name = "<strong>Powtarzanie</strong>")]
        [NotMapped]
        public string Holder7 { get; set; }

        [Display(Name = "Prosimy pacjenta o powtórzenie następującego zwrotu: \"ANI TAK, ANI NIE, ANI ALE\".")]
        public int? RadioButtonList15 { get; set; }

        [Display(Name = "<strong>Rozumienie</strong>")]
        [NotMapped]
        public string Holder8 { get; set; }

        [Display(Name = "Pokazujemy pacjentowi pustą kartkę papieru i mówimy \"Proszę wziąć ten papier w lewą (lub prawą) rękę, złożyć go w pół i położyć na podłodze\". Polecenie \"lewa\" lub \"prawa\" powinno odnosić się do nie dominującej ręki. Następnie podajemy papier pacjentowi. Nie należy powtarzać instrukcji, ani pomagać pacjentowi. ")]
        public int? RadioButtonList16 { get; set; }

        [Display(Name = "<strong>Czytanie</strong>")]
        [NotMapped]
        public string Holder9 { get; set; }

        [Display(Name = "Dajemy pacjentowi kartkę z napisanym poleceniem \"Proszę zamknąć oczy\". Prosimy pacjenta, aby przeczytał kartkę i wykonał to, co jest tam napisane. ")]
        public int? RadioButtonList17 { get; set; }

        [Display(Name = "<strong>Pisanie</strong>")]
        [NotMapped]
        public string Holder10 { get; set; }

        [Display(Name = "Dajemy pacjentowi kartkę i prosimy o apisanie dowolnego zdania. Nie należy dyktować zdania, ma ono być napisane spontanicznie. Zdanie musi posiadać podmiot i orzeczenie oraz musi być sensowne. Poprawna gramatyka i interpunkcja nie są konieczne.")]
        public int? RadioButtonList18 { get; set; }

        [Display(Name = "<strong>Kopiowanie</strong>")]
        [NotMapped]
        public string Holder11 { get; set; }

        [Display(Name = "Dajemy pacjentowi kartkę z rysunkiem nakładających się pięciokątów o bokach około 2,5 cm i prosimy go o dokładne skopiowanie. W ocenie rysunku pomijamy drżenie i rotację.")]
        public int? RadioButtonList19 { get; set; }
        
    }
}


