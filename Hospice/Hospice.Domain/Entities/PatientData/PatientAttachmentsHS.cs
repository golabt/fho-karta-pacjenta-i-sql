﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("View_PatientAttachmentsHS")]
    public class PatientAttachmentsHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public Guid fldId { get; set; }
        public string fldFileName { get; set; }
        public string fldContentType { get; set; }
        public int fldContentLength { get; set; }
        public byte[] fldContent { get; set; }
        public int fldIWfId { get; set; }
        
    }
}


