﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTdfa3c31754134cea9009a9798f3cb5b7")]
    public class PatientRegisteryHospitalStayHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }



        [Display(Name = "Data przyjęcia pacjenta do szpitala")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DatePicker2 { get; set; }

        [Display(Name = "")]
        public TimeSpan? tmpgodzina { get; set; }



        [Display(Name = "Nazwa szpitala")]
        public string txtnazwaszpitala { get; set; }

        [Display(Name = "Adres szpitala")]
        public string txtadresszpitala { get; set; }

        [Display(Name = "Telefon do szpitala")]
        public string txttelefonszpital { get; set; }

        [Display(Name = "Powód pobytu w szpitalu")]
        public string txtapowod { get; set; }

        [Display(Name = "Uwagi")]
        [NotMapped]
        public string uwagi { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid? ZalacznikValue { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ZalacznikText { get; set; }
    }
}


