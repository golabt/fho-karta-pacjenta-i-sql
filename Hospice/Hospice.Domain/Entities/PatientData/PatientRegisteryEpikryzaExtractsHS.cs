﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTd715eaf74aec461489263677755158d7")]
    public class PatientRegisteryEpikryzaExtractsHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Data rezygnacji")]
        public DateTime? dtpDataRezygnacji { get; set; }

        [Display(Name = "Epikryza")]
        public string txtaepikryza { get; set; }

        [Display(Name = "Lekarz prowadzący")]
        public string txtlekarzprowadzacy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid? ZalacznikValue { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ZalacznikText { get; set; }
    }
}


