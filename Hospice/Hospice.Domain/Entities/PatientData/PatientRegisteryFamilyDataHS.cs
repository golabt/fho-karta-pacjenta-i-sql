﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT9b34929a538c4e96a60935eded611f07")]
    public class PatientRegisteryFamilyDataHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }



        [Display(Name = "Osoba udzielająca informacji")]
        public string RadioButtonList17 { get; set; }

        [Display(Name = "")]
        public string txtosobaudzielajacainformacji { get; set; }

        [Display(Name = "Pielęgniarka prowadząca")]
        public string cmbpielegniarkaEmployee_Name { get; set; }

        [Display(Name = "Chory wykorzystuje sprzęt pomocniczy")]
        public string RadioButtonList1 { get; set; }

        [Display(Name = "")]
        public string RadioButtonList2 { get; set; }

        [Display(Name = "")]
        public string RadioButtonList3 { get; set; }

        [Display(Name = "")]
        public string RadioButtonList4 { get; set; }

        [Display(Name = "")]
        public string txtinne { get; set; }

        [Display(Name = "Uczulenia")]
        public string txtuczulenia { get; set; }

        [Display(Name = "Przyzwyczajenia/ nałogi")]
        public string txtaNalogi { get; set; }

        [Display(Name = "Wyznanie")]
        public string txtwyznanie { get; set; }

        [Display(Name = "")]
        public string wierzacy { get; set; }

        [Display(Name = "")]
        public string praktykujacy { get; set; }

        [Display(Name = "Osoba będąca w bezpośrednim kontakcie z personelem medycznym")]
        public string RadioButtonList6 { get; set; }

        [Display(Name = "Liczba osób w rodzinie chorego")]
        public int? ntxtliczbarodzina { get; set; }

        [Display(Name = "Wymień członków")]
        public string txtarodzina { get; set; }

        [Display(Name = "Stan zdrowia członków rodziny chorego, choroby, uzależnienia")]
        public string txtastanzdrowiarodizny { get; set; }

        [Display(Name = "Główne problemy rodziny")]
        public string RadioButtonList7 { get; set; }

        [Display(Name = "Inne problemy")]
        public string txtinneproblemy { get; set; }

        [Display(Name = "Łączny dochód rodziny - miesięczny")]
        public int? ntxtdochodrodziny { get; set; }

        [Display(Name = "Dochód")]
        public string RadioButtonList8 { get; set; }

        [Display(Name = "Sposób porozumiewania się chorego z personelem medycznym")]
        public string porozumieniesiezpersonelem { get; set; }

        [Display(Name = "Sposób porozumiewania się chorego z rodziną")]
        public string porozumieniewaniezrodzina { get; set; }

        [Display(Name = "<br/><strong>Ocena sprawności pacjenta</strong>")]
        [NotMapped]
        public string Holder1 { get; set; }

        [Display(Name = "Porusza się w obrębie łóżka")]
        public string RadioButtonList9odpowiedz { get; set; }

        [Display(Name = "Porusza się poza łóżkiem")]
        public string RadioButtonList10odpowiedz { get; set; }

        [Display(Name = "Korzysta z WC")]
        public string RadioButtonList11odpowiedz { get; set; }

        [Display(Name = "Myje się, czesze")]
        public string RadioButtonList12odpowiedz { get; set; }

        [Display(Name = "Goli się")]
        public string RadioButtonList13odpowiedz { get; set; }

        [Display(Name = "Je, pije")]
        public string RadioButtonList14odpowiedz { get; set; }

        [Display(Name = "Wydalanie")]
        public string RadioButtonList15odpowiedz { get; set; }

        [Display(Name = "Czytanie, inne zajęcia")]
        public string RadioButtonList16odpowiedz { get; set; }

        [Display(Name = "Zdradza objawy")]
        public string objawy { get; set; }



        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList9fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList10fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList11fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList12fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList13fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList14fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList15fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList16fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? cmbpielegniarkafldID { get; set; }
    }
}


