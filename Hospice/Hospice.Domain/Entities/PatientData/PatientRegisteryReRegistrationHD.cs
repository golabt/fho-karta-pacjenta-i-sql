﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTba571a6ad1304ff9b84c6ea83cc4af2b")]
    public class PatientRegisteryReRegistrationHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Imię")]
        public string txtImiepacjenta { get; set; }

        [Display(Name = "Nazwisko")]
        public string Nazwiskopacjenta { get; set; }

        [Display(Name = "PESEL")]
        public string txtPESEL { get; set; }

        [Display(Name = "Telefon")]
        public string txttelefonpacjent { get; set; }

        [Display(Name = "Data powortu pod opiekę hospicyjną")]
        public DateTime? Datapowortu { get; set; }

        [Display(Name = "Pacjent posiada nowe skierowanie")]
        public bool? chbnoweskierowanietak { get; set; }

        [Display(Name = "<br/><strong>Skierowanie</strong>")]
        [NotMapped]
        public string Holder { get; set; }


        [Display(Name = "Data")]
        public DateTime? Skierowaniedata { get; set; }

        [Display(Name = "Placówka")]
        public string txtplacowka { get; set; }

        [Display(Name = "Regon")]
        public string txtREGON { get; set; }

        [Display(Name = "Kod resortowy VII")]
        public string TXTKODRESORTOWY7 { get; set; }

        [Display(Name = "Kod resortowy VIII")]
        public string txtkodresortowy8 { get; set; }

        [Display(Name = "Lekarz wystawiający skierowanie")]
        public string txtlekarz { get; set; }

        [Display(Name = "Numer prawa wykonywania zawodu")]
        public string ttxprawowykonywaniazawodu { get; set; }





        [HiddenInput(DisplayValue = false)]
        public DateTime? Datawypisuzhospicjum { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? nowyzespolfldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string nowyzespolDescription { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? dtpwypisszpital { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid? skanskierowaniaValue { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string skanskierowaniaText { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid? skanwypisuzeszpitalaValue { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string skanwypisuzeszpitalaText { get; set; }
    }
}


