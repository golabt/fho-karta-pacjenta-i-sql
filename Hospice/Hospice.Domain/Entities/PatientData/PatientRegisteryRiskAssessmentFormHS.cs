﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT92495a4484f44e91b2292d77d21e7de5")]
    public class PatientRegisteryRiskAssessmentFormHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "<strong>Formularz oceny ryzyka zakażenia</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "<strong>Czynniki ryzyka w chwilii przyjęcia</strong>")]
        [NotMapped]
        public string Holder1 { get; set; }

        [Display(Name = "Wiek > 75 lat")]
        public int? RadioButtonList2 { get; set; }

        [Display(Name = "Przyjęcie z innego szpitala")]
        public int? RadioButtonList3 { get; set; }

        [Display(Name = "Hospitalizacje w ciągu ostatnich 6 miesięcy")]
        public int? RadioButtonList4 { get; set; }

        [Display(Name = "Zabiegi operacyjne w ciągu ostatnich 3 miesięcy")]
        public int? RadioButtonList5 { get; set; }

        [Display(Name = "Zabiegi endoskopowe w ciągu ostatnich 3 miesięcy")]
        public int? RadioButtonList6 { get; set; }

        [Display(Name = "Nosicielstwo patogenu alarmowego")]
        public int? RadioButtonList7 { get; set; }

        [Display(Name = "")]
        public string txtpatogenalarmowy { get; set; }

        [Display(Name = "Cewnik moczowy/ cystostomia")]
        public int? RadioButtonList8 { get; set; }

        [Display(Name = "Tracheostomia")]
        public int? RadioButtonList9 { get; set; }

        [Display(Name = "PEG / gastrostomia/ ileostomia/ sonda")]
        public int? RadioButtonList10 { get; set; }

        [Display(Name = "Przetoki")]
        public int? RadioButtonList11 { get; set; }

        [Display(Name = "Wkłucie centralne")]
        public int? RadioButtonList12 { get; set; }

        [Display(Name = "Odleżyny")]
        public int? RadioButtonList1 { get; set; }

        [Display(Name = "Antybiotykoterapia w ciągu ostatnich 3 miesięcy")]
        public int? RadioButtonList13 { get; set; }

        [Display(Name = "Radio-/chemioterapia w ciągu ostatnich 3 miesięcy")]
        public int? RadioButtonList14 { get; set; }

        [Display(Name = "Cukrzyca")]
        public int? RadioButtonList15 { get; set; }

        [Display(Name = "Sterydoterapia")]
        public int? RadioButtonList16 { get; set; }

        [Display(Name = "Kacheksja")]
        public int? RadioButtonList17 { get; set; }

        [Display(Name = "Zły stan higieniczny")]
        public int? RadioButtonList18 { get; set; }

        [Display(Name = "Zaburzenia połykania")]
        public int? RadioButtonList19 { get; set; }
    }
}


