﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTb9985cfac1d54d90b8a894c212037f38")]
    public class PatientRegisteryFinalizingAdmissionHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [UIHint("StringZgodaNaLeczenie")]
        [Display(Name = "Czy pacjent podpisał zgodę na leczenie hospicyjne ?")]
        public string zgodaleczenie { get; set; }

        [Display(Name = "")]
        public string Powod { get; set; }

        [Display(Name = "Czy pacjent podpisał upoważnienie do udostępniania dokumentacji medycznej?")]
        public string UdostepnianieDokumnetacjiDescription { get; set; }

        [Display(Name = "Czy pacjent podpisał upoważnienie do udostępniania dokumentacji medycznej na wypadek śmierci?")]
        public string UdostepnianieDokumnetacjiPoSmierciDescription { get; set; }

        [Display(Name = "Czy pacjent podpisał upoważnienie do udostępniania informacji o stanie zdrowia i udzielonych świadczeniach?")]
        public string UdostepnianieInformacjioStanieZdrowiaDescription { get; set; }

        /*
        [UIHint("StringListTemplate")]
        [Display(Name = "Dane osób do kontaktu")]
        [NotMapped]
        public List<string> CalculatedOsoby { get; set; }

        */






        [HiddenInput(DisplayValue = false)]
        public string zgodaleczeniehospicyjneText { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? UdostepnianieDokumnetacjifldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? UdostepnianieDokumnetacjiPoSmiercifldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? UdostepnianieInformacjioStanieZdrowiafldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid skanupowaznieniadoinformacjimedycznychValue { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string skanupowaznieniadoinformacjimedycznychText { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid zgodaleczeniehospicyjneValue { get; set; }

    }
}


