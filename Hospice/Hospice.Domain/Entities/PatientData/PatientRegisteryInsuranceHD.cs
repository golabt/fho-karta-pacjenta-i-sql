﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTc5b9f23845d34c439a8fc1572f1714d2")]
    public class PatientRegisteryInsuranceHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }




        [Display(Name = "Rodzaj ubezpieczenia")]
        public string RodzajUbezpieczenia { get; set; }

        [Display(Name = "Data wystawienia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DataWystawienia { get; set; }

        [Display(Name = "NIP")]
        public string NIP { get; set; }

        [Display(Name = "Numer dokumentu")]
        public string NumerDokumentuLegitymacjaUbezpieczeniowa { get; set; }

        [Display(Name = "Identyfikator dokumentu")]
        public string IdentyfikatorDokumentu { get; set; }

        [Display(Name = "Data wazności od")]
        public DateTime? DataWaznosciOD { get; set; }

        [Display(Name = "Data wazności do")]
        public DateTime? DataWaznosciDO { get; set; }

        [Display(Name = "Instytucja")]
        public string Instytucja { get; set; }

        [Display(Name = "Data zgłoszenia do ubezpieczenia")]
        public DateTime? DataZgloszenia { get; set; }

        [Display(Name = "Data opłacenia składki")]
        public DateTime? DataOplaceniaSkladki { get; set; }

        [Display(Name = "Typ")]
        public string Typ { get; set; }



        [HiddenInput(DisplayValue = false)]
        public string SkanDecyzjiText { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? DataWaznosciDOMiesiacRok { get; set; }




    }
}


