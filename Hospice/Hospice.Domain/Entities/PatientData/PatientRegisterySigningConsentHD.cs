﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT8f45f0ee923c40b1a03b7d4d2f1bf7bd")]
    public class PatientRegisterySigningConsentHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [UIHint("CheckBoxTrueFalse")]
        [Display(Name = "Czy pacjent podpisał zgody na leczenie hospicyjne?")]
        public bool? chbzgodaleczenie { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid? zgodanaleczenieValue { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string zgodanaleczenieText { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid? zgodaleczeniehospcyjneValue { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string zgodaleczeniehospcyjneText { get; set; }
    }
}


