﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTe7134ccb2540437bb8f1756483ec0336")]
    public class PatientRegisteryEstablishmentOfMedicalHistoryHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "Imię")]
        public string txtimię { get; set; }

        [Display(Name = "Nazwisko")]
        public string txtnazwisko { get; set; }

        [Display(Name = "Nazwisko rodowe")]
        public string NazwiskoPanienskie { get; set; }

        [Display(Name = "Płeć")]
        public string plec { get; set; }

        [Display(Name = "")]
        public string txtPlecInna { get; set; }

        [Display(Name = "Miejsce urodzenia")]
        public string txtmiejsceurodzenia { get; set; }

        [Display(Name = "Data urodzenia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdataurodzenia { get; set; }

        [Display(Name = "PESEL")]
        public string pesel { get; set; }

        [Display(Name = "Stan cywilny")]
        public string stancywilny { get; set; }

        [Display(Name = "Zawód wyuczony")]
        public string txtZawódwyuczony { get; set; }

        [Display(Name = "Zawód wykonywany")]
        public string txtzawódwykonywany { get; set; }

        [Display(Name = "Adres zamieszkania")]
        [NotMapped]
        public string CalculatedAddress { get; set; }
        
        [Display(Name = "Zatrudnienie")]
        public string praca { get; set; }

        [Display(Name = "Miejsce pracy")]
        public string txtMiejscepracy { get; set; }

        [Display(Name = "Opiekun imię i nazwisko")]
        public string txtimieinazwiskoopiekuna { get; set; }

        [Display(Name = "Opiekun adres")]
        public string txtAdresopiekuna { get; set; }

        [Display(Name = "Opiekun telefon")]
        public string txttelefonopiekuna { get; set; }

        [Display(Name = "Powód przyjęcia")]
        public string RadioButtonList4 { get; set; }



        [HiddenInput(DisplayValue = false)]
        public string txtNrhistoriichoroby { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txthospicjum { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? dtpzgoda { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtkod { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtulica { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtnumerdomu { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtmiejscowosc { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string dtppoczatekchoroby { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string PodpisPacjenta { get; set; }
       
    }
}


