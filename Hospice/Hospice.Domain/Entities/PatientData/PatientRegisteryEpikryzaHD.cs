﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT6b9280a6ace0410e833f431177fc8210")]
    public class PatientRegisteryEpikryzaHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }
     
        [Display(Name = "Data")]
        public DateTime? DateTimePicker1 { get; set; }

        [Display(Name = "Epikryza")]
        public string txtaepikryza { get; set; }

        [Display(Name = "Przyczyna zgonu - wyjściowa")]
        public string txtprzyczynawyjsciowa { get; set; }

        [Display(Name = "Przyczyna zgonu - bezpośrednia")]
        public string txtprzycznabezposrednia { get; set; }

        [Display(Name = "Pacjent zmarł dnia")]
        public DateTime? dtpdatasmierci { get; set; }

        [Display(Name = "Zgon")]
        public string RadioButtonList1 { get; set; }

        [Display(Name = "Lekarz prowadzący")]
        public string txtlekarzprowadzacy { get; set; }



        [HiddenInput(DisplayValue = false)]
        public string txtkierownik { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid FileUpload1Value { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string FileUpload1Text { get; set; }
    }
}


