﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTc151b469aa8d488e8d689133cf4441f6")]
    public class PatientRegisteryEpikryzaHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Data")]
        public DateTime? dtpdataepikryzy { get; set; }

        [Display(Name = "Epikryza")]
        public string txtaepikryza { get; set; }

        [Display(Name = "Przyczyna zgonu - wyjściowa")]
        public string txtprzyczynawyjsciowa { get; set; }

        [Display(Name = "Przyczyna zgonu - bezpośrednia")]
        public string txtprzycznabezposrednia { get; set; }

        [Display(Name = "Pacjent zmarł dnia")]
        public DateTime? dtpDataSmierci { get; set; }

        [Display(Name = "Zgon")]
        public string RadioButtonList1 { get; set; }

        [Display(Name = "Lekarz prowadzący")]
        public string txtlekarzprowadzacy { get; set; }



        [HiddenInput(DisplayValue = false)]
        public string txtkierownik { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid ZalacznikValue { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ZalacznikText { get; set; }
    }
}


