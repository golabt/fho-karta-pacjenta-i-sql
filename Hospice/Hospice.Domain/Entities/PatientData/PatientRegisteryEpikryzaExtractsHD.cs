﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT761851d997e145b9bef3c36a7a50a23a")]
    public class PatientRegisteryEpikryzaExtractsHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }



        [Display(Name = "Data rezygnacji")]
        public DateTime? dtpDataRezygnacji { get; set; }

        [Display(Name = "Epikryza")]
        public string txtaepikryza { get; set; }

        [Display(Name = "Lekarz prowadzący")]
        public string txtlekarzprowadzacy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid? FileUpload1Value { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string FileUpload1Text { get; set; }
    }
}


