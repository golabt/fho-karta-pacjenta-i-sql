﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT2be5e5394d2d450f82f663e1f2ad0acd")]
    public class PatientPhysiotherapistBarthelPatientResultHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "<br/>Suma uzyskanych punktów</strong>")]
        public int? ntxtpacjent { get; set; }

        [Display(Name = "Interpretacja wyników")]
        public string txtaInterpretacja { get; set; }

    }
}


