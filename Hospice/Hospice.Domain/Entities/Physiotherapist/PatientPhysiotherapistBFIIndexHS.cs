﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT991b8df4773c4ff19af697f6b595f9aa")]
    public class PatientPhysiotherapistBFIIndexHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "<strong>Indeks czynności jelit BFI</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "<strong>Trudność wypróżniania się</strong>")]
        [NotMapped]
        public List<string> Holder1 { get; set; }

        [Display(Name = "W ciągu ostatnich 7 dni jak oceniłby Pan/Pani łatwość wypróżniania się w skali od 0 do 10, gdzie 0 = bez trudności, a 10 bardzo znaczna trudność")]
        public int? RadioButtonList1 { get; set; }


        [Display(Name = "<strong>Poczucie niekompletnego wypróżniania </strong>")]
        [NotMapped]
        public List<string> Holder2 { get; set; }

        [Display(Name = "W ciągu ostatnich 7 dni jak oceniłby Pan/Pani poczucie niekompletnego wypróżniania w skali od 0 do 10, gdzie 0 = brak poczucia niepełnego wypróżniania, a 10 = bardzo natężone poczucie niepełnego wypróżniania")]
        public int? RadioButtonList2 { get; set; }

        [Display(Name = "<strong>Ocena ciężkości zaparcia</strong>")]
        [NotMapped]
        public List<string> Holder3 { get; set; }

        [Display(Name = "W ciągu ostatnich 7 dni jak oceniłby Pan/Pani zaparcie stolca w skali od 0 do 10, gdzie 0 = brak zaparcia, a 10 = bardzo ciężkie zaparcie stolca")]
        public int? RadioButtonList3 { get; set; }

    }
}


