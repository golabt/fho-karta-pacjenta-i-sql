﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT776d3eae9b8348f5886c655fe699c19c")]
    public class PatientPhysiotherapistPatientCardHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "<strong>Karta pacjenta</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "Skala Karnofsky wg opinii fizjoterapeuty")]
        public string karnofsky { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "")]
        [NotMapped]
        public List<string> CalculatedProcesy { get; set; }


        [HiddenInput(DisplayValue = false)]
        public DateTime? dtpdatawizytyfizjoterapeuty { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtfizjoterapeuta { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtimie { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtnazwisko { get; set; }
    }
}


