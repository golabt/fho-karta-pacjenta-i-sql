﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTc0d948ccb10549bc9563d87f49139e0f")]
    public class PatientPhysiotherapistVisitHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "<strong>Wizyta</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "Data")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawizytyfizjoterapeuty { get; set; }

        [Display(Name = "Fizjoterapeuta")]
        public string txtfizjoterapeuta { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtimie { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtnazwisko { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string RadioButtonList1 { get; set; }
    }
}


