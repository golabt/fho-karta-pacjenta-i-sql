﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTa558bd769f244046a5753a6871c555ef")]
    public class PatientPhysiotherapistBarthelPatientHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList1 { get; set; }

        [Display(Name = "<strong>Skala Barthel wg pacjenta</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "Spożywanie posiłków")]
        public int? PACJENTRadioButtonList2 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList3 { get; set; }

        [Display(Name = "Przemieszczanie się /z łóżka na krzesło i z powrotem, siadanie/")]
        public int? PACJENTRadioButtonList4 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList5 { get; set; }

        [Display(Name = "Utrzymywanie higieny osobistej")]
        public int? PACJENTRadioButtonList6 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList8 { get; set; }

        [Display(Name = "Korzystanie z toalety /WC/")]
        public int? PACJENTRadioButtonList7 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList9 { get; set; }

        [Display(Name = "Mycie, kąpiel całego ciała")]
        public int? PACJENTRadioButtonList10 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList11 { get; set; }

        [Display(Name = "Poruszanie się /po powierzchni płaskich/")]
        public int? PACJENTRadioButtonList12 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList13 { get; set; }

        [Display(Name = "Wchodzenie i schodzenie po schodach")]
        public int? PACJENTRadioButtonList14 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList15 { get; set; }

        [Display(Name = "Ubieranie się i rozbieranie")]
        public int? PACJENTRadioButtonList16 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList17 { get; set; }

        [Display(Name = "Kontrolowanie stolca /zwieracza odbytu")]
        public int? PACJENTRadioButtonList18 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList19 { get; set; }

        [Display(Name = "Kontrolowanie moczu /zwieracza pęcherza moczowego")]
        public int? PACJENTRadioButtonList20 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string PACJENTRadioButtonList1 { get; set; }

    }
}


