﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT351b31f2da54445a9c7705fa00633b63")]
    public class PatientPhysiotherapistFirstVisitHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Data wizyty")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawizyty { get; set; }

        [Display(Name = "Fizjoterapeuta")]
        public string txtfizjoterapeuta { get; set; }


        [Display(Name = "<br/><strong>Karta fizjoterapii</strong>")]
        [NotMapped]
        public List<string> Holder1 { get; set; }
        
        [Display(Name = "Rozpoznanie")]
        public string Rozpoznanie { get; set; }

        [Display(Name = "Schorzenia towarzyszące")]
        public string txtaschorzeniatowarzyszace { get; set; }

        [Display(Name = "Data przyjęcia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdataprzyjecia { get; set; }

        [Display(Name = "Data rozpoczęcia fizjoterapii")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatarozpoczeciafizjoterapii { get; set; }



        [Display(Name = "<br/><strong>Podstawowe umiejętności pacjenta przed rozpoczęciem fizjoterapii</strong>")]
        [NotMapped]
        public List<string> Holder2 { get; set; }

        [Display(Name = "Pacjent unieruchomiony w łóżku")]
        public string unieruchomionywlozku { get; set; }

        [Display(Name = "Obraca się na prawy bok")]
        public string obracasienaprawo { get; set; }

        [Display(Name = "Obraca się na lewy bok")]
        public string obracasienalewybok { get; set; }

        [Display(Name = "Siada w łóżku")]
        public string siadawlozku { get; set; }

        [Display(Name = "Siedzi")]
        public string siedzi { get; set; }

        [Display(Name = "Wstaje")]
        public string wstaje { get; set; }

        [Display(Name = "Stoi")]
        public string stoi { get; set; }

        [Display(Name = "Chodzi")]
        public string chodzi { get; set; }

        [Display(Name = "Cel terapii")]
        public string txtacelterapii { get; set; }

        [Display(Name = "Uwagi")]
        public string txtaUwagi { get; set; }

    }
}


