﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT51b87692e2db49889591fdea6ba92868")]
    public class PatientPhysiotherapistRSCLHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList1fldId { get; set; }

        [Display(Name = "<strong>RSCL Rotterdamska lista objawów</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "Brak apetytu")]
        public string RadioButtonList1odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList2fldId { get; set; }

        [Display(Name = "Drażliwość, irytacja")]
        public string RadioButtonList2odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList3fldId { get; set; }

        [Display(Name = "Zmęczenie, znużenie")]
        public string RadioButtonList3odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList4fldId { get; set; }

        [Display(Name = "Martwienie się")]
        public string RadioButtonList4odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList5fldId { get; set; }

        [Display(Name = "Ból mięśni")]
        public string RadioButtonList5odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList6fldId { get; set; }

        [Display(Name = "Depresyjny nastrój")]
        public string RadioButtonList6odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList7fldId { get; set; }

        [Display(Name = "Brak energii (osłabienie)")]
        public string RadioButtonList7odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList8fldId { get; set; }

        [Display(Name = "bóle krzyża, pleców")]
        public string RadioButtonList8odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList9fldId { get; set; }

        [Display(Name = "Nerwowość")]
        public string RadioButtonList9odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList10fldId { get; set; }

        [Display(Name = "Nudności")]
        public string RadioButtonList10odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList11fldId { get; set; }

        [Display(Name = "Poczucie beznadziejności")]
        public string RadioButtonList11odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList12fldId { get; set; }

        [Display(Name = "Trudności ze snem")]
        public string RadioButtonList12odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList13fldId { get; set; }

        [Display(Name = "Bóle głowy")]
        public string RadioButtonList13odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList14fldId { get; set; }

        [Display(Name = "Wymioty")]
        public string RadioButtonList14odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList15fldId { get; set; }

        [Display(Name = "Zawroty głowy")]
        public string RadioButtonList15odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList16fldId { get; set; }

        [Display(Name = "Spadek zainteresowania seksem")]
        public string RadioButtonList16odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList17fldId { get; set; }

        [Display(Name = "Napięcie")]
        public string RadioButtonList17odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList18fldId { get; set; }

        [Display(Name = "Bóle brzucha")]
        public string RadioButtonList18odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList19fldId { get; set; }

        [Display(Name = "Lęk")]
        public string RadioButtonList19odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList20fldId { get; set; }

        [Display(Name = "Zaparcia")]
        public string RadioButtonList20odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList21fldId { get; set; }

        [Display(Name = "Biegunka")]
        public string RadioButtonList21odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList22fldId { get; set; }

        [Display(Name = "Zgaga, odbijanie")]
        public string RadioButtonList22odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList23fldId { get; set; }

        [Display(Name = "Drżenie")]
        public string RadioButtonList23odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList24fldId { get; set; }

        [Display(Name = "Mrowienie rąk i nóg")]
        public string RadioButtonList24odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList25fldId { get; set; }

        [Display(Name = "Trudność koncentracji")]
        public string RadioButtonList25odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList26fldId { get; set; }

        [Display(Name = "Zmiany w jamie ustnej, bóle przełykania")]
        public string RadioButtonList26odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList27fldId { get; set; }

        [Display(Name = "Utrata włosów")]
        public string RadioButtonList27odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList28fldId { get; set; }

        [Display(Name = "Pieczenie/ ból oczu")]
        public string RadioButtonList28odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList29fldId { get; set; }

        [Display(Name = "Duszność / krótki oddech")]
        public string RadioButtonList29odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList30fldId { get; set; }

        [Display(Name = "Suchość w ustach")]
        public string RadioButtonList30odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList31fldId { get; set; }

        [Display(Name = "Samoobsługa (mycie się itp)")]
        public string RadioButtonList31odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList32fldId { get; set; }

        [Display(Name = "Chodzenie po domu")]
        public string RadioButtonList32odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList33fldId { get; set; }

        [Display(Name = "Zajmowanie się pracami domowymi")]
        public string RadioButtonList33odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList34fldId { get; set; }

        [Display(Name = "Wchodzenie po schodach")]
        public string RadioButtonList34odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList35fldId { get; set; }

        [Display(Name = "Dorywcza praca")]
        public string RadioButtonList35odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList36fldId { get; set; }

        [Display(Name = "Wychodzenie z domu")]
        public string RadioButtonList36odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList37fldId { get; set; }

        [Display(Name = "Robienie zakupów")]
        public string RadioButtonList37odpowiedz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? RadioButtonList38fldId { get; set; }

        [Display(Name = "Chodzenie do pracy")]
        public string RadioButtonList38odpowiedz { get; set; }

        [Display(Name = "Jakość życia w ostatnim tygodniu")]
        public string RadioButtonList39 { get; set; }

    }
}


