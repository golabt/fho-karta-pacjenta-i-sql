﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTfc2395aaa8bf4fd4b9e492093df0b684")]
    public class PatientPhysiotherapistBarthelPhysiotherapistHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "<strong>Skala Barthel wg fizjoterapeuty</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "Spożywanie posiłków")]
        public int? FIZJORadioButtonList1 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PACJENTRadioButtonList2 { get; set; }

        [Display(Name = "Przemieszczanie się /z łóżka na krzesło i z powrotem, siadanie/")]
        public int? FIZJORadioButtonList3 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PACJENTRadioButtonList4 { get; set; }

        [Display(Name = "Utrzymywanie higieny osobistej")]
        public int? FIZJORadioButtonList5 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PACJENTRadioButtonList6 { get; set; }

        [Display(Name = "Korzystanie z toalety /WC/")]
        public int? FIZJORadioButtonList8 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PACJENTRadioButtonList7 { get; set; }

        [Display(Name = "Mycie, kąpiel całego ciała")]
        public int? FIZJORadioButtonList9 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PACJENTRadioButtonList10 { get; set; }

        [Display(Name = "Poruszanie się /po powierzchni płaskich/")]
        public int? FIZJORadioButtonList11 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PACJENTRadioButtonList12 { get; set; }

        [Display(Name = "Wchodzenie i schodzenie po schodach")]
        public int? FIZJORadioButtonList13 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PACJENTRadioButtonList14 { get; set; }

        [Display(Name = "Ubieranie się i rozbieranie")]
        public int? FIZJORadioButtonList15 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PACJENTRadioButtonList16 { get; set; }

        [Display(Name = "Kontrolowanie stolca /zwieracza odbytu")]
        public int? FIZJORadioButtonList17 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PACJENTRadioButtonList18 { get; set; }

        [Display(Name = "Kontrolowanie moczu /zwieracza pęcherza moczowego")]
        public int? FIZJORadioButtonList19 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PACJENTRadioButtonList20 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string PACJENTRadioButtonList1 { get; set; }




    }
}


