﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT13635254c31e4f1daaeacc66706f24ca")]
    public class PatientPhysiotherapistKarnofskyPerformanceStatusHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "<strong>Stopień sprawności wg Karnofsky</strong>")]
        [NotMapped]
        public string Holder0 { get; set; }

        [Display(Name = "Stopień sprawności wg. pacjenta")]
        public int? PACJENTRadioButtonList1 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FIZJORadioButtonList2 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtaskalaopis { get; set; }

    }
}


