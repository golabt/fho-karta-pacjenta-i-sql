﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTeb6f603d20e8432f90df5e0a4ff142a2")]
    public class PatientPhysiotherapistBarthelPhysiotherapistResultHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "<br/>Suma uzyskanych punktów</strong>")]
        public int? ntxtfizjo { get; set; }

        [Display(Name = "Interpretacja wyników")]
        public string txtainterpretacja { get; set; }

    }
}


