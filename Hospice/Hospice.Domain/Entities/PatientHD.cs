﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTf0f18fd8365e480cbbac90d0526c2f6b")]
    public class PatientHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }
        public string txtImiepacjenta { get; set; }
        public string txtNazwiskopacjenta { get; set; }
        public string PESEL { get; set; }
        public string txttelefonpacjent { get; set; }
        public string txttelefonopiekun1 { get; set; }
        public string kierunkowy { get; set; }
        public string kierunkowy2 { get; set; }
    }
}


