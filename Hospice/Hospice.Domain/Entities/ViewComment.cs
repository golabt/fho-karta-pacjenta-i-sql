﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("ViewComments")]
    public class ViewComment
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }

        
        [Display(Name = "Data")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }

        [Display(Name = "Autor")]        
        public string Author { get; set; }

        [Display(Name = "Content")]        
        public string Content { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ParentViewName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldId { get; set; }
    }
}


