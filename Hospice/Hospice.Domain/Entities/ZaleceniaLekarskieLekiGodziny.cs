﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("ZaleceniaLekarskieLekiGodziny")]
    public class ZaleceniaLekarskieLekiGodziny
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? IdZaleceniaStacjonarne { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? IdZaleceniaDomowe { get; set; }

        [HiddenInput(DisplayValue = false)]
        public TimeSpan? Godzina { get; set; }
        public Int16 Rodzaj { get; set; }
    }
}


