﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("TblInstanceWorkflows")]
    public class TblInstanceWorkflows
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }
        public string fldTemplateWfGuid { get; set; }
        public int? fldTemplateWfId { get; set; }
        public DateTime? fldCreationDate { get; set; }
        public int? fldEmployeeId { get; set; }
        public int? fldCreatedByOriginalId { get; set; }
        public DateTime? fldCompletionDate { get; set; }
        public bool? fldLate { get; set; }
        public DateTime fldLastUpdated { get; set; }
        public int? fldUpdatedBy { get; set; }
        public int? fldUpdatedByOriginalId { get; set; }
        public int? fldSourceIWfId { get; set; }
        public int? fldSourceIActId { get; set; }
        public DateTime? fldLastRedirectDate { get; set; }
        public DateTime? fldNextRedirectDate { get; set; }
        public string fldPropagation { get; set; }
        public int? fldStatus { get; set; }
        public bool? fldDebugMode { get; set; }
        public string fldDisplayField { get; set; }
        public string fldPending { get; set; }


    }
}


