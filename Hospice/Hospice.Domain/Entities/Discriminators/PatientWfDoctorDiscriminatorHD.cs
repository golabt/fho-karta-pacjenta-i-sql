﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTc1ee29a5ebde42d78c0528c7c4a460eb")]
    public class PatientWfDoctorDiscriminatorHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }
        public string txtimie { get; set; }
        public string txtnazwisko { get; set; }
        public string txtpesel { get; set; }
        public string txtiulica { get; set; }
        public string txtnumer { get; set; }
        public string txtkod { get; set; }
        public string txtmiejscowosc { get; set; }
        public string txtnumerpacjent { get; set; }
        public string txtnumeropiekun { get; set; }
        public string txticd10 { get; set; }
        public string txtlekarz { get; set; }
        public string txtpielegniarka { get; set; }
    }
}


