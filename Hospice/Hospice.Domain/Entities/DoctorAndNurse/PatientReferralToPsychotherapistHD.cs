﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTf2e8c2282acd41be959fd58a4a4b7e73")]
    public class PatientReferralToPsychotherapistHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Data wystawienia zlecenia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawystawieniazlecenia { get; set; }

        [Display(Name = "Lekarz")]
        public string txtlekarz { get; set; }

        [Display(Name = "Rozpoznanie")]
        public string txtarozpoznanie { get; set; }

        [Display(Name = "Uwagi")]
        public string txtauwagi { get; set; }


    }
}


