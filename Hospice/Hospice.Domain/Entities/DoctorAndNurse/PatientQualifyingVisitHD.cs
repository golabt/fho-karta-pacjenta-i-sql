﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTcd00270d03ff4ecc92cfccd36e7f1b25")]
    public class PatientQualifyingVisitHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }



        [Display(Name = "Data wystawienia zlecenia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpDataWizytyKwalifikacyjnej { get; set; }

        [Display(Name = "Badanie podmiotowe")]
        public string txtaBadaniepodmiotowe { get; set; }

        [Display(Name = "Badanie przedmiotowe")]
        public string txtaBadanieprzedmiotowe { get; set; }

        [Display(Name = "Rozpoznanie Kod ICD10")]
        public string kodICD10KODANDNazwaRozpoznania { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string kodICD10KOD { get; set; }

        [Display(Name = "Przebieg leczenia")]
        public string przebiegleczenia { get; set; }

        [Display(Name = "Schorzenia współistniejące")]
        public string Field0KODANDNazwaRozpoznania { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Field0KOD { get; set; }

        [Display(Name = "Data przyjęcia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpPrzyjeciePoWizycieKwalifikacyjnej { get; set; }

        [Display(Name = "Czy zakwalifikowano pacjenta do opieki w hospicjum domowym?")]
        public string czyzakwalifikowanoDescription { get; set; }



        [HiddenInput(DisplayValue = false)]
        public int? czyzakwalifikowanofldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? czysprzetfldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string czysprzetDescription { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtauzasadnienieodmowy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtaschorzeniawspolistniejace { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string zleceniadlapielegniarki { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string skierowaniepsycholog { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string skierowaniefizjoterapeuta { get; set; }
    }
}


