﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTdbfc872e7ef64d3b9ef58417cd289591")]
    public class PatientOrdersForNursesTreatmentHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Rodzaj zabiegu")]
        public string Zabiegrodzajzabiegu { get; set; }

        [Display(Name = "Data rozpoczęcia")]
        public DateTime? Datarozpoczecia { get; set; }

        [Display(Name = "Data zakończenia")]
        public DateTime? Datazakonczenia { get; set; }

        [Display(Name = "Uwagi")]
        public string Uwagi { get; set; }

        [Display(Name = "Częstotliwość")]
        public string CzestotliwoscCzestotliwosc { get; set; }

        [Display(Name = "Interwał")]
        public int? Interwal { get; set; }
        
        [Display(Name = "Godziny")]
        [NotMapped]
        public List<ZaleceniaLekarskieLekiGodziny> CalculatedGodziny { get; set; }

    }
}


