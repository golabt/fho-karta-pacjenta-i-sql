﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT51e5a5839ef4422995a33e7d91642d87")]
    public class PatientOrdersOtherHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Opis zlecenia")]
        public string txtaopiszlecenia { get; set; }

        [Display(Name = "Data wystawienia zlecenia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatazlecenia { get; set; }

        [Display(Name = "Lekarz zlecający")]
        public string txtlekarzzlecajacy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? cboosobadozleceniaId { get; set; }

        [Display(Name = "Lekarz odbierający")]
        public string cboosobadozleceniaLastName { get; set; }
    }
}


