﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT9ea300b75bbc4f7691459d546a02215d")]
    public class PatientVisitHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Data")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawizyty { get; set; }

        [Display(Name = "Lekarz")]        
        public string txtlekarz { get; set; }

        [Display(Name = "Uwagi")]        
        public string txtaUwagi { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int? zleceniapielegniarskiefldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string zleceniapielegniarskieDescription { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? skierowaniepsychologfldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string skierowaniepsychologDescription { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? skierowaniefizjoterapeutafldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string skierowaniefizjoterapeutaDescription { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? skierowanienabadaniafldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string skierowanienabadaniaDescription { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? zleceniesprzetmedycznyfldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string zleceniesprzetmedycznyDescription { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? kwestionariuszmedycznyfldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string kwestionariuszmedycznyDescription { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? zleceniainnefldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string zleceniainneDescription { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? DatePicker1 { get; set; }
    }
}


