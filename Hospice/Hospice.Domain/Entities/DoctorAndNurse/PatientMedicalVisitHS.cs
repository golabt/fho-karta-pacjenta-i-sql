﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT66ec3f2fb5ca4519afd801631b68b583")]
    public class PatientMedicalVisitHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Data")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawizyty { get; set; }

        [Display(Name = "Lekarz")]        
        public string txtlekarz { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string zleceniapielegniarskieDescription { get; set; }

        [Display(Name = "Skierowanie do psychologa")]
        public string skierowaniepsychologDescription { get; set; }

        [Display(Name = "Skierowanie do fizjoterapeuty")]
        public string skierowaniefizjoterapeutaDescription { get; set; }

        [Display(Name = "Skierowanie na badania")]        
        public string skierowanienabadaniaDescription { get; set; }

        
        
        
        [HiddenInput(DisplayValue = false)]
        public int? skierowanienabadaniafldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? zleceniainnefldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? skierowaniefizjoterapeutafldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? skierowaniepsychologfldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string zleceniapielegniarskiefldId { get; set; }
    }
}


