﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT3ee279dab0e8423cb5ec1413b431c0d6")]
    public class PatientIndividualNursingCardHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "Data wizyty")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawizyty { get; set; }

        [Display(Name = "Zalecenia i uwagi ")]
        public string txtazaleceniaiuwagi { get; set; }

        [Display(Name = "Opis problemu pielęgnacyjnego. Diagnoza pielęgniarska")]
        public string txtaopis { get; set; }

        [UIHint("CheckBoxTrueFalse")]
        [Display(Name = "Czy uwzględnić opis w raporcie z dyżuru ?")]
        public bool? chbDoraportu { get; set; }



        [HiddenInput(DisplayValue = false)]
        public string Button1 { get; set; }
    }
}


