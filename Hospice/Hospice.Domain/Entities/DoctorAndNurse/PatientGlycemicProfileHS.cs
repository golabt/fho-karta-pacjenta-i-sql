﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT829419f8d5bb4562a2abe2b92b9744dc")]
    public class PatientGlycemicProfileHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Button1 { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Profile")]
        [NotMapped]
        public List<string> CalculatedProfiles { get; set; }
    }
}


