﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT29408faab9e34d21aa23a615a5dbe06b")]
    public class PatientFullExaminationDrugsHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }
        
        public string NazwaLeku { get; set; }

        public string Dawka { get; set; }
        public DateTime? DataWlaczenia { get; set; }
        public int? DrogapodaniafldId { get; set; }
        public string Drogapodaniadrogapodania { get; set; }
        public string Uwagi { get; set; }
    }
}


