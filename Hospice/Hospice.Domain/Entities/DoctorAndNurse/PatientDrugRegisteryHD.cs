﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTb415f0b0fe074b019bc14a132346c28e")]
    public class PatientDrugRegisteryHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Opis zleceń dla rodziny pacjenta")]
        public string TextArea1 { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Zarejestrowane leki")]
        [NotMapped]
        public List<string> CalculatedDrugs { get; set; }
    }
}


