﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTb0754dc342914290a483a53eddf8c8b6")]
    public class PatientRegisteryHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "Data")]
        public DateTime? dtpData { get; set; }

        [Display(Name = "Imię pacjenta")]
        public string txtImiepacjenta { get; set; }

        [Display(Name = "Nazwisko pacjenta")]
        public string txtNazwiskopacjenta { get; set; }

        [Display(Name = "PESEL")]
        public string PESEL { get; set; }

        [Display(Name = "Nr telefonu")]
        public string txttelefonpacjent { get; set; }



        [Display(Name = "Adres")]
        [NotMapped]
        public string CalculatedAdresPacjenta { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtulica { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtnumerdomu { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string lblkodpocztowy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtmiejscowosc { get; set; }

        [Display(Name = "Seria i numer dokumentu tożsamości")]
        public string Seriainumerdowodu { get; set; }

        [Display(Name = "Skierowanie - data")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdataskierowanie { get; set; }


        [Display(Name = "<br/><strong>Skierowanie - jednostka kierująca</strong>")]
        [NotMapped]
        public string Holder1 { get; set; }

        [Display(Name = "Regon")]
        public string txtRegon { get; set; }

        [Display(Name = "Kod resortowy 7")]
        public string kodresortowy7 { get; set; }

        [Display(Name = "Kod resortowy 8")]
        public string kodresortowy8Kod { get; set; }

        [Display(Name = "<br/><strong>Skierowanie - lekarz kierujący</strong>")]
        [NotMapped]
        public string Holder2 { get; set; }

        [Display(Name = "Imię lekarza")]
        public string skierowanielekarz { get; set; }

        [Display(Name = "Nazwisko lekarza")]
        public string NazwiskoLekarzSkierowanie { get; set; }

        [Display(Name = "Numer prawa wykonywania zawodu")]
        public string skierowanielekarzprawozawodu { get; set; }

        [Display(Name = "Data potwierdzenia choroby nowotworowej")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Potwierdzeniechorobynowotworowej { get; set; }

        [Display(Name = "Rozpoznanie wg kodu ICD10")]
        public string ComboBox1KODANDNazwaRozpoznania { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Guid? SkandowoduValue { get; set; } //uniq

        [HiddenInput(DisplayValue = false)]
        public Guid? skandowodu2Value { get; set; } //uniq

        [HiddenInput(DisplayValue = false)]
        public Guid? skandowoduubezpieczeniaValue { get; set; } //uniq

        [HiddenInput(DisplayValue = false)]
        public Guid? skanpotwierdzeniachorobynowotworowejValue { get; set; } //uniq        

        [HiddenInput(DisplayValue = false)]
        public Guid? skierowanieValue { get; set; } //uniq
        
        [HiddenInput(DisplayValue = false)]
        public Guid? FileUpload1Value { get; set; } //uniq

        [HiddenInput(DisplayValue = false)]
        public string Potwierdzeniechorobynowotworowejnumeridentyfikacyjny { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string UbezpieczenieNFZ { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? UbezpieczenieNFZterminważności { get; set; }        

        [HiddenInput(DisplayValue = false)]
        public string SkandowoduText { get; set; }        

        [HiddenInput(DisplayValue = false)]
        public string skanpotwierdzeniachorobynowotworowejText { get; set; }
                
        [HiddenInput(DisplayValue = false)]
        public string skandowoduubezpieczeniaText { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtubezpieczyciel { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string skierowanieText { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtskierowanieinfo { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string skandowodu2Text { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ComboBox1KOD { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string FileUpload1Text { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? oddzialfldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string oddzialNazwa { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? kodresortowy8fldId { get; set; }

    }
}


