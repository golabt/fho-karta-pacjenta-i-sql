﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT2259bc2125f74b7aae99fad72a7914f7")]
    public class PatientIndividualNursingCardHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Data wizyty")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawizyty { get; set; }


        [Display(Name = "<br/><strong>Opis</strong>")]
        [NotMapped]
        public List<string> HolderOpis { get; set; }


        [Display(Name = "Stan pacjenta")]
        public string stanpacjenta { get; set; }

        [Display(Name = "Ból")]
        public string bol { get; set; }

        [Display(Name = "Ciśnienie")]
        [NotMapped]
        public string CalculatedPressure { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string cisnienie { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextBox1 { get; set; }

        [Display(Name = "Saturacja [%]")]
        public string saturacja { get; set; }

        [Display(Name = "Poziom cukru [mg / dL]")]
        public string poziomcukru { get; set; }

        [UIHint("CheckBoxTrueFalse")]
        [Display(Name = "Wymioty")]
        public bool? wymioty { get; set; }

        [UIHint("CheckBoxTrueFalse")]
        [Display(Name = "Biegunka")]
        public bool? biegunka { get; set; }

        [UIHint("CheckBoxTrueFalse")]
        [Display(Name = "Krwioplucie")]
        public bool? krwioplucie { get; set; }

        [UIHint("CheckBoxTrueFalse")]
        [Display(Name = "Duszność")]
        public bool? dusznosc { get; set; }

        [UIHint("CheckBoxTrueFalse")]
        [Display(Name = "Odleżyny")]
        public bool? odlezyny { get; set; }

        [UIHint("CheckBoxTrueFalse")]
        [Display(Name = "Rany nowotworowe")]
        public bool? rannowotworowe { get; set; }

        [Display(Name = "Zalecenia dietetyczne")]
        public string zaleceniadietetyczne { get; set; }

        [Display(Name = "Zalecenia i uwagi")]
        public string txtazaleceniaiuwagi { get; set; }

        [Display(Name = "Data następnej planowanej wizyty")]
        public DateTime? dtpnastepnawizyta { get; set; }


        [UIHint("StringListTemplate")]
        [Display(Name = "Toaleta chorego")]
        [NotMapped]
        public List<string> CalculatedToilet { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Zabiegi")]
        [NotMapped]
        public List<string> CalculatedTreatments { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Świadczenia edukacyjne")]
        [NotMapped]
        public List<string> CalculatedEducation { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Świadczenia medyczne")]
        [NotMapped]
        public List<string> CalculatedMedical { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Świadczenia rehabilitacyjne")]
        [NotMapped]
        public List<string> CalculatedRehabilitation { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Inne")]
        [NotMapped]
        public List<string> CalculatedOther { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtaopiswizytylekarskiej { get; set; }
    }
}


