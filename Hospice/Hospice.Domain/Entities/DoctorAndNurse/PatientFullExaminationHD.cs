﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT4c0de6fa33fd49319a98c95544674719")]
    public class PatientFullExaminationHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Rozpoznanie Kod ICD10")]
        public string KODICD10 { get; set; }

        [Display(Name = "Rozpoznanie")]
        public string txtarozpoznanie { get; set; }

        [Display(Name = "Choroby współistniejące")]
        public string chorobywspolistniejace { get; set; }



        [Display(Name = "<br/><strong>Badanie podmiotowe</strong>")]
        [NotMapped]
        public string BadaniePodmiotowe { get; set; }

        [Display(Name = "Wywiad")]
        public string Wywiad { get; set; }

        [Display(Name = "Uczulenia na leki")]
        public string RadioButtonList7 { get; set; }

        [Display(Name = "")]
        public string UczulenianaLeki { get; set; }

        [Display(Name = "Nałogi")]
        public string RadioButtonList8 { get; set; }

        [Display(Name = "")]
        public string Nalogi { get; set; }

        [Display(Name = "Ból w skali VAS")]
        public string RadioButtonList6 { get; set; }

        [Display(Name = "Lokalizacja bólu")]
        public string TextArea1 { get; set; }

        [Display(Name = "<br/><strong>Badanie przedmiotowe</strong>")]
        [NotMapped]
        public string BadaniePrzedmiotowe { get; set; }

        [Display(Name = "Stan ogólny")]
        public string RadioButtonList1 { get; set; }

        [Display(Name = "Stan świadomości")]
        public string Wrazenieogolne1 { get; set; }

        [Display(Name = "")]
        public string StanSwiadomosci { get; set; }

        [Display(Name = "Zaburzenia świadomości")]
        public string RadioButtonList2 { get; set; }

        [Display(Name = "")]
        public string ZaburzeniaSwiadomosci { get; set; }

        [Display(Name = "Stan odżywienia")]
        public string RadioButtonList3 { get; set; }

        [Display(Name = "")]
        public string StanOdzywienia { get; set; }

        [Display(Name = "Kontakt słowny")]
        public string kontaktslowny { get; set; }

        [Display(Name = "")]
        public string KontaktSlow { get; set; }

        [Display(Name = "Skóra")]
        public string skora { get; set; }

        [Display(Name = "")]
        public string SkoraOpis { get; set; }

        [Display(Name = "Ucieplenie skóry")]
        public string ucieplenieskory { get; set; }

        [Display(Name = "")]
        public string Ucieplnenie { get; set; }

        [Display(Name = "Zmiany skórne")]
        public string wykwity { get; set; }

        [Display(Name = "")]
        public string RadioButtonList9 { get; set; }

        [Display(Name = "")]
        public string wykwity2 { get; set; }

        //[Display(Name = "")]
        //public string Zmianyskorne { get; set; }

        [Display(Name = "")]
        [HiddenInput(DisplayValue = false)]
        public Guid? BliznySkanValue { get; set; }

        [Display(Name = "Odleżyny")]
        public string odlezyny { get; set; }

        [Display(Name = "")]
        public string OdlezynyOpis { get; set; }

        [Display(Name = "")]
        [HiddenInput(DisplayValue = false)]
        public Guid? OdlezynySkanValue { get; set; }


        [Display(Name = "Owrzodzenia nowotworowe")]
        public string Blizny { get; set; }

        [Display(Name = "")]
        public string OwrzodzeniaOpis { get; set; }

        [Display(Name = "")]
        [HiddenInput(DisplayValue = false)]
        public Guid? OwrzodzeniaSkanValue { get; set; }


        [Display(Name = "Obrzęki")]
        public string obrzeki { get; set; }

        [Display(Name = "")]
        public string ObrzekiOpis { get; set; }


        [Display(Name = "Węzły chłonne")]
        public string wezlychlonne { get; set; }

        [Display(Name = "")]
        public string WezlyChlonneOpis { get; set; }


        [Display(Name = "Głowa")]
        public string RadioButtonList11 { get; set; }

        [Display(Name = "")]
        public string GlowaOpis { get; set; }


        [Display(Name = "Spojówki")]
        public string spojowki { get; set; }

        [Display(Name = "")]
        public string SpojowkiOpis { get; set; }


        [Display(Name = "Twardówka")]
        public string bialkowka { get; set; }

        [Display(Name = "")]
        public string TwardowkaOpis { get; set; }

        [Display(Name = "Źrenice")]
        public string zrenice1 { get; set; }

        [Display(Name = "")]
        public string zrenice2 { get; set; }

        [Display(Name = "")]
        public string zrenice3 { get; set; }

        [Display(Name = "")]
        public string ZreniceOpis { get; set; }

        [Display(Name = "Jama ustna, śluzówka")]
        public string jamaustna1 { get; set; }

        [Display(Name = "")]
        public string jamaustna2 { get; set; }

        [Display(Name = "")]
        public string jamaustna3 { get; set; }

        [Display(Name = "")]
        public string JamaUstnaOpis { get; set; }

        [Display(Name = "Szyja")]
        public string szyja { get; set; }

        [Display(Name = "")]
        public string SzyjaOpis { get; set; }

        [Display(Name = "Tracheostomia")]
        public string RadioButtonList4 { get; set; }

        [Display(Name = "")]
        public string TracheostomiaOpis { get; set; }

        [Display(Name = "Klatka piersiowa")]
        public string klatkapiersiowa { get; set; }

        [Display(Name = "")]
        public string KlatkaOpis { get; set; }

        [Display(Name = "Ruchomość oddechowa")]
        public string ruchowoscoddechowa { get; set; }

        [Display(Name = "Liczba oddechów /min")]
        public string txtliczbaoddechow { get; set; }

        [Display(Name = "")]
        public string RuchomowscOpis { get; set; }

        [Display(Name = "Szmery oddechowe")]
        public string osluchowo { get; set; }

        [Display(Name = "")]
        public string OsluchowoOpis { get; set; }

        [Display(Name = "Czynność serca")]
        public string czynnoscserca { get; set; }

        [Display(Name = "")]
        public string CzynnoscsercaOpis { get; set; }

        [Display(Name = "Tony")]
        public string tony { get; set; }

        [Display(Name = "")]
        public string tony2 { get; set; }

        [Display(Name = "")]
        public string TonyOpis { get; set; }

        [Display(Name = "Szmery")]
        public string szmery { get; set; }

        [Display(Name = "")]
        public string SzmeryOpis { get; set; }

        [Display(Name = "Ciśnienie tętnicze")]
        [NotMapped]
        public string CisnienieCalculated { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtcisnienietetnicze { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string cisnienietetnicze2 { get; set; }

        [Display(Name = "")]
        public string CisnienieOpis { get; set; }

        [Display(Name = "Układ żż. obwodowy")]
        public string ukladobwodowy1 { get; set; }

        [Display(Name = "")]
        public string UkladObwodowyOpis { get; set; }

        [Display(Name = "Brzuch (wygląd ogólny)")]
        public string brzuch1 { get; set; }

        [Display(Name = "")]
        public string brzuch2 { get; set; }

        [Display(Name = "")]
        public string Brzuch { get; set; }

        [Display(Name = "Palpacyjnie - Powłoki miękkie")]
        public string RadioButtonList14 { get; set; }

        [Display(Name = "Palpacyjnie - Obrona mięśniowa")]
        public string RadioButtonList15 { get; set; }

        [Display(Name = "Palpacyjnie - Objawy otrzewnowe")]
        public string RadioButtonList16 { get; set; }

        [Display(Name = "Palpacyjnie - Objaw Goldflama L")]
        public string RadioButtonList17 { get; set; }

        [Display(Name = "Palpacyjnie - Objaw Goldflama P")]
        public string RadioButtonList18 { get; set; }

        [Display(Name = "Palpacyjnie - Objaw Chełmońskiego")]
        public string RadioButtonList19 { get; set; }

        [Display(Name = "Palpacyjnie - Opis")]
        public string PalpacyjnieOpis { get; set; }

        [Display(Name = "Wątroba")]
        public string watroba1 { get; set; }

        [Display(Name = "")]
        public string watroba2 { get; set; }

        [Display(Name = "")]
        public string watroba3 { get; set; }

        [Display(Name = "")]
        public string watroba4 { get; set; }

        [Display(Name = "")]
        public string txtwatrobawystaje { get; set; }

        [Display(Name = "")]
        public string WatrobaOpis { get; set; }

        [Display(Name = "Inne narządy w jamie brzusznej/ patologiczne opory")]
        public string innewjamiebrzusznej { get; set; }

        [Display(Name = "")]
        public string OporyOpis { get; set; }

        [Display(Name = "Perystaltyka")]
        public string RadioButtonList12 { get; set; }

        [Display(Name = "")]
        public string Perystaltyka { get; set; }

        [Display(Name = "PEG / Gastrostomia")]
        public string RadioButtonList5 { get; set; }

        [Display(Name = "")]
        public string PEGopis { get; set; }

        [Display(Name = "<br/>Inne")]
        public string Inne { get; set; }

        [Display(Name = "<br/><strong>Badanie neurologiczne</strong>")]
        [NotMapped]
        public string BadanieNeurologiczne { get; set; }

        [Display(Name = "Objawy oponowe")]
        public string RadioButtonList13 { get; set; }

        [Display(Name = "Niedowłady")]
        public string Niedowlady { get; set; }

        [Display(Name = "Inne")]
        public string Neuroligiczne { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Obecnie przyjmowane leki")]
        [NotMapped]
        public List<string> DrugsCalculated { get; set; }

        /*
        public string Wrazenieogolne2 {get;set;}
        public string Wrazenieogolne3 {get;set;}
        public string wykwity2 {get;set;}
        public string ucieplenieskory {get;set;}
        public string obrzeki {get;set;}
        public string txtlokalizacjaobrzeku {get;set;}
        public string wezlychlonne {get;set;}
        public string txtlokalizacjawezlychlonne {get;set;}
        public string spojowki {get;set;}
        public string bialkowka {get;set;}
         public string zrenice1 {get;set;}
         public string zrenice2 {get;set;}
         public string zrenice3 {get;set;}
        public string jamaustna1 {get;set;}
         public string jamaustna2 {get;set;}
         public string jamaustna3 {get;set;}
         public string szyja {get;set;}
        public string klatkapiersiowa {get;set;}
         public string ruchowoscoddechowa {get;set;}
         public string txtliczbaoddechow {get;set;}
         public string txtszmerydodtowe {get;set;}
        public string osluchowo {get;set;}
         public string czynnoscserca {get;set;}
         public string txtczynnoscserca {get;set;}
         public string tony {get;set;}
        public string tony2 {get;set;}
         public string szmery {get;set;}
        public string txtcisnienietetnicze {get;set;}
         public string ukladobwodowy1 {get;set;}
        public string brzuch1 {get;set;}
         public string brzuch2 {get;set;}
        public string Palpacyjnie {get;set;}
         public string Palpacyjnie2 {get;set;}
        public string Palpacyjnie3 {get;set;}
         public string Palpacyjnie4 {get;set;}
        public string Palpacyjnie5 {get;set;}
        public string Palpacyjnie6 {get;set;}
        public string Palpacyjnie7 {get;set;}
        public string watroba1 {get;set;}
        public string watroba2 {get;set;}
        public string watroba3 {get;set;}
        public string watroba4 {get;set;}
        public string txtwatrobawystaje {get;set;}
        public string innewjamiebrzusznej {get;set;}
        public string txtinnewjamiebrzusznej {get;set;}
        public string txtperrectum {get;set;}
        public string txtbollokalizacja {get;set;}
        public string txtaprzebiegleczenia {get;set;}
        public string ICD10KODANDNazwaRozpoznania {get;set;}
        public string TextBox1 {get;set;}
        public string RadioButtonList4 {get;set;}
        public string RadioButtonList5 {get;set;}
        public string cisnienietetnicze2 {get;set;}

        */


    }
}


