﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT97577b8be1d14aa18e6a71007927ca1c")]
    public class PatientMedicalDescriptionHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Data")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpData { get; set; }

        [Display(Name = "Lekarz prowadzący")]
        public string TextBox1 { get; set; }

        [Display(Name = "Ocena bólu według skali VAS")]
        public string RadioButtonList1 { get; set; }

        [Display(Name = "Opis lekarski")]
        public string TextArea1 { get; set; }
    }
}


