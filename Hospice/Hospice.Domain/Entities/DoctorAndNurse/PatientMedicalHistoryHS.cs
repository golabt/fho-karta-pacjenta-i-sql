﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT9a0885e607604a2992f170e2c415df67")]
    public class PatientMedicalHistoryHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }
        [Display(Name = "Nr historii choroby")]
        public string txtNrhistoriichoroby { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? lekarzprowadzacyfldID { get; set; }

        [Display(Name = "Lekarz prowadzący")]
        public string lekarzprowadzacyEmployee_Name { get; set; }

        [Display(Name = "Imię")]
        public string txtimię { get; set; }

        [Display(Name = "Nazwisko")]
        public string txtnazwisko { get; set; }

        [Display(Name = "Nazwisko rodowe")]
        public string NazwiskoPanienskie { get; set; }

        [Display(Name = "Płeć")]
        public string plec { get; set; }

        [Display(Name = "")]
        public string txtPlecInna { get; set; }

        [Display(Name = "Miejsce urodzenia")]
        public string txtmiejsceurodzenia { get; set; }

        [Display(Name = "Data urodzenia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdataurodzenia { get; set; }

        [Display(Name = "PESEL")]
        public string pesel { get; set; }

        [Display(Name = "Stan cywilny")]
        public string stancywilny { get; set; }

        [Display(Name = "Zawód wyuczony")]
        public string txtZawódwyuczony { get; set; }

        [Display(Name = "Zawód wykonywany")]
        public string txtzawódwykonywany { get; set; }        

        [Display(Name = "Adres zamieszkania")]
        [NotMapped]
        public string AddressCalculated { get; set; }

        
        [Display(Name = "Zatrudnienie")]
        public string praca { get; set; }

        [Display(Name = "Miejsce pracy")]
        public string txtMiejscepracy { get; set; }


         [Display(Name = "Początek choroby")]
         [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtppoczatekchoroby { get; set; }

         [HiddenInput(DisplayValue = false)]
         public string icd10KOD { get; set; }

         [Display(Name = "Rozpoznanie")]
         public string icd10KODANDNazwaRozpoznania { get; set; }

         [Display(Name = "Oświadczenie")]
         [NotMapped]
         public string StatementText { get; set; }

        [Display(Name = "Adres")]
        public static string adresHospicjum = GlobalSettings.HospiceAddress;

        [Display(Name = "Zgoda")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
         public DateTime? dtpzgoda { get; set; }
        [Display(Name = "Podpis pacjenta")]
         public string PodpisPacjenta { get; set; }



        [HiddenInput(DisplayValue = false)]
        public string txtulica { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtnumerdomu { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtkod { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtmiejscowosc { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtAdresopiekuna { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txttelefonopiekuna { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? dtprzyjety1 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtimieinazwiskoopiekuna { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string RadioButtonList4 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string TextBox1 { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txthospicjum { get; set; }
    }
}


