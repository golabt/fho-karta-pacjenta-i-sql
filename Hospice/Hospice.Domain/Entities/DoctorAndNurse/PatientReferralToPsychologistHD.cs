﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTc5cf62dfe55143fa87e057bb23e856f7")]
    public class PatientReferralToPsychologistHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Data wystawienia zlecenia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawystawieniazlecenia { get; set; }

        [Display(Name = "Lekarz")]
        public string txtlekarz { get; set; }

        [Display(Name = "Cel/Powód")]
        public string txtarozpoznanie { get; set; }

        [Display(Name = "Uwagi")]
        public string txtauwagi { get; set; }
    }
}


