﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT5bf9030f4d5a4a55ba5f24f9160f091e")]
    public class PatientCumulativePatientConditionHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Ból 1")]
        public string Bol1 { get; set; }

        [Display(Name = "")]
        public string RadioButtonList34 { get; set; }

        [Display(Name = "Ból 2")]
        public string Bol2 { get; set; }

        [Display(Name = "")]
        public string RadioButtonList33 { get; set; }

        [Display(Name = "Ból 3")]
        public string Bol3 { get; set; }

        [Display(Name = "")]
        public string RadioButtonList32 { get; set; }

        [Display(Name = "Trudności ze znem")]
        public string TrudnosciSen { get; set; }

        [Display(Name = "")]
        public string RadioButtonList31 { get; set; }

        [Display(Name = "Brak apetytu")]
        public string RadioButtonList30 { get; set; }

        [Display(Name = "Nudności, wymioty")]
        public string RadioButtonList29 { get; set; }

        [Display(Name = "Zaparcia")]
        public string RadioButtonList28 { get; set; }

        [Display(Name = "Biegunka")]
        public string RadioButtonList27 { get; set; }

        [Display(Name = "Zgaga, odbijanie")]
        public string RadioButtonList26 { get; set; }

        [Display(Name = "Czkawka")]
        public string RadioButtonList25 { get; set; }

        [Display(Name = "Wzdęcia")]
        public string RadioButtonList24 { get; set; }

        [Display(Name = "Zmiany w jamie ustnej")]
        public string RadioButtonList23 { get; set; }

        [Display(Name = "Suchość w ustach")]
        public string RadioButtonList22 { get; set; }

        [Display(Name = "Duszność")]
        public string RadioButtonList21 { get; set; }

        [Display(Name = "Kaszel")]
        public string RadioButtonList20 { get; set; }

        [Display(Name = "Trudności w oddawaniu moczu")]
        public string RadioButtonList19 { get; set; }

        [Display(Name = "Częstomocz, skąpomocz")]
        public string RadioButtonList18 { get; set; }

        [Display(Name = "Obrzęki")]
        public string RadioButtonList17 { get; set; }

        [Display(Name = "Odleżyny")]
        public string RadioButtonList16 { get; set; }

        [Display(Name = "Zmęczenie, znużenie")]
        public string RadioButtonList15 { get; set; }

        [Display(Name = "Brak energii, osłabienie")]
        public string RadioButtonList14 { get; set; }

        [Display(Name = "Trudności w koncentracji")]
        public string RadioButtonList13 { get; set; }

        [Display(Name = "Drażliwość, irytacja")]
        public string RadioButtonList12 { get; set; }

        [Display(Name = "Martwienie się")]
        public string RadioButtonList11 { get; set; }

        [Display(Name = "Depresyjny nastrój")]
        public string RadioButtonList10 { get; set; }

        [Display(Name = "Nerwowość")]
        public string RadioButtonList9 { get; set; }

        [Display(Name = "Poczucie beznadziejności")]
        public string RadioButtonList8 { get; set; }

        [Display(Name = "Napięcie \"wewnętrzne\"")]
        public string RadioButtonList7 { get; set; }

        [Display(Name = "Lęk")]
        public string RadioButtonList6 { get; set; }

        [Display(Name = "Zaburzenia świadomości")]
        public string RadioButtonList5 { get; set; }

        [Display(Name = "Zaburzenia otępienne")]
        public string RadioButtonList4 { get; set; }

        [Display(Name = "Główny objaw (najbardziej dokuczliwy) dla chorego")]
        public string glownyobjaw { get; set; }

        [Display(Name = "")]
        public string RadioButtonList3 { get; set; }

        [Display(Name = "Jakość życia")]
        public string RadioButtonList1 { get; set; }

        [Display(Name = "Odpowiedzi udzielał ")]
        public string RadioButtonList2 { get; set; }
    

    }
}


