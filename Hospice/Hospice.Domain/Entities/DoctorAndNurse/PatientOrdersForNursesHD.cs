﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTe92b8f0a598245929aff74666be0acbf")]
    public class PatientOrdersForNursesHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }


        [Display(Name = "Data wystawienia zlecenia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DatePicker1 { get; set; }

        [Display(Name = "Zlecenie dodatkowej wizyty pielęgniarskiej")]
        [UIHint("CheckBoxTrueFalse")]
        public bool? chbdodatkowawizyta { get; set; }

        [Display(Name = "")]
        public string txtadodatkowawizyta { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Leki")]
        [NotMapped]
        public List<string> CalculatedDrugs { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Zabiegi")]
        [NotMapped]
        public List<string> CalculatedTreatments { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Opatrunki")]
        [NotMapped]
        public List<string> CalculatedDressings { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Dieta")]
        [NotMapped]
        public List<string> CalculatedDiet { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Badanie parametrów życiowych")]
        [NotMapped]
        public List<string> CalculatedLifeParameters { get; set; }
        
        [Display(Name = "Uwagi")]
        public string txtauwagi { get; set; }



    }
}


