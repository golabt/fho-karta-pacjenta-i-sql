﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT375729a022a249a09a5da1d62cd79ef8")]
    public class PatientReferralToPsychologistHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [Display(Name = "Data wystawienia zlecenia")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawystawieniazlecenia { get; set; }

        [Display(Name = "Lekarz")]
        public string txtlekarz { get; set; }

        [Display(Name = "Cel/Powód")]
        public string txtarozpoznanie { get; set; }

        [Display(Name = "Uwagi")]
        public string txtauwagi { get; set; }
    }
}


