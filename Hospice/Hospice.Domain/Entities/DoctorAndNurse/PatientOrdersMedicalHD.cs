﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT0d4c209a23ab40788016b7ace5b5b7f5")]
    public class PatientOrdersMedicalHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Leki")]
        [NotMapped]
        public List<string> CalculatedDrugs { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Zabiegi")]
        [NotMapped]
        public List<string> CalculatedTreatments { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Opatrunki")]
        [NotMapped]
        public List<string> CalculatedDressings { get; set; }

        [Display(Name = "Uwagi dla lekarza")]
        public string txtaUwagidlaLekarza { get; set; }

        [Display(Name = "Przekaż uwagi lekarzowi")]
        [UIHint("CheckBoxTrueFalse")]
        public bool? CheckBox1 { get; set; }
    }
}


