﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT8cd2e113d2e9403aa0dd562fed57a20e")]
    public class PatientFebrileCardHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [UIHint("StringListTemplate")]
        [Display(Name = "Pomiary")]
        [NotMapped]
        public List<string> CalculatedMeasurements { get; set; }

        [Display(Name = "<br/><strong>Rano</strong>")]
        [NotMapped]
        public string RanoHolder { get; set; }

        [Display(Name = "Dieta")]
        public string txtadietaR { get; set; }

        [Display(Name = "Stolec")]
        public string txtstolecR { get; set; }

        [Display(Name = "Mocz")]
        [NotMapped]
        public string CalculatedntxtmoczR { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? ntxtmoczR { get; set; }
        
        [Display(Name = "Ciężarz")]
        [NotMapped]
        public string CalculatedntxtciezarR { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? ntxtciezarR { get; set; }


        [Display(Name = "<br/><strong>Wieczór</strong>")]
        [NotMapped]
        public string WieczorHolder { get; set; }

        [Display(Name = "Dieta")]
        public string txtadietaW { get; set; }

        [Display(Name = "Stolec")]
        public string txtstolecW { get; set; }

        [Display(Name = "Mocz")]
        [NotMapped]
        public string CalculatedntxtmoczW { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? ntxtmoczW { get; set; }

        [Display(Name = "Ciężarz")]
        [NotMapped]
        public string CalculatedntxtciezarW { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? ntxtciezarW { get; set; }
    }
}


