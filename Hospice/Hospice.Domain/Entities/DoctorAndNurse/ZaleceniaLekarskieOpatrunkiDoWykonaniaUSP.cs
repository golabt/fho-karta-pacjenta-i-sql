﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    public class ZaleceniaLekarskieOpatrunkiDoWykonaniaUSP
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [Display(Name = "Zlecono na")]
        public string Godzina { get; set; }

        [Display(Name = "Rodzaj opatrunku")]
        public string Rodzajopatrunku { get; set; }

        [Display(Name = "Miejsce")]
        public string Miejsce { get; set; }

        [Display(Name = "Czynność")]
        public string Czynnosc { get; set; }

        [Display(Name = "Data wykonania")]
        public DateTime? DataWykonania { get; set; }

        [Display(Name = "Wykonano")]
        public bool? Wykonano { get; set; }

        [Display(Name = "Uwagi")]
        public string Uwagi { get; set; }


    }
}


