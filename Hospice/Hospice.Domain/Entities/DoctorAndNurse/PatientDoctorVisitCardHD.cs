﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT277f8de77e0746588b30e796b0d155ee")]
    public class PatientDoctorVisitCardHD
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [Display(Name = "Data wizyty")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpdatawizyty { get; set; }

        [Display(Name = "Opis wizyty lekarskiej")]
        public string txtaopiswizytylekarskiej { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string txtazaleceniaiuwagi { get; set; }

        [Display(Name = "Data następnej planowanej wizyty")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? dtpnastepnawizyta { get; set; }
    }
}


