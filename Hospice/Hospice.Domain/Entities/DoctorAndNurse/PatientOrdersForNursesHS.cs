﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACT447f42706de54e6e8e7db1b9a4f2c372")]
    public class PatientOrdersForNursesHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }



        [Display(Name = "Lekarz")]
        public string LekarzZlecajacy { get; set; }

        [Display(Name = "Rozpoznanie")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DataZlecenia { get; set; }
                
        [UIHint("ListTemplateDrugsHS")]
        [Display(Name = "Leki")]
        [NotMapped]
        public List<PatientOrdersForNursesDrugHS> CalculatedDrugs { get; set; }

        [UIHint("ListTemplateTreatmentsHS")]
        [Display(Name = "Zabiegi")]
        [NotMapped]
        public List<PatientOrdersForNursesTreatmentHS> CalculatedTreatments { get; set; }

        [UIHint("ListTemplateDressingsHS")]
        [Display(Name = "Opatrunki")]
        [NotMapped]
        public List<PatientOrdersForNursesDressingHS> CalculatedDressings { get; set; }

        [Display(Name = "<strong>Badanie parametrów życiowych</strong>")]
        [NotMapped]
        public  string Holder1 { get; set; }

        [UIHint("DisplayTrueIfNotEmpty")]
        [Display(Name = "Ciśnienie")]
        public string Cisnienie { get; set; }

        [UIHint("DisplayTrueIfNotEmpty")]
        [Display(Name = "Saturacja")]
        public string Saturacja { get; set; }

        [UIHint("DisplayTrueIfNotEmpty")]
        [Display(Name = "Tętno")]
        public string Tętno { get; set; }

        [UIHint("DisplayTrueIfNotEmpty")]
        [Display(Name = "Temperatura")]
        public string Temperatura { get; set; }

        [Display(Name = "Inne")]
        public string Inne { get; set; }

        [Display(Name = "Żywienie - rodzaj")]
        public string Rodzajzywienia { get; set; }

        [Display(Name = "Żywienie - uwagi")]
        public string Zywienie { get; set; }

        [Display(Name = "Bilans płynów")]
        public string bilansplynowDescription { get; set; }

        [Display(Name = "Profil glikemii")]
        public string profilglikemii { get; set; }

        [Display(Name = "Uwagi dla pielęgniarki")]
        public string txtauwagi { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int? bilansplynowfldId { get; set; }

    }
}


