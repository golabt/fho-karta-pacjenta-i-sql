﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Hospice.Domain.Entities
{
    [Table("UACTff06667115f142dda4e10a6ae0b0e8f4")]
    public class PatientOrdersMedicalHS
    {
        [HiddenInput(DisplayValue = false)]
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int fldId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIWfId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int fldIActId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? fldAIId { get; set; }

        [UIHint("ListTemplateDrugsNowHS")]
        [Display(Name = "Leki do podania na zmianie")]
        [NotMapped]
        public List<ZaleceniaLekarskieLekiDoPodaniaUSP> CalculatedDrugs { get; set; }

        [UIHint("ListTemplateDressingsNowHS")]
        [Display(Name = "Opatrunki do wykonania na zmianie")]
        [NotMapped]
        public List<ZaleceniaLekarskieOpatrunkiDoWykonaniaUSP> CalculatedDressings { get; set; }

        [UIHint("ListTemplateTreatmentsNowHS")]
        [Display(Name = "Zabiegi do wykonania na zmianie")]
        [NotMapped]
        public List<ZaleceniaLekarskieZabiegiDoWykonaniaUSP> CalculatedTreatments { get; set; }

        [Display(Name = "<strong>Notatki pielęgniarki</strong>")]
        [NotMapped]
        public string Notes { get; set; }

        //[Display(Name = "Ciśnienie")]
        //public string Cisnienie { get; set; }

        [Display(Name = "Ciśnienie skurczowe")]
        public int? CisnienieSkurczowe { get; set; }

        [Display(Name = "Ciśnienie rozkurczowe")]
        public int? CisnienieRozkurczowe { get; set; }

        [Display(Name = "Saturacja")]
        public string Saturacja { get; set; }

        [Display(Name = "Tętno")]
        public string Tetno { get; set; }

        [Display(Name = "Uwagi")]
        public string Uwagi { get; set; }




        [HiddenInput(DisplayValue = false)]
        public string txtauwagi { get; set; }
    }
}


