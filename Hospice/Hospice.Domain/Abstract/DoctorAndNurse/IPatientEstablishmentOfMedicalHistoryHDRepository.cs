﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Abstract
{
    public interface IPatientEstablishmentOfMedicalHistoryHDRepository
    {
        IQueryable<PatientEstablishmentOfMedicalHistoryHD> PatientEstablishmentOfMedicalHistoryHD { get; }
    }
}
