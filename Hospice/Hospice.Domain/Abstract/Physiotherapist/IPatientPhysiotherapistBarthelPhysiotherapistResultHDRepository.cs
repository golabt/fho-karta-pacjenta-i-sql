﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Abstract
{
    public interface IPatientPhysiotherapistBarthelPhysiotherapistResultHDRepository
    {
        IQueryable<PatientPhysiotherapistBarthelPhysiotherapistResultHD> PatientPhysiotherapistBarthelPhysiotherapistResultHD { get; }
    }
}
