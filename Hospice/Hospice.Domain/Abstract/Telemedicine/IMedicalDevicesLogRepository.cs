﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hospice.Domain.Entities;

namespace Hospice.Domain.Abstract
{
    public interface IMedicalDevicesLogRepository
    {
        IQueryable<MedicalDevicesLog> MedicalDevicesLog { get; }
        void UpdateMedicalDevicesLog(int? Id);
    }
}
