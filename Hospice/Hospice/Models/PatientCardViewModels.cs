﻿using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hospice.Models
{
    public class PatientSearchModel
    {
        [Display(Name = "Imię")]
        public string FirstName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nazwisko")]
        public string LastName { get; set; }

        [Display(Name = "PESEL")]
        public string PESEL { get; set; }

        public int SelectedHospiceTypeId { get; set; }

        [Display(Name = "Hospicjum")]
        public List<SelectListItem> HospiceTypes { get; set; }
    }


}