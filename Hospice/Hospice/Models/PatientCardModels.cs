﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hospice.Models
{
    public class PatientCardPatientModel
    {
        public int fldId { get; set; }
        public string txtImiepacjenta { get; set; }
        public string txtNazwiskopacjenta { get; set; }
        public string PESEL { get; set; }
        public bool HospiceStationary { get; set; }

        public PatientCardPatientModel(int id, string imie, string nazwisko, string pesel, bool stacjonarne)
        {
            fldId = id;
            txtImiepacjenta = imie;
            txtNazwiskopacjenta = nazwisko;
            PESEL = pesel;
            HospiceStationary = stacjonarne;
        }
    }
}