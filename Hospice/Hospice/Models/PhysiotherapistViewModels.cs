﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hospice.Domain.Entities;

namespace Hospice.Models
{
    public class PhysiotherapistPatientBarthelPatientViewModelHS
    {
        public PatientPhysiotherapistBarthelPatientHS PatientPhysiotherapistBarthelPatientHS { get; set; }
        public PatientPhysiotherapistBarthelPatientResultHS PatientPhysiotherapistBarthelPatientResultHS { get; set; }
    }

    public class PhysiotherapistPatientBarthelPhysiotherapistViewModelHS
    {
        public PatientPhysiotherapistBarthelPhysiotherapistHS PatientPhysiotherapistBarthelPhysiotherapistHS { get; set; }
        public PatientPhysiotherapistBarthelPhysiotherapistResultHS PatientPhysiotherapistBarthelPhysiotherapistResultHS { get; set; }
    }

    public class PhysiotherapistPatientVisitAndPatientCardViewModelHS
    {
        public PatientPhysiotherapistVisitHS PatientPhysiotherapistVisitHS { get; set; }
        public PatientPhysiotherapistPatientCardHS PatientPhysiotherapistPatientCardHS { get; set; }
    }

    public class PhysiotherapistPatientBarthelPatientViewModelHD
    {
        public PatientPhysiotherapistBarthelPatientHD PatientPhysiotherapistBarthelPatientHD { get; set; }
        public PatientPhysiotherapistBarthelPatientResultHD PatientPhysiotherapistBarthelPatientResultHD { get; set; }
    }

    public class PhysiotherapistPatientBarthelPhysiotherapistViewModelHD
    {
        public PatientPhysiotherapistBarthelPhysiotherapistHD PatientPhysiotherapistBarthelPhysiotherapistHD { get; set; }
        public PatientPhysiotherapistBarthelPhysiotherapistResultHD PatientPhysiotherapistBarthelPhysiotherapistResultHD { get; set; }
    }

    public class PhysiotherapistPatientVisitAndPatientCardViewModelHD
    {
        public PatientPhysiotherapistVisitHD PatientPhysiotherapistVisitHD { get; set; }
        public PatientPhysiotherapistPatientCardHD PatientPhysiotherapistPatientCardHD { get; set; }
    }
}