﻿using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hospice.Models
{
    public class PatientRegisteryFinalizingAdmissionContactPersonsHS
    {
        [UIHint("StringListTemplate")]
        [Display(Name = "Dane osób do kontaktu")]
        [NotMapped]
        public List<string> CalculatedOsoby { get; set; }
    }


}