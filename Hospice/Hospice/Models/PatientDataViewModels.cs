﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hospice.Domain.Entities;

namespace Hospice.Models
{
    public class PatientRegisteryRiskAssessmentFormViewModelHS
    {
        public PatientRegisteryRiskAssessmentFormHS PatientRegisteryRiskAssessmentFormHS { get; set; }
        public PatientRegisteryRiskAssessmentFormResultHS PatientRegisteryRiskAssessmentFormResultHS { get; set; }
    }
}