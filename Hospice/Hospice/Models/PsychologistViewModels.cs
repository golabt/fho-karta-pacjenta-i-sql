﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hospice.Domain.Entities;

namespace Hospice.Models
{
    public class PsychologistPatientQuestionnaireHADSMViewModelHS
    {
        public PatientPsychologistQuestionnaireHADSMHS PatientPsychologistQuestionnaireHADSMHS { get; set; }
        public PatientPsychologistQuestionnaireHADSMResultHS PatientPsychologistQuestionnaireHADSMResultHS { get; set; }
    }

    public class PsychologistPatientGeriatricEvaluationScaleViewModelHS
    {
        public PatientPsychologistGeriatricEvaluationScaleHS PatientPsychologistGeriatricEvaluationScaleHS { get; set; }
        public PatientPsychologistGeriatricEvaluationScaleResultHS PatientPsychologistGeriatricEvaluationScaleResultHS { get; set; }
    }

    public class PsychologistPatientMMSETestViewModelHS
    {
        public PatientPsychologistMMSETestHS PatientPsychologistMMSETestHS { get; set; }
        public PatientPsychologistMMSETestResultHS PatientPsychologistMMSETestResultHS { get; set; }
    }

    public class PsychologistPatientQuestionnaireHADSMViewModelHD
    {
        public PatientPsychologistQuestionnaireHADSMHD PatientPsychologistQuestionnaireHADSMHD { get; set; }
        public PatientPsychologistQuestionnaireHADSMResultHD PatientPsychologistQuestionnaireHADSMResultHD { get; set; }
    }

    public class PsychologistPatientGeriatricEvaluationScaleViewModelHD
    {
        public PatientPsychologistGeriatricEvaluationScaleHD PatientPsychologistGeriatricEvaluationScaleHD { get; set; }
        public PatientPsychologistGeriatricEvaluationScaleResultHD PatientPsychologistGeriatricEvaluationScaleResultHD { get; set; }
    }

    public class PsychologistPatientMMSETestViewModelHD
    {
        public PatientPsychologistMMSETestHD PatientPsychologistMMSETestHD { get; set; }
        public PatientPsychologistMMSETestResultHD PatientPsychologistMMSETestResultHD { get; set; }
    }
}