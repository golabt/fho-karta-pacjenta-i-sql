﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hospice.Models
{
    public class Category
    {
        public string ID { get; set; }
        public string Parent_ID { get; set; }
        public string Name { get; set; }
        public int FldId { get; set; }
        public string Area { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public Guid? FileGUID { get; set; }
    }

    public class SeededCategories
    {
        public string Seed { get; set; }
        public IList<Category> Categories { get; set; }
    }
}