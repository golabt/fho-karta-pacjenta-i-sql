﻿ecgDimensions.currentWindow = null;
ecgDimensions.UNITS = {
    rr: "ms",
    hr: "/min",
    p: "ms",
    qrs: "ms",
    t: "ms",
    qt: "ms",
    pq: "ms",
    qtc: "ms",
    axp: "\u00b0",
    axqrs: "\u00b0",
    axt: "\u00b0",
    measure: "ms"
};
function ecgDimensions(a, b, c) {
    this.startY = this.startX = 0;
    this.lastParams = this.lastLine = null;
    this.lineStarted = !1;
    this.pressedButton = null;
    this.dimensions = b;
    this.panel = a;
    this.onSave = c;
    this.onClose = null;
    ecgDimensions.currentWindow = this;
    $("input[id|=w]").click(function () {
        ecgDimensions.currentWindow.startMark(this)
    });
    0 < $("input[id|=w]").length && (a.enableAdjust = !0);
    this.panel.getDimensions = this.getDimensions;
}
ecgDimensions.prototype.reset = function () {
    $(document).unbind("mousemove", this.mouseMoving);
    this.panel.unbindMouse(this.toggleLine);
    this.lastLine && (this.panel.remove(this.lastLine), this.lastLine = null);
    this.lastParams && (this.lastParams.style.display = "none");
    this.lineStarted = !1;
    this.pressedButton.checked = !1;
    this.pressedButton = null
};
ecgDimensions.prototype.startMark = function (a) {
    if (this.pressedButton) {
        var b = this.pressedButton;
        this.reset();
        if (b == a)
            return
    }
    null == this.pressedButton && (this.panel.bindMouse(this.toggleLine), this.pressedButton = a)
};
ecgDimensions.prototype.toggleLine = function (a) {
    if (null == ecgDimensions.currentWindow)
        return !1;
    if (2 == a.button)
        return ecgDimensions.currentWindow.reset(), !1;
    ecgDimensions.currentWindow.lineStarted ? (ecgDimensions.currentWindow.lineStarted = !1, ecgDimensions.currentWindow.endingDrawing(a)) : (ecgDimensions.currentWindow.lineStarted = !0, ecgDimensions.currentWindow.startingDrawing(a));
    return !1
};
ecgDimensions.prototype.startingDrawing = function (a) {
    var b = this.offset();
    this.startX = a.pageX - b.left;
    this.startY = a.pageY - b.top;
    $(document).bind("mousemove", this.mouseMoving)
};
ecgDimensions.prototype.mouseMoving = function (a) {
    if (null == ecgDimensions.currentWindow)
        return !1;
    var b = ecgDimensions.currentWindow.offset(),
		c = a.pageX - b.left,
		b = a.pageY - b.top;
    null == ecgDimensions.currentWindow.lastLine ? ecgDimensions.currentWindow.lastLine = ecgDimensions.currentWindow.panel.line(ecgDimensions.currentWindow.startX, ecgDimensions.currentWindow.startY, c, b) : ecgDimensions.currentWindow.panel.lineTo(ecgDimensions.currentWindow.lastLine, c, b);
    ecgDimensions.currentWindow.measureParams(a)
};
ecgDimensions.prototype.measureParams = function (a) {
    var b = this.offset(),
		c = a.pageY - b.top,
		b = Math.round(4 * Math.abs(this.startX - (a.pageX - b.left)) * this.panel.getXfactor()),
		c = Math.round((this.startY - c) / (10 * this.panel.getYfactor())) / 10;
    null == this.lastParams && ($("body").append('<div id="mesureParams"></dir>'), this.lastParams = $("#mesureParams").get(0));
    this.lastParams.style.display = "block";
    this.lastParams.style.top = a.pageY - 16 + "px";
    this.lastParams.style.left = a.pageX + 16 + "px";
    this.lastParams.innerHTML = b + "ms<br/>" +
	c + "mV"
};
ecgDimensions.prototype.calculateMeasurments = function () { };
ecgDimensions.prototype.endingDrawing = function (a) {
    if (null != this.pressedButton) {
        var b = this.offset(),
			a = a.pageX - b.left,
			b = Math.round(4 * Math.abs(this.startX - a) * this.panel.getXfactor());
        this.dimensions[this.pressedButton.id.substr(2)] = {
            size: b,
            start: 4 * this.startX * this.panel.getXfactor(),
            stop: 4 * a * this.panel.getXfactor()
        };
        this.calculateMeasurments(this.dimensions, this.panel.leadData);
        if (this.onSave)
            this.onSave(this.dimensions);
        this.panel.redrawAllDims()
    }
    this.reset()
};
ecgDimensions.prototype.getDimensions = function () {
    return {};
    // return null == ecgDimensions.currentWindow ? {}

    // 	: ecgDimensions.currentWindow.dimensions
};
ecgDimensions.setValueUnit = function (a, b) {
    var c = a.value,
		d = /[0-9]$/i;
    "" != c && (null != c.match(d) && "" != b && null != ecgDimensions.UNITS[b]) && (c += ecgDimensions.UNITS[b]);
    a.value = c
};
ecgDimensions.prototype.offset = function () {
    return this.panel.offset()
};
var ecgPanel = null, ecgData = null, jQready = !1, ecgDataReady = !1, eventUrl = null, mainEcg = null, otherEcg = null, waitForOther = "CZEKAJ", ecgDimMeasure = null, currXscale = null, currYscale = null, hMargin = 215, vMargin = 180, sMargin = 27, sliderExams, sliderQrs;
function factorChange(a) {
    ecgPanel.scaleChange(a);
    adjustWrapped()
}
function adjustWrapped() {
    resizeWrapped();
    ecgPanel.setWindowHeight(spaceHeight())
}
function storeValues(a) {
    for (var b in a) {
        var c = $("input[name=fw" + b + "]");
        if (1 == c.length) {
            var d = ecgDimensions.UNITS[b],
				e = a[b];
            if (!(null == e.size || 999 == e.size))
                if (c.get(0).value = e.size + (null == d ? "" : d), null != e.start && (c = $("input[name=fw" + b + "start]"), 1 == c.length && (c.get(0).value = e.start)), null != e.stop)
                    c = $("input[name=fw" + b + "stop]"), 1 == c.length && (c.get(0).value = e.stop)
        }
    }
}
function getLeadNr(a, b) {
    for (var c = 0; c < b.length() ; c++)
        if (b.chanel(c) == a)
            return c;
    return -1
}
function calcAxis(a, b, c) {
    if (null == c || null == a || null == b)
        return 999;
    var d = Math.round(250 * a / 1E3),
		e = Math.round(250 * b / 1E3),
		f = getLeadNr("I", c),
		g = getLeadNr("aVF", c);
    if (0 > f || 0 > g || !c.valid(f) || !c.valid(g))
        return 999;
    for (var h = (c.lead(f, d) + c.lead(f, e)) / 2, i = (c.lead(g, d) + c.lead(g, e)) / 2, l = d <= e ? 1 : -1, a = b = 0; d != e; d += l)
        b += c.lead(f, d) - h, a += c.lead(g, d) - i;
    if (0 == b && 0 < a)
        return 90;
    if (0 == b && 0 > a)
        return -90;
    if (0 < b && 0 == a)
        return 0;
    if (0 > b && 0 == a)
        return 180;
    if (0 == b && 0 == a)
        return 999;
    c = Math.round(Math.round(180 * Math.atan(a / b) / Math.PI));
    0 > b && 0 < a && (c += 180);
    0 > b && 0 > a && (c -= 180);
    -180 > c && (c += 360);
    180 < c && (c -= 360);
    return c
}
ecgDimensions.prototype.calculateMeasurments = function (a, b) {
    if (a.rr && 0 < a.rr.size) {
        var c = Math.round(6E4 / a.rr.size);
        a.hr = {
            size: c
        }
    }
    a.p && a.qrs && (c = Math.round(a.qrs.start - a.p.start), a.pq = {
        size: c
    }, a.pr = {
        size: c
    });
    a.t && a.qrs && (c = Math.round(a.t.stop - a.qrs.start), a.qt = {
        size: c
    });
    a.rr && a.qt && (c = Math.round(a.qt.size / Math.sqrt(a.rr.size / 1E3)), a.qtc = {
        size: c
    });
    a.p && (c = calcAxis(a.p.start, a.p.stop, b), a.axp = {
        size: c
    });
    a.qrs && (c = calcAxis(a.qrs.start, a.qrs.stop, b), a.axqrs = {
        size: c
    });
    a.t && (c = calcAxis(a.t.start, a.t.stop, b),
		a.axt = {
		    size: c
		})
};
function slideGraph(a) {
    ecgPanel.changeDistance("incDist" == a ? 8 : -8);
    adjustWrapped()
}
function zoom(a) {
    ecgPanel.changeZoom("zoomIn" == a ? 1 : -1);
    adjustWrapped()
}
function resizeWrapped() {
    var a = spaceHeight(),
		b = window.innerWidth - hMargin,
		c = 0;
    ecgPanel.setWindowHeight(a) || c++;
    parseInt($("#page").css("min-width")) >= window.innerWidth ? c++ : ecgPanel.setWindowWidth(b) || c++;
    if (2 == c)
        return !0;
    $("#measure #descriptions DIV").css("width", b - 398 + "px");
    return !1
}
function initSize() {
    resizeWrapped() ? (ecgPanel.setWindowHeight(null), ecgPanel.setWindowWidth(null)) : ($("#examinationBottom").css("display", "none"), $("#footer").css("display", "none"), $(window).resize(resizeWrapped))
}
function openDescr() {
    "block" === $("#examinationBottom").css("display") ? closeDescr() : $("#examinationBottom").css("display", "block")
}
function closeDescr() {
    $("#examinationBottom").css("display", "none")
}
function cookieName() {
    return "CardioPortal.ecg." + graphKey.key + ".1"
}
function fieldVal(a) {
    var b = {},
		c = !0,
		d = $("input[name=fw" + a + "start]");
    0 < d.length && "" != d.get(0).value && (b.start = d.get(0).value, c = !1);
    d = $("input[name=fw" + a + "stop]");
    0 < d.length && "" != d.get(0).value && (b.stop = d.get(0).value, c = !1);
    d = $("input[name=fw" + a + "]");
    0 < d.length && "" != d.get(0).value && (b.size = d.get(0).value, c = !1);
    return c ? null : b
}
function initDim() {
    for (var a = {}, b = "rr hr p t qrs axp axt axqrs pq qtc qt".split(" "), c = 0; c < b.length; c++) {
        var d = fieldVal(b[c]);
        null != d && (a[b[c]] = d)
    }
    $("input[name^=fw][type=text]").each(function () {
        $(this).attr("disabled", !0);
        "" != this.value && ecgDimensions.setValueUnit(this, this.id.substr(2))
    });
    ecgDimMeasure = new ecgDimensions(ecgPanel, a, storeValues)
}
function beforeSubmit() {
    $("input[name^=fw][type=text]").removeAttr("disabled")
}
function spaceHeight() {
    return window.innerHeight - vMargin
}
function initAnalyse(a, b) {
    var c = !1;
    a.hrAvr && ($("#hrAvr").html(a.hrAvr + "/min"), c = !0);
    a.hrMin && ($("#hrMin").html(a.hrMin + "/min"), c = !0);
    a.hrMax && ($("#hrMax").html(a.hrMax + "/min"), c = !0);
    c && $("#hrResult").css("display", "inline-block");
    c = !1;
    if (a.avg) {
        var d = a.avg[2 <= a.avg.length ? 1 : 0].pqrst;
        if (d) {
            d.rr && (d.rr.start && d.rr.stop) && (d.rr.size = d.rr.stop - d.rr.start);
            d.p && (d.p.start && d.p.stop) && (d.p.size = d.p.stop - d.p.start);
            d.qrs && (d.qrs.start && d.qrs.stop) && (d.qrs.size = d.qrs.stop - d.qrs.start);
            d.t && (d.t.start &&
				d.t.stop) && (d.t.size = d.t.stop - d.t.start);
            ecgDimensions.prototype.calculateMeasurments(d, b);
            for (var e = "p t qrs axp axt axqrs pq qtc qt".split(" "), f = 0; f < e.length; f++) {
                var g = e[f],
					h = d[g];
                h && h.size && (h = h.size, 999 == h && 0 == g.search("ax") || ($("#avr" + g).html(h + ecgDimensions.UNITS[g]), c = !0))
            }
        }
    }
    c && $("#pqrstResult").css("display", "inline-block");
    c = $("filterAvgQrs").get(0);
    null != c && null != a.avg && (ecgPanel.setAvgWidth(), sliderQrs.display(c.checked))
}
function panelReady(a) {
    if (jQready && ecgDataReady) {
        initDim();
        a.holterStart = ecgData.holterStart;
        a.refElectrode = ecgData.refElectrode;
        var b = ecgData.analyse;
        a.rrPos = null == b ? null : b.rrPos;
        a.rrPosFiltered = null == b ? null : b.rrPosFiltered;
        a.setData(ecgData.data, spaceHeight(), null == b ? null : b.avg);
        a.redrawAllDims();
        a.centerToDim();
        sliderExams = new Slider(".slider", 0);
        sliderQrs = new Slider(".qrsSlider", sMargin);
        initSize();
        null != b && initAnalyse(b, a.avgLeadData);
        1 != ecgPanel.zoom && (ecgPanel.changeZoom(0), adjustWrapped())
    } else
        setTimeout(function () {
            panelReady(a)
        },
			200)
}
function useAverage() {
    var a = ecgData.analyse;
    if (a && a.avg)
        for (var b = 2 <= a.avg.length ? 1 : 0, c = "rr hr p t qrs axp axt axqrs pq qtc qt".split(" "), d = 0; d < c.length; d++) {
            var e = c[d],
				f = a.avg[b].pqrst[e];
            if (f && f.size && !(999 == f.size && 0 == e.search("ax"))) {
                var g = $("input[name=fw" + e + "]");
                1 == g.length && (e = ecgDimensions.UNITS[e], g.get(0).value = f.size + (null == e ? "" : e))
            }
        }
}
function clickOnEcg() {
    var a = this.id;
    if (a != mainEcg) {
        var b = this.firstChild.textContent,
			c = $("#other-button").get(0);
        c.disabled = !0;
        c.checked = !1;
        var d = $("#other-button + LABEL");
        d.text(waitForOther);
        $("#slider-button").get(0).checked = !1;
        sliderExams.show(!1);
        eventUrl && Tapestry.ajaxRequest(eventUrl + "/" + a, function (e) {
            200 == e.status && (ecgPanel.setOtherData(e.responseJSON.data), c.disabled = !1, c.checked = !0, ecgPanel.showOther(!0), d.text(b), otherEcg = a)
        })
    }
}
function showOtherExams(a) {
    a.show(!0);
    $("TABLE.t-data-grid TBODY TR").click(clickOnEcg);
    null != mainEcg && markMainEcg(mainEcg, "mainEcg");
    null != otherEcg && markMainEcg(otherEcg, "otherEcg")
}
function requestOther(a, b) {
    b.checked ? ($("otherExamZone").observe(Tapestry.ZONE_UPDATED_EVENT, function () {
        showOtherExams(a)
    }), $("otherExamUpdate").click()) : a.show(!1)
}
function sliderButton(a, b) {
    a.show(b.checked)
}
function showOther() {
    ecgPanel.showOther(this.checked)
}
function markMainEcg(a, b) {
    $("#ecgExams TR#" + a).addClass(b)
}
function setFilter50() {
    ecgPanel.setFilter50($("#filter50").get(0).checked)
}
function setFilterRR() {
    ecgPanel.setFilterRR($("#filterRR").get(0).checked)
}
function saveDefaults() {
    var a = $("#filter50").get(0),
		b = $("#filterRR").get(0),
		c = $("#filterAvgQrs").get(0),
		d = getDefaults(),
		e = {};
    a && (e.filter50 = a.checked);
    b && (e.filterRR = b.checked);
    c && (e.filterAvgQrs = c.checked);
    currXscale && (e.scaleX = currXscale);
    currYscale && (e.scaleY = currYscale);
    e.chanelDistance = ecgDisplay.chanelDistance;
    e.zoom = ecgPanel.zoom;
    a = jQuery.extend(d, e);
    storeDefaults(a)
}
function restoreDefaults() {
    var a = getDefaults();
    if (null != a) {
        if (a.filter50) {
            var b = $("#filter50").get(0);
            null != b && (b.checked = !0, setFilter50())
        }
        a.filterRR && (b = $("#filterRR").get(0), null != b && (b.checked = !0, setFilterRR()));
        a.filterAvgQrs && (b = $("#filterAvgQrs").get(0), null != b && (b.checked = !0));
        null != a.scaleX && (ecgPanel.scaleChange(a.scaleX), currXscale = a.scaleX, $("#" + currXscale).get(0).checked = !0);
        null != a.scaleY && (ecgPanel.scaleChange(a.scaleY), currYscale = a.scaleY, $("#" + currYscale).get(0).checked = !0);
        null != a.chanelDistance && (ecgDisplay.chanelDistance = a.chanelDistance);
        null != a.zoom && (ecgPanel.zoom = a.zoom)
    }
}
$(function () {
    ecgPanel = new ecgDisplay("#ecg", panelReady);
    jQready = !0;
    $("input[name=xfactor]").click(function () {
        currXscale = this.id;
        factorChange(this.id);
        saveDefaults()
    });
    $("input[name=yfactor]").click(function () {
        currYscale = this.id;
        factorChange(this.id);
        saveDefaults()
    });
    $("#wrap").click(openDescr);
    $("#hideDescription").click(closeDescr);
    $("#incDist").click(function () {
        slideGraph(this.id);
        saveDefaults()
    });
    $("#decDist").click(function () {
        slideGraph(this.id);
        saveDefaults()
    });
    $("#zoomIn").click(function () {
        zoom(this.id);
        saveDefaults()
    });
    $("#zoomOut").click(function () {
        zoom(this.id);
        saveDefaults()
    });
    $("#filter50").click(function () {
        setFilter50(this.id);
        saveDefaults()
    });
    $("#filterRR").click(function () {
        setFilterRR(this.id);
        saveDefaults()
    });
    $("#slider-button").click(function () {
        requestOther(sliderExams, this)
    });
    $("#filterAvgQrs").click(function () {
        sliderButton(sliderQrs, this);
        saveDefaults()
    });
    $("#other-button").click(showOther);
    $("input[type=submit]").click(beforeSubmit);
    $("#useAvr").click(useAverage);
    restoreDefaults();
    "function" === typeof Confirm && (Confirm.canAsk = function (a) {
        return $(a).hasClass("selected-exam")
    })
});



ecgDisplay.scale = 0.82;
ecgDisplay.yScale = 1E3;
ecgDisplay.xScale = 250;
ecgDisplay.rrScale = 50;
ecgDisplay.rrMin = 30;
ecgDisplay.chanelDistance = ecgDisplay.yScale * ecgDisplay.scale;
ecgDisplay.MIN_DISTANCE = 50;
ecgDisplay.COLOR = "blue red chocolate green black darkGreen brown".split(" ");
ecgDisplay.OTHER_COLOR = "#707070";
ecgDisplay.HOLTER_COLOR = "gray";
ecgDisplay.transformPrefixes = ["", "-webkit-", "-o-", "-moz-", "-ms-"];
ecgDisplay.dragControl = null;
ecgDisplay.dimDragControl = null;
ecgDisplay.filter50 = !1;
ecgDisplay.svgInitPath = "/CardioPortal/";

function ecgDisplay(a, b) {
    this.sizeLines = this.sigDrawing = this.drawing = null;
    this.diagramFactor = 2;
    this.xFactor = 500 / (ecgDisplay.xScale * ecgDisplay.scale);
    this.yFactor = 50 * ecgDisplay.scale / ecgDisplay.yScale;
    this.leadOtherData = this.leadData = null;
    this.otherVisible = !0;
    this.rrPosDrawing = this.rrPosFiltered = this.rrPos = this.holterStart = null;
    this.rrVisible = !1;
    this.height = 2 * ecgDisplay.chanelDistance;
    this.zoom = 1;
    this.enableAdjust = !1;
    this.ecg = $(a);
    this.ecg.html('<table><tr><td></td><td><div id="ecgScaleContainer"><div id="ecgScale"></div></div></td></tr><tr><td><div id="ecgSignatureContainer"><div id="ecgSignature"></div></div></td><td><div id="ecgDiagramContainer"><div id="ecgDiagram"></div><div id="ecgOtherDiagram"></div></div></td></tr></table>');
    this.scaleCont =
	$("#ecgScale");
    this.diagCont = $("#ecgDiagram");
    this.diagOtherCont = $("#ecgOtherDiagram");
    this.signCont = $("#ecgSignature");
    this.scrollCont = $("#ecgDiagramContainer");
    this.qrsSlider = this.qrsSliderCont = null;
    this.onReady = b;
    this.scrollCont.bind("contextmenu", function () {
        return !1
    });
    var c = this;
    this.svgQrsSlider = this.svgObjOtherDiag = this.svgObjSig = this.svgObjDiag = this.svgObjScale = null;
    this.scaleCont.svg({
        onLoad: function (a) {
            c.svgObjScale = a;
            c.ready()
        },
        initPath: ecgDisplay.svgInitPath
    });
    this.signCont.svg({
        onLoad: function (a) {
            c.svgObjSig =
			a;
            c.ready()
        },
        initPath: ecgDisplay.svgInitPath
    });
    this.diagCont.svg({
        onLoad: function (a) {
            c.svgObjDiag = a;
            c.ready()
        },
        initPath: ecgDisplay.svgInitPath
    });
    this.diagOtherCont.svg({
        onLoad: function (a) {
            c.svgObjOtherDiag = a
        },
        initPath: ecgDisplay.svgInitPath
    });
    var d = $("#ecgScaleContainer").get(0),
		e = $("#ecgDiagramContainer").get(0),
		f = $("#ecgSignatureContainer").get(0),
		g = this;
    this.scrollCont.scroll(function () {
        var a = e.scrollTop,
			b = e.scrollLeft;
        f.scrollTop = a;
        d.scrollLeft = b;
        null != g.qrsSlider && g.qrsSlider.scrollTop(a)
    });
    this.getDimensions = function () {
        return {};

    };
    this.enableDrag(!0)
}
ecgDisplay.prototype.bindMouse = function (a) {
    this.enableDrag(!1);
    this.scrollCont.bind("mousedown", a)
};
ecgDisplay.prototype.unbindMouse = function (a) {
    this.scrollCont.unbind("mousedown", a);
    this.enableDrag(!0)
};
ecgDisplay.prototype.enableDrag = function (a) {
    var b = $("body");
    a ? (ecgDisplay.dragControl = {
        moving: !1,
        other: !1,
        dragOffset: null,
        diagCont: this.diagCont,
        diagOtherCont: this.diagOtherCont,
        scroller: this.scrollCont
    }, ecgDisplay.dimDragControl = {
        marker: null,
        panel: this
    }, this.scrollCont.bind("mousedown", this.startDrag), b.bind("mouseup", this.stopDrag), b.bind("mousemove", this.doDrag), setCursor(this.scrollCont, "grabbable"), this.allowAdjust(!0)) : (this.scrollCont.unbind("mousedown", this.startDrag), b.unbind("mouseup",
		this.stopDrag), b.unbind("mousemove", this.doDrag), setCursor(this.scrollCont, ""), this.allowAdjust(!1), ecgDisplay.dragControl = null, ecgDisplay.dimDragControl = null)
};
function setCursor(a, b) {
    a && (a.removeClass("grabbing grabbable adjusting"), a.addClass(b))
}
function checkMarker(a, b, c) {
    var b = 4 * b.xFactor / b.zoom,
		c = c * b,
		b = 2 * b,
		d;
    for (d in a)
        if (a[d].start && a[d].stop) {
            if (Math.abs(a[d].start - c) <= b)
                return {
                    dim: d,
                    point: "start"
                };
            if (Math.abs(a[d].stop - c) <= b)
                return {
                    dim: d,
                    point: "stop"
                }
        }
    return null
}
ecgDisplay.prototype.startDrag = function (a) {
    if (ecgDisplay.dimDragControl) {
        var b = ecgDisplay.dimDragControl.panel;
        if (b.enableAdjust) {
            var c = ecgDisplay.dimDragControl.panel.offset(),
				//b = checkMarker(b.getDimensions(), b, a.pageX - c.left);
				b = checkMarker({}, b, a.pageX - c.left);
            if (null != b) {
                ecgDisplay.dimDragControl.marker = b;
                a.preventDefault();
                setCursor(ecgDisplay.dragControl.scroller, "adjusting");
                return
            }
        }
    }
    if (ecgDisplay.dragControl && !isOnScrollerClick(a) && (a.preventDefault(), ecgDisplay.dragControl.other = 2 == a.button, ecgDisplay.dragControl.other ? (b = ecgDisplay.dragControl.diagOtherCont.css("left"),
		b = parseInt(b.substr(0, b.length - 2), 10), c = ecgDisplay.dragControl.diagOtherCont.css("top"), c = parseInt(c.substr(0, c.length - 2), 10), ecgDisplay.dragControl.dragOffset = {
        left: b - a.pageX,
        top: c - a.pageY
    }) : ecgDisplay.dragControl.dragOffset = {
        left: ecgDisplay.dragControl.scroller.scrollLeft() + a.pageX,
        top: ecgDisplay.dragControl.scroller.scrollTop() + a.pageY
    }, !ecgDisplay.dragControl.moving))
        ecgDisplay.dragControl.moving = !0, setCursor(ecgDisplay.dragControl.scroller, "grabbing")
};
function isOnScrollerClick(a) {
    if (!ecgDisplay.dragControl.scroller)
        return !1;
    var b = ecgDisplay.dragControl.scroller.offset(),
		c = a.pageX - b.left,
		a = a.pageY - b.top,
		b = ecgDisplay.dragControl.scroller.get(0).clientWidth,
		d = ecgDisplay.dragControl.scroller.get(0).clientHeight;
    return c >= b || a >= d ? !0 : !1
}
ecgDisplay.prototype.stopDrag = function (a) {
    if (ecgDisplay.dimDragControl) {
        var b = ecgDisplay.dimDragControl.marker;
        if (null != b) {
            var c = ecgDisplay.dimDragControl.panel,
				d = {},//c.getDimensions(),
				a = a.pageX - c.offset().left,
				e = d[b.dim];
            e[b.point] = 4 * a * c.getXfactor();
            d[b.dim].size = Math.round(Math.abs(e.start - e.stop));
            ecgDimensions.prototype.calculateMeasurments(d, c.leadData);
            storeValues(d);
            ecgDisplay.dimDragControl.marker = null;
            setCursor(ecgDisplay.dragControl.scroller, "grabbable");
            return
        }
    }
    ecgDisplay.dragControl && ecgDisplay.dragControl.moving &&
	(ecgDisplay.dragControl.moving = !1, setCursor(ecgDisplay.dragControl.scroller, "grabbable"))
};
ecgDisplay.prototype.doDrag = function (a) {
    if (ecgDisplay.dimDragControl) {
        var b = ecgDisplay.dimDragControl.marker;
        if (null != b) {
            var c = ecgDisplay.dimDragControl.panel,
				a = (a.pageX - c.offset().left) / c.zoom,
				b = $("#" + b.dim + "DIM" + b.point).get(0);
            b.setAttribute("x1", a);
            b.setAttribute("x2", a);
            return
        }
    }
    ecgDisplay.dragControl && ecgDisplay.dragControl.moving && (ecgDisplay.dragControl.other ? ecgDisplay.dragControl.diagOtherCont.css({
        left: ecgDisplay.dragControl.dragOffset.left + a.pageX + "px",
        top: ecgDisplay.dragControl.dragOffset.top +
		a.pageY + "px"
    }) : (ecgDisplay.dragControl.scroller.scrollLeft(ecgDisplay.dragControl.dragOffset.left - a.pageX), ecgDisplay.dragControl.scroller.scrollTop(ecgDisplay.dragControl.dragOffset.top - a.pageY)))
};
ecgDisplay.prototype.ready = function () {
    if (null != this.onReady && null != this.svgObjDiag && null != this.svgObjSig && null != this.svgObjScale)
        this.onReady(this)
};
ecgDisplay.prototype.setData = function (a, b, c) {
    this.leadData = this.calculateLeads(a);
    this.height = ecgDisplay.chanelDistance * (this.leadData.length() + 1);
    b -= 10;
    null != b && this.height > b && (a = b, b = a / (this.leadData.length() + 1), b >= ecgDisplay.MIN_DISTANCE ? (this.height = a, ecgDisplay.chanelDistance = b) : (ecgDisplay.chanelDistance = ecgDisplay.MIN_DISTANCE, this.height = ecgDisplay.chanelDistance * (this.leadData.length() + 1)));
    if (null != c) {
        this.avgMarkers = [];
        a = Number.MAX_VALUE;
        for (b = 0; b < c.length; b++) {
            null != c[b].pqrst && (this.avgMarkers[b] =
				c[b].pqrst);
            var d = c[b].sample.length;
            0 < d && d < a && (a = d)
        }
        this.avgLen = a;
        for (b = 0; b < c.length; b++)
            c[b].sample.length > a && c[b].sample.splice(a, c[b].sample.length - a + 1), c[b].sample = c[b].sample.concat(c[b].sample);
        this.avgLeadData = this.calculateLeads(c);
        this.makeQrsSlider()
    }
    this.drawDiagram()
};
ecgDisplay.prototype.makeQrsSlider = function () {
    this.ecg.append('<div id="qrsSliderContainer" class="qrsSlider width"><div id="qrsSlider"></div></div>');
    this.qrsSliderCont = $("#qrsSlider");
    this.qrsSlider = $("#qrsSliderContainer");
    var a = this;
    this.qrsSliderCont.svg({
        onLoad: function (b) {
            a.svgQrsSlider = b
        },
        initPath: ecgDisplay.svgInitPath
    })
};
ecgDisplay.prototype.setOtherData = function (a) {
    this.otherVisible = !0;
    this.leadOtherData = this.calculateLeads(a);
    this.diagOtherCont.css({
        left: "0px",
        top: "0px"
    });
    this.drawOtherDiagram()
};
ecgDisplay.prototype.showOther = function (a) {
    this.otherVisible = a;
    this.diagOtherCont.css({
        display: a ? null : "none"
    })
};
ecgDisplay.prototype.calculateLeads = function (a) {
    2 <= a.length && ("I" == a[0].chanel && "II" == a[1].chanel) && a.splice(2, 0, {
        chanel: "III",
        calc: function (a, c) {
            return c - a
        }
    }, {
        chanel: "aVR",
        calc: function (a, c) {
            return -(a + c) / 2
        }
    }, {
        chanel: "aVL",
        calc: function (a, c) {
            return a - c / 2
        }
    }, {
        chanel: "aVF",
        calc: function (a, c) {
            return c - a / 2
        }
    });
    return {
        lead: function (a, c) {
            var d;
            d = null != this.values[a].calc ? this.values[a].calc(this.values[0].sample[c], this.values[1].sample[c]) : this.values[a].sample[c];
            ecgDisplay.filter50 ? (null == this.values[a].filter &&
				(this.values[a].filter = new Filter50), d = this.values[a].filter.doFilter(d)) : this.values[a].filter = null;
            return d
        },
        length: function () {
            return this.values.length
        },
        chanel: function (a) {
            return this.values[a].chanel
        },
        samples: function () {
            return this.values[0].sample.length
        },
        valid: function (a) {
            return null != this.values[a].calc || null != this.values[a].sample
        },
        values: a
    }
};
ecgDisplay.prototype.chanelColor = function (a) {
    return ecgDisplay.COLOR[a % ecgDisplay.COLOR.length]
};
ecgDisplay.prototype.otherColor = function () {
    return ecgDisplay.OTHER_COLOR
};
ecgDisplay.prototype.setWidthAndHeight = function (a, b) {
    this.diagCont.css({
        width: a + "px",
        height: b + "px"
    });
    this.signCont.css("heigh", b + 20 + "px");
    this.scaleCont.css("width", a + 20 + "px")
};
ecgDisplay.prototype.scaleChange = function (a) {
    "50mm/s" == a && (this.xFactor = 250 / (ecgDisplay.xScale * ecgDisplay.scale));
    "25mm/s" == a && (this.xFactor = 500 / (ecgDisplay.xScale * ecgDisplay.scale));
    "10mm/s" == a && (this.xFactor = 1250 / (ecgDisplay.xScale * ecgDisplay.scale));
    "20mm/mV" == a && (this.yFactor = 100 * ecgDisplay.scale / ecgDisplay.yScale);
    "10mm/mV" == a && (this.yFactor = 50 * ecgDisplay.scale / ecgDisplay.yScale);
    "5mm/mV" == a && (this.yFactor = 25 * ecgDisplay.scale / ecgDisplay.yScale);
    this.drawDiagram();
    this.redrawAllDims()
};
ecgDisplay.sizeColor = {
    p: "red",
    qrs: "blue",
    t: "green",
    rr: "gray",
    rrPos: "black"
};
ecgDisplay.prototype.redrawAllDims = function () {
    this.resetAll(!0, !1);
    this.drawAllDims({});//this.getDimensions());
    this.allowAdjust(!0)
};
ecgDisplay.prototype.allowAdjust = function (a) {
    $("svg g g line").css("cursor", a && this.enableAdjust ? "col-resize" : "inherit")
};
ecgDisplay.prototype.drawAllDims = function (a) {
    for (var b in a)
        this.drawDimension(a, b)
};
ecgDisplay.prototype.drawDimension = function (a, b) {
    a[b].start && a[b].stop && (null == this.sizeLines && (this.sizeLines = this.svgObjDiag.group(this.drawing)), this.drawVertMarker(this.sizeLines, a[b].start, b, "DIMstart"), this.drawVertMarker(this.sizeLines, a[b].stop, b, "DIMstop"))
};
ecgDisplay.prototype.drawVertMarker = function (a, b, c, d) {
    b /= 4 * this.xFactor;
    this.svgObjDiag.line(a, b, 0, b, this.height, {
        stroke: ecgDisplay.sizeColor[c],
        "stroke-width": 1,
        id: c + d
    })
};
ecgDisplay.prototype.centerToDim = function () {
    var a = 999999,
		b = 0,
		c = {},//this.getDimensions(),
		d;
    for (d in c)
        if (c[d].start && c[d].stop) {
            var e = c[d].start / (4 * this.xFactor),
				f = c[d].stop / (4 * this.xFactor);
            e < a && (a = e);
            f < a && (a = f);
            e > b && (b = e);
            f > b && (b = f)
        }
    999999 == a && 0 == b || this.scrollCont.scrollLeft((b + a) / 2 - this.scrollCont.width() / 2)
};
ecgDisplay.prototype.resetAll = function (a, b) {
    a && null != this.sizeLines && (this.svgObjDiag.remove(this.sizeLines), this.sizeLines = null);
    b && null != this.rrPosDrawing && (this.svgObjDiag.remove(this.rrPosDrawing), this.rrPosDrawing = null);
    b && null != this.drawing && (this.svgObjDiag.remove(this.drawing), this.drawing = null);
    b && null != this.sigDrawing && (this.svgObjSig.remove(this.sigDrawing), this.sigDrawing = null);
    b && null != this.scaleDrawing && (this.svgObjScale.remove(this.scaleDrawing), this.scaleDrawing = null);
    this.resetOther(a,
		b);
    this.resetAvg(a, b)
};
ecgDisplay.prototype.resetOther = function (a, b) {
    b && this.drawingOther && (this.svgObjOtherDiag.remove(this.drawingOther), this.drawingOther = null)
};
ecgDisplay.prototype.resetAvg = function (a, b) {
    b && this.drawingAvg && (this.svgQrsSlider.remove(this.drawingAvg), this.drawingAvg = null);
    b && this.sizeLinesAvg && (this.svgQrsSlider.remove(this.sizeLinesAvg), this.sizeLinesAvg = null)
};
ecgDisplay.prototype.remove = function (a) {
    this.svgObjDiag.remove(a)
};
ecgDisplay.prototype.line = function (a, b, c, d) {
    return this.svgObjDiag.line(null, a / this.zoom, b / this.zoom, c / this.zoom, d / this.zoom, {
        stroke: "red",
        "stroke-width": 1
    })
};
ecgDisplay.prototype.lineTo = function (a, b, c) {
    a.setAttribute("x2", b / this.zoom);
    a.setAttribute("y2", c / this.zoom)
};
ecgDisplay.prototype.drawOtherDiagram = function () {
    if (!(null == this.leadOtherData || 0 == this.leadOtherData.length())) {
        this.resetOther(!0, !0);
        var a = this.diagOtherCont.css("left"),
			a = parseInt(a.substr(0, a.length - 2), 10) * this.diagramFactor,
			b = this.leadOtherData.samples() / this.xFactor,
			c = ecgDisplay.chanelDistance * (this.leadOtherData.length() + 1);
        this.svgObjOtherDiag.configure({
            width: b,
            height: c
        });
        this.diagOtherCont.css({
            width: b + "px",
            height: c + "px"
        });
        this.drawingOther = this.svgObjOtherDiag.group();
        this.drawEcg(this.drawingOther,
			this.leadOtherData, this.svgObjOtherDiag, this.otherColor);
        this.diagramFactor != this.getXfactor() && this.diagOtherCont.css("left", a / this.getXfactor() + "px")
    }
};
ecgDisplay.prototype.setAvgWidth = function () {
    var a = this.avgLeadData.samples() / this.xFactor * this.zoom,
		b = this.getWindowHeight(),
		b = b - ("scroll" == $("#ecgDiagramContainer").css("overflowX") ? 16 : 0);
    this.svgQrsSlider.configure({
        width: a,
        height: this.height
    });
    this.qrsSliderCont.css({
        width: a + "px",
        height: this.height + "px"
    });
    this.qrsSlider.css({
        width: a + "px",
        height: b + "px"
    });
    b = this.qrsSlider.css("margin-right");
    b = parseInt(b.substr(0, b.length - 2), 10);
    0 > b && this.qrsSlider.css("margin-right", "-" + a + "px")
};
ecgDisplay.prototype.drawAvgDiagram = function () {
    if (!(null == this.avgLeadData || 0 == this.avgLeadData.length()))
        if (this.resetAvg(!0, !0), this.setAvgWidth(), this.drawingAvg = this.svgQrsSlider.group(), this.drawEcg(this.drawingAvg, this.avgLeadData, this.svgQrsSlider, this.chanelColor), null != this.avgMarkers && null != this.avgMarkers[1]) {
            null == this.sizeLinesAvg && (this.sizeLinesAvg = this.svgQrsSlider.group());
            for (var a in this.avgMarkers[1]) {
                var b = this.avgMarkers[1][a];
                b.start && this.drawAvgMarker(b.start, a);
                b.stop &&
				this.drawAvgMarker(b.stop, a)
            }
        }
};
ecgDisplay.prototype.drawAvgMarker = function (a, b) {
    var c = this.avgLen / this.xFactor + a / (4 * this.xFactor);
    this.svgQrsSlider.line(this.sizeLinesAvg, c, 0, c, this.height, {
        stroke: ecgDisplay.sizeColor[b],
        "stroke-width": 1
    })
};
ecgDisplay.prototype.drawDiagram = function () {
    if (!(null == this.leadData || 0 == this.leadData.length())) {
        var a = (this.scrollCont.scrollLeft() + this.scrollCont.width() / 2) * this.diagramFactor;
        this.resetAll(!0, !0);
        var b = this.leadData.samples();
        this.setWidthAndHeight(b / this.xFactor, this.height);
        this.svgObjScale.configure({
            width: b / this.xFactor
        });
        this.svgObjDiag.configure({
            width: b / this.xFactor,
            height: this.height
        });
        this.svgObjSig.configure({
            height: this.height + 20
        });
        this.drawing = this.svgObjDiag.group();
        this.drawEcg(this.drawing,
			this.leadData, this.svgObjDiag, this.chanelColor);
        if (this.holterStart) {
            var c = this.holterStart / (4 * this.xFactor);
            this.svgObjDiag.line(this.drawing, c, 0, c, this.height, {
                stroke: ecgDisplay.HOLTER_COLOR,
                "stroke-width": 2,
                "stroke-dasharray": "10,10"
            })
        }
        if (this.rrPos) {
            this.mkRrDrawing();
            for (var d = 0; d < this.rrPos.length; d++)
                this.drawVertMarker(this.rrPosDrawing, this.rrPos[d], "rrPos", "DIMrrPos")
        }
        if (this.rrPosFiltered) {
            this.mkRrDrawing();
            for (var e = this.height, d = 0; d < this.rrPosFiltered.length; d++) {
                for (var f = [], g = this.rrPosFiltered[d],
					h = 0; h < g.length - 1; h++) {
                    var i = g[h + 1] - g[h];
                    0 != i && (c = (g[h + 1] + g[h]) / 2 / (4 * this.xFactor), f.push([c, e - 50 * (6E4 / i - ecgDisplay.rrMin) * ecgDisplay.scale / ecgDisplay.rrScale]))
                }
                0 < f.length && this.svgObjDiag.polyline(this.rrPosDrawing, f, {
                    fill: "none",
                    stroke: "red",
                    strokeWidth: 2,
                    "stroke-linejoin": "round",
                    id: "rrPosDIMrrPos"
                })
            }
        }
        this.scaleDrawing = this.svgObjScale.group();
        for (c = 0; c <= b; c += 250)
            this.svgObjScale.text(this.scaleDrawing, c / this.xFactor - 4, 10, "" + c / 250);
        this.drawOtherDiagram();
        null != this.avgLeadData && this.drawAvgDiagram();
        this.diagramFactor != this.getXfactor() && (this.scrollCont.scrollLeft(a / this.getXfactor() - this.scrollCont.width() / 2), this.diagramFactor = this.getXfactor());
        this.drawSignature()
    }
};
ecgDisplay.prototype.mkRrDrawing = function () {
    null == this.rrPosDrawing && (this.rrPosDrawing = this.svgObjDiag.group(), this.setFilterRR(this.rrVisible))
};
ecgDisplay.prototype.drawEcg = function (a, b, c, d) {
    for (var e = b.samples(), f = ecgDisplay.chanelDistance, g = 0; g < b.length() ; g++) {
        var h = [],
			i = 0;
        if (1 <= this.xFactor)
            for (var l = 0, j = 0, m = this.xFactor / 2, i = j, k = 0; k < e; k++)
                i = b.lead(g, k), m++, i = Math.round(100 * (f - i * this.yFactor)) / 100, m >= this.xFactor ? (h.push([l++, i]), m -= this.xFactor, j = i) : i != j && (h.push([l, i]), j = i);
        else
            for (k = 0; k < e; k++)
                i = b.lead(g, k), h.push([k / this.xFactor, f - i * this.yFactor]);
        c.polyline(a, h, {
            fill: "none",
            stroke: d(g),
            strokeWidth: 1,
            "stroke-linejoin": "round",
            id: b.chanel(g)
        });
        f += ecgDisplay.chanelDistance
    }
};
ecgDisplay.prototype.drawSignature = function () {
    this.sigDrawing = this.svgObjSig.group();
    for (var a = ecgDisplay.yScale * this.yFactor, b = 0; b < this.leadData.length() ; b++) {
        var c = ecgDisplay.chanelDistance * (b + 1);
        this.svgObjSig.polyline(this.sigDrawing, [[0, c], [5, c], [5, c - a], [20, c - a], [20, c], [25, c]], {
            fill: "none",
            stroke: this.chanelColor(b),
            strokeWidth: 1
        });
        var d = this.leadData.chanel(b);
        this.svgObjSig.text(this.sigDrawing, 10 - 1.5 * d.length, c - a / 2 + 5, d, {
            fill: this.chanelColor(b)
        })
    }
    a = this.refElectrode;
    if (null != a) {
        for (var b =
			0, c = ecgDisplay.chanelDistance * (this.leadData.length() + 1) + 15, d = this.svgObjSig.createText(), e = 0; e < a.length; e++)
            d.span(a[e], {
                x: 0,
                dy: b
            }), b += 10;
        this.svgObjSig.text(this.sigDrawing, 15, c, d, {
            transform: "rotate(90,15," + c + ")",
            id: "refElectrode"
        })
    }
};
ecgDisplay.prototype.getXfactor = function () {
    return this.xFactor / this.zoom
};
ecgDisplay.prototype.getYfactor = function () {
    return this.yFactor * this.zoom * ecgDisplay.yScale / 100
};
ecgDisplay.prototype.offset = function () {
    return this.diagCont.offset()
};
ecgDisplay.prototype.setWindowHeight = function (a) {
    var b = $("#ecgDiagramContainer"),
		c = !0,
		d = a;
    null != a ? (this.getWindowHeight() > a && (c = !1), d = this.diagCont.height() * this.zoom, a > d ? (a = d + ("scroll" == b.css("overflowX") ? 20 : 0), b.css("overflowY", "hidden")) : b.css("overflowY", "scroll"), d = a + "px") : b.css("overflowY", "scroll");
    $("#ecgSignatureContainer").css("height", d);
    b.css("height", d);
    null != this.qrsSlider && null != a && (a -= "scroll" == b.css("overflowX") ? 16 : 0, this.qrsSlider.css("height", a + "px"));
    return c
};
ecgDisplay.prototype.getWindowHeight = function () {
    return $("#ecgDiagramContainer").height()
};
ecgDisplay.prototype.setWindowWidth = function (a) {
    var b = $("#ecgDiagramContainer"),
		c = !0;
    if (null != a) {
        this.getWindowWidth() > a && (c = !1);
        var d = this.diagCont.width() * this.zoom;
        a > d ? (a = d + ("scroll" == b.css("overflowY") ? 20 : 0), b.css("overflowX", "hidden")) : b.css("overflowX", "scroll");
        a += "px"
    } else
        b.css("overflowX", "scroll");
    $("#ecgScaleContainer").css("width", a);
    b.css("width", a);
    return c
};
ecgDisplay.prototype.getWindowWidth = function () {
    return $("#ecgDiagramContainer").width()
};
ecgDisplay.prototype.setFilter50 = function (a) {
    ecgDisplay.filter50 = a;
    this.drawDiagram();
    this.redrawAllDims()
};
ecgDisplay.prototype.setFilterRR = function (a) {
    this.rrVisible = a;
    this.rrPosDrawing && $(this.rrPosDrawing).css("visibility", a ? "visible" : "hidden")
};
ecgDisplay.prototype.changeDistance = function (a) {
    ecgDisplay.chanelDistance += a;
    this.height = ecgDisplay.chanelDistance * (this.leadData.length() + 1);
    this.drawDiagram();
    this.redrawAllDims()
};
ecgDisplay.prototype.changeZoom = function (a) {
    a = this.zoom + a;
    if (!(1 > a || 10 < a)) {
        for (var b = (this.scrollCont.scrollLeft() + this.scrollCont.width() / 2) * this.diagramFactor, c = this.diagOtherCont.css("left"), c = parseInt(c.substr(0, c.length - 2), 10) * this.diagramFactor, d = this.diagOtherCont.css("top"), d = parseInt(d.substr(0, d.length - 2), 10) * this.diagramFactor, e = {}, f = {}, g = {}, h = "scale(" + a + ")", i = "scale(1," + a + ")", l = "scale(" + a + ",1)", j = 0; j < ecgDisplay.transformPrefixes.length; j++)
            e[ecgDisplay.transformPrefixes[j] + "transform"] =
			h, e[ecgDisplay.transformPrefixes[j] + "transform-origin"] = "0px 0px", f[ecgDisplay.transformPrefixes[j] + "transform"] = i, f[ecgDisplay.transformPrefixes[j] + "transform-origin"] = "0px 0px", g[ecgDisplay.transformPrefixes[j] + "transform"] = l, g[ecgDisplay.transformPrefixes[j] + "transform-origin"] = "0px 0px";
        this.diagCont.css(e);
        this.diagOtherCont.css(e);
        this.signCont.css(f);
        this.scaleCont.css(g);
        this.zoom = a;
        this.diagOtherCont.css({
            left: c / this.getXfactor() + "px",
            top: d / this.getXfactor() + "px"
        });
        this.scrollCont.scrollLeft(b /
			this.getXfactor() - this.scrollCont.width() / 2);
        this.diagramFactor = this.getXfactor();
        null != this.qrsSliderCont && (this.qrsSliderCont.css(e), this.setAvgWidth())
    }
};
function Filter50() {
    this.tail = [0, 0, 0, 0, 0];
    this.sum = 0;
    this.samples = 1;
    this.index = 0
}
Filter50.prototype.doFilter = function (a) {
    this.sum -= this.tail[this.index];
    this.sum += a;
    var b = this.sum / this.samples;
    this.tail[this.index] = a;
    5 > this.samples && this.samples++;
    this.index++;
    5 <= this.index && (this.index = 0);
    return b
};
function Slider(a, b) {
    this.visible = !1;
    this.selector = a;
    this.margin = b
}
Slider.prototype.show = function (a) {
    if (this.visible != a) {
        var b = this.margin;
        $(this.selector).each(function () {
            var c = Slider.getWidth(this) + b,
				c = (a ? "+" : "-") + "=" + c;
            $(this).animate({
                "margin-right": c
            })
        });
        this.visible = a
    }
};
Slider.prototype.display = function (a) {
    var b = this.margin;
    $(this.selector).each(function () {
        var c = Slider.getWidth(this),
			c = a ? b : -c;
        $(this).css({
            "margin-right": c + "px"
        })
    });
    this.visible = a
};
Slider.getWidth = function (a) {
    a = $(a).css("width");
    return a = parseInt(a.substr(0, a.length - 2), 10)
};
function storeDefaults(a) {
    var b = new Date,
		b = (new Date(b.getTime() + 7776E6)).toGMTString(),
		a = JSON.stringify(a),
		a = encodeURIComponent(a);
    document.cookie = cookieName() + "=" + a + ";expires=" + b + ";path=" + graphKey.path
}
function getDefaults() {
    var a = getCookie(cookieName()),
		a = decodeURIComponent(a);
    return null == a ? null : JSON.parse(a)
}
function getCookie(a) {
    for (var a = a + "=", b = document.cookie.split(";"), c = 0; c < b.length; c++) {
        var d = b[c].trim();
        if (0 == d.indexOf(a))
            return d.substring(a.length, d.length)
    }
    return null
};