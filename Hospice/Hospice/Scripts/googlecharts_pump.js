google.load('visualization', '1', { packages: ['corechart'] });
google.setOnLoadCallback(drawPumpCharts);


function drawPumpCharts() {
  try {
    drawStaticPumpChart();
  } catch (err) {
    console.log(err);
  }
  try {
    getLastDate();
    prepareDynamicChart();
    drawDynamicPumpChart();
  } catch (err) {
    console.log(err);
  }
}

function drawStaticPumpChart() {
  try {
    var data = new google.visualization.DataTable();
    var json = JSON.parse(infusionValue);

    var dateTime = json.dateTime;
    var flowRate = json.FlowRate;
    var vol = json.Volume;

    data.addColumn('string', 'Data');
    data.addColumn('number', 'Wydajność');
    data.addColumn('number', 'Podana objętość');

    var length = dateTime.length;

    var preparedData = [];
    for (var i = 0; i < length; i++) {
      var item = [];
      var dt = new Date(Date.parse(dateTime[i]));
      item.push(prepareDateFormat(dt, true));
      item.push(flowRate[i]);
      item.push(vol[i]);

      preparedData.push(item);
    }

    data.addRows(preparedData);

    var options = {
	  legend: 'top',
      width: window.innerWidth * 0.85,
      height: 500,
      curveType: 'none',
      lineWidth: 1,
      pointSize: 2,
      fontSize: 12,
      hAxis: {
        format: 'hh:mm dd/MM/yy',

        slantedText: true,
        slantedTextAngle: 45,
      },
	  vAxes: {
		0: {
			title: 'Wydajność', 
			titleTextStyle: {
				color: '#0000FF'
			},
			viewWindow : {
				min: 0
			},
			minorGridlines: {
				color: '#eee',
				count: 4,
			},
		},
		1: {
			title: 'Podana objętość',
			titleTextStyle: {
				color: '#FF0000'
			},
			viewWindow : {
				min: 0
			},
		},
	  },
	  series: {
		0: {
			targetAxisIndex:0
		},
        1: {
			targetAxisIndex:1
		}
	  },
	  explorer: {
		maxZoomIn:10,
		maxZoomOut: 2,
		keepInBounds: true,
	  }, 
    };

    var chart = new google.visualization.LineChart($('#infusionStaticChart')[0]);
    chart.draw(data, options);
  }
  catch (err) {
    console.log(err);
  }
}

var minDate;
var maxDate;
var selectedDate;

function prepareDynamicChart() {
  var parentControl = $('#infusionDynamicChart');
  var flowChartDiv = $('<div id="infusionDynamicFlowChartDiv"></div>');
  var volumeChartDiv = $('<div id="infusionDynamicVolChartDiv"></div>');

  var prevBtn = $('<button id="prevBtn" type="button" style="display: inline; margin: 5px">Wcześniej</button>');
  var dateLabel = $('<div style="display: inline; margin: 5px">' + prepareDateFormat(selectedDate) + '</div>');
  var nextBtn = $('<button id="nextBtn" type="button" disabled style="display: inline; margin: 5px">Później</button>');

  prevBtn.click(function () {
    var tempDate = new Date(selectedDate.getTime());
    tempDate.setDate(tempDate.getDate() - 1);
    if (Date.parse(tempDate.toString()) < Date.parse(maxDate.toString())) {
      nextBtn.prop('disabled', false);
    }
    if (Date.parse(tempDate.toString()) == Date.parse(minDate.toString())) {
      prevBtn.prop('disabled', true);
    }
    dateLabel.html(prepareDateFormat(tempDate));
    selectedDate = tempDate;
    drawDynamicPumpChart();
  });

  nextBtn.click(function () {
    var tempDate = new Date(selectedDate.getTime());
    tempDate.setDate(tempDate.getDate() + 1);
    if (Date.parse(tempDate.toString()) > Date.parse(minDate.toString())) {
      prevBtn.prop('disabled', false);
    }
    if (Date.parse(tempDate.toString()) == Date.parse(maxDate.toString())) {
      nextBtn.prop('disabled', true);
    }
    dateLabel.html(prepareDateFormat(tempDate));
    selectedDate = tempDate;
    drawDynamicPumpChart();
  });

  var buttonDiv = $('<div></div>');
  buttonDiv.append(prevBtn);
  buttonDiv.append(dateLabel);
  buttonDiv.append(nextBtn);

  parentControl.append(buttonDiv);
  parentControl.append(flowChartDiv);
  parentControl.append(volumeChartDiv);
}

function getLastDate() {
  var json = JSON.parse(infusionValue);
  var dateTime = json.dateTime;

  $.each(dateTime, function (index, value) {
    var tempDate = new Date(Date.parse(value));
    var date = new Date(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate());

    if (!selectedDate || selectedDate < date) {
      selectedDate = date;
    }
    if (!minDate || minDate > date) {
      minDate = date;
    }
    if (!maxDate || maxDate < date) {
      maxDate = date;
    }
  });
}

function prepareDateFormat(dateTime, withTime) {
  var d = addZeroIfNecessary(dateTime.getUTCDate()) + "/" + addZeroIfNecessary(dateTime.getUTCMonth() + 1) + "/" + addZeroIfNecessary(dateTime.getUTCFullYear());
  var t = addZeroIfNecessary(dateTime.getUTCHours()) + ":" + addZeroIfNecessary(dateTime.getUTCMinutes());
  if(!withTime || withTime === false) {
    return d;
  } else {
    return t + " " + d;
  }
}

function addZeroIfNecessary(part) {
  if(parseInt(part) < 10) {
    return 0 + part.toString();
  }
  return part.toString();
}

function drawDynamicPumpChart() {
  try {
    var flowRateData = new google.visualization.DataTable();
    var volumeData = new google.visualization.DataTable();
    var json = JSON.parse(infusionValue);

    var dateTime = json.dateTime;
    var flowRate = json.FlowRate;
    var vol = json.Volume;

    flowRateData.addColumn('string', 'Data');
    flowRateData.addColumn('number', 'Wydajność');
    
    volumeData.addColumn('string', 'Data');
    volumeData.addColumn('number', 'Podana objętość');

    var length = dateTime.length;

    var flowRatePreparedData = [];
    var volumePreparedData = [];
    for (var i = 0; i < length; i++) {
      var tempDate = new Date(Date.parse(dateTime[i]));

      if (tempDate.getFullYear() === selectedDate.getFullYear() &&
        tempDate.getMonth() === selectedDate.getMonth() &&
        tempDate.getDate() === selectedDate.getDate()) {

        var flowRateItem = [];
        var volumeItem = [];
        var dt = new Date(Date.parse(dateTime[i]));
        flowRateItem.push(prepareDateFormat(dt, true));
        flowRateItem.push(flowRate[i]);
        volumeItem.push(prepareDateFormat(dt, true));
        volumeItem.push(vol[i]);

        flowRatePreparedData.push(flowRateItem);
        volumePreparedData.push(volumeItem);
      }
    }

    flowRateData.addRows(flowRatePreparedData);
    volumeData.addRows(volumePreparedData);

    var options = {
	  legend: 'top',
      width: window.innerWidth * 0.85,
      height: 400,
      curveType: 'none',
      lineWidth: 1,
      pointSize: 2,
      fontSize: 11,
      hAxis: {
        format: 'hh:mm dd/MM/yy',

        slantedText: true,
        slantedTextAngle: 45,
      },
      vAxis: {
        minorGridlines: {
          color: '#eee',
          count: 4,
        },
		viewWindow: {
			min: 0,
		}
      },
	  explorer: {
		maxZoomIn:10,
		maxZoomOut: 2,
		keepInBounds: true,
	  },
      series : {
          0 : { color: "#00f"},
      },
    };

    var flowRateChart = new google.visualization.LineChart($('#infusionDynamicFlowChartDiv')[0]);
    flowRateChart.draw(flowRateData, options);
    
    options.series[0].color = "#f00";
    var volumeChart = new google.visualization.LineChart($('#infusionDynamicVolChartDiv')[0]);
    volumeChart.draw(volumeData, options);
  }
  catch (err) {
    console.log(err);
  }
}