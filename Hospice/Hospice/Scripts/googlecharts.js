google.load('visualization', '1', { packages: ['corechart'] });
google.setOnLoadCallback(drawLineCharts);

var daysInChart = 5;

var weightDayIndex = 0;
var weightDatesTab = [];

var sugarDayIndex = 0;
var sugarDatesTab = [];

var pressureDayIndex = 0;
var pressureDatesTab = [];

var spo2DayIndex = 0;
var spo2DatesTab = [];

var PompDayIndex = 0;
var PompDatesTab = [];

var PompInDayIndex = 0;
var PompInDatesTab = [];

function drawLineCharts() {
  try {
    createControls('weight', weightValue);
    getDates('weight', weightValue);
    drawLineChart(getJsonByDate('weight', weightValue), 'weight');
  } catch (err) {
    console.log(err);
  }
  try {
    createControls('sugar', sugarValue);
    getDates('sugar', sugarValue);
    drawLineChart(getJsonByDate('sugar', sugarValue), 'sugar');
  } catch (err) {
    console.log(err);
  }
  try {
    createControls('pressure', pressureValue);
    getDates('pressure', pressureValue);
    drawLineChart(getJsonByDate('pressure', pressureValue), 'pressure');
  } catch (err) {
    console.log(err);
  }
  try {
    createControls('spo2', spo2Value);
    getDates('spo2', spo2Value);
    drawLineChart(getJsonByDate('spo2', spo2Value), 'spo2');
  } catch (err) {
    console.log(err);
  }
}

function drawLineChart(response, chartId) {
  try {
    var data = new google.visualization.DataTable();
    var json = JSON.parse(response);
    var labels = [];
    $.each(json, function (index, key) {
      if (index === 'dateTime') {
        data.addColumn('string', 'Day');
      } else {
        data.addColumn('number', index.replace("Value", ""));
      }
      labels.push(index);
    });

    var length = json[labels[0]].length;

    var preparedData = [];
    for (var i = 0; i < length; i++) {
      var item = [];
      for (var j = 0; j < labels.length; j++) {
        if (j === 0) {
          var dt = new Date(Date.parse(json.dateTime[i]));
          item.push(prepareDateFormat(dt, true));
        } else {
          item.push(json[labels[j]][i]);
        }
      }
      preparedData.push(item);
    }

    data.addRows(preparedData);


    var options = {
	  legend: 'top',
      width: window.innerWidth * 0.85,
      height: 500,
      curveType: 'none',
      lineWidth: 1,
      pointSize: 2,
      fontSize: 12,
      hAxis: {
        format: 'hh:mm dd/MM/yy',

        slantedText: true,
        slantedTextAngle: 45,
      },
      vAxis: {
        minorGridlines: {
          color: '#eee',
          count: 4,
        },
      }
    };

    if (chartId === 'pressure') {
      options['seriesType'] = 'line';
      options['series'] = {};
      options['series']['2'] = {};
      options['series']['2']['type'] = 'bars';
    }

    var parentElement = $('#' + chartId + 'Chart');
    if (parentElement.children().length === 3) {
      parentElement.children().last().remove();
    }

    var chartElement = $('<div></div>');
    parentElement.append(chartElement);

    var chart = new google.visualization.ComboChart(chartElement[0]);
    chart.draw(data, options);
  }
  catch (err) {
    console.log(err);
  }
}

function prepareDateFormat(dateTime, withTime) {
  var d = addZeroIfNecessary(dateTime.getUTCDate()) + "/" + addZeroIfNecessary(dateTime.getUTCMonth() + 1) + "/" + addZeroIfNecessary(dateTime.getUTCFullYear());
  var t = addZeroIfNecessary(dateTime.getUTCHours()) + ":" + addZeroIfNecessary(dateTime.getUTCMinutes() + ":" + addZeroIfNecessary(dateTime.getUTCSeconds()));
  if(!withTime || withTime === false) {
    return d;
  } else {
    return t + " " + d;
  }
}

function addZeroIfNecessary(part) {
  if(parseInt(part) < 10) {
    return 0 + part.toString();
  }
  return part.toString();
}

function createControls(parentControlId, values) {
  var parentControl = $('#' + parentControlId + 'Chart');

  var prevBtn = $('<button id="prevBtn"' + parentControlId + ' type="button" style="display: inline; margin-right: 10px">Wcześniej</button>');
  var nextBtn = $('<button id="nextBtn"' + parentControlId + ' type="button" style="display: inline; margin-left: 10px">Później</button>');


  prevBtn.click(function () {
    var dayIndex = window[parentControlId + "DayIndex"];
    var max = (window[parentControlId + "DatesTab"]).length;

    if (dayIndex < max - daysInChart) {
      dayIndex++;
      window[parentControlId + "DayIndex"] = dayIndex;
      if (dayIndex === max - daysInChart) {
        prevBtn.prop('disabled', true);
      }
      if (dayIndex > 0) {
        nextBtn.prop('disabled', false);
      }
      drawLineChart(getJsonByDate(parentControlId, values), parentControlId);
    }
  });

  nextBtn.click(function () {
    var dayIndex = window[parentControlId + "DayIndex"];
    var max = (window[parentControlId + "DatesTab"]).length;

    if (dayIndex > 0) {
      dayIndex--;
      window[parentControlId + "DayIndex"] = dayIndex;
      if (dayIndex === 0) {
        nextBtn.prop('disabled', true);
      }
      if (dayIndex < max - daysInChart) {
        prevBtn.prop('disabled', false);
      }
      drawLineChart(getJsonByDate(parentControlId, values), parentControlId);
    }
  });

  parentControl.append(prevBtn);
  parentControl.append(nextBtn);
}

function addToArrayIfNotExist(dateArray, date) {
  var contains = false;
  for (var i = 0; i < dateArray.length; i++) {
    if (dateArray[i] == Date.parse(date.toString())) {
      contains = true;
      break;
    }
  }
  if (contains == false) {
    dateArray.push(Date.parse(date.toString()))
  }
}

function getDates(chartId, response) {

  var json = JSON.parse(response);
  var length = json.dateTime.length;

  for (var i = 0; i < length; i++) {
    var tempDate = new Date(Date.parse(json.dateTime[i]));
    var onlyDate = new Date(tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate());
    addToArrayIfNotExist(window[chartId + 'DatesTab'], onlyDate);
  }
}

function getFiveDates(label) {
  var index = window[label + 'DayIndex'];
  var allDates = window[label + 'DatesTab'];
  var length = allDates.length;

  var dates = [];

  if (length - index - 1 - daysInChart < 0) {
    for (var i = 0; i < daysInChart; i++) {
      dates.push(allDates[i]);
      if (i === length - 1) {
        break;
      }
    }
  } else {
    for (var i = length - index - daysInChart; i < length - index; i++) {
      dates.push(allDates[i]);
      if (i === length - 1) {
        break;
      }
    }
  }

  return dates;
}

function getJsonByDate(label, response) {
  var filteredJson = {};
  var dateTime = [];
  var data = {};

  var fiveDates = getFiveDates(label);

  var json = JSON.parse(response);

  var length = json.dateTime.length;

  for (var i = 0; i < length; i++) {
    var dt = new Date(Date.parse(json.dateTime[i]));
    var onlyDate = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());

    if (fiveDates.indexOf(Date.parse(onlyDate.toString())) != -1) {
      dateTime.push(json.dateTime[i]);

      $.each(json, function (index, item) {
        if (index !== "dateTime" && index !== "comment") {
          var tmp = data[index];
          if (!tmp) {
            tmp = [];
          }
          tmp.push(json[index][i]);
          data[index] = tmp;
        }
      });
    }
  }


  filteredJson["dateTime"] = dateTime;
  $.each(json, function (index, item) {
    if (index !== "dateTime" && index !== "comment") {
      filteredJson[index] = data[index];
    }
  });

  return JSON.stringify(filteredJson);
}