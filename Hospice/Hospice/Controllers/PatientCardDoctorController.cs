﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;
using Hospice.Infrastructure.Helpers;
using System.Data;
using Hospice.Infrastructure.Attributes;

namespace Hospice.Controllers
{
    [CustomAuthorize]
    public class PatientCardDoctorController : Controller
    {
        private IPatientMedicalHistoryHSRepository patientMedicalHistoryHSRepository;
        private IPatientFullExaminationHSRepository patientFullExaminationHSRepository;
        private IPatientFullExaminationDrugsHSRepository patientFullExaminationDrugsHSRepository;
        private IPatientMedicalVisitHSRepository patientMedicalVisitHSRepository;
        private IPatientOrdersForNursesHSRepository patientOrdersForNursesHSRepository;
        private IPatientOrdersForNursesDrugHSRepository patientOrdersForNursesDrugHSRepository;
        private IPatientOrdersForNursesTreatmentHSRepository patientOrdersForNursesTreatmentHSRepository;
        private IPatientOrdersForNursesDressingHSRepository patientOrdersForNursesDressingHSRepository;
        private IZaleceniaLekarskieLekiGodzinyRepository zaleceniaLekarskieLekiGodzinyRepository;
        private IPatientReferralToPsychologistHSRepository patientReferralToPsychologistHSRepository;
        private IPatientReferralToPsychotherapistHSRepository patientReferralToPsychotherapistHSRepository;
        private IPatientReferralForExaminationHSRepository patientReferralForExaminationHSRepository;
        private IPatientMedicalDescriptionHSRepository patientMedicalDescriptionHSRepository;
        private IPatientFirstVisitHDRepository patientFirstVisitHDRepository;
        private IPatientVisitHDRepository patientVisitHDRepository;
        private IPatientInterventionHDRepository patientInterventionHDRepository;
        private IPatientFullExaminationHDRepository patientFullExaminationHDRepository;
        private IPatientOrdersForNursesHDRepository patientOrdersForNursesHDRepository;
        private IPatientReferralToPsychologistHDRepository patientReferralToPsychologistHDRepository;
        private IPatientReferralToPsychotherapistHDRepository patientReferralToPsychotherapistHDRepository;
        private IPatientOrdersOtherHDRepository patientOrdersOtherHDRepository;
        private IPatientReferralForExaminationHDRepository patientReferralForExaminationHDRepository;
        private IPatientDrugRegisteryHDRepository patientDrugRegisteryHDRepository;
        private IPatientDoctorVisitCardHDRepository patientDoctorVisitCardHDRepository;
        private IPatientRegisteryHDRepository patientRegisteryHDRepository;
        private IPatientQualifyingVisitHDRepository patientQualifyingVisitHDRepository;
        private IPatientEstablishmentOfMedicalHistoryHDRepository patientEstablishmentOfMedicalHistoryHDRepository;

        public PatientCardDoctorController(
            IPatientHSRepository patientHSRepo, IPatientHDRepository patientHDRepo,
            IPatientMedicalHistoryHSRepository patientMedicalHistoryHSRepo, IPatientFullExaminationHSRepository patientFullExaminationHSRepo,
            IPatientFullExaminationDrugsHSRepository patientFullExaminationDrugsHSRepo,
            IPatientMedicalVisitHSRepository patientMedicalVisitHSRepo, IPatientOrdersForNursesHSRepository patientOrdersForNursesHSRepo,
            IPatientOrdersForNursesDrugHSRepository patientOrdersForNursesDrugHSRepo, IPatientOrdersForNursesTreatmentHSRepository patientOrdersForNursesTreatmentHSRepo, IPatientOrdersForNursesDressingHSRepository patientOrdersForNursesDressingHSRepo, IZaleceniaLekarskieLekiGodzinyRepository zaleceniaLekarskieLekiGodzinyRepo,
            IPatientReferralToPsychologistHSRepository patientReferralToPsychologistHSRepos, IPatientReferralToPsychotherapistHSRepository patientReferralToPsychotherapistHSRepo,
            IPatientReferralForExaminationHSRepository patientReferralForExaminationHSRepo, IPatientMedicalDescriptionHSRepository patientMedicalDescriptionHSRepo,
            IPatientFirstVisitHDRepository patientFirstVisitHDRepo, IPatientVisitHDRepository patientVisitHDRepo, IPatientInterventionHDRepository patientInterventionHDRepo,
            IPatientFullExaminationHDRepository patientFullExaminationHDRepo, IPatientOrdersForNursesHDRepository patientOrdersForNursesHDRepo,
            IPatientReferralToPsychologistHDRepository patientReferralToPsychologistHDRepo, IPatientReferralToPsychotherapistHDRepository patientReferralToPsychotherapistHDRepo,
            IPatientOrdersOtherHDRepository patientOrdersOtherHDRepo, IPatientReferralForExaminationHDRepository patientReferralForExaminationHDRepo,
            IPatientDrugRegisteryHDRepository patientDrugRegisteryHDRepo, IPatientDoctorVisitCardHDRepository patientDoctorVisitCardHDRepo,
            IPatientRegisteryHDRepository patientRegisteryHDRepo, IPatientQualifyingVisitHDRepository patientQualifyingVisitHDRepo,
            IPatientEstablishmentOfMedicalHistoryHDRepository patientEstablishmentOfMedicalHistoryHDRepo
            )
        {
            patientMedicalHistoryHSRepository = patientMedicalHistoryHSRepo;
            patientFullExaminationHSRepository = patientFullExaminationHSRepo;
            patientFullExaminationDrugsHSRepository = patientFullExaminationDrugsHSRepo;
            patientMedicalVisitHSRepository = patientMedicalVisitHSRepo;
            patientOrdersForNursesHSRepository = patientOrdersForNursesHSRepo;
            patientOrdersForNursesDrugHSRepository = patientOrdersForNursesDrugHSRepo;
            patientOrdersForNursesTreatmentHSRepository = patientOrdersForNursesTreatmentHSRepo;
            patientOrdersForNursesDressingHSRepository = patientOrdersForNursesDressingHSRepo;
            zaleceniaLekarskieLekiGodzinyRepository = zaleceniaLekarskieLekiGodzinyRepo;
            patientReferralToPsychologistHSRepository = patientReferralToPsychologistHSRepos;
            patientReferralToPsychotherapistHSRepository = patientReferralToPsychotherapistHSRepo;
            patientReferralForExaminationHSRepository = patientReferralForExaminationHSRepo;
            patientMedicalDescriptionHSRepository = patientMedicalDescriptionHSRepo;
            patientFirstVisitHDRepository = patientFirstVisitHDRepo;
            patientVisitHDRepository = patientVisitHDRepo;
            patientInterventionHDRepository = patientInterventionHDRepo;
            patientFullExaminationHDRepository = patientFullExaminationHDRepo;
            patientOrdersForNursesHDRepository = patientOrdersForNursesHDRepo;
            patientReferralToPsychologistHDRepository = patientReferralToPsychologistHDRepo;
            patientReferralToPsychotherapistHDRepository = patientReferralToPsychotherapistHDRepo;
            patientOrdersOtherHDRepository = patientOrdersOtherHDRepo;
            patientReferralForExaminationHDRepository = patientReferralForExaminationHDRepo;
            patientDrugRegisteryHDRepository = patientDrugRegisteryHDRepo;
            patientDoctorVisitCardHDRepository = patientDoctorVisitCardHDRepo;
            patientRegisteryHDRepository = patientRegisteryHDRepo;
            patientQualifyingVisitHDRepository = patientQualifyingVisitHDRepo;
            patientEstablishmentOfMedicalHistoryHDRepository = patientEstablishmentOfMedicalHistoryHDRepo;
        }

        public ActionResult PatientCardPatientMedicalHistoryDoctorHS()
        {
            MenuHelper.SetCurrentCardMenuItem(1);
            return View(TreeGenerateModelForDoctorHS());
        }

        public ActionResult PatientCardPatientMedicalHistoryDoctorHD()
        {
            MenuHelper.SetCurrentCardMenuItem(1);
            return View(TreeGenerateModelForDoctorHD());
        }

        #region trees doctor
        private Hospice.Models.SeededCategories TreeGenerateModelForDoctorHS()
        {
            int i = 1;
            List<int>  workflowIds = SessionHelper.GetUICurrentSettings().SelectedPatientForDoctorWorkflowIdsHS.ToList();
            IList<Hospice.Models.Category> categories = new List<Hospice.Models.Category>();

            //historia choroby
            categories.Add(new Hospice.Models.Category { ID = "1", Parent_ID = null, Name = "Historia choroby" });
                       
            i = 1;
            foreach (PatientMedicalHistoryHS item in patientMedicalHistoryHSRepository.PatientMedicalHistoriesHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "1_" + i, Parent_ID = "1", Name = "Historia choroby " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT9a0885e607604a2992f170e2c415df67", item.fldIWfId), Area = "", Action = "DisplayPatientMedicalHisotryHS", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }
            
            //Karta badania przedmiotowego
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Karta badania przedmiotowego" });

            i = 1;
            foreach (PatientFullExaminationHS item in patientFullExaminationHSRepository.PatientFullExaminationsHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Karta badania podmiotowego i przedmiotowego " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT310fab7627d4493fba7fc808ab864c3e", item.fldIWfId), Area = "", Action = "DisplayPatientFullExaminationHS", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }
            
            //Wizyta
            categories.Add(new Hospice.Models.Category { ID = "3", Parent_ID = null, Name = "Wizyta" });
            i = 1;
            foreach (PatientMedicalVisitHS item in patientMedicalVisitHSRepository.PatientMedicalVisitsHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "3_" + i, Parent_ID = "3", Name = "Wizyta " + (item.dtpdatawizyty.HasValue ? item.dtpdatawizyty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientMedicalVisitHS", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }
            
            //Zlecenia dla pielęgniarki
            categories.Add(new Hospice.Models.Category { ID = "4", Parent_ID = null, Name = "Zlecenia dla pielęgniarki" });

            i = 1;
            foreach (PatientOrdersForNursesHS item in patientOrdersForNursesHSRepository.PatientOrdersForNursesHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "4_" + i, Parent_ID = "4", Name = "Zlecenie dla pielęgniarki " + (item.DataZlecenia.HasValue ? item.DataZlecenia.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientOrdersForNursesHS", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //Skierowanie do psychologa
            categories.Add(new Hospice.Models.Category { ID = "5", Parent_ID = null, Name = "Skierowanie do psychologa" });

            i = 1;
            foreach (PatientReferralToPsychologistHS item in patientReferralToPsychologistHSRepository.PatientReferralsToPsychologistHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "5_" + i, Parent_ID = "5", Name = "Skierowanie " + (item.dtpdatawystawieniazlecenia.HasValue ? item.dtpdatawystawieniazlecenia.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientReferralToPsychologistHS", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //Skierowanie do fizjoterapeut
            categories.Add(new Hospice.Models.Category { ID = "6", Parent_ID = null, Name = "Skierowanie do fizjoterapeuty" });

            i = 1;
            foreach (PatientReferralToPsychotherapistHS item in patientReferralToPsychotherapistHSRepository.PatientReferralToPsychotherapistHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "6_" + i, Parent_ID = "6", Name = "Skierowanie " + (item.dtpdatawystawieniazlecenia.HasValue ? item.dtpdatawystawieniazlecenia.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientReferralToPsychotherapistHS", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //Skierowania na badania
            categories.Add(new Hospice.Models.Category { ID = "7", Parent_ID = null, Name = "Skierowania na badania" });

            i = 1;
            foreach (PatientReferralForExaminationHS item in patientReferralForExaminationHSRepository.PatientReferralForExaminationHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "7_" + i, Parent_ID = "7", Name = "Skierowanie " + (item.dtpdataskierowania.HasValue ? item.dtpdataskierowania.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientReferralForExaminationHS", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //Opis lekarski
            categories.Add(new Hospice.Models.Category { ID = "8", Parent_ID = null, Name = "Opis lekarski" });

            i = 1;
            foreach (PatientMedicalDescriptionHS item in patientMedicalDescriptionHSRepository.PatientMedicalDescriptionHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "8_" + i, Parent_ID = "8", Name = "Opis " + (item.dtpData.HasValue ? item.dtpData.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientMedicalDescriptionHS", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }
            
            return new Models.SeededCategories { Seed = null, Categories = categories };

        }

        private Hospice.Models.SeededCategories TreeGenerateModelForDoctorHD()
        {
            int i = 1;
            List<int> workflowIds = SessionHelper.GetUICurrentSettings().SelectedPatientDoctorWorkflowIdsHD.ToList();
            List<int> workflowRegisteryIds = SessionHelper.GetUICurrentSettings().SelectedPatientRegisteryWorkflowIdsHD.ToList();
            IList<Hospice.Models.Category> categories = new List<Hospice.Models.Category>();

            //Pierwsza wizyta lekarska
            categories.Add(new Hospice.Models.Category { ID = "1", Parent_ID = null, Name = "Pierwsza wizyta lekarska" });
            i = 1;
            foreach (PatientFirstVisitHD item in patientFirstVisitHDRepository.PatientFirstVisitHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "1_" + i, Parent_ID = "1", Name = "Pierwsza wizyta " + (item.dtpdatawizyty.HasValue ? item.dtpdatawizyty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientFirstVisitHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }
            
            //wizyta
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Wizyta" });
            i = 1;
            foreach (PatientVisitHD item in patientVisitHDRepository.PatientVisitHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Wizyta " + (item.dtpdatawizyty.HasValue ? item.dtpdatawizyty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientVisitHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //interwencja
            categories.Add(new Hospice.Models.Category { ID = "3", Parent_ID = null, Name = "Interwencja" });
            i = 1;
            foreach (PatientInterventionHD item in patientInterventionHDRepository.PatientInterventionHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "3_" + i, Parent_ID = "3", Name = "Interwencja " + (item.dtpdatawizytyinterwencjainterwencja.HasValue ? item.dtpdatawizytyinterwencjainterwencja.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientInterventionHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }
            //Badanie przedmiotowe i podmiotowe
            categories.Add(new Hospice.Models.Category { ID = "4", Parent_ID = null, Name = "Badanie przedmiotowe i podmiotowe" });
            i = 1;
            foreach (PatientFullExaminationHD item in patientFullExaminationHDRepository.PatientFullExaminationHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "4_" + i, Parent_ID = "4", Name = "Karta badania podmiotowego i przedmiotowego " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT4c0de6fa33fd49319a98c95544674719", item.fldIWfId), Area = "", Action = "DisplayPatientFullExaminationHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //Zlecenia dla pielęgniarki
            categories.Add(new Hospice.Models.Category { ID = "5", Parent_ID = null, Name = "Zlecenia dla pielęgniarki" });
            i = 0;
            foreach (PatientOrdersForNursesHD item in patientOrdersForNursesHDRepository.PatientOrdersForNursesHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "5_" + i, Parent_ID = "5", Name = "Zelecenie " + (item.DatePicker1.HasValue ? item.DatePicker1.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientOrdersForNursesHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //Skierowanie do psychologa
            categories.Add(new Hospice.Models.Category { ID = "6", Parent_ID = null, Name = "Skierowanie do psychologa" });
            i = 0;
            foreach (PatientReferralToPsychologistHD item in patientReferralToPsychologistHDRepository.PatientReferralsToPsychologistHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "6_" + i, Parent_ID = "6", Name = "Skierowanie " + (item.dtpdatawystawieniazlecenia.HasValue ? item.dtpdatawystawieniazlecenia.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientReferralToPsychologistHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //skierowanie do fizjoterapeuty
            categories.Add(new Hospice.Models.Category { ID = "7", Parent_ID = null, Name = "Skierowanie do fizjoterapeuty" });
            i = 0;
            foreach (PatientReferralToPsychotherapistHD item in patientReferralToPsychotherapistHDRepository.PatientReferralToPsychotherapistHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "7_" + i, Parent_ID = "7", Name = "Skierowanie " + (item.dtpdatawystawieniazlecenia.HasValue ? item.dtpdatawystawieniazlecenia.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientReferralToPsychotherapistHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //Zlecenia inne
            categories.Add(new Hospice.Models.Category { ID = "8", Parent_ID = null, Name = "Zlecenia inne" });
            i = 0;
            foreach (PatientOrdersOtherHD item in patientOrdersOtherHDRepository.PatientOrdersOtherHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "8_" + i, Parent_ID = "8", Name = "Zelecenie " + (item.dtpdatazlecenia.HasValue ? item.dtpdatazlecenia.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientOrdersOtherHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }
            
            //Skierowania na badania
            categories.Add(new Hospice.Models.Category { ID = "9", Parent_ID = null, Name = "Skierowania na badania" });
            i = 0;
            foreach (PatientReferralForExaminationHD item in patientReferralForExaminationHDRepository.PatientReferralForExaminationHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "9_" + i, Parent_ID = "9", Name = "Skierowanie " + (item.dtpdataskierowania.HasValue ? item.dtpdataskierowania.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientReferralForExaminationHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }
            
            //Rejestracja leków
            categories.Add(new Hospice.Models.Category { ID = "10", Parent_ID = null, Name = "Rejestracja leków" });
            i = 0;
            foreach (PatientDrugRegisteryHD item in patientDrugRegisteryHDRepository.PatientDrugRegisteryHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "10_" + i, Parent_ID = "10", Name = "Rejestracja " + item.fldId, Area = "", Action = "DisplayPatientDrugRegisteryHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //Karta wizyt lekarskich
            categories.Add(new Hospice.Models.Category { ID = "11", Parent_ID = null, Name = "Karta wizyt lekarskich" });
            i = 0;
            foreach (PatientDoctorVisitCardHD item in patientDoctorVisitCardHDRepository.PatientDoctorVisitCardHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "11_" + i, Parent_ID = "11", Name = "Wizyta " + (item.dtpdatawizyty.HasValue ? item.dtpdatawizyty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientDoctorVisitCardHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }

            //Wizyta kwalifikacyjna
            categories.Add(new Hospice.Models.Category { ID = "12", Parent_ID = null, Name = "Wizyta kwalifikacyjna" });  
            i = 0;
            foreach (PatientQualifyingVisitHD item in patientQualifyingVisitHDRepository.PatientQualifyingVisitHD.Where(p => workflowRegisteryIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "12_" + i, Parent_ID = "12", Name = "Wizyta " + (item.dtpDataWizytyKwalifikacyjnej.HasValue ? item.dtpDataWizytyKwalifikacyjnej.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientQualifyingVisitHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }
            
            //Założenie historii choroby
            categories.Add(new Hospice.Models.Category { ID = "13", Parent_ID = null, Name = "Założenie historii choroby" });
            i = 0;
            foreach (PatientEstablishmentOfMedicalHistoryHD item in patientEstablishmentOfMedicalHistoryHDRepository.PatientEstablishmentOfMedicalHistoryHD.Where(p => workflowRegisteryIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "13_" + i, Parent_ID = "13", Name = "Historia " + item.txtNrhistoriichoroby, Area = "", Action = "DisplayPatientEstablishmentOfMedicalHistoryHD", Controller = "PatientCardDoctor", FldId = item.fldId });
                i++;
            }



            return new Models.SeededCategories { Seed = null, Categories = categories };

        }

        #endregion

        


        public ActionResult DisplayPatientMedicalHisotryHS(int fldId)
        {
            PatientMedicalHistoryHS model = patientMedicalHistoryHSRepository.PatientMedicalHistoriesHS.FirstOrDefault(p => p.fldId == fldId);
            model.AddressCalculated = model.txtulica + " " + model.txtnumerdomu + ", " + model.txtkod + " " + model.txtmiejscowosc;
            model.StatementText = "Zgadzam się na proponowane leczenie i objęcie opieką przez hospicjum stacjonarne";

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardDoctor";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientMedicalHisotryHS";
            

            return View(model);
        }

        public ActionResult DisplayPatientFullExaminationHS(int fldId)
        {
            PatientFullExaminationHS model = patientFullExaminationHSRepository.PatientFullExaminationsHS.FirstOrDefault(p => p.fldId == fldId);
            model.CisnienieCalculated = model.txtcisnienietetnicze + "/" + model.cisnienietetnicze2;
            model.DrugsCalculated = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACT29408faab9e34d21aa23a615a5dbe06b", model.fldIWfId, new List<int> { 4, 6, 8, 9 }, new List<string> { "Nazwa leku", "Dawka", "Droga podania", "Uwagi" });
            
            return View(model);
        }

        public ActionResult DisplayPatientMedicalVisitHS(int fldId)
        {
            PatientMedicalVisitHS model = patientMedicalVisitHSRepository.PatientMedicalVisitsHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientOrdersForNursesHS(int fldId)
        {
            PatientOrdersForNursesHS model = patientOrdersForNursesHSRepository.PatientOrdersForNursesHS.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedDrugs = patientOrdersForNursesDrugHSRepository.PatientOrdersForNursesDrugHS.Where(p => p.fldIWfId == model.fldIWfId).ToList();
            model.CalculatedTreatments = patientOrdersForNursesTreatmentHSRepository.PatientOrdersForNursesTreatmentHS.Where(p => p.fldIWfId == model.fldIWfId).ToList();
            model.CalculatedDressings = patientOrdersForNursesDressingHSRepository.PatientOrdersForNursesDressingHS.Where(p => p.fldIWfId == model.fldIWfId).ToList();

            foreach(PatientOrdersForNursesDrugHS drug in model.CalculatedDrugs)
            {
                drug.CalculatedGodziny = zaleceniaLekarskieLekiGodzinyRepository.ZaleceniaLekarskieLekiGodziny.Where(d => d.IdZaleceniaStacjonarne == drug.fldId && d.Rodzaj == 0).OrderBy(ob => ob.Godzina).ToList();
            }

            foreach (PatientOrdersForNursesTreatmentHS treatment in model.CalculatedTreatments)
            {
                treatment.CalculatedGodziny = zaleceniaLekarskieLekiGodzinyRepository.ZaleceniaLekarskieLekiGodziny.Where(d => d.IdZaleceniaStacjonarne == treatment.fldId && d.Rodzaj == 1).OrderBy(ob => ob.Godzina).ToList();
            }

            foreach (PatientOrdersForNursesDressingHS dressing in model.CalculatedDressings)
            {
                dressing.CalculatedGodziny = zaleceniaLekarskieLekiGodzinyRepository.ZaleceniaLekarskieLekiGodziny.Where(d => d.IdZaleceniaStacjonarne == dressing.fldId && d.Rodzaj == 2).OrderBy(ob => ob.Godzina).ToList();
            }

            return View(model);
        }
        
        public ActionResult DisplayPatientReferralToPsychologistHS(int fldId)
        {
            PatientReferralToPsychologistHS model = patientReferralToPsychologistHSRepository.PatientReferralsToPsychologistHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientReferralToPsychotherapistHS(int fldId)
        {
            PatientReferralToPsychotherapistHS model = patientReferralToPsychotherapistHSRepository.PatientReferralToPsychotherapistHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientReferralForExaminationHS(int fldId)
        {
            PatientReferralForExaminationHS model = patientReferralForExaminationHSRepository.PatientReferralForExaminationHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientMedicalDescriptionHS(int fldId)
        {
            PatientMedicalDescriptionHS medicalDescription = patientMedicalDescriptionHSRepository.PatientMedicalDescriptionHS.FirstOrDefault(p => p.fldId == fldId);
            return View(medicalDescription);
        }


        //domowe
        public ActionResult DisplayPatientFirstVisitHD(int fldId)
        {
            PatientFirstVisitHD model = patientFirstVisitHDRepository.PatientFirstVisitHD.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedPatientDocumentNameHD = "Pierwsza wizyta";
            SessionHelper.GetUICurrentSettings().SelectedPatientDocumentDataHD = (model.dtpdatawizyty.HasValue ? model.dtpdatawizyty.Value.ToShortDateString() : model.fldId.ToString());

            return View(model);
        }

        public ActionResult DisplayPatientVisitHD(int fldId)
        {
            PatientVisitHD model = patientVisitHDRepository.PatientVisitHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }
        public ActionResult DisplayPatientInterventionHD(int fldId)
        {
            PatientInterventionHD model = patientInterventionHDRepository.PatientInterventionHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientFullExaminationHD(int fldId)
        {
            
            PatientFullExaminationHD model = patientFullExaminationHDRepository.PatientFullExaminationHD.FirstOrDefault(p => p.fldId == fldId);
            model.CisnienieCalculated = model.txtcisnienietetnicze + "/" + model.cisnienietetnicze2;
            model.DrugsCalculated = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACT07e02e094f66435a898fe07a02b57ca5", model.fldIWfId, new List<int> { 4, 6, 8, 9 }, new List<string> { "Nazwa leku", "Dawka", "Droga podania", "Uwagi" });
            
            return View(model);
        }

        public ActionResult DisplayPatientOrdersForNursesHD(int fldId)
        {
            PatientOrdersForNursesHD model = patientOrdersForNursesHDRepository.PatientOrdersForNursesHD.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedDrugs = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACT27a2a9fbe4f44556bb09f8ba6d9f4a1c", model.fldIWfId, new List<int> { 4, 5, 10, 6, 7, 8 }, new List<string> { "Nazwa leku", "Dawka", "Droga podania", "Data rozpoczęcia", "Data zakończenia", "Częstotliwość podawania" });
            model.CalculatedTreatments = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACT6772b3c90a0a4aea80ef844072d6b2b0", model.fldIWfId, new List<int> { 6, 4 }, new List<string> { "Zabieg", "Uwagi" });
            model.CalculatedDressings = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACT5728fb01fe9344e89b276195fafec3c2", model.fldIWfId, new List<int> { 4, 8, 6, 5 }, new List<string> { "Rodzaj opatrunku", "Czynność", "Umiejscowienie", "Uwagi" });            

            model.CalculatedDiet = Hospice.Infrastructure.SQLHelper.ListForTableXBesideIdColumns("UACT0f9e4fe520f447958ba9a7db1da6cfe1", model.fldIWfId, 4);
            model.CalculatedLifeParameters = Hospice.Infrastructure.SQLHelper.ListForTableXBesideIdColumns("UACTb50c57f266464faa886064d27454dded", model.fldIWfId, 4);      

            return View(model);
        }

        public ActionResult DisplayPatientReferralToPsychologistHD(int fldId)
        {
            PatientReferralToPsychologistHD model = patientReferralToPsychologistHDRepository.PatientReferralsToPsychologistHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientReferralToPsychotherapistHD(int fldId)
        {
            PatientReferralToPsychotherapistHD model = patientReferralToPsychotherapistHDRepository.PatientReferralToPsychotherapistHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientOrdersOtherHD(int fldId)
        {
            PatientOrdersOtherHD model = patientOrdersOtherHDRepository.PatientOrdersOtherHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }
        public ActionResult DisplayPatientReferralForExaminationHD(int fldId)
        {
            PatientReferralForExaminationHD model = patientReferralForExaminationHDRepository.PatientReferralForExaminationHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientDrugRegisteryHD(int fldId)
        {
            PatientDrugRegisteryHD model = patientDrugRegisteryHDRepository.PatientDrugRegisteryHD.FirstOrDefault(p => p.fldId == fldId);

            model.CalculatedDrugs = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACTf1bfbe56dcb8437dad21397bc8505335", model.fldIWfId, new List<int> { 4,5,6,9,7,8,10 }, new List<string> { "Nazwa preparatu", "Formuła", "Ilość", "Dawkowanie", "Nr recepty", "Odpłatność", "Uzasadnienie refundacji" }); 

            return View(model);
        }

        public ActionResult DisplayPatientDoctorVisitCardHD(int fldId)
        {
            PatientDoctorVisitCardHD model = patientDoctorVisitCardHDRepository.PatientDoctorVisitCardHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientQualifyingVisitHD(int fldId)
        {
            PatientQualifyingVisitHD model = patientQualifyingVisitHDRepository.PatientQualifyingVisitHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientEstablishmentOfMedicalHistoryHD(int fldId)
        {
            PatientEstablishmentOfMedicalHistoryHD model = patientEstablishmentOfMedicalHistoryHDRepository.PatientEstablishmentOfMedicalHistoryHD.FirstOrDefault(p => p.fldId == fldId);

            model.AddressCalculated = model.txtulica + " " + model.txtnumerdomu + ", " + model.txtkod + " " + model.txtmiejscowosc;
            model.StatementText = "Zgadzam się na proponowane leczenie i objęcie opieką przez hospicjum stacjonarne";

            return View(model);
        }
         
	}
}