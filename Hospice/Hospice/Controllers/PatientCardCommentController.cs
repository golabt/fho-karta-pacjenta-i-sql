﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;
using Hospice.Infrastructure.Attributes;

namespace Hospice.Controllers
{
    [CustomAuthorize]
    public class PatientCardCommentController : Controller
    {
        private IViewCommentRepository viewCommentRepository;
        public PatientCardCommentController(IViewCommentRepository viewCommentRepo)
        {
            viewCommentRepository = viewCommentRepo;
        }

        public PartialViewResult GetComments()
        {
            int itemId = Hospice.Infrastructure.Helpers.SessionHelper.GetUICurrentSettings().SelectedCommentItemId;
            string viewName = Hospice.Infrastructure.Helpers.SessionHelper.GetUICurrentSettings().SelectedCommentViewName;

            ViewBag.Message = viewName + itemId;

            return PartialView(viewCommentRepository.ViewComment.Where(p => p.ParentViewName == viewName && p.fldId == itemId).ToList());
        }

        public PartialViewResult AddComment()
        {
            return PartialView(new ViewComment());
        }


        [HttpPost]
        public ActionResult AddComment(ViewComment model)
        {
            if (ModelState.IsValid)
            {
                ViewComment comment = new ViewComment();
                comment.Content = model.Content;
                comment.Author = Hospice.Infrastructure.Helpers.SessionHelper.GetUserSettings().AuthenticatedUser.CurrentUserInfo.DisplayName;
                comment.fldId = Hospice.Infrastructure.Helpers.SessionHelper.GetUICurrentSettings().SelectedCommentItemId;
                comment.ParentViewName = Hospice.Infrastructure.Helpers.SessionHelper.GetUICurrentSettings().SelectedCommentViewName;
                comment.Date = DateTime.Now;
                viewCommentRepository.SaveViewComment(comment);

            }

            return RedirectToAction(Hospice.Infrastructure.Helpers.SessionHelper.GetUICurrentSettings().SelectedCommentViewName, Hospice.Infrastructure.Helpers.SessionHelper.GetUICurrentSettings().SelectedCommentControllerName, new { fldId = Hospice.Infrastructure.Helpers.SessionHelper.GetUICurrentSettings().SelectedCommentItemId });

        }
    }
}