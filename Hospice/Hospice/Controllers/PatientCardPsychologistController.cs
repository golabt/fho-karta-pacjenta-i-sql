﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hospice.Infrastructure.Helpers;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;
using Hospice.Models;
using Hospice.Infrastructure.Attributes;

namespace Hospice.Controllers
{
    [CustomAuthorize]
    public class PatientCardPsychologistController : Controller
    {
        private IPatientPsychologistVisitHSRepository patientPsychologistVisitHSRepository;
        private IPatientPsychologistDiseaseAcceptanceScaleHSRepository patientPsychologistDiseaseAcceptanceScaleHSRepository;
        private IPatientPsychologistQuestionnaireHADSMHSRepository patientPsychologistQuestionnaireHADSMHSRepository;
        private IPatientPsychologistQuestionnaireHADSMResultHSRepository patientPsychologistQuestionnaireHADSMResultHSRepository;
        private IPatientPsychologistGeriatricEvaluationScaleHSRepository patientPsychologistGeriatricEvaluationScaleHSRepository;
        private IPatientPsychologistGeriatricEvaluationScaleResultHSRepository patientPsychologistGeriatricEvaluationScaleResultHSRepository;
        private IPatientPsychologistMMSETestHSRepository patientPsychologistMMSETestHSRepository;
        private IPatientPsychologistMMSETestResultHSRepository patientPsychologistMMSETestResultHSRepository;
        private IPatientPsychologistVisitHDRepository patientPsychologistVisitHDRepository;
        private IPatientPsychologistDiseaseAcceptanceScaleHDRepository patientPsychologistDiseaseAcceptanceScaleHDRepository;
        private IPatientPsychologistQuestionnaireHADSMHDRepository patientPsychologistQuestionnaireHADSMHDRepository;
        private IPatientPsychologistQuestionnaireHADSMResultHDRepository patientPsychologistQuestionnaireHADSMResultHDRepository;
        private IPatientPsychologistGeriatricEvaluationScaleHDRepository patientPsychologistGeriatricEvaluationScaleHDRepository;
        private IPatientPsychologistGeriatricEvaluationScaleResultHDRepository patientPsychologistGeriatricEvaluationScaleResultHDRepository;
        private IPatientPsychologistMMSETestHDRepository patientPsychologistMMSETestHDRepository;
        private IPatientPsychologistMMSETestResultHDRepository patientPsychologistMMSETestResultHDRepository;

        public PatientCardPsychologistController(
            IPatientPsychologistVisitHSRepository patientPsychologistVisitHSRepo, IPatientPsychologistDiseaseAcceptanceScaleHSRepository patientPsychologistDiseaseAcceptanceScaleHSRepo,
            IPatientPsychologistQuestionnaireHADSMHSRepository patientPsychologistQuestionnaireHADSMHSRepo, IPatientPsychologistQuestionnaireHADSMResultHSRepository patientPsychologistQuestionnaireHADSMResultHSRepo,
            IPatientPsychologistGeriatricEvaluationScaleHSRepository patientPsychologistGeriatricEvaluationScaleHSRepo, IPatientPsychologistGeriatricEvaluationScaleResultHSRepository patientPsychologistGeriatricEvaluationScaleResultHSRepo,
            IPatientPsychologistMMSETestHSRepository patientPsychologistMMSETestHSRepo, IPatientPsychologistMMSETestResultHSRepository patientPsychologistMMSETestResultHSRepo,            
            IPatientPsychologistVisitHDRepository patientPsychologistVisitHDRepo, IPatientPsychologistDiseaseAcceptanceScaleHDRepository patientPsychologistDiseaseAcceptanceScaleHDRepo,
            IPatientPsychologistQuestionnaireHADSMHDRepository patientPsychologistQuestionnaireHADSMHDRepo, IPatientPsychologistQuestionnaireHADSMResultHDRepository patientPsychologistQuestionnaireHADSMResultHDRepo,
            IPatientPsychologistGeriatricEvaluationScaleHDRepository patientPsychologistGeriatricEvaluationScaleHDRepo, IPatientPsychologistGeriatricEvaluationScaleResultHDRepository patientPsychologistGeriatricEvaluationScaleResultHDRepo,
            IPatientPsychologistMMSETestHDRepository patientPsychologistMMSETestHDRepo, IPatientPsychologistMMSETestResultHDRepository patientPsychologistMMSETestResultHDRepo
            )
        {
            patientPsychologistVisitHSRepository = patientPsychologistVisitHSRepo;
            patientPsychologistDiseaseAcceptanceScaleHSRepository = patientPsychologistDiseaseAcceptanceScaleHSRepo;
            patientPsychologistQuestionnaireHADSMHSRepository = patientPsychologistQuestionnaireHADSMHSRepo;
            patientPsychologistQuestionnaireHADSMResultHSRepository = patientPsychologistQuestionnaireHADSMResultHSRepo;
            patientPsychologistGeriatricEvaluationScaleHSRepository = patientPsychologistGeriatricEvaluationScaleHSRepo;
            patientPsychologistGeriatricEvaluationScaleResultHSRepository = patientPsychologistGeriatricEvaluationScaleResultHSRepo;
            patientPsychologistMMSETestHSRepository = patientPsychologistMMSETestHSRepo;
            patientPsychologistMMSETestResultHSRepository = patientPsychologistMMSETestResultHSRepo;
            patientPsychologistVisitHDRepository = patientPsychologistVisitHDRepo;
            patientPsychologistDiseaseAcceptanceScaleHDRepository = patientPsychologistDiseaseAcceptanceScaleHDRepo;
            patientPsychologistQuestionnaireHADSMHDRepository = patientPsychologistQuestionnaireHADSMHDRepo;
            patientPsychologistQuestionnaireHADSMResultHDRepository = patientPsychologistQuestionnaireHADSMResultHDRepo;
            patientPsychologistGeriatricEvaluationScaleHDRepository = patientPsychologistGeriatricEvaluationScaleHDRepo;
            patientPsychologistGeriatricEvaluationScaleResultHDRepository = patientPsychologistGeriatricEvaluationScaleResultHDRepo;
            patientPsychologistMMSETestHDRepository = patientPsychologistMMSETestHDRepo;
            patientPsychologistMMSETestResultHDRepository = patientPsychologistMMSETestResultHDRepo;
        }


        public ActionResult PatientCardPatientMedicalHistoryPsychologistHS()
        {
            MenuHelper.SetCurrentCardMenuItem(3);
            return View(TreeGenerateModelForPsychologistHS());
        }

        public ActionResult PatientCardPatientMedicalHistoryPsychologistHD()
        {
            MenuHelper.SetCurrentCardMenuItem(3);
            return View(TreeGenerateModelForPsychologistHD());
        }

        private Hospice.Models.SeededCategories TreeGenerateModelForPsychologistHS()
        {
            int i = 0;
            List<int> workflowIds = SessionHelper.GetUICurrentSettings().SelectedPatientForPsychologistWorkflowIdsHS.ToList();
            IList<Hospice.Models.Category> categories = new List<Hospice.Models.Category>();

            //Formularz wizyty psychologa
            categories.Add(new Hospice.Models.Category { ID = "1", Parent_ID = null, Name = "Formularz wizyty psychologa" });
            i = 1;
            foreach (PatientPsychologistVisitHS item in patientPsychologistVisitHSRepository.PatientPsychologistVisitHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "1_" + i, Parent_ID = "1", Name = "Formularz " + (item.dtpdatawizyty.HasValue ? item.dtpdatawizyty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientPsychologistVisitHS", Controller = "PatientCardPsychologist", FldId = item.fldId });
                i++;
            }

            //Skala akceptacji chroby
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Skala akceptacji chroby" });
            i = 1;
            foreach (PatientPsychologistDiseaseAcceptanceScaleHS item in patientPsychologistDiseaseAcceptanceScaleHSRepository.PatientPsychologistDiseaseAcceptanceScaleHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Skala akceptacji " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTe78866b3d79d4ac0a0f81090b7d7dd17", item.fldIWfId), Area = "", Action = "DisplayPatientPsychologistDiseaseAcceptanceScaleHS", Controller = "PatientCardPsychologist", FldId = item.fldId });
                i++;
            }

            //Kwestionariusz KADS-M wraz z wynikiem
            categories.Add(new Hospice.Models.Category { ID = "3", Parent_ID = null, Name = "Kwestionariusz HADS-M wraz z wynikiem" });
            i = 1;
            foreach (PatientPsychologistQuestionnaireHADSMHS item in patientPsychologistQuestionnaireHADSMHSRepository.PatientPsychologistQuestionnaireHADSMHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "3_" + i, Parent_ID = "3", Name = "Kwestionariusz " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT71c3591aa2aa44a3980b1f735171c5c8", item.fldIWfId), Area = "", Action = "DisplayPatientPsychologistQuestionnaireHADSMHS", Controller = "PatientCardPsychologist", FldId = item.fldId });
                i++;
            }

            //Geriatryczna skala oceny depresji wraz z wynikiem
            categories.Add(new Hospice.Models.Category { ID = "4", Parent_ID = null, Name = "Geriatryczna skala oceny depresji wraz z wynikiem" });
            i = 1;
            foreach (PatientPsychologistGeriatricEvaluationScaleHS item in patientPsychologistGeriatricEvaluationScaleHSRepository.PatientPsychologistGeriatricEvaluationScaleHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "4_" + i, Parent_ID = "4", Name = "Kwestionariusz " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTe31388ca5c7f4510bf69e43af3cedc5d", item.fldIWfId), Area = "", Action = "DisplayPatientPsychologistGeriatricEvaluationScaleHS", Controller = "PatientCardPsychologist", FldId = item.fldId });
                i++;
            }
            
            //Test MMSE wraz z wynikiem
            categories.Add(new Hospice.Models.Category { ID = "5", Parent_ID = null, Name = "Test MMSE wraz z wynikiem" });
            i = 1;
            foreach (PatientPsychologistMMSETestHS item in patientPsychologistMMSETestHSRepository.PatientPsychologistMMSETestHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "5_" + i, Parent_ID = "5", Name = "Kwestionariusz " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTeb005a929f244fd5bb7a9c40788cc1e6", item.fldIWfId), Area = "", Action = "DisplayPatientPsychologistMMSETestHS", Controller = "PatientCardPsychologist", FldId = item.fldId });
                i++;
            }

            return new Models.SeededCategories { Seed = null, Categories = categories };

        }

        private Hospice.Models.SeededCategories TreeGenerateModelForPsychologistHD()
        {

            int i = 0;
            List<int> workflowIds = SessionHelper.GetUICurrentSettings().SelectedPatientForPsychologistWorkflowIdsHD.ToList();
            IList<Hospice.Models.Category> categories = new List<Hospice.Models.Category>();

            //Formularz wizyty psychologa
            categories.Add(new Hospice.Models.Category { ID = "1", Parent_ID = null, Name = "Formularz wizyty psychologa" });
            i = 1;
            foreach (PatientPsychologistVisitHD item in patientPsychologistVisitHDRepository.PatientPsychologistVisitHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "1_" + i, Parent_ID = "1", Name = "Formularz " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTf2d9417c5d8b4a41a6a5b29764927501", item.fldIWfId), Area = "", Action = "DisplayPatientPsychologistVisitHD", Controller = "PatientCardPsychologist", FldId = item.fldId });
                i++;
            }

            //Skala akceptacji chroby
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Skala akceptacji chroby" });
            i = 1;
            foreach (PatientPsychologistDiseaseAcceptanceScaleHD item in patientPsychologistDiseaseAcceptanceScaleHDRepository.PatientPsychologistDiseaseAcceptanceScaleHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Skala akceptacji " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTc62d268b6aef4d819fb614abcfef2c07", item.fldIWfId), Area = "", Action = "DisplayPatientPsychologistDiseaseAcceptanceScaleHD", Controller = "PatientCardPsychologist", FldId = item.fldId });
                i++;
            }

            //Kwestionariusz KADS-M wraz z wynikiem
            categories.Add(new Hospice.Models.Category { ID = "3", Parent_ID = null, Name = "Kwestionariusz HADS-M wraz z wynikiem" });
            i = 1;
            foreach (PatientPsychologistQuestionnaireHADSMHD item in patientPsychologistQuestionnaireHADSMHDRepository.PatientPsychologistQuestionnaireHADSMHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "3_" + i, Parent_ID = "3", Name = "Kwestionariusz " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT2a7c3e7c56b943be86d9dff74e159433", item.fldIWfId), Area = "", Action = "DisplayPatientPsychologistQuestionnaireHADSMHD", Controller = "PatientCardPsychologist", FldId = item.fldId });
                i++;
            }

            //Geriatryczna skala oceny depresji wraz z wynikiem
            categories.Add(new Hospice.Models.Category { ID = "4", Parent_ID = null, Name = "Geriatryczna skala oceny depresji wraz z wynikiem" });
            i = 1;
            foreach (PatientPsychologistGeriatricEvaluationScaleHD item in patientPsychologistGeriatricEvaluationScaleHDRepository.PatientPsychologistGeriatricEvaluationScaleHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "4_" + i, Parent_ID = "4", Name = "Kwestionariusz " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTf28343c0cd9e4e74a4fca212755b28cc", item.fldIWfId), Area = "", Action = "DisplayPatientPsychologistGeriatricEvaluationScaleHD", Controller = "PatientCardPsychologist", FldId = item.fldId });
                i++;
            }


            //Test MMSE wraz z wynikiem
            categories.Add(new Hospice.Models.Category { ID = "5", Parent_ID = null, Name = "Test MMSE wraz z wynikiem" });
            i = 1;
            foreach (PatientPsychologistMMSETestHD item in patientPsychologistMMSETestHDRepository.PatientPsychologistMMSETestHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "5_" + i, Parent_ID = "5", Name = "Kwestionariusz " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT6ef6aed69eb445579a44392764b493f5", item.fldIWfId), Area = "", Action = "DisplayPatientPsychologistMMSETestHD", Controller = "PatientCardPsychologist", FldId = item.fldId });
                i++;
            }

            return new Models.SeededCategories { Seed = null, Categories = categories };

        }

        public ActionResult DisplayPatientPsychologistVisitHS(int fldId)
        {
            PatientPsychologistVisitHS model = patientPsychologistVisitHSRepository.PatientPsychologistVisitHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientPsychologistDiseaseAcceptanceScaleHS(int fldId)
        {
            PatientPsychologistDiseaseAcceptanceScaleHS model = patientPsychologistDiseaseAcceptanceScaleHSRepository.PatientPsychologistDiseaseAcceptanceScaleHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientPsychologistQuestionnaireHADSMHS(int fldId)
        {
            PsychologistPatientQuestionnaireHADSMViewModelHS model = new PsychologistPatientQuestionnaireHADSMViewModelHS();
            model.PatientPsychologistQuestionnaireHADSMHS = patientPsychologistQuestionnaireHADSMHSRepository.PatientPsychologistQuestionnaireHADSMHS.FirstOrDefault(p => p.fldId == fldId);
            model.PatientPsychologistQuestionnaireHADSMResultHS = patientPsychologistQuestionnaireHADSMResultHSRepository.PatientPsychologistQuestionnaireHADSMResultHS.FirstOrDefault(p => p.fldIWfId == model.PatientPsychologistQuestionnaireHADSMHS.fldIWfId);
            return View(model);
        }
        public ActionResult DisplayPatientPsychologistGeriatricEvaluationScaleHS(int fldId)
        {
            PsychologistPatientGeriatricEvaluationScaleViewModelHS model = new PsychologistPatientGeriatricEvaluationScaleViewModelHS();
            model.PatientPsychologistGeriatricEvaluationScaleHS = patientPsychologistGeriatricEvaluationScaleHSRepository.PatientPsychologistGeriatricEvaluationScaleHS.FirstOrDefault(p => p.fldId == fldId);
            model.PatientPsychologistGeriatricEvaluationScaleResultHS = patientPsychologistGeriatricEvaluationScaleResultHSRepository.PatientPsychologistGeriatricEvaluationScaleResultHS.FirstOrDefault(p => p.fldIWfId == model.PatientPsychologistGeriatricEvaluationScaleHS.fldIWfId);
            return View(model);
        }

        public ActionResult DisplayPatientPsychologistMMSETestHS(int fldId)
        {
            PsychologistPatientMMSETestViewModelHS model = new PsychologistPatientMMSETestViewModelHS();
            model.PatientPsychologistMMSETestHS = patientPsychologistMMSETestHSRepository.PatientPsychologistMMSETestHS.FirstOrDefault(p => p.fldId == fldId);
            model.PatientPsychologistMMSETestResultHS = patientPsychologistMMSETestResultHSRepository.PatientPsychologistMMSETestResultHS.FirstOrDefault(p => p.fldIWfId == model.PatientPsychologistMMSETestHS.fldIWfId);
            return View(model);
        }

        public ActionResult DisplayPatientPsychologistVisitHD(int fldId)
        {
            PatientPsychologistVisitHD model = patientPsychologistVisitHDRepository.PatientPsychologistVisitHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientPsychologistDiseaseAcceptanceScaleHD(int fldId)
        {
            PatientPsychologistDiseaseAcceptanceScaleHD model = patientPsychologistDiseaseAcceptanceScaleHDRepository.PatientPsychologistDiseaseAcceptanceScaleHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientPsychologistQuestionnaireHADSMHD(int fldId)
        {
            PsychologistPatientQuestionnaireHADSMViewModelHD model = new PsychologistPatientQuestionnaireHADSMViewModelHD();
            model.PatientPsychologistQuestionnaireHADSMHD = patientPsychologistQuestionnaireHADSMHDRepository.PatientPsychologistQuestionnaireHADSMHD.FirstOrDefault(p => p.fldId == fldId);
            model.PatientPsychologistQuestionnaireHADSMResultHD = patientPsychologistQuestionnaireHADSMResultHDRepository.PatientPsychologistQuestionnaireHADSMResultHD.FirstOrDefault(p => p.fldIWfId == model.PatientPsychologistQuestionnaireHADSMHD.fldIWfId);
            return View(model);
        }
        public ActionResult DisplayPatientPsychologistGeriatricEvaluationScaleHD(int fldId)
        {
            PsychologistPatientGeriatricEvaluationScaleViewModelHD model = new PsychologistPatientGeriatricEvaluationScaleViewModelHD();
            model.PatientPsychologistGeriatricEvaluationScaleHD = patientPsychologistGeriatricEvaluationScaleHDRepository.PatientPsychologistGeriatricEvaluationScaleHD.FirstOrDefault(p => p.fldId == fldId);
            model.PatientPsychologistGeriatricEvaluationScaleResultHD = patientPsychologistGeriatricEvaluationScaleResultHDRepository.PatientPsychologistGeriatricEvaluationScaleResultHD.FirstOrDefault(p => p.fldIWfId == model.PatientPsychologistGeriatricEvaluationScaleHD.fldIWfId);
            return View(model);
        }

        public ActionResult DisplayPatientPsychologistMMSETestHD(int fldId)
        {
            PsychologistPatientMMSETestViewModelHD model = new PsychologistPatientMMSETestViewModelHD();
            model.PatientPsychologistMMSETestHD = patientPsychologistMMSETestHDRepository.PatientPsychologistMMSETestHD.FirstOrDefault(p => p.fldId == fldId);
            model.PatientPsychologistMMSETestResultHD = patientPsychologistMMSETestResultHDRepository.PatientPsychologistMMSETestResultHD.FirstOrDefault(p => p.fldIWfId == model.PatientPsychologistMMSETestHD.fldIWfId);
            return View(model);
        }

        
	}
}