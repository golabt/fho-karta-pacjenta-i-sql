﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;
using Hospice.Infrastructure.Helpers;
using System.Data;
using Hospice.Infrastructure.Attributes;

namespace Hospice.Controllers
{
    [CustomAuthorize]
    public class PatientCardTelemedicineController : Controller
    {
        private IInfusionInfoRepository infusionInfoRepository;
        private IMedicalDevicesLogRepository medicalDevicesLogRepository;
        private ICentralkaRepository centralkaRepository;
        private IPompyRepository pompyRepository;
        private IHireMedicalDeviceRepository hireMedicalDeviceRepository;
        private IReturnMedicalDeviceRepository returnMedicalDeviceRepository;
        private IDeviceRentalRepository deviceRentalRepository;

        public PatientCardTelemedicineController(IInfusionInfoRepository infusionInfoRepo, IMedicalDevicesLogRepository medicalDevicesLogRepo,
            ICentralkaRepository centralkaRepo, IPompyRepository pompyRepo,
            IHireMedicalDeviceRepository hireMedicalDeviceRepo, IReturnMedicalDeviceRepository returnMedicalDeviceRepo,
            IDeviceRentalRepository deviceRentalRepo)
        {
            infusionInfoRepository = infusionInfoRepo;
            medicalDevicesLogRepository = medicalDevicesLogRepo;
            centralkaRepository = centralkaRepo;
            pompyRepository = pompyRepo;
            hireMedicalDeviceRepository = hireMedicalDeviceRepo;
            returnMedicalDeviceRepository = returnMedicalDeviceRepo;
            deviceRentalRepository = deviceRentalRepo;
        }
        public ActionResult ChartPump(string deviceId, int? dataId, DateTime researchDate)
        {
            SequenceService.SequenceService service = new SequenceService.SequenceService();
            if (deviceId != null)
            {
                string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;
                ViewBag.DaneWS = service.GetInfusionInfoData(deviceId, pesel);
                ViewBag.TextBox1 = SessionHelper.GetUICurrentSettings().SelectedPatientFirstName + " " + SessionHelper.GetUICurrentSettings().SelectedPatientLastName;
                ViewBag.TextBox2 = pesel;
                ViewBag.TextBox3 = deviceId;

                infusionInfoRepository.UpdateInfusionInfo(dataId);
            }

            ViewBag.ResearchDate = researchDate;
            return View();
        }
        public ActionResult ListPump()
        {
            MenuHelper.SetCurrentTeleMenuItem(1);
            string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;

            List<string> hiredItems = deviceRentalRepository.DeviceRental.Where(p => p.PatientPESEL == pesel).Select(s => s.DeviceId).ToList();

            IQueryable<InfusionInfo> model = infusionInfoRepository.InfusionInfo.Where(p => hiredItems.Contains(p.DeviceId) && p.PatientId == pesel).OrderByDescending(o => o.StatusDate);

            return View(model);
        }

        public ActionResult ChartPumpIn(string deviceId, int? dataId, DateTime researchDate)
        {
            SequenceService.SequenceService service = new SequenceService.SequenceService();
            if (deviceId != null)
            {
                string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;
                ViewBag.DaneWS = service.GetInfusionInfoData(deviceId,pesel);
                ViewBag.TextBox1 = SessionHelper.GetUICurrentSettings().SelectedPatientFirstName + " " + SessionHelper.GetUICurrentSettings().SelectedPatientLastName;
                ViewBag.TextBox2 = pesel;
                ViewBag.TextBox3 = deviceId;

                infusionInfoRepository.UpdateInfusionInfo(dataId);
            }

            ViewBag.ResearchDate = researchDate;
            return View();
        }
        public ActionResult ListPumpIn()
        {
            MenuHelper.SetCurrentTeleMenuItem(2);
            string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;

            List<string> hiredItems = deviceRentalRepository.DeviceRental.Where(p => p.PatientPESEL == pesel).Select(s => s.DeviceId).ToList();

            IQueryable<InfusionInfo> model = infusionInfoRepository.InfusionInfo.Where(p => hiredItems.Contains(p.DeviceId) && p.PatientId == pesel).OrderByDescending(o => o.StatusDate);

            return View(model);
        }

        public ActionResult ChartPressure(string deviceId, int? dataId, DateTime researchDate)
        {
            SequenceService.SequenceService service = new SequenceService.SequenceService();
            if (deviceId != null)
            {
                string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;
                ViewBag.DaneWS = service.GetPressure(deviceId, pesel);
                ViewBag.TextBox1 = SessionHelper.GetUICurrentSettings().SelectedPatientFirstName + " " + SessionHelper.GetUICurrentSettings().SelectedPatientLastName;
                ViewBag.TextBox2 = pesel;
                ViewBag.TextBox3 = deviceId;

                medicalDevicesLogRepository.UpdateMedicalDevicesLog(dataId);
            }

            ViewBag.ResearchDate = researchDate;
            return View();
        }
        public ActionResult ListPressure()
        {
            MenuHelper.SetCurrentTeleMenuItem(3);
            string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;

            List<string> hiredItems = deviceRentalRepository.DeviceRental.Where(p => p.PatientPESEL == pesel).Select(s => s.DeviceId).ToList();

            IQueryable<MedicalDevicesLog> model = medicalDevicesLogRepository.MedicalDevicesLog.Where(p => hiredItems.Contains(p.deviceID) && p.diastolicVal != null && p.systolicVal != null && p.patientID == pesel).OrderByDescending(o => o.dateTime);

            return View(model);
        }

        public ActionResult ChartEKG(string deviceId, int? dataId, DateTime researchDate)
        {
            SequenceService.SequenceService service = new SequenceService.SequenceService();
            if (deviceId != null)
            {
                string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;
                ViewBag.DaneWSEKG = service.GetEcg(deviceId, pesel);
                ViewBag.DaneWSEKGALL = service.GetEcgAll(deviceId, pesel);
                ViewBag.TextBox1 = SessionHelper.GetUICurrentSettings().SelectedPatientFirstName + " " + SessionHelper.GetUICurrentSettings().SelectedPatientLastName;
                ViewBag.TextBox2 = pesel;
                ViewBag.TextBox3 = deviceId;

                medicalDevicesLogRepository.UpdateMedicalDevicesLog(dataId);
            }

            ViewBag.ResearchDate = researchDate;
            return View();
        }
        public ActionResult ListEKG()
        {
            MenuHelper.SetCurrentTeleMenuItem(4);
            string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;

            List<string> hiredItems = deviceRentalRepository.DeviceRental.Where(p => p.PatientPESEL == pesel).Select(s => s.DeviceId).ToList();

            IQueryable<MedicalDevicesLog> model = medicalDevicesLogRepository.MedicalDevicesLog.Where(p => hiredItems.Contains(p.deviceID) && p.ecgJson != null && p.patientID == pesel).OrderByDescending(o => o.dateTime);
            
            return View(model);
        }

        public ActionResult ChartSpO2(string deviceId, int? dataId, DateTime researchDate)
        {
            SequenceService.SequenceService service = new SequenceService.SequenceService();
            if (deviceId != null)
            {
                string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;
                ViewBag.DaneWS = service.GetSpo2(deviceId, pesel);
                ViewBag.TextBox1 = SessionHelper.GetUICurrentSettings().SelectedPatientFirstName + " " + SessionHelper.GetUICurrentSettings().SelectedPatientLastName;
                ViewBag.TextBox2 = pesel;
                ViewBag.TextBox3 = deviceId;

                medicalDevicesLogRepository.UpdateMedicalDevicesLog(dataId);
            }

            ViewBag.ResearchDate = researchDate;
            return View();
        }
        public ActionResult ListSpO2()
        {
            MenuHelper.SetCurrentTeleMenuItem(5);
            string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;

            List<string> hiredItems = deviceRentalRepository.DeviceRental.Where(p => p.PatientPESEL == pesel).Select(s => s.DeviceId).ToList();

            IQueryable<MedicalDevicesLog> model = medicalDevicesLogRepository.MedicalDevicesLog.Where(p => hiredItems.Contains(p.deviceID) && p.spo2Val != null && p.patientID == pesel).OrderByDescending(o => o.dateTime);

            return View(model);
        }

        public ActionResult ChartWeight(string deviceId, int? dataId, DateTime researchDate)
        {
            SequenceService.SequenceService service = new SequenceService.SequenceService();
            
            if (deviceId != null)
            {      
                string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;
                ViewBag.DaneWS = service.GetWeight(deviceId, pesel);
                ViewBag.TextBox1 = SessionHelper.GetUICurrentSettings().SelectedPatientFirstName + " " + SessionHelper.GetUICurrentSettings().SelectedPatientLastName;
                ViewBag.TextBox2 = pesel;
                ViewBag.TextBox3 = deviceId;

                medicalDevicesLogRepository.UpdateMedicalDevicesLog(dataId);
            }

            ViewBag.ResearchDate = researchDate;
            return View();
        }
        public ActionResult ListWeight()
        {
            MenuHelper.SetCurrentTeleMenuItem(6);
            string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;

            List<string> hiredItems = deviceRentalRepository.DeviceRental.Where(p => p.PatientPESEL == pesel).Select(s => s.DeviceId).ToList();

            IQueryable<MedicalDevicesLog> model = medicalDevicesLogRepository.MedicalDevicesLog.Where(p => hiredItems.Contains(p.deviceID) && p.weighVal != null && p.patientID == pesel).OrderByDescending(o => o.dateTime);

            return View(model);
        }
        public ActionResult ChartSugar(string deviceId, int? dataId, DateTime researchDate)
        {
            SequenceService.SequenceService service = new SequenceService.SequenceService();

            if(deviceId != null)
            {
                string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;
                ViewBag.DaneWS = service.GetSugar(deviceId, pesel);
                ViewBag.TextBox1 = SessionHelper.GetUICurrentSettings().SelectedPatientFirstName + " " + SessionHelper.GetUICurrentSettings().SelectedPatientLastName;
                ViewBag.TextBox2 = pesel;
                ViewBag.TextBox3 = deviceId;

                medicalDevicesLogRepository.UpdateMedicalDevicesLog(dataId);
            }

            ViewBag.ResearchDate = researchDate;
            return View();
        }
        public ActionResult ListSugar()
        {
            MenuHelper.SetCurrentTeleMenuItem(7);
            string pesel = SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;

            List<string> hiredItems = deviceRentalRepository.DeviceRental.Where(p => p.PatientPESEL == pesel).Select(s => s.DeviceId).ToList();

            IQueryable<MedicalDevicesLog> model = medicalDevicesLogRepository.MedicalDevicesLog.Where(p => hiredItems.Contains(p.deviceID) && p.sugarValue != null && p.patientID == pesel).OrderByDescending(o => o.dateTime);

            return View(model);
        }
	}
}