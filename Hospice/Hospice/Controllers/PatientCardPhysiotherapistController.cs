﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;
using Hospice.Infrastructure.Helpers;
using Hospice.Models;
using Hospice.Infrastructure.Attributes;

namespace Hospice.Controllers
{
    [CustomAuthorize]
    public class PatientCardPhysiotherapistController : Controller
    {
        private IPatientPhysiotherapistFirstVisitHSRepository patientPhysiotherapistFirstVisitHSRepository;
        private IPatientPhysiotherapistPatientCardHSRepository patientPhysiotherapistPatientCardHSRepository;
        private IPatientPhysiotherapistVisitHSRepository patientPhysiotherapistVisitHSRepository;
        private IPatientPhysiotherapistBorgScaleHSRepository patientPhysiotherapistBorgScaleHSRepository;
        private IPatientPhysiotherapistKarnofskyPerformanceStatusHSRepository patientPhysiotherapistKarnofskyPerformanceStatusHSRepository;
        private IPatientPhysiotherapistBFIIndexHSRepository patientPhysiotherapistBFIIndexHSRepository;
        private IPatientPhysiotherapistRSCLHSRepository patientPhysiotherapistRSCLHSRepository;
        private IPatientPhysiotherapistVASHSRepository patientPhysiotherapistVASHSRepository;
        private IPatientPhysiotherapistBarthelPatientHSRepository patientPhysiotherapistBarthelPatientHSRepository;
        private IPatientPhysiotherapistBarthelPatientResultHSRepository patientPhysiotherapistBarthelPatientResultHSRepository;
        private IPatientPhysiotherapistBarthelPhysiotherapistHSRepository patientPhysiotherapistBarthelPhysiotherapistHSRepository;
        private IPatientPhysiotherapistBarthelPhysiotherapistResultHSRepository patientPhysiotherapistBarthelPhysiotherapistResultHSRepository;

        private IPatientPhysiotherapistFirstVisitHDRepository patientPhysiotherapistFirstVisitHDRepository;
        private IPatientPhysiotherapistPatientCardHDRepository patientPhysiotherapistPatientCardHDRepository;
        private IPatientPhysiotherapistVisitHDRepository patientPhysiotherapistVisitHDRepository;
        private IPatientPhysiotherapistBorgScaleHDRepository patientPhysiotherapistBorgScaleHDRepository;
        private IPatientPhysiotherapistKarnofskyPerformanceStatusHDRepository patientPhysiotherapistKarnofskyPerformanceStatusHDRepository;
        private IPatientPhysiotherapistBFIIndexHDRepository patientPhysiotherapistBFIIndexHDRepository;
        private IPatientPhysiotherapistRSCLHDRepository patientPhysiotherapistRSCLHDRepository;
        private IPatientPhysiotherapistVASHDRepository patientPhysiotherapistVASHDRepository;
        private IPatientPhysiotherapistBarthelPatientHDRepository patientPhysiotherapistBarthelPatientHDRepository;
        private IPatientPhysiotherapistBarthelPatientResultHDRepository patientPhysiotherapistBarthelPatientResultHDRepository;
        private IPatientPhysiotherapistBarthelPhysiotherapistHDRepository patientPhysiotherapistBarthelPhysiotherapistHDRepository;
        private IPatientPhysiotherapistBarthelPhysiotherapistResultHDRepository patientPhysiotherapistBarthelPhysiotherapistResultHDRepository;

        public PatientCardPhysiotherapistController(
            IPatientPhysiotherapistFirstVisitHSRepository patientPhysiotherapistFirstVisitHSRepo, IPatientPhysiotherapistPatientCardHSRepository patientPhysiotherapistPatientCardHSRepo,
            IPatientPhysiotherapistVisitHSRepository patientPhysiotherapistVisitHSRepo, IPatientPhysiotherapistBorgScaleHSRepository patientPhysiotherapistBorgScaleHSRepo,
            IPatientPhysiotherapistKarnofskyPerformanceStatusHSRepository patientPhysiotherapistKarnofskyPerformanceStatusHSRepo,
            IPatientPhysiotherapistBFIIndexHSRepository patientPhysiotherapistBFIIndexHSRepo, IPatientPhysiotherapistRSCLHSRepository patientPhysiotherapistRSCLHSRepo,
            IPatientPhysiotherapistVASHSRepository patientPhysiotherapistVASHSRepo,
            IPatientPhysiotherapistBarthelPatientHSRepository patientPhysiotherapistBarthelPatientHSRepo, 
            IPatientPhysiotherapistBarthelPatientResultHSRepository patientPhysiotherapistBarthelPatientResultHSRepo,
            IPatientPhysiotherapistBarthelPhysiotherapistHSRepository patientPhysiotherapistBarthelPhysiotherapistHSRepo, 
            IPatientPhysiotherapistBarthelPhysiotherapistResultHSRepository patientPhysiotherapistBarthelPhysiotherapistResultHSRepo,

            IPatientPhysiotherapistFirstVisitHDRepository patientPhysiotherapistFirstVisitHDRepo, IPatientPhysiotherapistPatientCardHDRepository patientPhysiotherapistPatientCardHDRepo,
            IPatientPhysiotherapistVisitHDRepository patientPhysiotherapistVisitHDRepo, IPatientPhysiotherapistBorgScaleHDRepository patientPhysiotherapistBorgScaleHDRepo,
            IPatientPhysiotherapistKarnofskyPerformanceStatusHDRepository patientPhysiotherapistKarnofskyPerformanceStatusHDRepo,
            IPatientPhysiotherapistBFIIndexHDRepository patientPhysiotherapistBFIIndexHDRepo, IPatientPhysiotherapistRSCLHDRepository patientPhysiotherapistRSCLHDRepo,
            IPatientPhysiotherapistVASHDRepository patientPhysiotherapistVASHDRepo,
            IPatientPhysiotherapistBarthelPatientHDRepository patientPhysiotherapistBarthelPatientHDRepo, 
            IPatientPhysiotherapistBarthelPatientResultHDRepository patientPhysiotherapistBarthelPatientResultHDRepo,
            IPatientPhysiotherapistBarthelPhysiotherapistHDRepository patientPhysiotherapistBarthelPhysiotherapistHDRepo, 
            IPatientPhysiotherapistBarthelPhysiotherapistResultHDRepository patientPhysiotherapistBarthelPhysiotherapistResultHDRepo
            )
        {
            patientPhysiotherapistFirstVisitHSRepository = patientPhysiotherapistFirstVisitHSRepo;
            patientPhysiotherapistPatientCardHSRepository = patientPhysiotherapistPatientCardHSRepo;
            patientPhysiotherapistVisitHSRepository = patientPhysiotherapistVisitHSRepo;
            patientPhysiotherapistBorgScaleHSRepository = patientPhysiotherapistBorgScaleHSRepo;
            patientPhysiotherapistKarnofskyPerformanceStatusHSRepository = patientPhysiotherapistKarnofskyPerformanceStatusHSRepo;
            patientPhysiotherapistBFIIndexHSRepository = patientPhysiotherapistBFIIndexHSRepo;
            patientPhysiotherapistRSCLHSRepository = patientPhysiotherapistRSCLHSRepo;
            patientPhysiotherapistVASHSRepository = patientPhysiotherapistVASHSRepo;
            patientPhysiotherapistBarthelPatientHSRepository = patientPhysiotherapistBarthelPatientHSRepo;
            patientPhysiotherapistBarthelPatientResultHSRepository = patientPhysiotherapistBarthelPatientResultHSRepo;
            patientPhysiotherapistBarthelPhysiotherapistHSRepository = patientPhysiotherapistBarthelPhysiotherapistHSRepo;
            patientPhysiotherapistBarthelPhysiotherapistResultHSRepository = patientPhysiotherapistBarthelPhysiotherapistResultHSRepo;

            patientPhysiotherapistFirstVisitHDRepository = patientPhysiotherapistFirstVisitHDRepo;
            patientPhysiotherapistPatientCardHDRepository = patientPhysiotherapistPatientCardHDRepo;
            patientPhysiotherapistVisitHDRepository = patientPhysiotherapistVisitHDRepo;
            patientPhysiotherapistBorgScaleHDRepository = patientPhysiotherapistBorgScaleHDRepo;
            patientPhysiotherapistKarnofskyPerformanceStatusHDRepository = patientPhysiotherapistKarnofskyPerformanceStatusHDRepo;
            patientPhysiotherapistBFIIndexHDRepository = patientPhysiotherapistBFIIndexHDRepo;
            patientPhysiotherapistRSCLHDRepository = patientPhysiotherapistRSCLHDRepo;
            patientPhysiotherapistVASHDRepository = patientPhysiotherapistVASHDRepo;
            patientPhysiotherapistBarthelPatientHDRepository = patientPhysiotherapistBarthelPatientHDRepo;
            patientPhysiotherapistBarthelPatientResultHDRepository = patientPhysiotherapistBarthelPatientResultHDRepo;
            patientPhysiotherapistBarthelPhysiotherapistHDRepository = patientPhysiotherapistBarthelPhysiotherapistHDRepo;
            patientPhysiotherapistBarthelPhysiotherapistResultHDRepository = patientPhysiotherapistBarthelPhysiotherapistResultHDRepo;
        }

        public ActionResult PatientCardPatientMedicalHistoryPhysiotherapistHS()
        {
            MenuHelper.SetCurrentCardMenuItem(4);
            return View(TreeGenerateModelForPhysiotherapistHS());
        }

        public ActionResult PatientCardPatientMedicalHistoryPhysiotherapistHD()
        {
            MenuHelper.SetCurrentCardMenuItem(4);
            return View(TreeGenerateModelForPhysiotherapistHD());
        }

        private Hospice.Models.SeededCategories TreeGenerateModelForPhysiotherapistHS()
        {

            int i = 1;
            List<int> workflowIds = SessionHelper.GetUICurrentSettings().SelectedPatientForPhysiotherapistWorkflowIdsHS.ToList();
            IList<Hospice.Models.Category> categories = new List<Hospice.Models.Category>();

            //Pierwsza wizyta fizjoterapeuty
            categories.Add(new Hospice.Models.Category { ID = "1", Parent_ID = null, Name = "Pierwsza wizyta fizjoterapeuty" });
            i = 1;
            foreach (PatientPhysiotherapistFirstVisitHS item in patientPhysiotherapistFirstVisitHSRepository.PatientPhysiotherapistFirstVisitHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "1_" + i, Parent_ID = "1", Name = "Wizyta " + (item.dtpdatawizyty.HasValue ? item.dtpdatawizyty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientPhysiotherapistFirstVisitHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Wizyta fizjoterapeuty i karta pacjenta" });
            i = 1;
            foreach (PatientPhysiotherapistVisitHS item in patientPhysiotherapistVisitHSRepository.PatientPhysiotherapistVisitHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Wizyta i karta pacjenta " + (item.dtpdatawizytyfizjoterapeuty.HasValue ? item.dtpdatawizytyfizjoterapeuty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientPhysiotherapistVisitAndCardHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldIWfId });
                i++;
            }
            /*
            //Karta pacjenta
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Karta pacjenta" });
            i = 1;
            foreach (PatientPhysiotherapistPatientCardHS item in patientPhysiotherapistPatientCardHSRepository.PatientPhysiotherapistPatientCardHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Karta nr " + item.fldId, Area = "", Action = "DisplayPatientPhysiotherapistPatientCardHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            //Wizyta fizjoterapeuty
            categories.Add(new Hospice.Models.Category { ID = "3", Parent_ID = null, Name = "Wizyta fizjoterapeuty" });
            i = 1;
            foreach (PatientPhysiotherapistVisitHS item in patientPhysiotherapistVisitHSRepository.PatientPhysiotherapistVisitHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "3_" + i, Parent_ID = "3", Name = "Wizyta " + (item.dtpdatawizytyfizjoterapeuty.HasValue ? item.dtpdatawizytyfizjoterapeuty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientPhysiotherapistVisitHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }
            */

            //Skala Borga
            categories.Add(new Hospice.Models.Category { ID = "4", Parent_ID = null, Name = "Skala Borga" });
            i = 1;
            foreach (PatientPhysiotherapistBorgScaleHS item in patientPhysiotherapistBorgScaleHSRepository.PatientPhysiotherapistBorgScaleHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "4_" + i, Parent_ID = "4", Name = "Skala Borga " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTc2956493e9bc496b9cdc6bc2dd54c07f", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistBorgScaleHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            //Stopień sprawności wg Karnofsky
            categories.Add(new Hospice.Models.Category { ID = "5", Parent_ID = null, Name = "Stopień sprawności wg Karnofsky" });
            i = 1;
            foreach (PatientPhysiotherapistKarnofskyPerformanceStatusHS item in patientPhysiotherapistKarnofskyPerformanceStatusHSRepository.PatientPhysiotherapistKarnofskyPerformanceStatusHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "5_" + i, Parent_ID = "5", Name = "Stopień sprawości " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT78d5ab4cc7a246fa8742d49791866fcc", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistKarnofskyPerformanceStatusHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            //Indeks czynności jelit BFI
            categories.Add(new Hospice.Models.Category { ID = "6", Parent_ID = null, Name = "Indeks czynności jelit BFI" });
            i = 1;
            foreach (PatientPhysiotherapistBFIIndexHS item in patientPhysiotherapistBFIIndexHSRepository.PatientPhysiotherapistBFIIndexHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "6_" + i, Parent_ID = "6", Name = "Index BFI " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT991b8df4773c4ff19af697f6b595f9aa", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistBFIIndexHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            //RSCL Rotterdamska lista objawów
            categories.Add(new Hospice.Models.Category { ID = "7", Parent_ID = null, Name = "RSCL Rotterdamska lista objawów" });
            i = 1;
            foreach (PatientPhysiotherapistRSCLHS item in patientPhysiotherapistRSCLHSRepository.PatientPhysiotherapistRSCLHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "7_" + i, Parent_ID = "7", Name = "Lista obiawów " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT51b87692e2db49889591fdea6ba92868", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistRSCLHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            //Skala bólu VAS
            categories.Add(new Hospice.Models.Category { ID = "8", Parent_ID = null, Name = "Skala bólu VAS" });
            i = 1;
            foreach (PatientPhysiotherapistVASHS item in patientPhysiotherapistVASHSRepository.PatientPhysiotherapistVASHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "8_" + i, Parent_ID = "8", Name = "Skala bólu " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTc4f3c045469a4f56b9c3294e8b1e17af", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistVASHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }
            categories.Add(new Hospice.Models.Category { ID = "9", Parent_ID = null, Name = "Skala Barthel wg pacjenta" });
            i = 1;
            foreach (PatientPhysiotherapistBarthelPatientHS item in patientPhysiotherapistBarthelPatientHSRepository.PatientPhysiotherapistBarthelPatientHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "9_" + i, Parent_ID = "9", Name = "Skala bólu " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTd8de471604944469a115cfd4d5cf6532", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistBarthelPatientHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            categories.Add(new Hospice.Models.Category { ID = "10", Parent_ID = null, Name = "Skala Barthel wg fizjoterapeuty" });
            i = 1;
            foreach (PatientPhysiotherapistBarthelPhysiotherapistHS item in patientPhysiotherapistBarthelPhysiotherapistHSRepository.PatientPhysiotherapistBarthelPhysiotherapistHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "10_" + i, Parent_ID = "10", Name = "Skala bólu " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTfc2395aaa8bf4fd4b9e492093df0b684", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistBarthelPhysiotherapistHS", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            return new Models.SeededCategories { Seed = null, Categories = categories };

        }

        private Hospice.Models.SeededCategories TreeGenerateModelForPhysiotherapistHD()
        {

            int i = 1;
            List<int> workflowIds = SessionHelper.GetUICurrentSettings().SelectedPatientForPhysiotherapistWorkflowIdsHD.ToList();
            IList<Hospice.Models.Category> categories = new List<Hospice.Models.Category>();

            //Pierwsza wizyta fizjoterapeuty
            categories.Add(new Hospice.Models.Category { ID = "1", Parent_ID = null, Name = "Pierwsza wizyta fizjoterapeuty" });
            i = 1;
            foreach (PatientPhysiotherapistFirstVisitHD item in patientPhysiotherapistFirstVisitHDRepository.PatientPhysiotherapistFirstVisitHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "1_" + i, Parent_ID = "1", Name = "Wizyta " + (item.dtpdatawizyty.HasValue ? item.dtpdatawizyty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientPhysiotherapistFirstVisitHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Wizyta fizjoterapeuty i karta pacjenta" });
            i = 1;
            foreach (PatientPhysiotherapistVisitHD item in patientPhysiotherapistVisitHDRepository.PatientPhysiotherapistVisitHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Wizyta i karta pacjenta " + (item.dtpdatawizytyfizjoterapeuty.HasValue ? item.dtpdatawizytyfizjoterapeuty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientPhysiotherapistVisitAndCardHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldIWfId });
                i++;
            }
            /*
            //Karta pacjenta
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Karta pacjenta" });
            i = 1;
            foreach (PatientPhysiotherapistPatientCardHD item in patientPhysiotherapistPatientCardHDRepository.PatientPhysiotherapistPatientCardHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Karta " + (item.dtpdatawizytyfizjoterapeuty.HasValue ? item.dtpdatawizytyfizjoterapeuty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientPhysiotherapistPatientCardHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            //Wizyta fizjoterapeuty
            categories.Add(new Hospice.Models.Category { ID = "3", Parent_ID = null, Name = "Wizyta fizjoterapeuty" });
            i = 1;
            foreach (PatientPhysiotherapistVisitHD item in patientPhysiotherapistVisitHDRepository.PatientPhysiotherapistVisitHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "3_" + i, Parent_ID = "3", Name = "Wizyta " + (item.dtpdatawizytyfizjoterapeuty.HasValue ? item.dtpdatawizytyfizjoterapeuty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientPhysiotherapistVisitHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }
            */
            //Skala Borga
            categories.Add(new Hospice.Models.Category { ID = "4", Parent_ID = null, Name = "Skala Borga" });
            i = 1;
            foreach (PatientPhysiotherapistBorgScaleHD item in patientPhysiotherapistBorgScaleHDRepository.PatientPhysiotherapistBorgScaleHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "4_" + i, Parent_ID = "4", Name = "Skala Borga " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTbe03dd3ad74d415a8cdb313f76e5ba9a", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistBorgScaleHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            //Stopień sprawności wg Karnofsky
            categories.Add(new Hospice.Models.Category { ID = "5", Parent_ID = null, Name = "Stopień sprawności wg Karnofsky" });
            i = 1;
            foreach (PatientPhysiotherapistKarnofskyPerformanceStatusHD item in patientPhysiotherapistKarnofskyPerformanceStatusHDRepository.PatientPhysiotherapistKarnofskyPerformanceStatusHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "5_" + i, Parent_ID = "5", Name = "Stopień sprawości " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT13635254c31e4f1daaeacc66706f24ca", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistKarnofskyPerformanceStatusHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            //Indeks czynności jelit BFI
            categories.Add(new Hospice.Models.Category { ID = "6", Parent_ID = null, Name = "Indeks czynności jelit BFI" });
            i = 1;
            foreach (PatientPhysiotherapistBFIIndexHD item in patientPhysiotherapistBFIIndexHDRepository.PatientPhysiotherapistBFIIndexHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "6_" + i, Parent_ID = "6", Name = "Index BFI " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTe58116fa66734723855b2f7a68db2d24", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistBFIIndexHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            //RSCL Rotterdamska lista objawów
            categories.Add(new Hospice.Models.Category { ID = "7", Parent_ID = null, Name = "RSCL Rotterdamska lista objawów" });
            i = 1;
            foreach (PatientPhysiotherapistRSCLHD item in patientPhysiotherapistRSCLHDRepository.PatientPhysiotherapistRSCLHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "7_" + i, Parent_ID = "7", Name = "Lista obiawów " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT83bf3cdc7b2d45918b0efb708c8577f3", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistRSCLHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            //Skala bólu VAS
            categories.Add(new Hospice.Models.Category { ID = "8", Parent_ID = null, Name = "Skala bólu VAS" });
            i = 1;
            foreach (PatientPhysiotherapistVASHD item in patientPhysiotherapistVASHDRepository.PatientPhysiotherapistVASHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "8_" + i, Parent_ID = "8", Name = "Skala bólu " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT0c94724cc3e44370872fe7cd4eb64991", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistVASHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }
            categories.Add(new Hospice.Models.Category { ID = "9", Parent_ID = null, Name = "Skala Barthel wg pacjenta" });
            i = 1;
            foreach (PatientPhysiotherapistBarthelPatientHD item in patientPhysiotherapistBarthelPatientHDRepository.PatientPhysiotherapistBarthelPatientHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "9_" + i, Parent_ID = "9", Name = "Skala bólu " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTa558bd769f244046a5753a6871c555ef", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistBarthelPatientHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            categories.Add(new Hospice.Models.Category { ID = "10", Parent_ID = null, Name = "Skala Barthel wg fizjoterapeuty" });
            i = 1;
            foreach (PatientPhysiotherapistBarthelPhysiotherapistHD item in patientPhysiotherapistBarthelPhysiotherapistHDRepository.PatientPhysiotherapistBarthelPhysiotherapistHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "10_" + i, Parent_ID = "10", Name = "Skala bólu " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTb822bfb0345f4820aa8e93a870604401", item.fldIWfId), Area = "", Action = "DisplayPatientPhysiotherapistBarthelPhysiotherapistHD", Controller = "PatientCardPhysiotherapist", FldId = item.fldId });
                i++;
            }

            return new Models.SeededCategories { Seed = null, Categories = categories };

        }

        public ActionResult DisplayPatientPhysiotherapistFirstVisitHS(int fldId)
        {
            PatientPhysiotherapistFirstVisitHS model = patientPhysiotherapistFirstVisitHSRepository.PatientPhysiotherapistFirstVisitHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }



        public ActionResult DisplayPatientPhysiotherapistVisitAndCardHS(int fldId)
        {
            PhysiotherapistPatientVisitAndPatientCardViewModelHS model = new PhysiotherapistPatientVisitAndPatientCardViewModelHS();
            PatientPhysiotherapistPatientCardHS model1 = patientPhysiotherapistPatientCardHSRepository.PatientPhysiotherapistPatientCardHS.Where(p => p.fldIWfId == fldId).OrderByDescending(o => o.fldId).First();
            model1.CalculatedProcesy = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACT0998ae0bcbca4dc2b8b2040d0841ba71", model1.fldIWfId, new List<int> { 5, 7, 9 }, new List<string> { "Procedura", "Procedura szczegółowa", "Kod ICD-9" });
            PatientPhysiotherapistVisitHS model2 = patientPhysiotherapistVisitHSRepository.PatientPhysiotherapistVisitHS.FirstOrDefault(p => p.fldIWfId == model1.fldIWfId);
            
            model.PatientPhysiotherapistPatientCardHS = model1; 
            model.PatientPhysiotherapistVisitHS = model2;
            
            return View(model);
        }
        /*
        public ActionResult DisplayPatientPhysiotherapistPatientCardHS(int fldId)
        {
            PatientPhysiotherapistPatientCardHS model = patientPhysiotherapistPatientCardHSRepository.PatientPhysiotherapistPatientCardHS.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedProcesy = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACT0998ae0bcbca4dc2b8b2040d0841ba71", model.fldIWfId, new List<int> { 5, 7, 9 }, new List<string> { "Procedura", "Procedura szczegółowa", "Kod ICD-9"});
            return View(model);
        }

        public ActionResult DisplayPatientPhysiotherapistVisitHS(int fldId)
        {
            PatientPhysiotherapistVisitHS model = patientPhysiotherapistVisitHSRepository.PatientPhysiotherapistVisitHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }
        */
        public ActionResult DisplayPatientPhysiotherapistBorgScaleHS(int fldId)
        {
            PatientPhysiotherapistBorgScaleHS model = patientPhysiotherapistBorgScaleHSRepository.PatientPhysiotherapistBorgScaleHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientPhysiotherapistKarnofskyPerformanceStatusHS(int fldId)
        {
            PatientPhysiotherapistKarnofskyPerformanceStatusHS model = patientPhysiotherapistKarnofskyPerformanceStatusHSRepository.PatientPhysiotherapistKarnofskyPerformanceStatusHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }
        public ActionResult DisplayPatientPhysiotherapistBFIIndexHS(int fldId)
        {
            PatientPhysiotherapistBFIIndexHS model = patientPhysiotherapistBFIIndexHSRepository.PatientPhysiotherapistBFIIndexHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }
        public ActionResult DisplayPatientPhysiotherapistRSCLHS(int fldId)
        {
            PatientPhysiotherapistRSCLHS model = patientPhysiotherapistRSCLHSRepository.PatientPhysiotherapistRSCLHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }
        public ActionResult DisplayPatientPhysiotherapistVASHS(int fldId)
        {
            PatientPhysiotherapistVASHS model = patientPhysiotherapistVASHSRepository.PatientPhysiotherapistVASHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientPhysiotherapistBarthelPatientHS(int fldId)
        {
            PhysiotherapistPatientBarthelPatientViewModelHS model = new PhysiotherapistPatientBarthelPatientViewModelHS();
            model.PatientPhysiotherapistBarthelPatientHS = patientPhysiotherapistBarthelPatientHSRepository.PatientPhysiotherapistBarthelPatientHS.FirstOrDefault(p => p.fldId == fldId);
            model.PatientPhysiotherapistBarthelPatientResultHS = patientPhysiotherapistBarthelPatientResultHSRepository.PatientPhysiotherapistBarthelPatientResultHS.FirstOrDefault(p => p.fldIWfId == model.PatientPhysiotherapistBarthelPatientHS.fldIWfId);
            return View(model);
        }

        public ActionResult DisplayPatientPhysiotherapistBarthelPhysiotherapistHS(int fldId)
        {
            PhysiotherapistPatientBarthelPhysiotherapistViewModelHS model = new PhysiotherapistPatientBarthelPhysiotherapistViewModelHS();
            model.PatientPhysiotherapistBarthelPhysiotherapistHS = patientPhysiotherapistBarthelPhysiotherapistHSRepository.PatientPhysiotherapistBarthelPhysiotherapistHS.FirstOrDefault(p => p.fldId == fldId);
            model.PatientPhysiotherapistBarthelPhysiotherapistResultHS = patientPhysiotherapistBarthelPhysiotherapistResultHSRepository.PatientPhysiotherapistBarthelPhysiotherapistResultHS.FirstOrDefault(p => p.fldIWfId == model.PatientPhysiotherapistBarthelPhysiotherapistHS.fldIWfId);
            return View(model);
        }

        public ActionResult DisplayPatientPhysiotherapistFirstVisitHD(int fldId)
        {
            PatientPhysiotherapistFirstVisitHD model = patientPhysiotherapistFirstVisitHDRepository.PatientPhysiotherapistFirstVisitHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientPhysiotherapistVisitAndCardHD(int fldId)
        {
            PhysiotherapistPatientVisitAndPatientCardViewModelHD model = new PhysiotherapistPatientVisitAndPatientCardViewModelHD();
            PatientPhysiotherapistPatientCardHD model1 = patientPhysiotherapistPatientCardHDRepository.PatientPhysiotherapistPatientCardHD.Where(p => p.fldIWfId == fldId).OrderByDescending(o => o.fldId).First();
            model1.CalculatedProcesy = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACT60b96b6395444b8890fbd2e1ccac74db", model1.fldIWfId, new List<int> { 7, 4, 5 }, new List<string> { "Procedura", "Procedura szczegółowa", "Kod ICD-9" });
            PatientPhysiotherapistVisitHD model2 = patientPhysiotherapistVisitHDRepository.PatientPhysiotherapistVisitHD.FirstOrDefault(p => p.fldIWfId == model1.fldIWfId);

            model.PatientPhysiotherapistPatientCardHD = model1;
            model.PatientPhysiotherapistVisitHD = model2;

            return View(model);
        }
        /*
        public ActionResult DisplayPatientPhysiotherapistPatientCardHD(int fldId)
        {
            PatientPhysiotherapistPatientCardHD model = patientPhysiotherapistPatientCardHDRepository.PatientPhysiotherapistPatientCardHD.FirstOrDefault(p => p.fldId == fldId);
            //model.CalculatedProcesy = Hospice.Infrastructure.SQLHelper.ListForTableXBesideIdColumns("UACT60b96b6395444b8890fbd2e1ccac74db", model.fldIWfId, 4);

            model.CalculatedProcesy = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACT0998ae0bcbca4dc2b8b2040d0841ba71", model.fldIWfId, new List<int> { 7, 4, 5 }, new List<string> { "Procedura", "Procedura szczegółowa", "Kod ICD-9" });
            return View(model);
        }

        public ActionResult DisplayPatientPhysiotherapistVisitHD(int fldId)
        {
            PatientPhysiotherapistVisitHD model = patientPhysiotherapistVisitHDRepository.PatientPhysiotherapistVisitHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }
        */
        public ActionResult DisplayPatientPhysiotherapistBorgScaleHD(int fldId)
        {
            PatientPhysiotherapistBorgScaleHD model = patientPhysiotherapistBorgScaleHDRepository.PatientPhysiotherapistBorgScaleHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientPhysiotherapistKarnofskyPerformanceStatusHD(int fldId)
        {
            PatientPhysiotherapistKarnofskyPerformanceStatusHD model = patientPhysiotherapistKarnofskyPerformanceStatusHDRepository.PatientPhysiotherapistKarnofskyPerformanceStatusHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }
        public ActionResult DisplayPatientPhysiotherapistBFIIndexHD(int fldId)
        {
            PatientPhysiotherapistBFIIndexHD model = patientPhysiotherapistBFIIndexHDRepository.PatientPhysiotherapistBFIIndexHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }
        public ActionResult DisplayPatientPhysiotherapistRSCLHD(int fldId)
        {
            PatientPhysiotherapistRSCLHD model = patientPhysiotherapistRSCLHDRepository.PatientPhysiotherapistRSCLHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }
        public ActionResult DisplayPatientPhysiotherapistVASHD(int fldId)
        {
            PatientPhysiotherapistVASHD model = patientPhysiotherapistVASHDRepository.PatientPhysiotherapistVASHD.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientPhysiotherapistBarthelPatientHD(int fldId)
        {
            PhysiotherapistPatientBarthelPatientViewModelHD model = new PhysiotherapistPatientBarthelPatientViewModelHD();
            model.PatientPhysiotherapistBarthelPatientHD = patientPhysiotherapistBarthelPatientHDRepository.PatientPhysiotherapistBarthelPatientHD.FirstOrDefault(p => p.fldId == fldId);
            model.PatientPhysiotherapistBarthelPatientResultHD = patientPhysiotherapistBarthelPatientResultHDRepository.PatientPhysiotherapistBarthelPatientResultHD.FirstOrDefault(p => p.fldIWfId == model.PatientPhysiotherapistBarthelPatientHD.fldIWfId);
            return View(model);
        }

        public ActionResult DisplayPatientPhysiotherapistBarthelPhysiotherapistHD(int fldId)
        {
            PhysiotherapistPatientBarthelPhysiotherapistViewModelHD model = new PhysiotherapistPatientBarthelPhysiotherapistViewModelHD();
            model.PatientPhysiotherapistBarthelPhysiotherapistHD = patientPhysiotherapistBarthelPhysiotherapistHDRepository.PatientPhysiotherapistBarthelPhysiotherapistHD.FirstOrDefault(p => p.fldId == fldId);
            model.PatientPhysiotherapistBarthelPhysiotherapistResultHD = patientPhysiotherapistBarthelPhysiotherapistResultHDRepository.PatientPhysiotherapistBarthelPhysiotherapistResultHD.FirstOrDefault(p => p.fldIWfId == model.PatientPhysiotherapistBarthelPhysiotherapistHD.fldIWfId);
            return View(model);
        }

        
        
	}
}