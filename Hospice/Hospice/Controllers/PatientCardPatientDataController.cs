﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hospice.Infrastructure.Helpers;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;
using Hospice.Models;
using Hospice.Infrastructure.Attributes;

namespace Hospice.Controllers
{
    [CustomAuthorize]
    public class PatientCardPatientDataController : Controller
    {
        private IPatientRegisteryRegisteryHSRepository patientRegisteryRegisteryHSRepository;
        private IPatientRegisteryInsuranceHSRepository patientRegisteryInsuranceHSRepository;
        private IPatientRegisteryEstablishmentOfMedicalHistoryHSRepository patientRegisteryEstablishmentOfMedicalHistoryHSRepository;
        private IPatientRegisteryFamilyDataHSRepository patientRegisteryFamilyDataHSRepository;
        private IPatientRegisteryFinalizingAdmissionHSRepository patientRegisteryFinalizingAdmissionHSRepository;
        private IPatientRegisteryRiskAssessmentFormHSRepository patientRegisteryRiskAssessmentFormHSRepository;
        private IPatientRegisteryRiskAssessmentFormResultHSRepository patientRegisteryRiskAssessmentFormResultHSRepository;
        private IPatientRegisteryEpikryzaExtractsHSRepository patientRegisteryEpikryzaExtractsHSRepository;
        private IPatientRegisteryEpikryzaHSRepository patientRegisteryEpikryzaHSRepository;
        private IPatientRegisteryHospitalStayHSRepository patientRegisteryHospitalStayHSRepository;
        private IPatientRegisteryHDRepository patientRegisteryHDRepository;
        private IPatientRegisteryReRegistrationHDRepository patientRegisteryReRegistrationHDRepository;
        private IPatientRegisteryInsuranceHDRepository patientRegisteryInsuranceHDRepository;
        private IPatientRegisterySigningConsentHDRepository patientRegisterySigningConsentHDRepository;
        private IPatientRegisteryEpikryzaHDRepository patientRegisteryEpikryzaHDRepository;
        private IPatientRegisteryEpikryzaExtractsHDRepository patientRegisteryEpikryzaExtractsHDRepository;
        private IPatientRegisteryPermanentlyToHospitalHDRepository patientRegisteryPermanentlyToHospitalHDRepository;
        private IPatientRegisteryHospitalStayHDRepository patientRegisteryHospitalStayHDRepository;
        private IPatientAttachmentsHSRepository patientAttachmentsHSRepository;
        private IPatientAttachmentsHDRepository patientAttachmentsHDRepository;

        public PatientCardPatientDataController(
            IPatientRegisteryRegisteryHSRepository patientRegisteryRegisteryHSRepo,
            IPatientRegisteryInsuranceHSRepository patientRegisteryInsuranceHSRepo,
            IPatientRegisteryEstablishmentOfMedicalHistoryHSRepository patientRegisteryEstablishmentOfMedicalHistoryHSRepo,
            IPatientRegisteryFamilyDataHSRepository patientRegisteryFamilyDataHSRepo,
            IPatientRegisteryFinalizingAdmissionHSRepository patientRegisteryFinalizingAdmissionHSRepo,
            IPatientRegisteryRiskAssessmentFormHSRepository patientRegisteryRiskAssessmentFormHSRepo,
            IPatientRegisteryRiskAssessmentFormResultHSRepository patientRegisteryRiskAssessmentFormResultHSRepo,
            IPatientRegisteryEpikryzaExtractsHSRepository patientRegisteryEpikryzaExtractsHSRepo,
            IPatientRegisteryEpikryzaHSRepository patientRegisteryEpikryzaHSRepo,
            IPatientRegisteryHospitalStayHSRepository patientRegisteryHospitalStayHSRepo,
            IPatientRegisteryHDRepository patientRegisteryHDRepo,
            IPatientRegisteryReRegistrationHDRepository patientRegisteryReRegistrationHDRepo,
            IPatientRegisteryInsuranceHDRepository patientRegisteryInsuranceHDRepo,
            IPatientRegisterySigningConsentHDRepository patientRegisterySigningConsentHDRepo,
            IPatientRegisteryEpikryzaHDRepository patientRegisteryEpikryzaHDRepo,
            IPatientRegisteryEpikryzaExtractsHDRepository patientRegisteryEpikryzaExtractsHDRepo,
            IPatientRegisteryPermanentlyToHospitalHDRepository patientRegisteryPermanentlyToHospitalHDRepo,
            IPatientRegisteryHospitalStayHDRepository patientRegisteryHospitalStayHDRepo,
            IPatientAttachmentsHSRepository patientAttachmentsHSRepo,
            IPatientAttachmentsHDRepository patientAttachmentsHDRepo
            )
        {
        patientRegisteryRegisteryHSRepository = patientRegisteryRegisteryHSRepo;
        patientRegisteryInsuranceHSRepository = patientRegisteryInsuranceHSRepo;
        patientRegisteryEstablishmentOfMedicalHistoryHSRepository = patientRegisteryEstablishmentOfMedicalHistoryHSRepo;
        patientRegisteryFamilyDataHSRepository = patientRegisteryFamilyDataHSRepo;
        patientRegisteryFinalizingAdmissionHSRepository = patientRegisteryFinalizingAdmissionHSRepo;
        patientRegisteryRiskAssessmentFormHSRepository = patientRegisteryRiskAssessmentFormHSRepo;
        patientRegisteryRiskAssessmentFormResultHSRepository = patientRegisteryRiskAssessmentFormResultHSRepo;
        patientRegisteryEpikryzaExtractsHSRepository = patientRegisteryEpikryzaExtractsHSRepo;
        patientRegisteryEpikryzaHSRepository = patientRegisteryEpikryzaHSRepo;
        patientRegisteryHospitalStayHSRepository = patientRegisteryHospitalStayHSRepo;
        patientRegisteryHDRepository = patientRegisteryHDRepo;
        patientRegisteryReRegistrationHDRepository = patientRegisteryReRegistrationHDRepo;
        patientRegisteryInsuranceHDRepository = patientRegisteryInsuranceHDRepo;
        patientRegisterySigningConsentHDRepository = patientRegisterySigningConsentHDRepo;
        patientRegisteryEpikryzaHDRepository = patientRegisteryEpikryzaHDRepo;
        patientRegisteryEpikryzaExtractsHDRepository = patientRegisteryEpikryzaExtractsHDRepo;
        patientRegisteryPermanentlyToHospitalHDRepository = patientRegisteryPermanentlyToHospitalHDRepo;
        patientRegisteryHospitalStayHDRepository = patientRegisteryHospitalStayHDRepo;
        patientAttachmentsHSRepository = patientAttachmentsHSRepo;
        patientAttachmentsHDRepository = patientAttachmentsHDRepo;
        }

        public ActionResult PatientCardPatientMedicalHistoryPatientDataHS()
        {
            MenuHelper.SetCurrentCardMenuItem(5);
            return View(TreeGenerateModelForPatientDataHS());
        }

        public ActionResult PatientCardPatientMedicalHistoryPatientDataHD()
        {
            MenuHelper.SetCurrentCardMenuItem(5);
            return View(TreeGenerateModelForPatientDataHD());
        }


        private Hospice.Models.SeededCategories TreeGenerateModelForPatientDataHS()
        {
            int i = 0;
            List<int> workflowIds = SessionHelper.GetUICurrentSettings().SelectedPatientForRegisteryWorkflowIdsHS.ToList();
            IList<Hospice.Models.Category> categories = new List<Hospice.Models.Category>();

            //Rejestracja pacjenta
            categories.Add(new Hospice.Models.Category { ID = "1", Parent_ID = null, Name = "Rejestracja pacjenta" });
            i = 1;
            foreach (PatientRegisteryRegisteryHS item in patientRegisteryRegisteryHSRepository.PatientRegisteryRegisteryHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "1_" + i, Parent_ID = "1", Name = "Rejestracja " + (item.dtpData.HasValue ? item.dtpData.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientRegisteryRegisteryHS", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }


            //Ubezpieczenie
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Ubezpieczenie" });
            i = 1;
            foreach (PatientRegisteryInsuranceHS item in patientRegisteryInsuranceHSRepository.PatientRegisteryInsuranceHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Ubezpieczenie " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTd44c30b0c7034b3fa0bb5cc21cae9caa", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryInsuranceHS", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }
            
            //Założenie historii choroby
            categories.Add(new Hospice.Models.Category { ID = "3", Parent_ID = null, Name = "Założenie historii choroby" });
            i = 1;
            foreach (PatientRegisteryEstablishmentOfMedicalHistoryHS item in patientRegisteryEstablishmentOfMedicalHistoryHSRepository.PatientRegisteryEstablishmentOfMedicalHistoryHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "3_" + i, Parent_ID = "3", Name = "Historia choroby " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTe7134ccb2540437bb8f1756483ec0336", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryEstablishmentOfMedicalHistoryHS", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //Dane o pacjencie i rodzinie
            categories.Add(new Hospice.Models.Category { ID = "4", Parent_ID = null, Name = "Dane o pacjencie i rodzinie" });
            i = 1;
            foreach (PatientRegisteryFamilyDataHS item in patientRegisteryFamilyDataHSRepository.PatientRegisteryFamilyDataHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "4_" + i, Parent_ID = "4", Name = "Dane " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT9b34929a538c4e96a60935eded611f07", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryFamilyDataHS", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }            

            //Finalizowanie przyjęcia pacjenta
            categories.Add(new Hospice.Models.Category { ID = "5", Parent_ID = null, Name = "Podpisanie dokumentów z pacjentem" });
            i = 1;
            foreach (PatientRegisteryFinalizingAdmissionHS item in patientRegisteryFinalizingAdmissionHSRepository.PatientRegisteryFinalizingAdmissionHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "5_" + i, Parent_ID = "5", Name = "Dokumenty " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTb9985cfac1d54d90b8a894c212037f38", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryFinalizingAdmissionHS", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            
            //Formularz oceny ryzyka zakażenia
            categories.Add(new Hospice.Models.Category { ID = "6", Parent_ID = null, Name = "Formularz oceny ryzyka zakażenia wraz z wynikiem" });
            i = 1;
            foreach (PatientRegisteryRiskAssessmentFormHS item in patientRegisteryRiskAssessmentFormHSRepository.PatientRegisteryRiskAssessmentFormHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "6_" + i, Parent_ID = "6", Name = "Formularz " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT92495a4484f44e91b2292d77d21e7de5", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryRiskAssessmentFormHS", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //Epikryza wypisowa
            categories.Add(new Hospice.Models.Category { ID = "7", Parent_ID = null, Name = "Epikryza wypisowa" });
            i = 1;
            foreach (PatientRegisteryEpikryzaExtractsHS item in patientRegisteryEpikryzaExtractsHSRepository.PatientRegisteryEpikryzaExtractsHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "7_" + i, Parent_ID = "7", Name = "Epikryza wypisowa " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTd715eaf74aec461489263677755158d7", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryEpikryzaExtractsHS", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }
            

            //Epikryza
            categories.Add(new Hospice.Models.Category { ID = "8", Parent_ID = null, Name = "Epikryza" });
            i = 1;
            foreach (PatientRegisteryEpikryzaHS item in patientRegisteryEpikryzaHSRepository.PatientRegisteryEpikryzaHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "8_" + i, Parent_ID = "8", Name = "Epikryza " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTc151b469aa8d488e8d689133cf4441f6", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryEpikryzaHS", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //Pobyt w szpitalu
            categories.Add(new Hospice.Models.Category { ID = "9", Parent_ID = null, Name = "Pobyt w szpitalu" });
            i = 1;
            foreach (PatientRegisteryHospitalStayHS item in patientRegisteryHospitalStayHSRepository.PatientRegisteryHospitalStayHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "9_" + i, Parent_ID = "9", Name = "Pobyt " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTdfa3c31754134cea9009a9798f3cb5b7", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryHospitalStayHS", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //Osoby kontaktowe

            categories.Add(new Hospice.Models.Category { ID = "10", Parent_ID = null, Name = "Osoby do kontaktu" });
            i = 1;
            foreach (PatientRegisteryFinalizingAdmissionHS item in patientRegisteryFinalizingAdmissionHSRepository.PatientRegisteryFinalizingAdmissionHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "10_" + i, Parent_ID = "10", Name = "List osób do kontaktu", Area = "", Action = "DisplayPatientRegisteryFinalizingAdmissionContactPersonsHS", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //Załączniki

            categories.Add(new Hospice.Models.Category { ID = "11", Parent_ID = null, Name = "Skany dokumentów pacjenta" });
            i = 1;
            foreach (PatientAttachmentsHS item in patientAttachmentsHSRepository.PatientAttachmentsHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderBy(ob => ob.fldFileName))
            {
                categories.Add(new Hospice.Models.Category { ID = "11_" + i, Parent_ID = "11", Name = item.fldFileName, Area = "", Action = "DisplayPatientAttachmentsHS", Controller = "PatientCardPatientData", FileGUID = item.fldId });
                i++;
            } 

            return new Models.SeededCategories { Seed = null, Categories = categories };

        }

        private Hospice.Models.SeededCategories TreeGenerateModelForPatientDataHD()
        {

            int i = 0;
            List<int> workflowIds = SessionHelper.GetUICurrentSettings().SelectedPatientForRegisteryWorkflowIdsHD.ToList();
            workflowIds.AddRange(SessionHelper.GetUICurrentSettings().SelectedPatientForCommissionedHireWorkflowIdsHD);

            IList<Hospice.Models.Category> categories = new List<Hospice.Models.Category>();

            //Rejestracja pacjenta
            categories.Add(new Hospice.Models.Category { ID = "1", Parent_ID = null, Name = "Rejestracja pacjenta" });
            i = 1;
            foreach (PatientRegisteryHD item in patientRegisteryHDRepository.PatientRegisteryHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "1_" + i, Parent_ID = "1", Name = "Rejestracja " + (item.dtpData.HasValue ? item.dtpData.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientRegisteryHD", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //Ponowna rejestracja pacjenta
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Ponowna rejestracja pacjenta" });
            i = 1;
            foreach (PatientRegisteryReRegistrationHD item in patientRegisteryReRegistrationHDRepository.PatientRegisteryReRegistrationHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Rejestracja " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTba571a6ad1304ff9b84c6ea83cc4af2b", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryReRegistrationHD", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //ubezpieczenie
            categories.Add(new Hospice.Models.Category { ID = "3", Parent_ID = null, Name = "Ubezpieczenie" });
            i = 1;
            foreach (PatientRegisteryInsuranceHD item in patientRegisteryInsuranceHDRepository.PatientRegisteryInsuranceHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "3_" + i, Parent_ID = "3", Name = "Ubezpieczenie " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTc5b9f23845d34c439a8fc1572f1714d2", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryInsuranceHD", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }
                        
            //Podpisanie zgody na leczenie hospicyjne
            categories.Add(new Hospice.Models.Category { ID = "4", Parent_ID = null, Name = "Podpisanie zgody na leczenie hospicyjne" });
            i = 1;
            foreach (PatientRegisterySigningConsentHD item in patientRegisterySigningConsentHDRepository.PatientRegisterySigningConsentHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "3_" + i, Parent_ID = "3", Name = "Zgoda " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT8f45f0ee923c40b1a03b7d4d2f1bf7bd", item.fldIWfId), Area = "", Action = "DisplayPatientRegisterySigningConsentHD", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }
            //Epikryza
            categories.Add(new Hospice.Models.Category { ID = "5", Parent_ID = null, Name = "Epikryza" });
            i = 1;
            foreach (PatientRegisteryEpikryzaHD item in patientRegisteryEpikryzaHDRepository.PatientRegisteryEpikryzaHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "5_" + i, Parent_ID = "5", Name = "Epikryza " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT6b9280a6ace0410e833f431177fc8210", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryEpikryzaHD", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //Epikryza wypisowa
            categories.Add(new Hospice.Models.Category { ID = "6", Parent_ID = null, Name = "Epikryza wypisowa" });
            i = 1;
            foreach (PatientRegisteryEpikryzaExtractsHD item in patientRegisteryEpikryzaExtractsHDRepository.PatientRegisteryEpikryzaExtractsHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "6_" + i, Parent_ID = "6", Name = "Epikryza wypisowa " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT761851d997e145b9bef3c36a7a50a23a", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryEpikryzaExtractsHD", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //Wypis na stałe do szpitala
            categories.Add(new Hospice.Models.Category { ID = "7", Parent_ID = null, Name = "Wypis na stałe do szpitala" });
            i = 1;
            foreach (PatientRegisteryPermanentlyToHospitalHD item in patientRegisteryPermanentlyToHospitalHDRepository.PatientRegisteryPermanentlyToHospitalHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "7_" + i, Parent_ID = "7", Name = "Wipis " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT19b7f45e38b5479f9ed95287351ba992", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryPermanentlyToHospitalHD", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //Pobyt w szpitalu
            categories.Add(new Hospice.Models.Category { ID = "8", Parent_ID = null, Name = "Pobyt w szpitalu" });
            i = 1;
            foreach (PatientRegisteryHospitalStayHD item in patientRegisteryHospitalStayHDRepository.PatientRegisteryHospitalStayHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "8_" + i, Parent_ID = "8", Name = "Pobyt " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTa88e52f48006475fbb5d936be3313948", item.fldIWfId), Area = "", Action = "DisplayPatientRegisteryHospitalStayHD", Controller = "PatientCardPatientData", FldId = item.fldId });
                i++;
            }

            //Załączniki

            categories.Add(new Hospice.Models.Category { ID = "9", Parent_ID = null, Name = "Skany dokumentów pacjenta" });
            i = 1;
            foreach (PatientAttachmentsHD item in patientAttachmentsHDRepository.PatientAttachmentsHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderBy(ob=> ob.fldFileName))
            {
                categories.Add(new Hospice.Models.Category { ID = "9_" + i, Parent_ID = "9", Name = item.fldFileName, Area = "", Action = "DisplayPatientAttachmentsHD", Controller = "PatientCardPatientData", FileGUID = item.fldId });
                i++;
            } 

            return new Models.SeededCategories { Seed = null, Categories = categories };

        }

        public ActionResult DisplayPatientRegisteryRegisteryHS(int fldId)
        {
            PatientRegisteryRegisteryHS model = patientRegisteryRegisteryHSRepository.PatientRegisteryRegisteryHS.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedAdresPacjenta = model.txtulica + " " + model.txtnumerdomu + ", " + model.lblkodpocztowy + " " + model.txtmiejscowosc;

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryRegisteryHS";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryInsuranceHS(int fldId)
        {
            PatientRegisteryInsuranceHS model = patientRegisteryInsuranceHSRepository.PatientRegisteryInsuranceHS.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryInsuranceHS";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryEstablishmentOfMedicalHistoryHS(int fldId)
        {
            PatientRegisteryEstablishmentOfMedicalHistoryHS model = patientRegisteryEstablishmentOfMedicalHistoryHSRepository.PatientRegisteryEstablishmentOfMedicalHistoryHS.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedAddress = model.txtulica + " " + model.txtnumerdomu + ", " + model.txtkod + " " + model.txtmiejscowosc;

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryEstablishmentOfMedicalHistoryHS";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryFamilyDataHS(int fldId)
        {
            PatientRegisteryFamilyDataHS model = patientRegisteryFamilyDataHSRepository.PatientRegisteryFamilyDataHS.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryFamilyDataHS";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryFinalizingAdmissionHS(int fldId)
        {
            PatientRegisteryFinalizingAdmissionHS model = patientRegisteryFinalizingAdmissionHSRepository.PatientRegisteryFinalizingAdmissionHS.FirstOrDefault(p => p.fldId == fldId);
            //model.CalculatedOsoby = Hospice.Infrastructure.SQLHelper.ListForTableXBesideIdColumns("UACT53cccc387e18462e9bc9094551eb1af2", model.fldIWfId, 3);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryFinalizingAdmissionHS";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryRiskAssessmentFormHS(int fldId)
        {
            PatientRegisteryRiskAssessmentFormViewModelHS model = new PatientRegisteryRiskAssessmentFormViewModelHS();
            model.PatientRegisteryRiskAssessmentFormHS = patientRegisteryRiskAssessmentFormHSRepository.PatientRegisteryRiskAssessmentFormHS.FirstOrDefault(p => p.fldId == fldId);
            model.PatientRegisteryRiskAssessmentFormResultHS = patientRegisteryRiskAssessmentFormResultHSRepository.PatientRegisteryRiskAssessmentFormResultHS.FirstOrDefault(p => p.fldIWfId == model.PatientRegisteryRiskAssessmentFormHS.fldIWfId);


            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryRiskAssessmentFormHS";

            return View(model);
        }
        
        public ActionResult DisplayPatientRegisteryEpikryzaExtractsHS(int fldId)
        {
            PatientRegisteryEpikryzaExtractsHS model = patientRegisteryEpikryzaExtractsHSRepository.PatientRegisteryEpikryzaExtractsHS.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryEpikryzaExtractsHS";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryEpikryzaHS(int fldId)
        {
            PatientRegisteryEpikryzaHS model = patientRegisteryEpikryzaHSRepository.PatientRegisteryEpikryzaHS.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryEpikryzaHS";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryHospitalStayHS(int fldId)
        {
            PatientRegisteryHospitalStayHS model = patientRegisteryHospitalStayHSRepository.PatientRegisteryHospitalStayHS.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryHospitalStayHS";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryFinalizingAdmissionContactPersonsHS(int fldId)
        {
            PatientRegisteryFinalizingAdmissionHS modelParent = patientRegisteryFinalizingAdmissionHSRepository.PatientRegisteryFinalizingAdmissionHS.FirstOrDefault(p => p.fldId == fldId);

            List<string> model = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACT53cccc387e18462e9bc9094551eb1af2", modelParent.fldIWfId, new List<int> { 4, 5, 6, 7, 9, 8, 10, 11, 13 }, new List<string> { "", "", "", "", "", "TFbool", "TFbool", "TFbool", "", }, new List<string> { "Imię i nazwisko", "PESEL", "Adres", "Telefon", "Numer dokumentu", "Upoważniony do udostępniania dokumentacji medycznej", "Upoważniony do udostępniania dokumentacji medycznej na wypadek śmierci", "Upoważniony do udostępniania informacji o stanie zdrowia i udzielonych świadczeniach", "Opiekun" });


            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryFinalizingAdmissionContactPersonsHS";
            
            return View(model);
        }

        public ActionResult DisplayPatientAttachmentsHS(Guid fldId)
        {
            PatientAttachmentsHS model = patientAttachmentsHSRepository.PatientAttachmentsHS.FirstOrDefault(p => p.fldId == fldId);

            byte[] contents = model.fldContent;
            return File(contents, model.fldContentType, model.fldFileName);                       
            
        }



        public ActionResult DisplayPatientRegisteryHD(int fldId)
        {
            PatientRegisteryHD model = patientRegisteryHDRepository.PatientRegisteryHD.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedAdresPacjenta = model.txtulica + " " + model.txtnumerdomu + ", " + model.lblkodpocztowy + " " + model.txtmiejscowosc;

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryHD";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryReRegistrationHD(int fldId)
        {
            PatientRegisteryReRegistrationHD model = patientRegisteryReRegistrationHDRepository.PatientRegisteryReRegistrationHD.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryReRegistrationHD";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryInsuranceHD(int fldId)
        {
            PatientRegisteryInsuranceHD model = patientRegisteryInsuranceHDRepository.PatientRegisteryInsuranceHD.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryInsuranceHD";

            return View(model);
        }
        public ActionResult DisplayPatientRegisterySigningConsentHD(int fldId)
        {
            PatientRegisterySigningConsentHD model = patientRegisterySigningConsentHDRepository.PatientRegisterySigningConsentHD.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisterySigningConsentHD";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryEpikryzaHD(int fldId)
        {
            PatientRegisteryEpikryzaHD model = patientRegisteryEpikryzaHDRepository.PatientRegisteryEpikryzaHD.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryEpikryzaHD";

            return View(model);
        }

        public ActionResult DisplayPatientRegisteryEpikryzaExtractsHD(int fldId)
        {
            PatientRegisteryEpikryzaExtractsHD model = patientRegisteryEpikryzaExtractsHDRepository.PatientRegisteryEpikryzaExtractsHD.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryEpikryzaExtractsHD";

            return View(model);
        }
        public ActionResult DisplayPatientRegisteryPermanentlyToHospitalHD(int fldId)
        {
            PatientRegisteryPermanentlyToHospitalHD model = patientRegisteryPermanentlyToHospitalHDRepository.PatientRegisteryPermanentlyToHospitalHD.FirstOrDefault(p => p.fldId == fldId);
            
            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryPermanentlyToHospitalHD";
            
            return View(model);
        }

        public ActionResult DisplayPatientRegisteryHospitalStayHD(int fldId)
        {
            PatientRegisteryHospitalStayHD model = patientRegisteryHospitalStayHDRepository.PatientRegisteryHospitalStayHD.FirstOrDefault(p => p.fldId == fldId);

            SessionHelper.GetUICurrentSettings().SelectedCommentItemId = fldId;
            SessionHelper.GetUICurrentSettings().SelectedCommentControllerName = "PatientCardPatientData";
            SessionHelper.GetUICurrentSettings().SelectedCommentViewName = "DisplayPatientRegisteryHospitalStayHD";

            return View(model);
        }

        public ActionResult DisplayPatientAttachmentsHD(Guid fldId)
        {
            PatientAttachmentsHD model = patientAttachmentsHDRepository.PatientAttachmentsHD.FirstOrDefault(p => p.fldId == fldId);

            byte[] contents = model.fldContent;
            return File(contents, model.fldContentType, model.fldFileName);

        }
	}
}