﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Hospice.Infrastructure.Helpers;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;
using Hospice.Infrastructure.Attributes;

namespace Hospice.Controllers
{
    [CustomAuthorize]
    public class PatientCardNurseController : Controller
    {
        private Hospice.Domain.Concrete.EFDbContext db = new Hospice.Domain.Concrete.EFDbContext();
        private IPatientFluidBalanceHSRepository patientFluidBalanceHSRepository;
        private IPatientGlycemicProfileHSRepository patientGlycemicProfileHSRepository;
        private IPatientCumulativePatientConditionHSRepository patientCumulativePatientConditionHSRepository;
        private IPatientOrdersMedicalHSRepository patientOrdersMedicalHSRepository;
        private IPatientIndividualNursingCardHSRepository patientIndividualNursingCardHSRepository;
        private IPatientFebrileCardHSRepository patientFebrileCardHSRepository;
        private IPatientNeoplasticTreatmentProtocolHSRepository patientNeoplasticTreatmentProtocolHSRepository;
        private IPatientBedsoresProtocolHSRepository patientBedsoresProtocolHSRepository;
        private IPatientOrdersMedicalHDRepository patientOrdersMedicalHDRepository;
        private IPatientIndividualNursingCardHDRepository patientIndividualNursingCardHDRepository;

        public PatientCardNurseController(
            IPatientFluidBalanceHSRepository patientFluidBalanceHSRepo, IPatientGlycemicProfileHSRepository patientGlycemicProfileHSRepo,
            IPatientCumulativePatientConditionHSRepository patientCumulativePatientConditionHSRepo, IPatientOrdersMedicalHSRepository patientOrdersMedicalHSRepo,
            IPatientIndividualNursingCardHSRepository patientIndividualNursingCardHSRepo, IPatientFebrileCardHSRepository patientFebrileCardHSRepo,
            IPatientNeoplasticTreatmentProtocolHSRepository patientNeoplasticTreatmentProtocolHSRepo, IPatientBedsoresProtocolHSRepository patientBedsoresProtocolHSRepo,
            IPatientOrdersMedicalHDRepository patientOrdersMedicalHDRepo, IPatientIndividualNursingCardHDRepository patientIndividualNursingCardHDRepo
            )
        {
            patientFluidBalanceHSRepository = patientFluidBalanceHSRepo;
            patientGlycemicProfileHSRepository = patientGlycemicProfileHSRepo;
            patientCumulativePatientConditionHSRepository = patientCumulativePatientConditionHSRepo;
            patientOrdersMedicalHSRepository = patientOrdersMedicalHSRepo;
            patientIndividualNursingCardHSRepository = patientIndividualNursingCardHSRepo;
            patientFebrileCardHSRepository = patientFebrileCardHSRepo;
            patientNeoplasticTreatmentProtocolHSRepository = patientNeoplasticTreatmentProtocolHSRepo;
            patientBedsoresProtocolHSRepository = patientBedsoresProtocolHSRepo;
            patientOrdersMedicalHDRepository = patientOrdersMedicalHDRepo;
            patientIndividualNursingCardHDRepository = patientIndividualNursingCardHDRepo;
        }

        public ActionResult PatientCardPatientMedicalHistoryNurseHS()
        {
            MenuHelper.SetCurrentCardMenuItem(2); 
            return View(TreeGenerateModelForNurseHS());
        }

        public ActionResult PatientCardPatientMedicalHistoryNurseHD()
        {
            MenuHelper.SetCurrentCardMenuItem(2); 
            return View(TreeGenerateModelForNurseHD());
        }

        #region trees nurse
        private Hospice.Models.SeededCategories TreeGenerateModelForNurseHS()
        {
            int i = 0;
            List<int> workflowIds = SessionHelper.GetUICurrentSettings().SelectedPatientForNurseWorkflowIdsHS.ToList();
            IList<Hospice.Models.Category> categories = new List<Hospice.Models.Category>();
                       
            //bilans płynów
            categories.Add(new Hospice.Models.Category { ID = "1", Parent_ID = null, Name = "Bilans płynów" });
            i = 1;
            foreach (PatientFluidBalanceHS item in patientFluidBalanceHSRepository.PatientFluidBalanceHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "1_" + i, Parent_ID = "1", Name = "Bilans", Area = "", Action = "DisplayPatientFluidBalanceHS", Controller = "PatientCardNurse", FldId = item.fldId });
                i++;
            }

            //Profil glikemii
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Profil glikemii" });
            i = 1;
            foreach (PatientGlycemicProfileHS item in patientGlycemicProfileHSRepository.PatientGlycemicProfileHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Profil", Area = "", Action = "DisplayPatientGlycemicProfileHS", Controller = "PatientCardNurse", FldId = item.fldId });
                i++;
            }

            //Zbiorcza uproszczona ocena stanu chorego
            categories.Add(new Hospice.Models.Category { ID = "3", Parent_ID = null, Name = "Zbiorcza uproszczona ocena stanu chorego" });
            i = 1;
            foreach (PatientCumulativePatientConditionHS item in patientCumulativePatientConditionHSRepository.PatientCumulativePatientConditionHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "3_" + i, Parent_ID = "3", Name = "Ocena " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT5bf9030f4d5a4a55ba5f24f9160f091e", item.fldIWfId), Area = "", Action = "DisplayPatientCumulativePatientConditionHS", Controller = "PatientCardNurse", FldId = item.fldId });
                i++;
            }

            //Zlecenia lekarskie
            categories.Add(new Hospice.Models.Category { ID = "4", Parent_ID = null, Name = "Zlecenia lekarskie" });
            i = 1;
            foreach (PatientOrdersMedicalHS item in patientOrdersMedicalHSRepository.PatientOrdersMedicalHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "4_" + i, Parent_ID = "4", Name = "Zlecenie " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACTff06667115f142dda4e10a6ae0b0e8f4", item.fldIWfId), Area = "", Action = "DisplayPatientOrdersMedicalHS", Controller = "PatientCardNurse", FldId = item.fldId });
                i++;
            }

            //Indywidualna karta pielęgniarska
            categories.Add(new Hospice.Models.Category { ID = "5", Parent_ID = null, Name = "Indywidualna karta pielęgniarska" });
            i = 1;
            foreach (PatientIndividualNursingCardHS item in patientIndividualNursingCardHSRepository.PatientIndividualNursingCardHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "5_" + i, Parent_ID = "5", Name = "Karta oceny " + (item.dtpdatawizyty.HasValue ? item.dtpdatawizyty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientIndividualNursingCardHS", Controller = "PatientCardNurse", FldId = item.fldId });
                i++;
            }

            //Karta gorączkowa
            categories.Add(new Hospice.Models.Category { ID = "6", Parent_ID = null, Name = "Karta gorączkowa" });
            i = 1;
            foreach (PatientFebrileCardHS item in patientFebrileCardHSRepository.PatientFebrileCardHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "6_" + i, Parent_ID = "6", Name = "Karta " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT8cd2e113d2e9403aa0dd562fed57a20e", item.fldIWfId), Area = "", Action = "DisplayPatientFebrileCardHS", Controller = "PatientCardNurse", FldId = item.fldId });
                i++;
            }

            //Protokół leczenia zmian nowotworowych
            categories.Add(new Hospice.Models.Category { ID = "7", Parent_ID = null, Name = "Protokół leczenia zmian nowotworowych" });
            i = 1;
            foreach (PatientNeoplasticTreatmentProtocolHS item in patientNeoplasticTreatmentProtocolHSRepository.PatientNeoplasticTreatmentProtocolHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "7_" + i, Parent_ID = "7", Name = "Protokół", Area = "", Action = "DisplayPatientNeoplasticTreatmentProtocolHS", Controller = "PatientCardNurse", FldId = item.fldId });
                i++;
            }

            //Protokół leczenia odleżyn
            categories.Add(new Hospice.Models.Category { ID = "8", Parent_ID = null, Name = "Protokół leczenia odleżyn" });
            i = 1;
            foreach (PatientBedsoresProtocolHS item in patientBedsoresProtocolHSRepository.PatientBedsoresProtocolHS.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "8_" + i, Parent_ID = "8", Name = "Protokół", Area = "", Action = "DisplayPatientBedsoresProtocolHS", Controller = "PatientCardNurse", FldId = item.fldId });
                i++;
            }

            

            return new Models.SeededCategories { Seed = null, Categories = categories };

        }

        private Hospice.Models.SeededCategories TreeGenerateModelForNurseHD()
        {

            int i = 0;
            List<int> workflowIds = SessionHelper.GetUICurrentSettings().SelectedPatientDoctorWorkflowIdsHD.ToList();
            IList<Hospice.Models.Category> categories = new List<Hospice.Models.Category>();

            //Zlecenia lekarskie
            categories.Add(new Hospice.Models.Category { ID = "1", Parent_ID = null, Name = "Zlecenia lekarskie" });
            i = 1;
            foreach (PatientOrdersMedicalHD item in patientOrdersMedicalHDRepository.PatientsOrdersMedicalHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "1_" + i, Parent_ID = "1", Name = "Zlecenia lekarskie " + Hospice.Infrastructure.SQLHelper.GetLastUpdateDateForActivity("UACT0d4c209a23ab40788016b7ace5b5b7f5", item.fldIWfId), Area = "", Action = "DisplayPatientOrdersMedicalHD", Controller = "PatientCardNurse", FldId = item.fldId });
                i++;
            }

            //Indywidualna karta pielęgniarska
            categories.Add(new Hospice.Models.Category { ID = "2", Parent_ID = null, Name = "Indywidualna karta pielęgniarska" });
            i = 1;
            foreach (PatientIndividualNursingCardHD item in patientIndividualNursingCardHDRepository.PatientIndividualNursingCardHD.Where(p => workflowIds.Contains(p.fldIWfId)).OrderByDescending(d => d.fldId))
            {
                categories.Add(new Hospice.Models.Category { ID = "2_" + i, Parent_ID = "2", Name = "Karta " + (item.dtpdatawizyty.HasValue ? item.dtpdatawizyty.Value.ToShortDateString() : item.fldId.ToString()), Area = "", Action = "DisplayPatientIndividualNursingCardHD", Controller = "PatientCardNurse", FldId = item.fldId });
                i++;
            }
            

            return new Models.SeededCategories { Seed = null, Categories = categories };

        }
        
        public ActionResult DisplayPatientFluidBalanceHS(int fldId)
        {
            PatientFluidBalanceHS model = patientFluidBalanceHSRepository.PatientFluidBalanceHS.FirstOrDefault(p => p.fldId == fldId);            
            model.CalculatedFuilds = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumns("UACTfb6f531f7170470f91a25de70945ac67", model.fldIWfId, new List<int> { 4, 5, 6 }, new List<string> { "Płyny doustnie", "Płyny dożylnie", "Diureza" });
            return View(model);
        }
        public ActionResult DisplayPatientGlycemicProfileHS(int fldId)
        {
            PatientGlycemicProfileHS model = patientGlycemicProfileHSRepository.PatientGlycemicProfileHS.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedProfiles = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACT8288eb4ff1a24b0f9b8ddb85148dfdc2", model.fldIWfId, new List<int> { 4, 5, 6, 7 }, new List<string> { "DateShort", "", "", "" }, new List<string> { "Data", "Godzina", "Poziom cukru", "Insulina" });
            return View(model);
        }

        public ActionResult DisplayPatientCumulativePatientConditionHS(int fldId)
        {
            PatientCumulativePatientConditionHS model = patientCumulativePatientConditionHSRepository.PatientCumulativePatientConditionHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientOrdersMedicalHS(int fldId)
        {
            PatientOrdersMedicalHS model = patientOrdersMedicalHSRepository.PatientOrdersMedicalHS.FirstOrDefault(p => p.fldId == fldId);
            /*
            model.CalculatedDrugs = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACTac8f3072d9ae4d13834c7bdae21b099a", model.fldIWfId, new List<int> { 4, 5, 10, 6, 7, 8, 11 }, new List<string> { "", "", "", "", "", "", "" }, new List<string> { "Nazwa leku", "Dawka", "Droga podania", "Data rozpoczęcia", "Data zakończenia", "Częstotliwość podawania", "Wykonano" });
            model.CalculatedTreatments = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACTdbfc872e7ef64d3b9ef58417cd289591", model.fldIWfId, new List<int> { 6, 4, 7 }, new List<string> { "", "", "" }, new List<string> { "Zabieg", "Uwagi", "Wykonano" });
            model.CalculatedDressings = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACT78f89301f60c46da8485465610a5ef9b", model.fldIWfId, new List<int> { 4, 8, 6, 5, 9 }, new List<string> { "", "", "", "", "" }, new List<string> { "Rodzaj opatrunku", "Czynność", "Umiejscowienie", "Uwagi", "Wykonano" });            
            */
            model.CalculatedDrugs = db.Database.SqlQuery<ZaleceniaLekarskieLekiDoPodaniaUSP>("USP_ZaleceniaLekarskieLekiDoPodania @day, @fldIWfId", new SqlParameter("day", DateTime.Now), new SqlParameter("fldIWfId", model.fldIWfId)).ToList();
            model.CalculatedDressings = db.Database.SqlQuery<ZaleceniaLekarskieOpatrunkiDoWykonaniaUSP>("USP_ZaleceniaLekarskieOpatrunkiDoWykonania @day, @fldIWfId", new SqlParameter("day", DateTime.Now), new SqlParameter("fldIWfId", model.fldIWfId)).ToList();
            model.CalculatedTreatments = db.Database.SqlQuery<ZaleceniaLekarskieZabiegiDoWykonaniaUSP>("USP_ZaleceniaLekarskieZabiegiDoWykonania @day, @fldIWfId", new SqlParameter("day", DateTime.Now), new SqlParameter("fldIWfId", model.fldIWfId)).ToList();

            //ViewBag.CalculatedDrugs = db.Database.SqlQuery<Contact>("Contact_Search @LastName, @FirstName",
            return View(model);
        }

        public ActionResult DisplayPatientIndividualNursingCardHS(int fldId)
        {
            PatientIndividualNursingCardHS model = patientIndividualNursingCardHSRepository.PatientIndividualNursingCardHS.FirstOrDefault(p => p.fldId == fldId);
            return View(model);
        }

        public ActionResult DisplayPatientFebrileCardHS(int fldId)
        {
            PatientFebrileCardHS model = patientFebrileCardHSRepository.PatientFebrileCardHS.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedntxtmoczR = model.ntxtmoczR.HasValue ? model.ntxtciezarR.ToString() + " ml" : " ml";
            model.CalculatedntxtciezarR = model.ntxtciezarR.HasValue ? model.ntxtciezarR.ToString() + " kg" : " kg";
            model.CalculatedntxtmoczW = model.ntxtmoczW.HasValue ? model.ntxtciezarW.ToString() + " ml" : " ml";
            model.CalculatedntxtciezarW = model.ntxtciezarW.HasValue ? model.ntxtciezarW.ToString() + " kg" : " kg";
            model.CalculatedMeasurements = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACT48f391f865e9438686e65188272943be", model.fldIWfId, new List<int> { 4, 6, 7 }, new List<string> { "DateShort","",""}, new List<string> { "Data", "Godzina", "Temperatura" });
            return View(model);
        }

        public ActionResult DisplayPatientNeoplasticTreatmentProtocolHS(int fldId)
        {
            PatientNeoplasticTreatmentProtocolHS model = patientNeoplasticTreatmentProtocolHSRepository.PatientNeoplasticTreatmentProtocolHS.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedProtocols = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACT9f827511c07b47c9876b293656f9dc01", model.fldIWfId, new List<int> { 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 17, 13 }, new List<string> { "DateShort", "", "TFbool", "TFbool", "TFbool", "TFbool", "", "", "TFbool", "", "", "" }, new List<string> { "Data", "Umiejscowienie", "Odór", "Ból", "Krwawienie", "Infekcje", "Inne", "Opatrunek", "Materac", "Stopień/Rozmiar", "Zmiana ułożenia", "Uwagi" });
            return View(model);
        }
        public ActionResult DisplayPatientBedsoresProtocolHS(int fldId)
        {
            PatientBedsoresProtocolHS model = patientBedsoresProtocolHSRepository.PatientBedsoresProtocolHS.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedProtocols = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACT757848ab39c140719f0da29f393ae644", model.fldIWfId, new List<int> { 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 17, 13 }, new List<string> { "DateShort", "", "TFbool", "TFbool", "TFbool", "TFbool", "", "", "TFbool", "", "", "" }, new List<string> { "Data", "Umiejscowienie", "Odór", "Ból", "Krwawienie", "Infekcje", "Inne", "Opatrunek", "Materac", "Stopień/Rozmiar", "Zmiana ułożenia", "Uwagi" });
            return View(model);
        }

        public ActionResult DisplayPatientOrdersMedicalHD(int fldId)
        {
            PatientOrdersMedicalHD model = patientOrdersMedicalHDRepository.PatientsOrdersMedicalHD.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedDrugs = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACT27a2a9fbe4f44556bb09f8ba6d9f4a1c", model.fldIWfId, new List<int> { 4, 5, 10, 6, 7, 8, 13 }, new List<string> { "", "", "", "", "", "", "" }, new List<string> { "Nazwa leku", "Dawka", "Droga podania", "Data rozpoczęcia", "Data zakończenia", "Częstotliwość podawania", "Wykonano" });
            model.CalculatedTreatments = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACT6772b3c90a0a4aea80ef844072d6b2b0", model.fldIWfId, new List<int> { 6, 4, 7 }, new List<string> { "", "", "" }, new List<string> { "Zabieg", "Uwagi", "Wykonano" });
            model.CalculatedDressings = Hospice.Infrastructure.SQLHelper.ListForTableXSelectedColumnsWithTypes("UACT5728fb01fe9344e89b276195fafec3c2", model.fldIWfId, new List<int> { 4, 8, 6, 5, 9 }, new List<string> { "", "", "", "", "" }, new List<string> { "Rodzaj opatrunku", "Czynność", "Umiejscowienie", "Uwagi", "Wykonano" });            
            
            return View(model);
        }
        public ActionResult DisplayPatientIndividualNursingCardHD(int fldId)
        {
            PatientIndividualNursingCardHD model = patientIndividualNursingCardHDRepository.PatientIndividualNursingCardHD.FirstOrDefault(p => p.fldId == fldId);
            model.CalculatedPressure = model.cisnienie + "/" + model.TextBox1;
            model.CalculatedToilet = Hospice.Infrastructure.SQLHelper.ListForTableXBesideIdColumnsNoHeader("UACTff3f780f7b3945449811c5d7aa0332a0", model.fldIWfId, 4);
            model.CalculatedTreatments = Hospice.Infrastructure.SQLHelper.ListForTableXBesideIdColumnsNoHeader("UACTf0960d5f4acd4141912d8ce189cb6989", model.fldIWfId, 4);
            model.CalculatedEducation = Hospice.Infrastructure.SQLHelper.ListForTableXBesideIdColumnsNoHeader("UACT74c9feaf0f0a444e95dba3109a5d8029", model.fldIWfId, 4);
            model.CalculatedMedical = Hospice.Infrastructure.SQLHelper.ListForTableXBesideIdColumnsNoHeader("UACT2451aefd8f2f40e4b6e25e6d5b65dccc", model.fldIWfId, 4);
            model.CalculatedRehabilitation = Hospice.Infrastructure.SQLHelper.ListForTableXBesideIdColumnsNoHeader("UACTa0f10413dcdc4644b3667f298d3e07ef", model.fldIWfId, 4);
            model.CalculatedOther = Hospice.Infrastructure.SQLHelper.ListForTableXBesideIdColumnsNoHeader("UACTc7086661af79433b8baa8b0ce48e2c3a", model.fldIWfId, 4);
            return View(model);
        }

        #endregion
	}
}