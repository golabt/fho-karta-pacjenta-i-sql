﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hospice.Domain.Abstract;
using Hospice.Domain.Entities;
using Hospice.Infrastructure.Helpers;
using Hospice.Models;
using Hospice.Infrastructure.Attributes;

namespace Hospice.Controllers
{
    [CustomAuthorize]
    public class PatientCardController : Controller
    {
        private IPatientHSRepository patientHSRepository;
        private IPatientHDRepository patientHDRepository;
        private IPatientWfDoctorDiscriminatorHDRepository patientWfDoctorDiscriminatorHDRepository;
        private IPatientMedicalHistoryHSRepository patientMedicalHistoryHSRepository;
        private IPatientRegisteryHDRepository patientRegisteryHDRepository;
        private IPatientVisitNurseHSRepository patientVisitNurseHSRepository;
        private ITblInstanceWorkflowsRepository tblInstanceWorkflowsRepository;
        private IPatientRegisteryRegisteryHSRepository patientRegisteryRegisteryHSRepository;
        private IPatientWfPsychologistDiscriminatorHSRepository patientWfPsychologistDiscriminatorHSRepository;
        private IPatientWfPsychologistDiscriminatorHDRepository patientWfPsychologistDiscriminatorHDRepository;
        private IPatientWfPhysiotherapistDiscriminatorHSRepository patientWfPhysiotherapistDiscriminatorHSRepository;
        private IPatientWfPhysiotherapistDiscriminatorHDRepository patientWfPhysiotherapistDiscriminatorHDRepository;
        private IPatientCommissionedHireHDRepository patientCommissionedHireHDRepository;

        public PatientCardController(
            IPatientHSRepository patientHSRepo, IPatientHDRepository patientHDRepo, 
            IPatientWfDoctorDiscriminatorHDRepository patientWfDoctorDiscriminatorHDRepo,
            IPatientMedicalHistoryHSRepository patientMedicalHistoryHSRepo,
            IPatientRegisteryHDRepository patientRegisteryHDRepo,
            IPatientVisitNurseHSRepository patientVisitNurseHSRepo,
            ITblInstanceWorkflowsRepository tblInstanceWorkflowsRepo,
            IPatientRegisteryRegisteryHSRepository patientRegisteryRegisteryHSRepo,
            IPatientWfPsychologistDiscriminatorHSRepository patientWfPsychologistDiscriminatorHSRepo,
            IPatientWfPsychologistDiscriminatorHDRepository patientWfPsychologistDiscriminatorHDRepo,
            IPatientWfPhysiotherapistDiscriminatorHSRepository patientWfPhysiotherapistDiscriminatorHSRepo,
            IPatientWfPhysiotherapistDiscriminatorHDRepository patientWfPhysiotherapistDiscriminatorHDRepo,
            IPatientCommissionedHireHDRepository patientCommissionedHireHDRepo
            )
        {
            patientHSRepository = patientHSRepo;
            patientHDRepository = patientHDRepo;
            patientWfDoctorDiscriminatorHDRepository = patientWfDoctorDiscriminatorHDRepo;
            patientMedicalHistoryHSRepository = patientMedicalHistoryHSRepo;
            patientRegisteryHDRepository = patientRegisteryHDRepo;
            patientVisitNurseHSRepository = patientVisitNurseHSRepo;
            tblInstanceWorkflowsRepository = tblInstanceWorkflowsRepo;
            patientRegisteryRegisteryHSRepository = patientRegisteryRegisteryHSRepo;
            patientWfPsychologistDiscriminatorHSRepository = patientWfPsychologistDiscriminatorHSRepo;
            patientWfPsychologistDiscriminatorHDRepository = patientWfPsychologistDiscriminatorHDRepo;
            patientWfPhysiotherapistDiscriminatorHSRepository = patientWfPhysiotherapistDiscriminatorHSRepo;
            patientWfPhysiotherapistDiscriminatorHDRepository = patientWfPhysiotherapistDiscriminatorHDRepo;
            patientCommissionedHireHDRepository = patientCommissionedHireHDRepo;
        }

        public ActionResult Index()
        {
            MenuHelper.SetCurrentCardMenuItem(0);
            MenuHelper.SetCurrentTeleMenuItem(0);
            MenuHelper.SetCurrentCardMiddleMenuItem(0);
            MenuHelper.SetCurrentMainAppMenuItem(1);

            var model = new PatientSearchModel
            {
                HospiceTypes = GetHospiceTypes()
            };

            return View(model);
        }

        public ActionResult PatientCardPatientSearchResults(PatientSearchModel model)
        {
            List<Hospice.Models.PatientCardPatientModel> patients = new List<PatientCardPatientModel>();
  

            if (model.FirstName != null && model.LastName == null && model.PESEL == null)
            {
                if (model.SelectedHospiceTypeId == 1 || model.SelectedHospiceTypeId == 0)
                foreach(Hospice.Domain.Entities.PatientHS patient in patientHSRepository.PatientsHS.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName)).OrderBy( ob => ob.txtNazwiskopacjenta))
                {
                    
                    patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, true));
                }

                if (model.SelectedHospiceTypeId == 2 || model.SelectedHospiceTypeId == 0)
                foreach (Hospice.Domain.Entities.PatientHD patient in patientHDRepository.PatientsHD.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName)).OrderBy(ob => ob.txtNazwiskopacjenta))
                {
                    patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, false));
                }
                return View("PatientCardPatientSearchResults", patients.OrderBy(ob => ob.txtNazwiskopacjenta).ToList());

                /*
                if (model.SelectedHospiceTypeId == 1) //stacjonarne
                    return View("PatientCardPatientSearchResultsHS", patientHSRepository.PatientsHS.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName)).OrderBy( ob => ob.txtNazwiskopacjenta));
                else //domowe
                    return View("PatientCardPatientSearchResultsHD", patientHDRepository.PatientsHD.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName)).OrderBy(ob => ob.txtNazwiskopacjenta));
                 */
            }
            else
                if (model.FirstName == null && model.LastName != null && model.PESEL == null)
                {
                    if (model.SelectedHospiceTypeId == 1 || model.SelectedHospiceTypeId == 0)
                    foreach (Hospice.Domain.Entities.PatientHS patient in patientHSRepository.PatientsHS.Where(p => p.txtNazwiskopacjenta.StartsWith(model.LastName)).OrderBy(ob => ob.txtNazwiskopacjenta))
                    {
                        patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, true));
                    }

                    if (model.SelectedHospiceTypeId == 2 || model.SelectedHospiceTypeId == 0)
                    foreach (Hospice.Domain.Entities.PatientHD patient in patientHDRepository.PatientsHD.Where(p => p.txtNazwiskopacjenta.StartsWith(model.LastName)).OrderBy(ob => ob.txtNazwiskopacjenta))
                    {
                        patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, false));
                    }
                    return View("PatientCardPatientSearchResults", patients.OrderBy(ob => ob.txtNazwiskopacjenta).ToList());
                    /*
                    if (model.SelectedHospiceTypeId == 1) //stacjonarne
                        return View("PatientCardPatientSearchResultsHS", patientHSRepository.PatientsHS.Where(p => p.txtNazwiskopacjenta.StartsWith(model.LastName)).OrderBy(ob => ob.txtNazwiskopacjenta));
                    else //domowe
                        return View("PatientCardPatientSearchResultsHD", patientHDRepository.PatientsHD.Where(p => p.txtNazwiskopacjenta.StartsWith(model.LastName)).OrderBy(ob => ob.txtNazwiskopacjenta));
                     */
                }
                else
                    if (model.FirstName == null && model.LastName == null && model.PESEL != null)
                    {
                        if (model.SelectedHospiceTypeId == 1 || model.SelectedHospiceTypeId == 0)
                        foreach (Hospice.Domain.Entities.PatientHS patient in patientHSRepository.PatientsHS.Where(p => p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta))
                        {
                            patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, true));
                        }

                        if (model.SelectedHospiceTypeId == 2 || model.SelectedHospiceTypeId == 0)
                        foreach (Hospice.Domain.Entities.PatientHD patient in patientHDRepository.PatientsHD.Where(p => p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta))
                        {
                            patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, false));
                        }
                        return View("PatientCardPatientSearchResults", patients.OrderBy(ob => ob.txtNazwiskopacjenta).ToList());

                        /*
                        if (model.SelectedHospiceTypeId == 1) //stacjonarne
                            return View("PatientCardPatientSearchResultsHS", patientHSRepository.PatientsHS.Where(p => p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta));
                        else //domowe
                            return View("PatientCardPatientSearchResultsHD", patientHDRepository.PatientsHD.Where(p => p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta));
                         */
                    }
                    else
                        if (model.FirstName != null && model.LastName != null && model.PESEL != null)
                        {
                            if (model.SelectedHospiceTypeId == 1 || model.SelectedHospiceTypeId == 0)
                            foreach (Hospice.Domain.Entities.PatientHS patient in patientHSRepository.PatientsHS.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.txtNazwiskopacjenta.StartsWith(model.LastName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta))
                            {
                                patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, true));
                            }

                            if (model.SelectedHospiceTypeId == 2 || model.SelectedHospiceTypeId == 0)
                            foreach (Hospice.Domain.Entities.PatientHD patient in patientHDRepository.PatientsHD.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.txtNazwiskopacjenta.StartsWith(model.LastName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta))
                            {
                                patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, false));
                            }
                            return View("PatientCardPatientSearchResults", patients.OrderBy(ob => ob.txtNazwiskopacjenta).ToList());

                            /*
                            if (model.SelectedHospiceTypeId == 1) //stacjonarne
                                return View("PatientCardPatientSearchResultsHS", patientHSRepository.PatientsHS.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.txtNazwiskopacjenta.StartsWith(model.LastName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta));
                            else //domowe
                                return View("PatientCardPatientSearchResultsHD", patientHDRepository.PatientsHD.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.txtNazwiskopacjenta.StartsWith(model.LastName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta));
                             */
                        }
                        else
                            if (model.FirstName == null && model.LastName != null && model.PESEL != null)
                            {
                                if (model.SelectedHospiceTypeId == 1 || model.SelectedHospiceTypeId == 0)
                                foreach (Hospice.Domain.Entities.PatientHS patient in patientHSRepository.PatientsHS.Where(p => p.txtNazwiskopacjenta.StartsWith(model.LastName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta))
                                {
                                    patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, true));
                                }

                                if (model.SelectedHospiceTypeId == 2 || model.SelectedHospiceTypeId == 0)
                                foreach (Hospice.Domain.Entities.PatientHD patient in patientHDRepository.PatientsHD.Where(p => p.txtNazwiskopacjenta.StartsWith(model.LastName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta))
                                {
                                    patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, false));
                                }
                                return View("PatientCardPatientSearchResults", patients.OrderBy(ob => ob.txtNazwiskopacjenta).ToList());

                                /*
                                if (model.SelectedHospiceTypeId == 1) //stacjonarne
                                    return View("PatientCardPatientSearchResultsHS", patientHSRepository.PatientsHS.Where(p => p.txtNazwiskopacjenta.StartsWith(model.LastName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta));
                                else //domowe
                                    return View("PatientCardPatientSearchResultsHD", patientHDRepository.PatientsHD.Where(p => p.txtNazwiskopacjenta.StartsWith(model.LastName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta));
                                 */
                            }
                            else
                                if (model.FirstName != null && model.LastName != null && model.PESEL == null)
                                {
                                    if (model.SelectedHospiceTypeId == 1 || model.SelectedHospiceTypeId == 0)
                                    foreach (Hospice.Domain.Entities.PatientHS patient in patientHSRepository.PatientsHS.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.txtNazwiskopacjenta.StartsWith(model.LastName)).OrderBy(ob => ob.txtNazwiskopacjenta))
                                    {
                                        patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, true));
                                    }

                                    if (model.SelectedHospiceTypeId == 2 || model.SelectedHospiceTypeId == 0)
                                    foreach (Hospice.Domain.Entities.PatientHD patient in patientHDRepository.PatientsHD.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.txtNazwiskopacjenta.StartsWith(model.LastName)).OrderBy(ob => ob.txtNazwiskopacjenta))
                                    {
                                        patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, false));
                                    }
                                    return View("PatientCardPatientSearchResults", patients.OrderBy(ob => ob.txtNazwiskopacjenta).ToList());

                                    /*
                                    if (model.SelectedHospiceTypeId == 1) //stacjonarne
                                        return View("PatientCardPatientSearchResultsHS", patientHSRepository.PatientsHS.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.txtNazwiskopacjenta.StartsWith(model.LastName)).OrderBy(ob => ob.txtNazwiskopacjenta));
                                    else //domowe
                                        return View("PatientCardPatientSearchResultsHD", patientHDRepository.PatientsHD.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.txtNazwiskopacjenta.StartsWith(model.LastName)).OrderBy(ob => ob.txtNazwiskopacjenta));
                                     */
                                }
                                else
                                    if (model.FirstName != null && model.LastName == null && model.PESEL != null)
                                    {
                                        if (model.SelectedHospiceTypeId == 1 || model.SelectedHospiceTypeId == 0)
                                        foreach (Hospice.Domain.Entities.PatientHS patient in patientHSRepository.PatientsHS.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta))
                                        {
                                            patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, true));
                                        }

                                        if (model.SelectedHospiceTypeId == 2 || model.SelectedHospiceTypeId == 0)
                                        foreach (Hospice.Domain.Entities.PatientHD patient in patientHDRepository.PatientsHD.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta))
                                        {
                                            patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, false));
                                        }
                                        return View("PatientCardPatientSearchResults", patients.OrderBy(ob => ob.txtNazwiskopacjenta).ToList());

                                        /*
                                        if (model.SelectedHospiceTypeId == 1) //stacjonarne
                                            return View("PatientCardPatientSearchResultsHS", patientHSRepository.PatientsHS.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta));
                                        else //domowe
                                            return View("PatientCardPatientSearchResultsHD", patientHDRepository.PatientsHD.Where(p => p.txtImiepacjenta.StartsWith(model.FirstName) && p.PESEL.StartsWith(model.PESEL)).OrderBy(ob => ob.txtNazwiskopacjenta));
                                         */
                                    }
                                    else
                                    {
                                        if (model.SelectedHospiceTypeId == 1 || model.SelectedHospiceTypeId == 0)
                                        foreach (Hospice.Domain.Entities.PatientHS patient in patientHSRepository.PatientsHS.Where(p => p.fldId != null).OrderBy(ob => ob.txtNazwiskopacjenta))
                                        {
                                            patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, true));
                                        }

                                        if (model.SelectedHospiceTypeId == 2 || model.SelectedHospiceTypeId == 0)
                                        foreach (Hospice.Domain.Entities.PatientHD patient in patientHDRepository.PatientsHD.Where(p => p.fldId != null).OrderBy(ob => ob.txtNazwiskopacjenta))
                                        {
                                            patients.Add(new PatientCardPatientModel(patient.fldId, patient.txtImiepacjenta, patient.txtNazwiskopacjenta, patient.PESEL, false));
                                        }
                                        return View("PatientCardPatientSearchResults", patients.OrderBy(ob => ob.txtNazwiskopacjenta).ToList());

                                        /*
                                        if (model.SelectedHospiceTypeId == 1) //stacjonarne
                                            return View("PatientCardPatientSearchResultsHS", patientHSRepository.PatientsHS.Where(p => p.fldId != null).OrderBy(ob => ob.txtNazwiskopacjenta));
                                        else //domowe
                                            return View("PatientCardPatientSearchResultsHD", patientHDRepository.PatientsHD.Where(p => p.fldId != null).OrderBy(ob => ob.txtNazwiskopacjenta));
                                         */
                                    }

            //return RedirectToAction("Index");
        }

        public ActionResult PatientCardPatientMedicalHistory(int fldId, int? selectedHospiceTypeId)
        {
            if (selectedHospiceTypeId != null)
            SessionHelper.GetUICurrentSettings().SelectedHospiceTypeStationary = (selectedHospiceTypeId == 1) ? true : false;
            FillUICurrentPatientToSession(fldId);
            MenuHelper.SetCurrentTeleMenuItem(0);
            MenuHelper.SetCurrentCardMiddleMenuItem(1);
            return View();
        }

        public ActionResult PatientCardPatientTelemedicine(int fldId, int? selectedHospiceTypeId)
        {
            if (selectedHospiceTypeId != null)
            SessionHelper.GetUICurrentSettings().SelectedHospiceTypeStationary = (selectedHospiceTypeId == 1) ? true : false;
            FillUICurrentPatientToSession(fldId);
            MenuHelper.SetCurrentTeleMenuItem(0);
            MenuHelper.SetCurrentCardMiddleMenuItem(2);
            return View();
        }

        public ActionResult _PatientCardSelectedPatientMenuMiddleTeleHistory()
        {
            return PartialView();
        }
                
        public ActionResult _PatientCardSelectedPatientSelected()
        {
            return PartialView();
        }

        public ActionResult _PatientCardSelectedPatientMenu()
        {
            return PartialView();
        }

        public ActionResult _PatientCardSelectedPatientMenuTelemedicine()
        {
            return PartialView();
        }

        #region helpers

        private void FillUICurrentPatientToSession(int fldId)
        {
            

            if(SessionHelper.GetUICurrentSettings().SelectedHospiceTypeStationary) //stacjonarne
            {
                PatientHS patient = patientHSRepository.PatientsHS.FirstOrDefault(p => p.fldId == fldId);

                SessionHelper.GetUICurrentSettings().SelectedPatientId = patient.fldId;
                SessionHelper.GetUICurrentSettings().SelectedPatientFirstName = patient.txtImiepacjenta;
                SessionHelper.GetUICurrentSettings().SelectedPatientLastName = patient.txtNazwiskopacjenta;
                SessionHelper.GetUICurrentSettings().SelectedPatientPESEL = patient.PESEL;

                SessionHelper.GetUICurrentSettings().SelectedPatientForDoctorWorkflowIdsHS = patientMedicalHistoryHSRepository.PatientMedicalHistoriesHS.Where(p => p.pesel == patient.PESEL).Select(s => s.fldIWfId);

                List<int> rootWorkflowIdsHSForPatient = patientRegisteryRegisteryHSRepository.PatientRegisteryRegisteryHS.Where(p => p.PESEL == patient.PESEL).Select(s => s.fldIWfId).ToList();
                SessionHelper.GetUICurrentSettings().SelectedPatientForNurseWorkflowIdsHS = tblInstanceWorkflowsRepository.TblInstanceWorkflows.Where(p => rootWorkflowIdsHSForPatient.Contains(p.fldSourceIWfId.Value)).Select(s => s.fldId);

                SessionHelper.GetUICurrentSettings().SelectedPatientForPsychologistWorkflowIdsHS = patientWfPsychologistDiscriminatorHSRepository.PatientWfPsychologistDiscriminatorHS.Where(p => p.txtpesel == patient.PESEL).Select(s => s.fldIWfId);

                SessionHelper.GetUICurrentSettings().SelectedPatientForPhysiotherapistWorkflowIdsHS = patientWfPhysiotherapistDiscriminatorHSRepository.PatientWfPhysiotherapistDiscriminatorHS.Where(p => p.txtpesel == patient.PESEL).Select(s => s.fldIWfId);

                SessionHelper.GetUICurrentSettings().SelectedPatientForRegisteryWorkflowIdsHS = patientHSRepository.PatientsHS.Where(p => p.PESEL == patient.PESEL).Select(s => s.fldIWfId);
            }
            else //domowe
            {
                PatientHD patient = patientHDRepository.PatientsHD.FirstOrDefault(p => p.fldId == fldId);

                SessionHelper.GetUICurrentSettings().SelectedPatientId = patient.fldId;
                SessionHelper.GetUICurrentSettings().SelectedPatientFirstName = patient.txtImiepacjenta;
                SessionHelper.GetUICurrentSettings().SelectedPatientLastName = patient.txtNazwiskopacjenta;
                SessionHelper.GetUICurrentSettings().SelectedPatientPESEL = patient.PESEL; 
                               
                SessionHelper.GetUICurrentSettings().SelectedPatientDoctorWorkflowIdsHD = patientWfDoctorDiscriminatorHDRepository.PatientWfDoctorDiscriminatorHD.Where(p => p.txtpesel == patient.PESEL).Select(s => s.fldIWfId);
                
                SessionHelper.GetUICurrentSettings().SelectedPatientRegisteryWorkflowIdsHD = patientRegisteryHDRepository.PatientRegisteryHD.Where(p => p.PESEL == patient.PESEL).Select(s => s.fldIWfId);

                SessionHelper.GetUICurrentSettings().SelectedPatientForPsychologistWorkflowIdsHD = patientWfPsychologistDiscriminatorHDRepository.PatientWfPsychologistDiscriminatorHD.Where(p => p.txtpesel == patient.PESEL).Select(s => s.fldIWfId);

                SessionHelper.GetUICurrentSettings().SelectedPatientForPhysiotherapistWorkflowIdsHD = patientWfPhysiotherapistDiscriminatorHDRepository.PatientWfPhysiotherapistDiscriminatorHD.Where(p => p.txtpesel == patient.PESEL).Select(s => s.fldIWfId);

                SessionHelper.GetUICurrentSettings().SelectedPatientForRegisteryWorkflowIdsHD = patientRegisteryHDRepository.PatientRegisteryHD.Where(p => p.PESEL == patient.PESEL).Select(s => s.fldIWfId);

                SessionHelper.GetUICurrentSettings().SelectedPatientForCommissionedHireWorkflowIdsHD = patientCommissionedHireHDRepository.PatientCommissionedHireHD.Where(p => p.PeselPacjenta == patient.PESEL).Select(s => s.fldIWfId);

                try
                {
                    SessionHelper.GetUICurrentSettings().SelectedPatientMainDoctorHD = patientWfDoctorDiscriminatorHDRepository.PatientWfDoctorDiscriminatorHD.FirstOrDefault(p => p.txtpesel == patient.PESEL).txtlekarz;
                }
                catch { }
                try
                {
                    SessionHelper.GetUICurrentSettings().SelectedPatientMainNurseHD = patientWfDoctorDiscriminatorHDRepository.PatientWfDoctorDiscriminatorHD.FirstOrDefault(p => p.txtpesel == patient.PESEL).txtpielegniarka;
                }
                catch { }   
                
                    
            
            }
            
            
        }        

        private List<SelectListItem> GetHospiceTypes()
        {

            List<SelectListItem> hospiceTypes = new List<SelectListItem>();

            hospiceTypes.Add(new SelectListItem { Value = "0", Text = "domowe i stacjonarne" });
            hospiceTypes.Add(new SelectListItem { Value = "1", Text = "stacjonarne" });
            hospiceTypes.Add(new SelectListItem { Value = "2", Text = "domowe" });


            return hospiceTypes;
        }

        #endregion

        
        
    }
}