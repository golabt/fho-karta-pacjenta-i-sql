﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PNMsoft.Sequence.Diagnostics;
using PNMsoft.Sequence.Runtime;
using PNMsoft.Sequence.Security;
using PNMsoft.Sequence.Utility;

namespace Hospice.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {            
            return View();
        }
    }
}