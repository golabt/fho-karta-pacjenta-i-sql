﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Hospice.Models;
using Hospice.Infrastructure.Helpers;
using PNMsoft.Sequence.Diagnostics;
using PNMsoft.Sequence.Runtime;
using PNMsoft.Sequence.Security;
using PNMsoft.Sequence.Utility;

namespace Hospice.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {            
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                WorkflowRuntime runtime = null;
                runtime = new WorkflowRuntime();
                runtime.Run(typeof(WorkflowEngine));
                IWorkflowEngine engine = WorkflowRuntime.Engine;

                AuthenticatedUser user = engine.GetServiceWithCheck<IAuthenticationService>().Authenticate(model.UserName, model.Password);

                
                if (user != null)
                {
                    
                    SessionHelper.GetUserSettings().AuthenticatedUser = user;
                    return RedirectToAction("Index", "PatientCard");
                }
                else
                {
                    ModelState.AddModelError("", "Niepoprawne hasło lub login użytkownika.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult LogOff(string returnUrl)
        {
            SessionHelper.ClearUserSessions();

            return RedirectToAction("Login", "Account");
        }

    }
}