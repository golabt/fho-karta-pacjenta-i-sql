﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hospice.Infrastructure
{
    public class UICurrentSettings
    {
        private string configWebServiceAddress = System.Configuration.ConfigurationManager.AppSettings["ConfigWebServiceAddress"].ToString();
        public string ConfigWebServiceAddress() { return configWebServiceAddress; }
        public int SelectedPatientId { get; set; }
        public string SelectedPatientFirstName { get; set; }
        public string SelectedPatientLastName { get; set; }
        public string SelectedPatientPESEL { get; set; }
        public bool SelectedHospiceTypeStationary { get; set; }
        public string SelectedCommentControllerName { get; set; }
        public string SelectedCommentViewName { get; set; }
        public int SelectedCommentItemId { get; set; }


        //HOSPICJUM STACJONARNE

        /// <summary>
        /// Is for workflows "Lekarz i pielęgniarka w HS - doctor tree" FIX
        /// </summary>
        public IQueryable<int> SelectedPatientForDoctorWorkflowIdsHS { get; set; }

        /// <summary>
        /// Is for workflows "Lekarz i pielęgniarka w HS - nurse tree" OK
        /// </summary>
        public IQueryable<int> SelectedPatientForNurseWorkflowIdsHS { get; set; }

        /// <summary>
        /// Is for workflows "Psycholog w HS - psychologist tree" FIX
        /// </summary>
        public IQueryable<int> SelectedPatientForPsychologistWorkflowIdsHS { get; set; }

        /// <summary>
        /// Is for workflows "Fizjoterapeuta w HS - fizjoterapeuta tree" FIX
        /// </summary>
        public IQueryable<int> SelectedPatientForPhysiotherapistWorkflowIdsHS { get; set; }
                
        /// <summary>
        /// Is for workflows "Rejestracja w HS - patient data tree" FIX
        /// </summary>
        public IQueryable<int> SelectedPatientForRegisteryWorkflowIdsHS { get; set; }

        public string SelectedPatientMainDoctorHS { get; set; }
        public string SelectedPatientMainNurseHS { get; set; }
        public string SelectedPatientPhysiotherapistHS { get; set; }
        public string SelectedPatientPsychologistHS { get; set; }
        public string SelectedPatientDocumentDataHS { get; set; }
        public string SelectedPatientDocumentNameHS { get; set; }

        //HOSPICJUM DOMOWE
        
        /// <summary>
        /// Is for workflows "Wizyta lekarza h domowe - doctor tree, nurse tree" FIX
        /// </summary>
        public IQueryable<int> SelectedPatientDoctorWorkflowIdsHD { get; set; }

        /// <summary>
        /// Is for workflows "Rejestracja pacjenta h domowe -- doctor tree" FIX
        /// </summary>
        public IQueryable<int> SelectedPatientRegisteryWorkflowIdsHD { get; set; }

        /// <summary>
        /// Is for workflows "Psycholog w HD - psychologist tree" FIX
        /// </summary>
        public IQueryable<int> SelectedPatientForPsychologistWorkflowIdsHD { get; set; }

        /// <summary>
        /// Is for workflows "Fizjoterapeuta w HD - fizjoterapeuta tree" FIX
        /// </summary>
        public IQueryable<int> SelectedPatientForPhysiotherapistWorkflowIdsHD { get; set; }

        /// <summary>
        /// Is for workflows "Rejestracja pacjenta h domowe -- doctor tree" FIX
        /// </summary>
        public IQueryable<int> SelectedPatientForRegisteryWorkflowIdsHD { get; set; }

        /// <summary>
        /// Is for workflows "Wypozyczenie sprzetu medycznego -- patient data tree" FIX
        /// </summary>
        public IQueryable<int> SelectedPatientForCommissionedHireWorkflowIdsHD { get; set; }

        public string SelectedPatientMainDoctorHD { get; set; }
        public string SelectedPatientMainNurseHD { get; set; }
        public string SelectedPatientPhysiotherapistHD { get; set; }
        public string SelectedPatientPsychologistHD { get; set; }
        public string SelectedPatientDocumentDataHD { get; set; }
        public string SelectedPatientDocumentNameHD { get; set; }
        
    }
}