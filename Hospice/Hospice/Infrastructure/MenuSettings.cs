﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hospice.Infrastructure
{
    public class MenuSettings
    {
        public string SelectedMainAppMenuItemPatientCard { get; set; }
        

        public string SelectedPatientTeleHistMenuItemTele { get; set; }
        public string SelectedPatientTeleHistMenuItemHist { get; set; }
        
        public string SelectedTeleMenuItemPump { get; set; }
        public string SelectedTeleMenuItemPumpIn { get; set; }
        public string SelectedTeleMenuItemWeight { get; set; }
        public string SelectedTeleMenuItemPressure { get; set; }
        public string SelectedTeleMenuItemSpO2 { get; set; }
        public string SelectedTeleMenuItemECG { get; set; }
        public string SelectedTeleMenuItemSugar { get; set; }



        public string SelectedCardMenuItemDoctor { get; set; }
        public string SelectedCardMenuItemNurse { get; set; }
        public string SelectedCardMenuItemPsychologist { get; set; }
        public string SelectedCardMenuItemPhysiotherapist { get; set; }
        public string SelectedCardMenuItemPatientData { get; set; }

        
    }
}