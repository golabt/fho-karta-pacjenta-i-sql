﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Hospice.Domain.Abstract;
using Hospice.Domain.Concrete;
using Ninject;

namespace Hospice.Infrastructure
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;

        public NinjectControllerFactory()
        {
            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            #region all
            ninjectKernel.Bind<ITblInstanceWorkflowsRepository>().To<EFTblInstanceWorkflowsRepository>();
            ninjectKernel.Bind<IPatientHSRepository>().To<EFPatientHSRepository>();
            ninjectKernel.Bind<IPatientHDRepository>().To<EFPatientHDRepository>();
            ninjectKernel.Bind<IPatientMedicalHistoryHSRepository>().To<EFPatientMedicalHistoryHSRepository>();
            ninjectKernel.Bind<IPatientFullExaminationHSRepository>().To<EFPatientFullExaminationHSRepository>();
            ninjectKernel.Bind<IPatientFullExaminationDrugsHSRepository>().To<EFPatientFullExaminationDrugsHSRepository>();
            ninjectKernel.Bind<IPatientMedicalVisitHSRepository>().To<EFPatientMedicalVisitHSRepository>();
            ninjectKernel.Bind<IPatientOrdersForNursesHSRepository>().To<EFPatientOrdersForNursesHSRepository>();
            ninjectKernel.Bind<IPatientOrdersForNursesDrugHSRepository>().To<EFPatientOrdersForNursesDrugHSRepository>();
            ninjectKernel.Bind<IPatientOrdersForNursesTreatmentHSRepository>().To<EFPatientOrdersForNursesTreatmentHSRepository>();
            ninjectKernel.Bind<IPatientOrdersForNursesDressingHSRepository>().To<EFPatientOrdersForNursesDressingHSRepository>();
            ninjectKernel.Bind<IZaleceniaLekarskieLekiGodzinyRepository>().To<EFZaleceniaLekarskieLekiGodzinyRepository>();
            ninjectKernel.Bind<IPatientReferralToPsychologistHSRepository>().To<EFPatientReferralToPsychologistHSRepository>();
            ninjectKernel.Bind<IPatientReferralToPsychotherapistHSRepository>().To<EFPatientReferralToPsychotherapistHSRepository>();
            ninjectKernel.Bind<IPatientReferralForExaminationHSRepository>().To<EFPatientReferralForExaminationHSRepository>();
            ninjectKernel.Bind<IPatientMedicalDescriptionHSRepository>().To<EFPatientMedicalDescriptionHSRepository>();
            ninjectKernel.Bind<IPatientWfDoctorDiscriminatorHDRepository>().To<EFPatientWfDoctorDiscriminatorHDRepository>();
            ninjectKernel.Bind<IPatientFirstVisitHDRepository>().To<EFPatientFirstVisitHDRepository>();
            ninjectKernel.Bind<IPatientVisitHDRepository>().To<EFPatientVisitHDRepository>();
            ninjectKernel.Bind<IPatientInterventionHDRepository>().To<EFPatientInterventionHDRepository>();
            ninjectKernel.Bind<IPatientFullExaminationHDRepository>().To<EFPatientFullExaminationHDRepository>();
            ninjectKernel.Bind<IPatientOrdersForNursesHDRepository>().To<EFPatientOrdersForNursesHDRepository>();
            ninjectKernel.Bind<IPatientReferralToPsychologistHDRepository>().To<EFPatientReferralToPsychologistHDRepository>();
            ninjectKernel.Bind<IPatientReferralToPsychotherapistHDRepository>().To<EFPatientReferralToPsychotherapistHDRepository>();
            ninjectKernel.Bind<IPatientOrdersOtherHDRepository>().To<EFPatientOrdersOtherHDRepository>();
            ninjectKernel.Bind<IPatientReferralForExaminationHDRepository>().To<EFPatientReferralForExaminationHDRepository>();
            ninjectKernel.Bind<IPatientDrugRegisteryHDRepository>().To<EFPatientDrugRegisteryHDRepository>();
            ninjectKernel.Bind<IPatientDoctorVisitCardHDRepository>().To<EFPatientDoctorVisitCardHDRepository>();
            ninjectKernel.Bind<IPatientRegisteryHDRepository>().To<EFPatientRegisteryHDRepository>();
            ninjectKernel.Bind<IPatientQualifyingVisitHDRepository>().To<EFPatientQualifyingVisitHDRepository>();
            ninjectKernel.Bind<IPatientEstablishmentOfMedicalHistoryHDRepository>().To<EFPatientEstablishmentOfMedicalHistoryHDRepository>();
            ninjectKernel.Bind<IPatientVisitNurseHSRepository>().To<EFPatientVisitNurseHSRepository>();
            //ninjectKernel.Bind<IPatientRegisteryHSRepository>().To<EFPatientRegisteryHSRepository>();
            ninjectKernel.Bind<IPatientFluidBalanceHSRepository>().To<EFPatientFluidBalanceHSRepository>();
            ninjectKernel.Bind<IPatientGlycemicProfileHSRepository>().To<EFPatientGlycemicProfileHSRepository>();
            ninjectKernel.Bind<IPatientCumulativePatientConditionHSRepository>().To<EFPatientCumulativePatientConditionHSRepository>();
            ninjectKernel.Bind<IPatientOrdersMedicalHSRepository>().To<EFPatientOrdersMedicalHSRepository>();
            ninjectKernel.Bind<IPatientIndividualNursingCardHSRepository>().To<EFPatientIndividualNursingCardHSRepository>();
            ninjectKernel.Bind<IPatientFebrileCardHSRepository>().To<EFPatientFebrileCardHSRepository>();
            ninjectKernel.Bind<IPatientNeoplasticTreatmentProtocolHSRepository>().To<EFPatientNeoplasticTreatmentProtocolHSRepository>();
            ninjectKernel.Bind<IPatientBedsoresProtocolHSRepository>().To<EFPatientBedsoresProtocolHSRepository>();
            ninjectKernel.Bind<IPatientOrdersMedicalHDRepository>().To<EFPatientOrdersMedicalHDRepository>();
            ninjectKernel.Bind<IPatientIndividualNursingCardHDRepository>().To<EFPatientIndividualNursingCardHDRepository>();
            ninjectKernel.Bind<IPatientWfPsychologistDiscriminatorHSRepository>().To<EFPatientWfPsychologistDiscriminatorHSRepository>();
            ninjectKernel.Bind<IPatientWfPsychologistDiscriminatorHDRepository>().To<EFPatientWfPsychologistDiscriminatorHDRepository>();
            ninjectKernel.Bind<IPatientPsychologistVisitHSRepository>().To<EFPatientPsychologistVisitHSRepository>();
            ninjectKernel.Bind<IPatientPsychologistDiseaseAcceptanceScaleHSRepository>().To<EFPatientPsychologistDiseaseAcceptanceScaleHSRepository>();
            ninjectKernel.Bind<IPatientPsychologistQuestionnaireHADSMHSRepository>().To<EFPatientPsychologistQuestionnaireHADSMHSRepository>();
            ninjectKernel.Bind<IPatientPsychologistQuestionnaireHADSMResultHSRepository>().To<EFPatientPsychologistQuestionnaireHADSMResultHSRepository>();
            ninjectKernel.Bind<IPatientPsychologistGeriatricEvaluationScaleHSRepository>().To<EFPatientPsychologistGeriatricEvaluationScaleHSRepository>();
            ninjectKernel.Bind<IPatientPsychologistGeriatricEvaluationScaleResultHSRepository>().To<EFPatientPsychologistGeriatricEvaluationScaleResultHSRepository>();
            ninjectKernel.Bind<IPatientPsychologistMMSETestHSRepository>().To<EFPatientPsychologistMMSETestHSRepository>();
            ninjectKernel.Bind<IPatientPsychologistMMSETestResultHSRepository>().To<EFPatientPsychologistMMSETestResultHSRepository>();
            ninjectKernel.Bind<IPatientPsychologistVisitHDRepository>().To<EFPatientPsychologistVisitHDRepository>();
            ninjectKernel.Bind<IPatientPsychologistDiseaseAcceptanceScaleHDRepository>().To<EFPatientPsychologistDiseaseAcceptanceScaleHDRepository>();
            ninjectKernel.Bind<IPatientPsychologistQuestionnaireHADSMHDRepository>().To<EFPatientPsychologistQuestionnaireHADSMHDRepository>();
            ninjectKernel.Bind<IPatientPsychologistQuestionnaireHADSMResultHDRepository>().To<EFPatientPsychologistQuestionnaireHADSMResultHDRepository>();
            ninjectKernel.Bind<IPatientPsychologistGeriatricEvaluationScaleHDRepository>().To<EFPatientPsychologistGeriatricEvaluationScaleHDRepository>();
            ninjectKernel.Bind<IPatientPsychologistGeriatricEvaluationScaleResultHDRepository>().To<EFPatientPsychologistGeriatricEvaluationScaleResultHDRepository>();
            ninjectKernel.Bind<IPatientPsychologistMMSETestHDRepository>().To<EFPatientPsychologistMMSETestHDRepository>();
            ninjectKernel.Bind<IPatientPsychologistMMSETestResultHDRepository>().To<EFPatientPsychologistMMSETestResultHDRepository>();
            ninjectKernel.Bind<IPatientAttachmentsHSRepository>().To<EFPatientAttachmentsHSRepository>();
            ninjectKernel.Bind<IPatientAttachmentsHDRepository>().To<EFPatientAttachmentsHDRepository>();
            ninjectKernel.Bind<IPatientCommissionedHireHDRepository>().To<EFPatientCommissionedHireHDRepository>();

            #endregion

            #region physiotherapis
            ninjectKernel.Bind<IPatientWfPhysiotherapistDiscriminatorHSRepository>().To<EFPatientWfPhysiotherapistDiscriminatorHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistFirstVisitHSRepository>().To<EFPatientPhysiotherapistFirstVisitHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistPatientCardHSRepository>().To<EFPatientPhysiotherapistPatientCardHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistVisitHSRepository>().To<EFPatientPhysiotherapistVisitHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBorgScaleHSRepository>().To<EFPatientPhysiotherapistBorgScaleHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistKarnofskyPerformanceStatusHSRepository>().To<EFPatientPhysiotherapistKarnofskyPerformanceStatusHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBFIIndexHSRepository>().To<EFPatientPhysiotherapistBFIIndexHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistRSCLHSRepository>().To<EFPatientPhysiotherapistRSCLHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistVASHSRepository>().To<EFPatientPhysiotherapistVASHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBarthelPatientHSRepository>().To<EFPatientPhysiotherapistBarthelPatientHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBarthelPatientResultHSRepository>().To<EFPatientPhysiotherapistBarthelPatientResultHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBarthelPhysiotherapistHSRepository>().To<EFPatientPhysiotherapistBarthelPhysiotherapistHSRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBarthelPhysiotherapistResultHSRepository>().To<EFPatientPhysiotherapistBarthelPhysiotherapistResultHSRepository>();

            ninjectKernel.Bind<IPatientWfPhysiotherapistDiscriminatorHDRepository>().To<EFPatientWfPhysiotherapistDiscriminatorHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistFirstVisitHDRepository>().To<EFPatientPhysiotherapistFirstVisitHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistPatientCardHDRepository>().To<EFPatientPhysiotherapistPatientCardHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistVisitHDRepository>().To<EFPatientPhysiotherapistVisitHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBorgScaleHDRepository>().To<EFPatientPhysiotherapistBorgScaleHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistKarnofskyPerformanceStatusHDRepository>().To<EFPatientPhysiotherapistKarnofskyPerformanceStatusHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBFIIndexHDRepository>().To<EFPatientPhysiotherapistBFIIndexHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistRSCLHDRepository>().To<EFPatientPhysiotherapistRSCLHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistVASHDRepository>().To<EFPatientPhysiotherapistVASHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBarthelPatientHDRepository>().To<EFPatientPhysiotherapistBarthelPatientHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBarthelPatientResultHDRepository>().To<EFPatientPhysiotherapistBarthelPatientResultHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBarthelPhysiotherapistHDRepository>().To<EFPatientPhysiotherapistBarthelPhysiotherapistHDRepository>();
            ninjectKernel.Bind<IPatientPhysiotherapistBarthelPhysiotherapistResultHDRepository>().To<EFPatientPhysiotherapistBarthelPhysiotherapistResultHDRepository>();

            #endregion

            ninjectKernel.Bind<IPatientRegisteryRegisteryHSRepository>().To<EFPatientRegisteryRegisteryHSRepository>();
            ninjectKernel.Bind<IPatientRegisteryInsuranceHSRepository>().To<EFPatientRegisteryInsuranceHSRepository>();
            ninjectKernel.Bind<IPatientRegisteryEstablishmentOfMedicalHistoryHSRepository>().To<EFPatientRegisteryEstablishmentOfMedicalHistoryHSRepository>();
            ninjectKernel.Bind<IPatientRegisteryFamilyDataHSRepository>().To<EFPatientRegisteryFamilyDataHSRepository>();
            ninjectKernel.Bind<IPatientRegisteryFinalizingAdmissionHSRepository>().To<EFPatientRegisteryFinalizingAdmissionHSRepository>();
            ninjectKernel.Bind<IPatientRegisteryRiskAssessmentFormHSRepository>().To<EFPatientRegisteryRiskAssessmentFormHSRepository>();
            ninjectKernel.Bind<IPatientRegisteryRiskAssessmentFormResultHSRepository>().To<EFPatientRegisteryRiskAssessmentFormResultHSRepository>();
            ninjectKernel.Bind<IPatientRegisteryEpikryzaExtractsHSRepository>().To<EFPatientRegisteryEpikryzaExtractsHSRepository>();
            ninjectKernel.Bind<IPatientRegisteryEpikryzaHSRepository>().To<EFPatientRegisteryEpikryzaHSRepository>();
            ninjectKernel.Bind<IPatientRegisteryHospitalStayHSRepository>().To<EFPatientRegisteryHospitalStayHSRepository>();

            ninjectKernel.Bind<IPatientRegisteryReRegistrationHDRepository>().To<EFPatientRegisteryReRegistrationHDRepository>();
            ninjectKernel.Bind<IPatientRegisteryInsuranceHDRepository>().To<EFPatientRegisteryInsuranceHDRepository>();
            ninjectKernel.Bind<IPatientRegisterySigningConsentHDRepository>().To<EFPatientRegisterySigningConsentHDRepository>();
            ninjectKernel.Bind<IPatientRegisteryEpikryzaHDRepository>().To<EFPatientRegisteryEpikryzaHDRepository>();
            ninjectKernel.Bind<IPatientRegisteryEpikryzaExtractsHDRepository>().To<EFPatientRegisteryEpikryzaExtractsHDRepository>();
            ninjectKernel.Bind<IPatientRegisteryPermanentlyToHospitalHDRepository>().To<EFPatientRegisteryPermanentlyToHospitalHDRepository>();
            ninjectKernel.Bind<IPatientRegisteryHospitalStayHDRepository>().To<EFPatientRegisteryHospitalStayHDRepository>();


            ninjectKernel.Bind<IViewCommentRepository>().To<EFViewCommentRepository>();
            ninjectKernel.Bind<IMedicalDevicesLogRepository>().To<EFMedicalDevicesLogRepository>();
            ninjectKernel.Bind<ICentralkaRepository>().To<EFCentralkaRepository>();
            ninjectKernel.Bind<IPompyRepository>().To<EFPompyRepository>();
            ninjectKernel.Bind<IInfusionInfoRepository>().To<EFInfusionInfoRepository>();
            ninjectKernel.Bind<IHireMedicalDeviceRepository>().To<EFHireMedicalDeviceRepository>();
            ninjectKernel.Bind<IReturnMedicalDeviceRepository>().To<EFReturnMedicalDeviceRepository>();
            ninjectKernel.Bind<IDeviceRentalRepository>().To<EFDeviceRentalRepository>();
        }
        
    }
}