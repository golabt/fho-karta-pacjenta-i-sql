﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace Hospice.Infrastructure
{
    public static class SQLHelper
    {
        public static List<string> ListForTableX(string tableName, int fldIWfId, int param1, int param2, int param3, int param4, int param5)
        {
            DataTable results = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            using (SqlCommand command = new SqlCommand("Select * from " + tableName + " where fldIWfId ='" + fldIWfId + "'", conn))
            using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
            dataAdapter.Fill(results);
            
            List<string> resultList = new List<string>();            

            if(results.AsEnumerable().Count() > 0)
            {
                resultList.Add("<table width=\"500px\">");
                resultList.Add("<tr>");
                if (param1 != -1 && param2 == -1 && param3 == -1 && param4 == -1 && param5 == -1)
                    resultList.Add("<td>" + results.DefaultView.Table.Columns[param1].ColumnName + "</td>");
                if (param1 != -1 && param2 != -1 && param3 == -1 && param4 == -1 && param5 == -1)
                    resultList.Add("<td>" + results.DefaultView.Table.Columns[param1].ColumnName + "</td><td>" + results.DefaultView.Table.Columns[param2].ColumnName + "</td>");
                if (param1 != -1 && param2 != -1 && param3 != -1 && param4 == -1 && param5 == -1)
                    resultList.Add("<td>" + results.DefaultView.Table.Columns[param1].ColumnName + "</td><td>" + results.DefaultView.Table.Columns[param2].ColumnName + "</td><td>" + results.DefaultView.Table.Columns[param3].ColumnName + "</td>");
                if (param1 != -1 && param2 != -1 && param3 != -1 && param4 != -1 && param5 == -1)
                    resultList.Add("<td>" + results.DefaultView.Table.Columns[param1].ColumnName + "</td><td>" + results.DefaultView.Table.Columns[param2].ColumnName + "</td><td>" + results.DefaultView.Table.Columns[param3].ColumnName + "</td><td>" + results.DefaultView.Table.Columns[param4].ColumnName + "</td>");
                if (param1 != -1 && param2 == -1 && param3 != -1 && param4 != -1 && param5 != -1)
                    resultList.Add("<td>" + results.DefaultView.Table.Columns[param1].ColumnName + "</td><td>" + results.DefaultView.Table.Columns[param2].ColumnName + "</td><td>" + results.DefaultView.Table.Columns[param3].ColumnName + "</td><td>" + results.DefaultView.Table.Columns[param4].ColumnName + "</td><td>" + results.DefaultView.Table.Columns[param5].ColumnName + "</td>");
                
                
                resultList.Add("</tr>");


            }

            foreach (DataRow row in results.AsEnumerable())
            {
                
                resultList.Add("<tr>");
                if (param1 != -1 && param2 == -1 && param3 == -1 && param4 == -1 && param5 == -1)
                    resultList.Add("<td>"+row[param1].ToString()+"</td>");
                if (param1 != -1 && param2 != -1 && param3 == -1 && param4 == -1 && param5 == -1)
                    resultList.Add("<td>" + row[param1].ToString() + "</td><td>" + row[param2].ToString() + "</td>");
                if (param1 != -1 && param2 != -1 && param3 != -1 && param4 == -1 && param5 == -1)
                    resultList.Add("<td>" + row[param1].ToString() + "</td><td>" + row[param2].ToString() + "</td><td>" + row[param3].ToString() + "</td>");
                if (param1 != -1 && param2 != -1 && param3 != -1 && param4 != -1 && param5 == -1)
                    resultList.Add("<td>" + row[param1].ToString() + "</td><td>" + row[param2].ToString() + "</td><td>" + row[param3].ToString() + "</td><td>" + row[param4].ToString() + "</td>");
                if (param1 != -1 && param2 == -1 && param3 != -1 && param4 != -1 && param5 != -1)
                    resultList.Add("<td>" + row[param1].ToString() + "</td><td>" + row[param2].ToString() + "</td><td>" + row[param3].ToString() + "</td><td>" + row[param4].ToString() + "</td><td>" + row[param5].ToString() + "</td>");
                resultList.Add("</tr>");
            }

            if(results.AsEnumerable().Count() > 0)
            {
                resultList.Add("</table>");
            }


            return resultList;
        }
        public static List<string> ListForTableXBesideIdColumns(string tableName, int fldIWfId, int columnToStart)
        {
            return ListForTableXBesideIdColumnsInner(tableName, fldIWfId, columnToStart, false);
        }

        public static List<string> ListForTableXBesideIdColumnsNoHeader(string tableName, int fldIWfId, int columnToStart)
        {
            return ListForTableXBesideIdColumnsInner(tableName, fldIWfId, columnToStart, true);
        }
        public static List<string> ListForTableXBesideIdColumnsInner(string tableName, int fldIWfId, int columnToStart, bool noHeader)
        {
            DataTable results = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            using (SqlCommand command = new SqlCommand("Select * from " + tableName + " where fldIWfId ='" + fldIWfId + "'", conn))
            using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
                dataAdapter.Fill(results);

            List<string> resultList = new List<string>();

            if (results.AsEnumerable().Count() > 0)
            {
                resultList.Add("<table>");

                if (!noHeader)
                {
                    resultList.Add("<tr>");

                    int i = 0;
                    foreach (var item in results.DefaultView.Table.Columns)
                    {
                        if (i > columnToStart)
                        {
                            resultList.Add("<td>" + results.DefaultView.Table.Columns[i].ColumnName + "</td>");
                        }
                        i++;
                    }

                    resultList.Add("</tr>");
                }

            }

            
            foreach (DataRow row in results.AsEnumerable())
            {                
                resultList.Add("<tr>");

                int y = 0;
                foreach (var value in row.ItemArray)
                {
                    if (y > columnToStart)
                    {
                        resultList.Add("<td>" + row[y].ToString() + "</td>");
                    }
                    y++;
                }
                resultList.Add("</tr>");
            }

            if (results.AsEnumerable().Count() > 0)
            {
                resultList.Add("</table>");
            }


            return resultList;
        }

        public static List<string> ListForTableXSelectedColumns(string tableName, int fldIWfId, List<int> columns, List<string> columnsNewNames)
        {
            return ListForTableXSelectedColumnsInner(tableName, fldIWfId, columns, null, columnsNewNames);
        }

        public static List<string> ListForTableXSelectedColumnsWithTypes(string tableName, int fldIWfId, List<int> columns, List<string> columnTypes, List<string> columnsNewNames)
        {
            return ListForTableXSelectedColumnsInner(tableName, fldIWfId, columns, columnTypes, columnsNewNames);
        }

        public static List<string> ListForTableXSelectedColumnsInner(string tableName, int fldIWfId, List<int> columns, List<string> columnTypes, List<string> columnsNewNames)
        {
            DataTable results = new DataTable();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            using (SqlCommand command = new SqlCommand("Select * from " + tableName + " where fldIWfId ='" + fldIWfId + "'", conn))
            using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
                dataAdapter.Fill(results);

            List<string> resultList = new List<string>();

            if (results.AsEnumerable().Count() > 0)
            {
                resultList.Add("<table  border=\"1px\" cellpadding=\"3px\" width=\"500px\">");
                resultList.Add("<tr>");

                int i = 0;
                foreach (var item in columns)
                {
                    
                    resultList.Add("<td>" + columnsNewNames[i].ToString() + "</td>");
                    i++;
                }

                resultList.Add("</tr>");


            }


            foreach (DataRow row in results.AsEnumerable())
            {
                resultList.Add("<tr>");

                int n = 0;
                foreach(int item in columns)
                {
                    int y = 0;
                    foreach (var value in row.ItemArray)
                    {
                        if (item == y)
                        {
                            if(columnTypes != null)
                            {

                                if(columnTypes[n] != "")
                                {
                                    if (columnTypes[n] == "DateShort")
                                    {
                                        DateTime readyToPrint = (DateTime)row[y];
                                        resultList.Add("<td>" + readyToPrint.ToShortDateString() + "</td>");
                                    }
                                    if (columnTypes[n] == "TFbool")
                                    {
                                        if (row[y] != null)
                                        {
                                            bool? readyToPrint = (bool?)row[y];
                                            if(readyToPrint == true)
                                               resultList.Add("<td>" + "Prawda" + "</td>");
                                            else
                                                resultList.Add("<td>" + "Fałsz" + "</td>");
                                        }
                                        else
                                            resultList.Add("<td>" + row[y].ToString() + "</td>");
                                    }
                                    
                                }else
                                {
                                    resultList.Add("<td>" + row[y].ToString() + "</td>");
                                }

                            }else
                            {
                                resultList.Add("<td>" + row[y].ToString() + "</td>");
                            }
                        }
                        y++;
                    }
                    n++;
                }

                
                resultList.Add("</tr>");
            }

            if (results.AsEnumerable().Count() > 0)
            {
                resultList.Add("</table>");
            }


            return resultList;
        }

        public static string GetLastUpdateDateForActivity(string sourceTableName, int fldIWfId)
        {
            string result = "";

            DataTable results = new DataTable();
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                SqlCommand command = new SqlCommand("select top 1 b.fldLastUpdateDate from " + sourceTableName + " a join tblInstanceActivities b on a.fldIActId = b.fldSourceActId where a.fldIWfId = '" + fldIWfId + "' order by b.fldLastUpdateDate desc", conn);
                                
                conn.Open();
                DateTime? dateUpdated = (DateTime)command.ExecuteScalar();
                if (dateUpdated != null)
                {
                    result = new DateTime(dateUpdated.Value.Year, dateUpdated.Value.Month, dateUpdated.Value.Day).ToShortDateString();
                }
                conn.Close();
            }catch(Exception e){}

            return result;
        }
    }
}