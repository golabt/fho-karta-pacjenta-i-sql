﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hospice.Infrastructure.Helpers
{
    public static class PatientCardSelectedPatientHelper
    {
        public static string GetFirstNameLastNamePESEL()
        {
            return SessionHelper.GetUICurrentSettings().SelectedPatientFirstName + " " + SessionHelper.GetUICurrentSettings().SelectedPatientLastName + " " + SessionHelper.GetUICurrentSettings().SelectedPatientPESEL;
                 
        }
    }
}