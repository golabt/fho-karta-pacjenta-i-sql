﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hospice.Infrastructure.Helpers
{
    public static class MenuHelper
    {
        public static string SetCurrentMedicalDataVisited(bool? visited, bool strForUi)
        {
            if(visited != null)
            {
                if(visited == true)
                {
                    if(strForUi)
                    {
                        return "Odwiedzone"; 
                    }
                    else
                    {
                        return ""; 
                    }
                }else
                {
                    if (strForUi)
                    {
                        return "Nowe";
                    }
                    else
                    {
                        return "success";
                    }
                }
            }else
            {
                if (strForUi)
                {
                    return "Nowe";
                }
                else
                {
                    return "success";
                }
            }
            
        }


        public static void SetCurrentMainAppMenuItem(int position)
        {
            switch (position)
            {
                case 0:
                    SessionHelper.GetMenuSettings().SelectedMainAppMenuItemPatientCard = "";
                    break;
                case 1:
                    SessionHelper.GetMenuSettings().SelectedMainAppMenuItemPatientCard = "active";
                    break;
            }
        }

        public static void SetCurrentCardMiddleMenuItem(int position)
        {
            switch (position)
            {
                case 0:
                    SessionHelper.GetMenuSettings().SelectedPatientTeleHistMenuItemTele = "";
                    SessionHelper.GetMenuSettings().SelectedPatientTeleHistMenuItemHist = "";
                    break;
                case 1:
                    SessionHelper.GetMenuSettings().SelectedPatientTeleHistMenuItemTele = "";
                    SessionHelper.GetMenuSettings().SelectedPatientTeleHistMenuItemHist = "active";
                    break;
                case 2:
                    SessionHelper.GetMenuSettings().SelectedPatientTeleHistMenuItemTele = "active";
                    SessionHelper.GetMenuSettings().SelectedPatientTeleHistMenuItemHist = "";
                    break;
            }
        }

        public static void SetCurrentCardMenuItem(int position)
        {
            switch (position)
            {
                case 0:
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemDoctor = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemNurse = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPsychologist = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPhysiotherapist = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPatientData = "";
                    break;
                case 1:
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemDoctor = "active";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemNurse = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPsychologist = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPhysiotherapist = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPatientData = "";
                    break;
                case 2:
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemDoctor = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemNurse = "active";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPsychologist = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPhysiotherapist = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPatientData = "";
                    break;
                case 3:
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemDoctor = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemNurse = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPsychologist = "active";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPhysiotherapist = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPatientData = "";
                    break;
                case 4:
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemDoctor = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemNurse = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPsychologist = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPhysiotherapist = "active";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPatientData = "";
                    break;
                case 5:
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemDoctor = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemNurse = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPsychologist = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPhysiotherapist = "";
                    SessionHelper.GetMenuSettings().SelectedCardMenuItemPatientData = "active";
                    break;
            }

        }
        
        public static void SetCurrentTeleMenuItem(int position)
        {

            switch (position)
            {
                case 0:
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPump = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPumpIn = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPressure = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemECG = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSpO2 = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemWeight = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSugar = "";
                    break;
                case 1:
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPump = "active";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPumpIn = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPressure = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemECG = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSpO2 = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemWeight = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSugar = "";
                    break;
                case 2:
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPump = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPumpIn = "active";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPressure = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemECG = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSpO2 = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemWeight = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSugar = "";
                    break;
                case 3:
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPump = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPumpIn = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPressure = "active";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemECG = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSpO2 = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemWeight = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSugar = "";
                    break;
                case 4:
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPump = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPumpIn = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPressure = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemECG = "active";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSpO2 = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemWeight = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSugar = "";
                    break;
                case 5:
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPump = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPumpIn = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPressure = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemECG = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSpO2 = "active";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemWeight = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSugar = "";
                    break;
                case 6:
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPump = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPumpIn = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPressure = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemECG = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSpO2 = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemWeight = "active";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSugar = "";
                    break;
                case 7:
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPump = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPumpIn = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemPressure = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemECG = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSpO2 = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemWeight = "";
                    SessionHelper.GetMenuSettings().SelectedTeleMenuItemSugar = "active";
                    break;
            }
        }
    }
}