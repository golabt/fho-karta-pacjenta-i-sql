﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hospice.Infrastructure.Helpers
{
    public class SessionHelper
    {
        private static string uiCurrentSettingsKey = "UICurrentSettings";
        private static string userSettingsKey = "UserSettings";
        private static string menuSettingsKey = "MenuSettings";       


        public static void ClearUserSessions()
        {
            HttpContext.Current.Session[uiCurrentSettingsKey] = null;
            HttpContext.Current.Session[userSettingsKey] = null;
            HttpContext.Current.Session[menuSettingsKey] = null;
        }

        public static MenuSettings GetMenuSettings()
        {
            MenuSettings menuSettings = (MenuSettings)HttpContext.Current.Session[menuSettingsKey];

            if (menuSettings == null)
            {
                menuSettings = new MenuSettings();
                HttpContext.Current.Session[menuSettingsKey] = menuSettings;
            }

            return menuSettings;
        }

        public static UserSettings GetUserSettings()
        {
            UserSettings userSettings = (UserSettings)HttpContext.Current.Session[userSettingsKey];

            if (userSettings == null)
            {
                userSettings = new UserSettings();
                HttpContext.Current.Session[userSettingsKey] = userSettings;
            }

            return userSettings;
        }

        public static UICurrentSettings GetUICurrentSettings()
        {
            UICurrentSettings uiCurrentSettings = (UICurrentSettings)HttpContext.Current.Session[uiCurrentSettingsKey];

            if (uiCurrentSettings == null)
            {
                uiCurrentSettings = new UICurrentSettings();
                HttpContext.Current.Session[uiCurrentSettingsKey] = uiCurrentSettings;
            }

            return uiCurrentSettings;
        }
    }
}