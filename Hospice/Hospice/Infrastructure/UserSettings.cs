﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PNMsoft.Sequence.Diagnostics;
using PNMsoft.Sequence.Runtime;
using PNMsoft.Sequence.Security;
using PNMsoft.Sequence.Utility;

namespace Hospice.Infrastructure
{
    public class UserSettings
    {

        public AuthenticatedUser AuthenticatedUser { get; set; }
        
    }
}