﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Web.Routing;
using Hospice.Infrastructure.Helpers;
using System.Web.Mvc;

namespace Hospice.Infrastructure.Attributes
{
    public class CustomAuthorizeAttribute:ActionFilterAttribute
    {
         public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (SessionHelper.GetUserSettings().AuthenticatedUser == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    action = "LogOff",
                    controller = "Account",
                    area = ""
                }));
            }

            base.OnActionExecuting(filterContext);
        }


        
}
}